var baseUrl = location.protocol + '//' + location.host;

jQuery(document).ready(function () {

    selectSidebarMenu();

    $.ajaxSetup({
        headers: {
            'X-CSRF-TOKEN': $('meta[name="csrf-token"]').attr('content')
        }
    });

    $('.datatable').dataTable();
    $('.category-datatable').dataTable({"order": []});

    $('.post_datepicker').datepicker('setDate', $('.post_datepicker').val())

    $('.datepicker').datepicker();

    $('#order_print').click(function () {
        window.print();
    })

//    $('.editor').wysihtml5();

    $('.recordDelete').click(function () {
        var id = $(this).attr('rel');
        var type = $(this).data('type');

        if (id != '' && confirm('Are you sure you want to delete the ' + type + '?')) {
            window.location.href = '/admin/' + type + '/delete/' + id;
        }
    });

    if ($('#post_id').val() != '') {
        $('.insert-image').removeClass('disabled');
    }

    $('#title').on('blur', function () {
        if ($(this).val() == '')
            $(this).val('Untitled post').change();

        var post_id = $('#post_id').val();
        var data = {};

        if (post_id) {
            data['post_id'] = post_id;
        }
        data['title'] = $(this).val();

        $.ajax({
            url: '/admin/post/create',
            type: 'post',
            dataType: 'json',
            data: data,
            success: function (response) {
                var action = $('form').attr('action');

                $('form').attr('action', baseUrl + '/admin/post/' + response.id);
                $('#post_id').val(response.id);
                $('#post-slug').text(baseUrl + '/blog/' + response.id + '/' + response.slug);
                $('#slug-group').removeClass('hidden');
                $('#submit-btn').removeAttr('disabled');

                $('.insert-image').removeClass('disabled');
            }
        });
    });


    $('#statuses').change(function () {
        if ($(this).val() == 'rejected') {
            $('#status_reason').removeClass('hidden');
        } else {
            $('#status_reason').addClass('hidden');
        }
    });


    $('#changeStatus').on('click', function () {
        var status = $('#statuses').val();

        if (status != 0) {
            var data = {};
            data['id'] = $(this).data('id');
            data['status'] = $('#statuses').val();

            if (status == 'rejected') {
                data['status_reason'] = $('#status_reason').val();
            }

            $.ajax({
                url: '/admin/user/status',
                type: 'post',
                dataType: 'json',
                data: data,
                success: function (response) {
                    window.location.reload();
                }
            });
        }
    });

    $(document).on('click', '.is_featured', function () {
        if ($(this).is(':checked')) {

            var attachment_id = $(this).data('id');

            var _this = $(this);

            $.ajax({
                url: '/admin/post/featured',
                type: 'post',
                dataType: 'json',
                data: {
                    post_id: $('#post_id').val(),
                    attachment_id: attachment_id
                },
                success: function (response) {
                    $('#featuredImage').attr('src', _this.closest('.attachment-item').find('img').attr('src'));
                    $('.modal').modal('hide');
                }
            });
        }
    });

    $(".add-portion").click(function () {
        var portion = $('#portions').val();
        var price = $('#price').val();

        if (portion && price) {
            if ($('.added-portions').length > 0) {
                var exists = false;
                $('.added-portions').each(function () {
                    if (portion == $(this).val()) {
                        exists = true;
                        return false;

                    }
                });
                if (!exists) {
                    $('.portions-body').append('<tr><td>' + $("#portions option:selected").text() + '</td><td>' + price + '</td><td class="text-center"><i class="fa fa-times remove-portion" role="button" data-id="' + portion + '" title="Remove" aria-hidden="true"></i></td></tr>');
                    $('#portions-hidden').append('<div id="p_' + portion + '"><input type="hidden" class="added-portions" name="portions[]" value="' + portion + '" /><input type="hidden" name="prices[]" value="' + price + '" /></div>');

                }
            } else {
                $('.portions-body').append('<tr><td>' + $("#portions option:selected").text() + '</td><td>' + price + '</td><td class="text-center"><i class="fa fa-times remove-portion" role="button" data-id="' + portion + '" title="Remove" aria-hidden="true"></i></td></tr>');
                $('#portions-hidden').append('<div id="p_' + portion + '"><input type="hidden" class="added-portions" name="portions[]" value="' + portion + '" /><input type="hidden" name="prices[]" value="' + price + '" /></div>');
            }
        }
    });

    $(document).on('click', '.remove-portion', function () {
        $(this).closest('tr').remove();
        $('#p_' + $(this).data('id')).remove();
    });

    $('#saveVideo').click(function () {
        var url = $('#videourl').val();
        if (url) {
            var regExp = /^.*((youtu.be\/)|(v\/)|(\/u\/\w\/)|(embed\/)|(watch\?))\??v?=?([^#\&\?]*).*/;
            var match = url.match(regExp);
            var videoId = (match && match[7].length == 11) ? match[7] : false;
            console.log(match);

            $.ajax({
                url: '/admin/product/add-video/' + $('#product_id').val(),
                type: 'post',
                dataType: 'json',
                data: {
                    media: videoId,
                },
                success: function (response) {
                    var html = '<li class="item">';
                    html += '<div class="product-img">';
                    html += '<img src="https://img.youtube.com/vi/' + videoId + '/default.jpg">';
                    html += '</div>';
                    html += '<div class="product-info">';
                    html += '<span class="fa fa-times remove-media pull-right" role="button" data-id="' + response.data.id + '" title="Remove" aria-hidden="true"></span>';
                    html += '<span class="label label-danger media-type">Video</span></a>';
                    html += '</div>';
                    html += '</li>';

                    $('.products-list').append(html);
                    $('#videoModal').modal('hide');
                }
            });
        }
    });

    $(document).on('click', '.remove-media', function () {
        var _this = $(this);
        $.ajax({
            url: '/admin/product/remove-media/' + $(this).data('id'),
            type: 'post',
            dataType: 'json',
            success: function (response) {
                if (response.status == 'success') {
                    _this.closest('li.item').remove();
                }
            }
        });
    });

    $('#updateOrderStatus').click(function () {
        var orderStatus = $('#orderStatus').val();
        var orderId = $('#orderId').val();
        var userId = $('#userId').val();

        $.ajax({
            url: '/admin/order/status-update',
            type: 'post',
            dataType: 'json',
            data: {
                order_status: orderStatus,
                order_id: orderId,
                user_id: userId
            },
            success: function (response) {
                if (response.status == 'success') {
                    window.location.reload();
                }
            }
        });
    });


    var settings = {
        url: "/admin/product/add-image/" + $('#product_id').val(),
        dragDrop: true,
        allowDuplicates: false,
        fileName: "image",
        showPreview: false,
        returnType: "json",
        maxFileSize: -1,
        autoSubmit: false,
        allowedTypes: "*",
        dragDropStr: "<span><b>Drag &amp; Drop Image</b></span>",
        multiple: true,
        sequentialCount: 1,
        onSubmit: function () {
            this.formData.append('media_info', JSON.stringify({title: $('input[name=title]').val()}));
        },
        onSuccess: function (files, response, xhr) {
            if (response.status == 'error') {
                  $('#imageModal').modal('show');
                  $('.image-error').removeClass('hidden');
                  $('.ajax-file-upload-statusbar').remove();
            } else {
                var html = '<li class="item">';
                html += '<div class="product-img">';
                html += '<img src="' + window.location.protocol + '//' + window.location.host + response.file_path + '">';
                html += '</div>';
                html += '<div class="product-info">';
                html += '<span class="fa fa-times remove-media pull-right" role="button" data-id="' + response.id + '" title="Remove" aria-hidden="true"></span>';
                html += '<span class="label label-info media-type">Image</span></a>';
                html += '</div>';
                html += '</li>';

                $('.products-list').append(html);
                $('#imageModal').modal('hide');
                 $('.image-error').addClass('hidden');
                 $('.ajax-file-upload-statusbar').remove();
            }
        },
    }
    var uploadObj = $("#media_uploader").uploadFile(settings);

    $('#saveImage').click(function () {
        uploadObj.startUpload();

    });

    $('#example-one').hierarchySelect({
        hierarchy: false,
        search: false,
        width: '100%'
    });


    $('.wysihtml5-sandbox').contents().find('body').on('keyup', function () {
        $('.validate-content').change()
    });
    var postVlaidation = Object.create(blogPostValidation);
    postVlaidation.init();

    $('.select2').select2();
    
    $('.category-status').change(function(){
        if($(this).val() == 'disabled' && $(this).data('has_product') != 0){
           if(confirm("This category has products attached, if you disable this category, the products also will not be shown. Are you sure you want to disable this category?")){
               $('#category_form').submit();
           };
        }
    })
    
    $('.post-category-status').change(function(){
        if($(this).val() == 'disabled' && $(this).data('has_product') != 0){
           if(confirm("This category has posts attached, if you disable this category, the posts also will not be shown. Are you sure you want to disable this category?")){
               $('#post_form').submit();
           };
        }
    })

});



