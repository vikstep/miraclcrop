var blogPostValidation = {
    fields: {
        title: {
            elem: '.validate-title',
            rules: {
                min: 3,
                max: 50,
                required: true
            },
        },
        excerpt: {
            elem: '.validate-excerpt',
            rules: {
                min: 20,
                max: 300,
                required: true
            }
        },
        content: {
            elem: '.validate-content',
            rules: {
                min: 100,
                required: true
            }
        },
        image: {
            elem: '#attachments',
            rules: {
                required: true
            }
        },
    },
    init: function () {
        var _this = this;

        $.each(this.fields, function (key, field) {

            _this.validate(field);

            $(document).on('change keyup', field.elem, function () {
                _this.validate(field);
            });
        });
    },
    validate: function (field) {

        var currentVal = $(field.elem).val();
        var isValid = false;

        if (typeof currentVal !== "undefined") {
            $.each(field.rules, function (name, value) {
                switch (name) {
                    case 'min':
                        isValid = currentVal.length < value ? false : true;
                        break;
                    case 'max':
                        isValid = currentVal.length > value ? false : true;
                        break;
                    case 'required':
                        isValid = (currentVal == '' || currentVal == '[]') ? false : true;
                        break;
                }

                return isValid;
            });
        }
        if (isValid) {
            $(field.elem + '-icon').removeClass('fa-info-circle validataion-invalid').addClass('fa-check-circle validataion-valid');
        } else {
            $(field.elem + '-icon').removeClass('fa-check-circle validataion-valid').addClass('fa-info-circle validataion-invalid');
        }
    }
};

function renderAttachmentItem(attachemnt) {
    var html = '<div class="row attachment-item">';
    html += '<div class="col-md-4"><img src="' + location.protocol + '//' + location.host + attachemnt.file_path + '-s.' + attachemnt.type + '" /></div>';
    html += '<div class="col-md-2 text-center"><input type="radio" name="select_size" value="' + attachemnt.file_path + '-s.' + attachemnt.type + '" /></div>';
    html += '<div class="col-md-2 text-center"><input type="radio" name="select_size" value="' + attachemnt.file_path + '-m.' + attachemnt.type + '" /></div>';
    html += '<div class="col-md-4 text-right"><input type="radio" name="is_featured" class="is_featured" data-id="' + attachemnt.id + '" value="' + attachemnt.is_featured + '" ' + (attachemnt.is_featured == 'yes' ? 'checked' : '') + ' /></div>';

    html += '</div>';

    return html;
}



function getUrl() {
    return window.location.href;
}

function selectSidebarMenu() {
    var url = getUrl().split('/');
    var currentItem = 'mcrop_dashboard';
    var sidebarItems = {
        'mcrop_dashboard': '0',
        'mcrop_languages': '0',
        'mcrop_language': '0',
        'mcrop_orders': '0',
        'mcrop_order': '0',
        'mcrop_patients': 'mcrop_users',
        'mcrop_patient': 'mcrop_users',
        'mcrop_doctors': 'mcrop_users',
        'mcrop_doctor': 'mcrop_users',
        'mcrop_post_create': 'mcrop_blog',
        'mcrop_posts': 'mcrop_blog',
        'mcrop_post': 'mcrop_blog',
        'mcrop_post_translation': 'mcrop_blog',
        'mcrop_categories': 'mcrop_blog',
        'mcrop_category': 'mcrop_blog',
        'mcrop_category_create': 'mcrop_blog',
        'mcrop_category_translation': 'mcrop_blog',
        'mcrop_products': 'mcrop_product',
        'mcrop_product_create': 'mcrop_product',
        'mcrop_product': 'mcrop_product',
        'mcrop_product_translation': 'mcrop_product',
        'mcrop_product-category': 'mcrop_product',
        'mcrop_product-categories': 'mcrop_product',
        'mcrop_product-category_create': 'mcrop_product',
        'mcrop_product-category_translation': 'mcrop_product',
        'mcrop_portions': 'mcrop_product',
        'mcrop_portion_create': 'mcrop_product',
        'mcrop_portion': 'mcrop_product',
        'mcrop_portion_translation': 'mcrop_product',
        'mcrop_strains': 'mcrop_product',
        'mcrop_strain': 'mcrop_product',
        'mcrop_strain_create': 'mcrop_product',
        'mcrop_strain_translation': 'mcrop_product',
        'mcrop_brands': 'mcrop_product',
        'mcrop_brand': 'mcrop_product',
        'mcrop_brand_create': 'mcrop_product',
        'mcrop_brand_translation': 'mcrop_product',
    }

    if (url[4])
        currentItem = 'mcrop_' + url[4];
    if (url[5]) {
        if (isNaN(url[5])) {
            currentItem = currentItem + '_' + url[5];
        }
    }

    if (sidebarItems[currentItem] == '0')
        $('.' + currentItem).addClass('active');
    else {

        $('.' + sidebarItems[currentItem]).addClass('active');
        $('.' + sidebarItems[currentItem]).find('.treeview-menu').show();
        $('.' + currentItem).addClass('active');
    }
}