/**
 * Note: This file is a modified copy of bootstrap3-wysihtml5/src/bootstrap3-wysihtml5.js
 */

!function ($, wysi) {
    "use strict";
    var tpl = {
        "font-styles": function (locale, options) {

        },
        "emphasis": function (locale, options) {
            var size = (options && options.size) ? ' btn-' + options.size : '';
            return "<li>" +
                    "<div class='btn-group'>" +
                    "<a class='btn btn-default" + size + "' data-wysihtml5-command='bold' title='CTRL+B' tabindex='-1'>" + locale.emphasis.bold + "</a>" +
                    "<a class='btn btn-default" + size + "' data-wysihtml5-command='italic' title='CTRL+I' tabindex='-1'>" + locale.emphasis.italic + "</a>" +
                    "<a class='btn btn-default" + size + "' data-wysihtml5-command='underline' title='CTRL+U' tabindex='-1'>" + locale.emphasis.underline + "</a>" +
                    "</div>" +
                    "</li>";
        },
        "lists": function (locale, options) {
            var size = (options && options.size) ? ' btn-' + options.size : '';
            return "<li>" +
                    "<div class='btn-group'>" +
                    "<a class='btn btn-default" + size + "' data-wysihtml5-command='insertUnorderedList' title='" + locale.lists.unordered + "' tabindex='-1'><i class='glyphicon glyphicon-list'></i></a>" +
                    "<a class='btn btn-default" + size + "' data-wysihtml5-command='insertOrderedList' title='" + locale.lists.ordered + "' tabindex='-1'><i class='glyphicon glyphicon-th-list'></i></a>" +
//                    "<a class='btn btn-default" + size + "' data-wysihtml5-command='Outdent' title='" + locale.lists.outdent + "' tabindex='-1'><i class='glyphicon glyphicon-indent-right'></i></a>" +
//                    "<a class='btn btn-default" + size + "' data-wysihtml5-command='Indent' title='" + locale.lists.indent + "' tabindex='-1'><i class='glyphicon glyphicon-indent-left'></i></a>" +
                    "</div>" +
                    "</li>";
        },
        "link": function (locale, options) {
            var size = (options && options.size) ? ' btn-' + options.size : '';
            return "<li>" +
                    "<div id='bootstrap-wysihtml5-insert-link-modal' class='bootstrap-wysihtml5-insert-link-modal modal fade' role='dialog'>" +
                    "<div class='modal-dialog'>" +
                    "<div class='modal-content'>" +
                    "<div class='modal-header'>" +
                    '<button type="button" class="close" data-dismiss="modal" aria-hidden="true">&times;</button>' +
                    "<h4>" + locale.link.insert + "</h4>" +
                    "</div>" +
                    "<div class='modal-body'>" +
                    "<input value='http://' class='bootstrap-wysihtml5-insert-link-url input-xlarge form-control'>" +
                    "<label class='checkbox' for='link_input'> <input type='checkbox' id='link_input' class='bootstrap-wysihtml5-insert-link-target' checked>" + locale.link.target + "</label>" +
                    "</div>" +
                    "<div class='modal-footer'>" +
                    "<a href='#' class='btn btn-default' data-dismiss='modal'>" + locale.link.cancel + "</a>" +
                    "<a href='#' class='btn btn-primary' data-dismiss='modal'>" + locale.link.insert + "</a>" +
                    "</div>" +
                    "</div>" +
                    "</div>" +
                    "</div>" +
                    "<a data-toggle='modal' href='#bootstrap-wysihtml5-insert-link-modal' class='btn btn-default" + size + "' data-wysihtml5-command='createLink' title='" + locale.link.insert + "' tabindex='-1a'><i class='glyphicon glyphicon-link'></i></a>" +
                    "</li>";
        },
        "video": function (locale, options) {
            var size = (options && options.size) ? ' btn-' + options.size : '';
            return "<li>" +
                    "<div id='bootstrap-wysihtml5-insert-link-modal' class='bootstrap-wysihtml5-insert-video-link-modal modal fade' role='dialog'>" +
                    "<div class='modal-dialog'>" +
                    "<div class='modal-content'>" +
                    "<div class='modal-header'>" +
                    '<button type="button" class="close" data-dismiss="modal" aria-hidden="true">&times;</button>' +
                    "<h4>" + locale.video.insert + "</h4>" +
                    "</div>" +
                    "<div class='modal-body'>" +
                    "<input value='' class='bootstrap-wysihtml5-insert-video-link-url input-xlarge form-control'>" +
                    "</div>" +
                    "<div class='modal-footer'>" +
                    "<a href='#' class='btn btn-default' data-dismiss='modal'>" + locale.video.cancel + "</a>" +
                    "<a href='#' class='btn btn-primary' data-dismiss='modal'>" + locale.video.insert + "</a>" +
                    "</div>" +
                    "</div>" +
                    "</div>" +
                    "</div>" +
                    "<a data-toggle='modal' href='#bootstrap-wysihtml5-insert-video-link-modal' class='btn btn-default" + size + "' data-wysihtml5-command='createVideoLink' title='" + locale.video.insert + "' tabindex='-1a'><i class='glyphicon glyphicon-expand'></i></a>" +
                    "</li>";
        },
        "image": function (locale, options) {
            var size = (options && options.size) ? ' btn-' + options.size : '';
            return "<li>" +
                    '<div id="bootstrap-wysihtml5-insert-image-modal" class="bootstrap-wysihtml5-insert-image-modal modal fade">' +
                    '<div class="modal-dialog">' +
                    '<div class="modal-content">' +
                    '<div class="modal-header">' +
                    '<button type="button" class="close" data-dismiss="modal" aria-hidden="true">&times;</button>' +
                    '<h4>' + locale.image.insert + '</h4>' +
                    '</div>' +
                    '<div class="modal-body">' +
                    '<p>' +
                    '<span class="btn btn-primary fileinput-button">' +
                    '<i class="glyphicon glyphicon-plus"></i> ' +
                    '<span>Select files…</span>' +
                    '<input id="fileupload" type="file" name="files[]" data-url="jquery-file-upload/server/php/" multiple>' +
                    '</span>' +
                    '</p>' +
                    '<div id="progress" class="progress progress-striped">' +
                    '<div class="progress-bar"></div>' +
                    '</div>' +
                    '<div id="filealerts" class="filealerts"></div>' +
                    '<ul id="files" class="files"></ul>' +
                    '</div>' +
                    '<div class="modal-footer">' +
                    '<a href="#" class="btn btn-default" data-dismiss="modal">' + locale.image.cancel + '</a>' +
                    '<a href="#" class="btn btn-primary" data-dismiss="modal">' + locale.image.insert + '</a>' +
                    '</div>' +
                    '</div>' +
                    '</div>' +
                    '</div>' +
                    '<a data-toggle="modal" href="#bootstrap-wysihtml5-insert-image-modal" class="btn btn-default' + size + '" data-wysihtml5-command="insertImage" title="' + locale.image.insert + '" tabindex="-1"><i class="glyphicon glyphicon-picture"></i></a>' +
                    '</li>';
        },
        "table": function (locale, options) {
            var size = (options && options.size) ? ' btn-' + options.size : '';
            return "<li>" +
                    '<div id="bootstrap-wysihtml5-insert-table-modal" class="bootstrap-wysihtml5-insert-table-modal modal fade">' +
                    '<div class="modal-dialog">' +
                    '<div class="modal-content">' +
                    '<div class="modal-header">' +
                    '<button type="button" class="close" data-dismiss="modal" aria-hidden="true">&times;</button>' +
                    '<h4>' + locale.table.insert + '</h4>' +
                    '</div>' +
                    '<div class="modal-body">' +
                    '<div>' +
                    '<table class="table">' +
                    '<tbody>' +
                    '<tr>' +
                    '<td>' +
                    '<div class="form-group">' +
                    '<label for="rows">Rows</lablel>' +
                    '<input type="number" value="3"  class="form-control" id="rows" placeholder="" >' +
                    '</div>' +
                    '</td>' +
                    '<td>' +
                    '<div class="form-group">' +
                    '<label for="width">Width</lablel>' +
                    '<input type="number" value="300" class="form-control" id="width" placeholder="" >' +
                    '</div>' +
                    '</td>' +
                    '</tr>' +
                    '<tr>' +
                    '<td>' +
                    '<div class="form-group">' +
                    '<label for="columns">Columns</lablel>' +
                    '<input type="number" value="2" class="form-control" id="columns" placeholder="" >' +
                    '</div>' +
                    '</td>' +
                    '<td>' +
                    '<div class="form-group">' +
                    '<label for="height">Height</lablel>' +
                    '<input type="number" value="100"  class="form-control" id="height" placeholder="" >' +
                    '</div>' +
                    '</td>' +
                    '</tr>' +
                    '</tbody>' +
                    '</table>' +
                    '</div>' +
                    '</div>' +
                    '<div class="modal-footer">' +
                    '<a href="#" class="btn btn-default" data-dismiss="modal">' + locale.table.cancel + '</a>' +
                    '<a href="#" class="btn btn-primary" data-dismiss="modal">' + locale.table.insert + '</a>' +
                    '</div>' +
                    '</div>' +
                    '</div>' +
                    '</div>' +
                    '<a data-toggle="modal" href="#bootstrap-wysihtml5-insert-table-modal" class="btn btn-default' + size + '" data-wysihtml5-command="insertTable" title="' + locale.table.insert + '" tabindex="-1"><span class="glyphicon glyphicon-list-alt"></span></a>' +
                    '</li>';
        },
        "html": function (locale, options) {
            var size = (options && options.size) ? ' btn-' + options.size : '';
            return "<li>" +
                    "<div class='btn-group'>" +
                    "<a class='btn btn-default" + size + "' data-wysihtml5-action='change_view' title='" + locale.html.edit + "' tabindex='-1'><i class='glyphicon glyphicon-pencil'></i></a>" +
                    "</div>" +
                    "</li>";
        },
        "color": function (locale, options) {
            var size = (options && options.size) ? ' btn-' + options.size : '';
            return "<li class='dropdown'>" +
                    "<a class='btn btn-default dropdown-toggle" + size + "' data-toggle='dropdown' href='#' tabindex='-1'>" +
                    "<i class='glyphicon glyphicon-tint'></i><span class='current-color'>" + locale.colours.black + "</span>&nbsp;<b class='caret'></b>" +
                    "</a>" +
                    "<ul class='dropdown-menu'>" +
                    "<li><div class='wysihtml5-colors' data-wysihtml5-command-value='black'></div><a class='wysihtml5-colors-title' data-wysihtml5-command='foreColor' data-wysihtml5-command-value='black'>" + locale.colours.black + "</a></li>" +
                    "<li><div class='wysihtml5-colors' data-wysihtml5-command-value='silver'></div><a class='wysihtml5-colors-title' data-wysihtml5-command='foreColor' data-wysihtml5-command-value='silver'>" + locale.colours.silver + "</a></li>" +
                    "<li><div class='wysihtml5-colors' data-wysihtml5-command-value='gray'></div><a class='wysihtml5-colors-title' data-wysihtml5-command='foreColor' data-wysihtml5-command-value='gray'>" + locale.colours.gray + "</a></li>" +
                    "<li><div class='wysihtml5-colors' data-wysihtml5-command-value='maroon'></div><a class='wysihtml5-colors-title' data-wysihtml5-command='foreColor' data-wysihtml5-command-value='maroon'>" + locale.colours.maroon + "</a></li>" +
                    "<li><div class='wysihtml5-colors' data-wysihtml5-command-value='red'></div><a class='wysihtml5-colors-title' data-wysihtml5-command='foreColor' data-wysihtml5-command-value='red'>" + locale.colours.red + "</a></li>" +
                    "<li><div class='wysihtml5-colors' data-wysihtml5-command-value='purple'></div><a class='wysihtml5-colors-title' data-wysihtml5-command='foreColor' data-wysihtml5-command-value='purple'>" + locale.colours.purple + "</a></li>" +
                    "<li><div class='wysihtml5-colors' data-wysihtml5-command-value='green'></div><a class='wysihtml5-colors-title' data-wysihtml5-command='foreColor' data-wysihtml5-command-value='green'>" + locale.colours.green + "</a></li>" +
                    "<li><div class='wysihtml5-colors' data-wysihtml5-command-value='olive'></div><a class='wysihtml5-colors-title' data-wysihtml5-command='foreColor' data-wysihtml5-command-value='olive'>" + locale.colours.olive + "</a></li>" +
                    "<li><div class='wysihtml5-colors' data-wysihtml5-command-value='navy'></div><a class='wysihtml5-colors-title' data-wysihtml5-command='foreColor' data-wysihtml5-command-value='navy'>" + locale.colours.navy + "</a></li>" +
                    "<li><div class='wysihtml5-colors' data-wysihtml5-command-value='blue'></div><a class='wysihtml5-colors-title' data-wysihtml5-command='foreColor' data-wysihtml5-command-value='blue'>" + locale.colours.blue + "</a></li>" +
                    "<li><div class='wysihtml5-colors' data-wysihtml5-command-value='orange'></div><a class='wysihtml5-colors-title' data-wysihtml5-command='foreColor' data-wysihtml5-command-value='orange'>" + locale.colours.orange + "</a></li>" +
                    "</ul>" +
                    "</li>";
        }
    };
    var templates = function (key, locale, options) {
        return tpl[key](locale, options);
    };
    var Wysihtml5 = function (el, options) {
        this.el = el;
        var toolbarOpts = options || defaultOptions;
        for (var t in toolbarOpts.customTemplates) {
            tpl[t] = toolbarOpts.customTemplates[t];
        }
        this.toolbar = this.createToolbar(el, toolbarOpts);
        this.editor = this.createEditor(options);
        window.editor = this.editor;
        $('iframe.wysihtml5-sandbox').each(function (i, el) {
            $(el.contentWindow).off('focus.wysihtml5').on({
                'focus.wysihtml5': function () {
                    $('li.dropdown').removeClass('open');
                }
            });
        });
    };
    Wysihtml5.prototype = {
        constructor: Wysihtml5,
        createEditor: function (options) {
            options = options || {};
            // Add the toolbar to a clone of the options object so multiple instances
            // of the WYISYWG don't break because "toolbar" is already defined
            options = $.extend(true, {}, options);
            options.toolbar = this.toolbar[0];
            var editor = new wysi.Editor(this.el[0], options);
            if (options && options.events) {
                for (var eventName in options.events) {
                    editor.on(eventName, options.events[eventName]);
                }
            }
            return editor;
        },
        createToolbar: function (el, options) {
            var self = this;
            var toolbar = $("<ul/>", {
                'class': "wysihtml5-toolbar",
                'style': "display:none"
            });
            var culture = options.locale || defaultOptions.locale || "en";
            for (var key in defaultOptions) {
                var value = false;
                if (options[key] !== undefined) {
                    if (options[key] === true) {
                        value = true;
                    }
                } else {
                    value = defaultOptions[key];
                }

                if (value === true) {
                    toolbar.append(templates(key, locale[culture], options));
                    if (key === "html") {
                        this.initHtml(toolbar);
                    }

                    if (key === "link") {
                        this.initInsertLink(toolbar);
                    }

                    if (key === "image") {
                        this.initInsertImage(toolbar);
                    }
                    if (key === "table") {
                        this.initInsertTable(toolbar);
                    }
                    if (key === "video") {
                        this.initInsertVideoLink(toolbar);
                    }
                }
            }

            if (options.toolbar) {
                for (key in options.toolbar) {
                    toolbar.append(options.toolbar[key]);
                }
            }

            toolbar.find("a[data-wysihtml5-command='formatBlock']").click(function (e) {
                var target = e.target || e.srcElement;
                var el = $(target);
                self.toolbar.find('.current-font').text(el.html());
            });
            toolbar.find("a[data-wysihtml5-command='foreColor']").click(function (e) {
                var target = e.target || e.srcElement;
                var el = $(target);
                self.toolbar.find('.current-color').text(el.html());
            });
            this.el.before(toolbar);
            return toolbar;
        },
        initHtml: function (toolbar) {
            var changeViewSelector = "a[data-wysihtml5-action='change_view']";
            toolbar.find(changeViewSelector).click(function (e) {
                toolbar.find('a.btn').not(changeViewSelector).toggleClass('disabled');
            });
        },
        initInsertImage: function (toolbar) {
            var self = this;
            var insertImageModal = toolbar.find('.bootstrap-wysihtml5-insert-image-modal');
            var insertButton = insertImageModal.find('a.btn-primary');
            var caretBookmark;
            var chooser = insertImageModal.find('.image_chooser.images');
            chooser.on('click', 'td', function (ev) {
                var row = $(this).attr('data-selected', 'true');
                row.data('selected', 'true');
            });
            var insertImage = function () {
                self.editor.currentView.element.focus();
                if (caretBookmark) {
                    self.editor.composer.selection.setBookmark(caretBookmark);
                    caretBookmark = null;
                }
                // Find all image URLs
                var is_checked = $("input[name='select_size']:checked").val();
                if (is_checked) {
//                    self.editor.composer.commands.exec("insertImage", is_checked);
                    self.editor.composer.commands.exec("insertImage", location.protocol + '//' + location.host + is_checked);
                }
            };
            insertButton.click(insertImage);
            // insertImageModal.on('shown', function() {
            // });

            insertImageModal.on('hide', function () {
                self.editor.currentView.element.focus();
            });
            toolbar.find('a[data-wysihtml5-command=insertImage]').click(function () {
                var activeButton = $(this).hasClass("wysihtml5-command-active");
                if (!activeButton) {
                    self.editor.currentView.element.focus(false);
                    caretBookmark = self.editor.composer.selection.getBookmark();
                    insertImageModal.appendTo('body').modal('show');
                    insertImageModal.on('click.dismiss.modal', '[data-dismiss="modal"]', function (e) {
                        e.stopPropagation();
                    });
                    // Remove previously uploaded images from list
                    $('.upload-section').remove();
                    // Reset progress bar
                    $('#progress .progress-bar').css('width', '0');
                    // Function to update status of "insert" button
                    var updateInsertBtn = function () {
//                        var imgCount = 0;
//                        $('.image-conteiner img').each(function () {
//                            var url = $(this).data('url');
//                            if (url) {
//                                imgCount++;
//                            }
//                        });
//                        if (imgCount) {
//                            insertButton.removeClass('disabled');
//                        } else {
//                            insertButton.addClass('disabled').text('Insert image');
//                        }
                    };

                    $('.upload-error ul').empty();
                    $('.upload-error').addClass('hidden');

                    var postAttachments = $('#attachments').val();
                    if (postAttachments != '' && postAttachments != '[]') {
                        postAttachments = JSON.parse(postAttachments);
                        $('.image-section .hidden').removeClass('hidden');
                        $('.image-chooser').empty();
                        $.each(postAttachments, function (index, attachment) {
                            $('.image-chooser').append(renderAttachmentItem(attachment));
                        });
                    }

                    // Update "insert" button now
                    updateInsertBtn();
                    // Init file upload
                    $("#image_uploader").uploadFile({
                        url: "/admin/post/upload",
                        dragDrop: true,
                        fileName: "image",
                        formData: {},
                        showPreview: false,
                        showError: false,
                        showStatusAfterSuccess: false,
                        returnType: "json",
                        maxFileSize: -1,
                        allowedTypes: "jpg,jpeg,png",
                        dragDropStr: "<span><b>Drag &amp; Drop Image</b></span>",
                        multiple: false,
                        onSubmit: function () {
                            this.formData.append('post_info', JSON.stringify({id: $('#post_id').val(), title: $('#title').val()}));
                        },
                        onSuccess: function (files, data, xhr)
                        {
                            if (data.status == 'error') {
                                $('.upload-error ul').empty();

                                $.each(data.messages, function (key, value) {
                                    $('.upload-error ul').append('<li>' + value + '</li>');
                                });
                                $('.upload-error').removeClass('hidden');
                                return false;
                            }

                            $('.upload-error').addClass('hidden');

                            $('.image-chooser').append(renderAttachmentItem(data));

                            updateInsertBtn();

                            var attachmentsArr = [];
                            var attachments = $('#attachments').val();

                            if (attachments) {
                                var attachmentsArr = JSON.parse(attachments);
                            }

                            if (data.is_featured == 'yes') {
                                $('#featuredImage').attr('src', location.protocol + '//' + location.host + data.file_path + '-s.' + data.type);
                            }

                            $('.image-section .hidden').removeClass('hidden');
                            attachmentsArr.push({'id': data.id, 'file_path': data.file_path, 'type': data.type, 'is_featured': data.is_featured});
                            $('#attachments').val(JSON.stringify(attachmentsArr));

                            $('#attachments').change();
                        },
                        showDelete: false,
                        deleteCallback: function (data, pd)
                        {
                            for (var i = 0; i < data.length; i++)
                            {
                                $.post("delete.php", {op: "delete", name: data[i]},
                                        function (resp, textStatus, jqXHR)
                                        {
                                            //Show Message
                                            $("#image_status").append("<div>File Deleted</div>");
                                        });
                            }
                            pd.statusbar.hide(); //You choice to hide/not.

                        }
                    });
                    return false;
                } else {
                    return true;
                }
            });
        },
        initInsertTable: function (toolbar) {
            var self = this;
            var insertTableModal = toolbar.find('.bootstrap-wysihtml5-insert-table-modal');
            var insertButton = insertTableModal.find('a.btn-primary');
            var caretBookmark;

            var insertTable = function () {
                var row_count = $('#rows').val();
                var column_count = $('#columns').val();
                var width = $('#width').val() ? $('#width').val() : 300;
                var height = $('#height').val() ? $('#height').val() : 100;

                self.editor.currentView.element.focus();
                if (caretBookmark) {
                    self.editor.composer.selection.setBookmark(caretBookmark);
                    caretBookmark = null;
                }

                var html = '<table border="1" cellspacing="1" cellpadding="1" style="width:' + width + 'px;height:' + height + 'px;">';
                html += '<tbody>';
                if (row_count > 0) {
                    for (var i = 0; i < row_count; i++) {
                        html += '<tr>';
                        if (row_count > 0) {
                            for (var j = 0; j < column_count; j++) {
                                html += '<td>&nbsp;</td>';
                            }
                        }
                        html += '</tr>';
                    }
                }
                html += '</tbody>';
                html += '</table>';
                self.editor.currentView.element.focus();
                if (caretBookmark) {
                    self.editor.composer.selection.setBookmark(caretBookmark);
                    caretBookmark = null;
                }
                self.editor.composer.commands.exec("insertHTML", html);

            };
            insertButton.click(insertTable);

            insertTableModal.on('hide', function () {
                self.editor.currentView.element.focus();
            });
            toolbar.find('a[data-wysihtml5-command=insertTable]').click(function () {
                var activeButton = $(this).hasClass("wysihtml5-command-active");
                if (!activeButton) {
                    self.editor.currentView.element.focus(false);
                    caretBookmark = self.editor.composer.selection.getBookmark();
                    insertTableModal.appendTo('body').modal('show');
                    insertTableModal.on('click.dismiss.modal', '[data-dismiss="modal"]', function (e) {
                        e.stopPropagation();
                    });

                    return false;
                } else {
                    return true;
                }
            });
        },
        initInsertLink: function (toolbar) {
            var self = this;
            var insertLinkModal = toolbar.find('.bootstrap-wysihtml5-insert-link-modal');
            var urlInput = insertLinkModal.find('.bootstrap-wysihtml5-insert-link-url');
            var targetInput = insertLinkModal.find('.bootstrap-wysihtml5-insert-link-target');
            var insertButton = insertLinkModal.find('a.btn-primary');
            var initialValue = urlInput.val();
            var caretBookmark;
            var insertLink = function () {
                var url = urlInput.val();
                urlInput.val(initialValue);
                self.editor.currentView.element.focus();
                if (caretBookmark) {
                    self.editor.composer.selection.setBookmark(caretBookmark);
                    caretBookmark = null;
                }

                var newWindow = targetInput.prop("checked");
                self.editor.composer.commands.exec("createLink", {
                    'href': url,
                    'target': (newWindow ? '_blank' : '_self'),
                    'rel': (newWindow ? 'nofollow' : '')
                });
            };
            var pressedEnter = false;
            urlInput.keypress(function (e) {
                if (e.which == 13) {
                    insertLink();
                    insertLinkModal.modal('hide');
                }
            });
            insertButton.click(insertLink);
            insertLinkModal.on('shown', function () {
                urlInput.focus();
            });
            insertLinkModal.on('hide', function () {
                self.editor.currentView.element.focus();
            });
            toolbar.find('a[data-wysihtml5-command=createLink]').click(function () {
                var activeButton = $(this).hasClass("wysihtml5-command-active");
                if (!activeButton) {
                    self.editor.currentView.element.focus(false);
                    caretBookmark = self.editor.composer.selection.getBookmark();
                    insertLinkModal.appendTo('body').modal('show');
                    insertLinkModal.on('click.dismiss.modal', '[data-dismiss="modal"]', function (e) {
                        e.stopPropagation();
                    });
                    return false;
                } else {
                    return true;
                }
            });
        },
        initInsertVideoLink: function (toolbar) {
            var self = this;
            var insertVideoLinkModal = toolbar.find('.bootstrap-wysihtml5-insert-video-link-modal');
            var urlInput = insertVideoLinkModal.find('.bootstrap-wysihtml5-insert-video-link-url');
            var targetInput = insertVideoLinkModal.find('.bootstrap-wysihtml5-insert-video-link-target');
            var insertButton = insertVideoLinkModal.find('a.btn-primary');
            var initialValue = urlInput.val();
            var caretBookmark;
            var insertLink = function () {
                var url = urlInput.val();
                if (url) {
                    var regExp = /^.*((youtu.be\/)|(v\/)|(\/u\/\w\/)|(embed\/)|(watch\?))\??v?=?([^#\&\?]*).*/;
                    var match = url.match(regExp);
                    var videoId = (match && match[7].length == 11) ? match[7] : false;
                    console.log(match);

                    self.editor.currentView.element.focus();
                    if (caretBookmark) {
                        self.editor.composer.selection.setBookmark(caretBookmark);
                        caretBookmark = null;
                    }
                    var insertUrlContent = '<img src="https://img.youtube.com/vi/' + videoId + '/default.jpg" rel="' + videoId + '"  class="youtube-video">';

                    self.editor.composer.commands.exec("insertHTML", insertUrlContent);
                }

            };
            var pressedEnter = false;
            urlInput.keypress(function (e) {
                if (e.which == 13) {
                    insertLink();
                    insertVideoLinkModal.modal('hide');
                }
            });
            insertButton.click(insertLink);
            insertVideoLinkModal.on('shown', function () {
                urlInput.focus();
            });
            insertVideoLinkModal.on('hide', function () {
                self.editor.currentView.element.focus();
            });
            toolbar.find('a[data-wysihtml5-command=createVideoLink]').click(function () {
                var activeButton = $(this).hasClass("wysihtml5-command-active");
                if (!activeButton) {
                    self.editor.currentView.element.focus(false);
                    caretBookmark = self.editor.composer.selection.getBookmark();
                    insertVideoLinkModal.appendTo('body').modal('show');
                    insertVideoLinkModal.on('click.dismiss.modal', '[data-dismiss="modal"]', function (e) {
                        e.stopPropagation();
                    });
                    return false;
                } else {
                    return true;
                }
            });
        }
    };
    // these define our public api
    var methods = {
        resetDefaults: function () {
            $.fn.wysihtml5.defaultOptions = $.extend(true, {}, $.fn.wysihtml5.defaultOptionsCache);
        },
        bypassDefaults: function (options) {
            return this.each(function () {
                var $this = $(this);
                $this.data('wysihtml5', new Wysihtml5($this, options));
            });
        },
        shallowExtend: function (options) {
            var settings = $.extend({}, $.fn.wysihtml5.defaultOptions, options || {}, $(this).data());
            var that = this;
            return methods.bypassDefaults.apply(that, [settings]);
        },
        deepExtend: function (options) {
            var settings = $.extend(true, {}, $.fn.wysihtml5.defaultOptions, options || {});
            var that = this;
            return methods.bypassDefaults.apply(that, [settings]);
        },
        init: function (options) {
            var that = this;
            return methods.shallowExtend.apply(that, [options]);
        }
    };
    $.fn.wysihtml5 = function (method) {
        if (methods[method]) {
            return methods[method].apply(this, Array.prototype.slice.call(arguments, 1));
        } else if (typeof method === 'object' || !method) {
            return methods.init.apply(this, arguments);
        } else {
            $.error('Method ' + method + ' does not exist on jQuery.wysihtml5');
        }
    };
    $.fn.wysihtml5.Constructor = Wysihtml5;
    var defaultOptions = $.fn.wysihtml5.defaultOptions = {
        "font-styles": true,
        "color": false,
        "emphasis": true,
        "lists": true,
        "html": true,
        "table": true,
        "link": true,
        "video": true,
        "image": true,
        events: {},
        parserRules: {
            classes: {
                // (path_to_project/lib/css/wysiwyg-color.css)
                "wysiwyg-color-silver": 1,
                "wysiwyg-color-gray": 1,
                "wysiwyg-color-white": 1,
                "wysiwyg-color-maroon": 1,
                "wysiwyg-color-red": 1,
                "wysiwyg-color-purple": 1,
                "wysiwyg-color-fuchsia": 1,
                "wysiwyg-color-green": 1,
                "wysiwyg-color-lime": 1,
                "wysiwyg-color-olive": 1,
                "wysiwyg-color-yellow": 1,
                "wysiwyg-color-navy": 1,
                "wysiwyg-color-blue": 1,
                "wysiwyg-color-teal": 1,
                "wysiwyg-color-aqua": 1,
                "wysiwyg-color-orange": 1
            },
            tags: {
                "b": {},
                "i": {},
                "br": {},
                "ol": {},
                "ul": {},
                "li": {},
                "h1": {},
                "h2": {},
                "h3": {},
                "h4": {},
                "h5": {},
                "h6": {},
                "blockquote": {},
                "u": 1,
                "img": {
                    "check_attributes": {
                        "width": "numbers",
                        "alt": "alt",
                        "src": "url",
                        "height": "numbers"
                    }
                },
                "a": {
                    check_attributes: {
                        'href': "url", // important to avoid XSS
                        'target': 'alt',
                        'rel': 'alt'
                    }
                },
                "span": 1,
                "div": 1,
                // to allow save and edit files with code tag hacks
                "code": 1,
                "pre": 1
            }
        },
        stylesheets: [], // (path_to_project/lib/css/wysiwyg-color.css)
        locale: "en"
    };
    if (typeof $.fn.wysihtml5.defaultOptionsCache === 'undefined') {
        $.fn.wysihtml5.defaultOptionsCache = $.extend(true, {}, $.fn.wysihtml5.defaultOptions);
    }

    var locale = $.fn.wysihtml5.locale = {
        en: {
            font_styles: {
                normal: "Normal text",
                h1: "Heading 1",
                h2: "Heading 2",
                h3: "Heading 3",
                h4: "Heading 4",
                h5: "Heading 5",
                h6: "Heading 6"
            },
            emphasis: {
                bold: "B",
                italic: "I",
                underline: "U"
            },
            lists: {
                unordered: "Unordered list",
                ordered: "Ordered list",
                outdent: "Outdent",
                indent: "Indent"
            },
            link: {
                insert: "Insert link",
                cancel: "Cancel",
                target: "Open link in new window"
            },
            image: {
                insert: "Insert image",
                cancel: "Cancel"
            },
            video: {
                insert: "Insert youtube video",
                cancel: "Cancel"
            },
            table: {
                insert: "Insert table",
                cancel: "Cancel"
            },
            html: {
                edit: "Edit HTML"
            },
            colours: {
                black: "Black",
                silver: "Silver",
                gray: "Grey",
                maroon: "Maroon",
                red: "Red",
                purple: "Purple",
                green: "Green",
                olive: "Olive",
                navy: "Navy",
                blue: "Blue",
                orange: "Orange"
            }
        }
    };
}(window.jQuery, window.wysihtml5);