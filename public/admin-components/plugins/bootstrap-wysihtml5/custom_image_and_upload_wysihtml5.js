$(function () {
    // override options
    var wysiwygOptions = {
        customTags: {
            "em": {},
            "strong": {},
            "hr": {}
        },
        parser: function (html) {
            return html;
        },
        customStyles: {
            // keys with null are used to preserve items with these classes, but not show them in the styles dropdown
            'shrink_wrap': null,
            'credit': null,
            'tombstone': null,
            'chat': null,
            'caption': null
        },
        customTemplates: {
            /* this is the template for the image button in the toolbar */
            image: function (locale) {
                return "<li>" +
                        "<div class='bootstrap-wysihtml5-insert-image-modal modal fade' data-wysihtml5-dialog='insertImage'>" +
                        "<div class='modal-dialog'>" +
                        "<div class='modal-content'>" +
                        "<div class='modal-header'>" +
                        "<div class='modal-header'>" +
                        "<a class='close' data-dismiss='modal'>&times;</a>" +
                        "<h3>" + locale.image.insert + "</h3>" +
                        "<div class='upload-error alert alert-danger hidden'><ul></ul></div>" +
                        "</div>" +
                        "<div class='modal-body'>" +
                        "<div class='chooser_wrapper'>" +
                        "<div class='image_chooser images'>" +
                        "<div class='image-section'>" +
                        "<div class='row attachment-header hidden'>" +
                        "<div class='col-md-4'>Image</div>" +
                        "<div class='col-md-2 text-center'>Small</div>" +
                        "<div class='col-md-2 text-center'>Medium</div>" +
                        "<div class='col-md-4 text-right'>Featured</div>" +
                        "</div>" +
                        "<div class='image-chooser'></div>" +
                        "</div>" +
                        "</div>" +
                        "<h4>Or Upload one to insert</h4>" +
                        "<div class='fileUpload'>" +
                        "<div id='image_uploader'>Upload</div>" +
                        "<div id='image_fields'></div>" +
                        "<div id='image_status'></div>" +
                        "</div>" +
                        "</div>" +
                        "</div>" +
                        "<div class='modal-footer'>" +
                        "<a href='#' class='btn btn-success' data-dismiss='modal'>Ok</a>" +
                        "<a class='btn btn-primary' data-dismiss='modal' data-wysihtml5-dialog-action='save' href='#'>Insert image</a>" +
                        "</div>" +
                        "</div>" +
                        "</div>" +
                        "</div>" +
                        "</div>" +
                        "</div>" +
                        "<a class='btn btn-default disabled insert-image' data-wysihtml5-command='insertImage' title='Insert image' tabindex='-1'  unselectable='on'><span class='glyphicon glyphicon-picture'></span></a>" +
                        "</li>" +
                        "<li><label><i class='fa fa-info-circle validataion-invalid' id='attachments-icon' aria-hidden='true'></i></label></li>";
            }
        }
    };

    var wysiwygOptions2 = {
        customTags: {
            "em": {},
            "strong": {},
            "hr": {}
        },
        parser: function (html) {
            return html;
        },
        customStyles: {
            // keys with null are used to preserve items with these classes, but not show them in the styles dropdown
            'shrink_wrap': null,
            'credit': null,
            'tombstone': null,
            'chat': null,
            'caption': null
        },
        customTemplates: {
            /* this is the template for the image button in the toolbar */
            image: function (locale) {
                return;
            },
           video: function (locale, options) {
                return;
            }
        }
    };


    $('.tip').tooltip();
    $('textarea.editor').each(function () {
        $(this).wysihtml5($.extend(wysiwygOptions, {html: true, color: false, stylesheets: []}));
    });

    $('textarea.product_editor').each(function () {
        $(this).wysihtml5($.extend(wysiwygOptions2, {html: true, color: false, stylesheets: []}));
    });

});