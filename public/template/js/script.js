var baseUrl = window.location.origin;

jQuery(document).ready(function () {
    $.ajaxSetup({
        headers: {
            'X-CSRF-TOKEN': $('meta[name="csrf-token"]').attr('content')
        }
    });

    var hash = location.hash.replace('#', '');
    if (hash != '') {
        updateView();
    }

    checkStockCount($('#qty'));

    $(document).on('mouseenter', '.add-to-favorite', function () {
        $(this).removeClass('glyphicon-star-empty').addClass('glyphicon-star');
    });

    $(document).on('mouseleave', '.add-to-favorite', function () {
        $(this).removeClass('glyphicon-star').addClass('glyphicon-star-empty');
    });

    $(document).on('click', '.add-to-favorite', function (e) {
        e.preventDefault();
        e.stopPropagation();
        var productId = $(this).attr('rel');
        var _this = $(this);
        console.log(productId)
        if (productId) {
            $.ajax({
                url: baseUrl + '/products/add-to-favorite',
                type: 'post',
                data: {'product_id': productId},
                success: function (response) {
                    if (response.status == 'success') {
                        _this.removeClass('glyphicon-star-empty add-to-favorite').addClass('glyphicon-star remove-from-favorite');
                        _this.attr('title', 'Remove from favorite list');
                    }
                }
            });
        }
    });

    $(document).on('click', '.remove-from-favorite', function (e) {
        e.preventDefault();
        e.stopPropagation();
        var productId = $(this).attr('rel');
        var _this = $(this);
        console.log(productId)
        if (productId) {
            $.ajax({
                url: baseUrl + '/products/remove-from-favorite',
                type: 'post',
                data: {'product_id': productId},
                success: function (response) {
                    if (response.status == 'success') {
                        _this.removeClass('glyphicon-star remove-from-favorite').addClass('glyphicon-star-empty add-to-favorite');
                        _this.attr('title', 'Add to favorite list');
                    }
                }
            });
        }
    });

    $(document).on('click', '.user-favorite-remove', function (e) {
        e.preventDefault();
        e.stopPropagation();
        var productId = $(this).attr('rel');
        var _this = $(this);
        console.log(productId)
        if (productId) {
            $.ajax({
                url: baseUrl + '/products/remove-from-favorite',
                type: 'post',
                data: {'product_id': productId},
                success: function (response) {
                    if (response.status == 'success') {
                        _this.closest('.product-item-block').remove();
                        if ($('.product-item-row').find('.product-item-block').length == 0)
                            $('.product-item-row').html('<h3>Nothing to show</h3>');
                    }
                }
            });
        }
    });

    $('.product-img').click(function () {
        var fileName = $(this).attr('rel');
        var fileExt = $(this).data('ext');
        // var defaultFileName = $('.product-default-img').attr('rel');
        // var defaultFileExt = $('.product-default-img').data('ext');
        //
        // $(this).attr('src', baseUrl + '/files/products/' + defaultFileName + '-s.' + defaultFileExt);
        // $(this).attr('rel', defaultFileName);
        // $(this).attr('data-ext', defaultFileExt);

        $('.product-default-img').attr('src', baseUrl + '/files/products/' + fileName + '-m.' + fileExt);
        // $('.product-default-img').attr('rel', fileName);
        // $('.product-default-img').attr('data-ext', fileExt);
    })

    $('form[data-toggle="validator"] select').on('change', function (event) {
        event.preventDefault();
        $(this).find('option[disabled]').remove();
    });

    $('#product_video').on('hidden.bs.modal', function (e) {
        $('#product_video .modal-body').html('');
    })

    //Custom validator
    $(".custom-validation-form").validator({
        custom: {
            unique: function ($el) {
                var now_date = new Date();
                var now_y = now_date.getFullYear();
                var min_y = now_y - 21;

                var custom_date = new Date($el.val());
                var custom_y = custom_date.getFullYear();

                if (min_y - custom_y < 0 || custom_y < 1930) {
                    return "Hey, your age must be more then 21"
                }
            }
        }
    }).off('focusout.bs.validator');

    $('.add-license').click(function () {
        var license = new FormData();

        license.append('registration_state', $('#states').val());
        license.append('medical_license', $('#medical_license').val());
        license.append('valid_until', $('#valid_until').val());
        license.append('license_image', $('#valid_license').prop('files')[0]);
        var profile_license = $('#medical_license').data('profile_license');

        console.log(license)

        if (medical_license == '') {
            $('.license-error').html('The medical license is required');
            return;
        } else {
            $('.license-error').html('');
        }
        if (valid_until == '') {
            $('.until-error').html('The valid date is required');
            return;
        } else {
            $('.until-error').html('');
        }

        if (profile_license == 'profile') {
            $.ajax({
                url: '../../doctor/account/license',
                type: 'post',
                contentType: false,
                processData: false,
                enctype: 'multipart/form-data',
                data: license,
                success: function (response) {
                    if (response.status == 'success') {
                        window.location.reload();
                    }
                }
            });
        } else {
            $.ajax({
                url: '../../doctor/license',
                type: 'post',
                dataType: 'json',
                data: {user_id: user_id, registration_state: registration_state, medical_license: medical_license, valid_until: valid_until},
                success: function (response) {
                    if (response.status == 'success') {
                        window.location.reload();
                    }
                }
            });

        }

    });


    $('#valid_until').datetimepicker({
        pickTime: false,
        format: 'MM/DD/YYYY',
        startDate: new Date(),
    });


    var appliedFilters = {};
    var appliedOrder = null;
    appliedFilters['strain'] = [];

    $('.filter-category').click(function () {
        var itemId = $(this).data('id');

        var strain = null;
        var sort = null;

        var hash = location.hash.replace('#', '');
        if (hash != '') {
            var segments = hash.split('/');

            segments = segments.filter(function (e) {
                return e;
            });

            $.each(segments, function (index, value) {
                if (value.indexOf('strain') != -1) {
                    strain = value;
                }
                if (value.indexOf('sort') != -1) {
                    sort = value;
                }
            });
        }

        $('.filter-category').each(function () {
            $(this).removeClass('active');
        });
        $(this).addClass('active');

        appliedFilters['category'] = [];
        if ($(this).data('id') != 0) {
            appliedFilters['category'].push(itemId);
        }

        if (itemId != 0) {
            window.location.hash = '/category:' + itemId + (strain ? '/' + strain : '') + (sort ? '/' + sort : '');
        } else {
            window.location.hash = (strain ? '/' + strain : '') + (sort ? '/' + sort : '');
        }

        updateView();

    });

    $('.filter-strain').click(function () {
        var itemId = $(this).data('id');

        var category = null;
        var sort = null;
        var hashStrains = null;

        var hash = location.hash.replace('#', '');
        if (hash != '') {
            var segments = hash.split('/');

            segments = segments.filter(function (e) {
                return e;
            });


            $.each(segments, function (index, value) {
                if (value.indexOf('category') != -1) {
                    category = value;
                }
                if (value.indexOf('strain') != -1) {
                    hashStrains = value.split(':');
                    hashStrains.shift();
                    
                    var hashStrains = hashStrains.map(Number);
                    appliedFilters['strain'] = hashStrains;

                }
                if (value.indexOf('sort') != -1) {
                    sort = value;
                }
            });
        }

        if ($(this).hasClass('list-group-item-success')) {
            $(this).removeClass('list-group-item-success');

            $.each(appliedFilters['strain'], function (key, val) {
                if (itemId == val) {
                    console.log(appliedFilters['strain'])
                    var index = appliedFilters['strain'].indexOf(parseInt(itemId, 10));
                    if (index != -1) {
                        appliedFilters['strain'].splice(index, 1);
                    }
                }
            });

            if (appliedFilters['strain'].length > 0) {
                window.location.hash = (category ? category : '') + '/strain:' + appliedFilters['strain'].join(':') + (sort ? '/' + sort : '');
            } else {
                window.location.hash = (category ? category : '') + (sort ? '/' + sort : '');
            }

        } else {
            $(this).addClass('list-group-item-success');

            appliedFilters['strain'].push(itemId);

            window.location.hash = (category ? category : '') + '/strain:' + appliedFilters['strain'].join(':') + (sort ? '/' + sort : '');

        }

        updateView();

    });

    $('#sortBy').change(function () {
        appliedOrder = $(this).val();

        var category = null;
        var strain = null;

        var hash = location.hash.replace('#', '');
        if (hash != '') {
            var segments = hash.split('/');

            segments = segments.filter(function (e) {
                return e;
            });


            $.each(segments, function (index, value) {
                if (value.indexOf('category') != -1) {
                    category = value;
                }
                if (value.indexOf('strain') != -1) {
                    strain = value;
                }
            });
        }

        if (appliedOrder != 0) {
            window.location.hash = (category ? category : '') + (strain ? '/' + strain : '') + '/sort:' + appliedOrder;
        } else {
            window.location.hash = (category ? category : '') + (strain ? '/' + strain : '');
        }

        updateView();

    });

    $('.selected-portion').click(function () {

        setTimeout(function () {
            checkStockCount($('#qty'));
        }, 100);
    });

    $('#qty').change(function () {
        checkStockCount($(this));
    });


    $('#portions :input').change(function (e) {
        $('#qty').val(1);
       
        $('.current-total').text(((($(this).data('price'))%1 == 0)?($(this).data('price')):($(this).data('price')).toFixed(2)) + ' ' + globalCurrentcy);
    });

    $('.qty').change(function (e) {
        var quantity = $(this).val();
        var itemWeight = $(this).closest('.cart-item').find('.weight').val();
        var inStockWeight = $(this).closest('.cart-item').find('.inStock').val();
        var price = $(this).closest('.cart-item').find('.price').val();

        if (itemWeight * quantity <= inStockWeight) {
            $(this).next('.current-subtotal').text(quantity * price);
        } else {
            $(this).next('.current-subtotal').text('Out of stock');
        }


    });

    $('#updateCart').click(function () {
        if (!$(this).hasClass('disabled')) {
            var _this = $(this);
            var productId = $('#productId').val();
            var portionId = $('.selected-portion.active').find('input').data('id');
            var quantity = $('#qty').val();

            if (productId && portionId && quantity) {
                var data = {
                    product_id: productId,
                    portion_id: portionId,
                    quantity: quantity
                };

                if (quantity > 0) {

                    updateCart(data, function (response) {
                        if (response.status == 'success') {
                            $('.cart-content').removeClass('hidden');
                            $('.cart-count').text(response.count);
                            _this.text('Update Cart');

                            $('.alert-success').removeClass('hidden');
                            setTimeout(function () {
                                $('.alert-success').addClass('hidden');
                            }, 3000);
                        } else {
                            $('.alert-danger').text(response.message).removeClass('hidden');
                            setTimeout(function () {
                                $('.alert-danger').addClass('hidden');
                            }, 3000);
                        }
                    });
                } else {
                    $('.alert-danger').text('Quantity sgould be greater then zero').removeClass('hidden');
                    setTimeout(function () {
                        $('.alert-danger').addClass('hidden');
                    }, 3000);
                }
            }
        }
    });

    $('.updateCart').click(function () {
        if ($('.cart-item').length > 0) {
            var data = [];
            var errorCheck = false;
            $('.cart-item').each(function () {
                var productId = $(this).find('.productId').val();
                var portionId = $(this).find('.portionId').val();
                var quantity = $(this).find('.qty').val();

                if (quantity > 0) {
                    if (productId && portionId && quantity) {
                        data.push({
                            product_id: productId,
                            portion_id: portionId,
                            quantity: quantity
                        });
                    }
                } else {
                    $('.alert-danger').text('Quantity sgould be greater then zero').removeClass('hidden');
                    setTimeout(function () {
                        $('.alert-danger').addClass('hidden');
                    }, 3000);
                    errorCheck = true;
                    return false;
                }

            });

            if (!errorCheck) {
                updateCartMultiple(data, function (response) {

                    if (response.status == 'success') {
                        var total = 0;
                        $('.current-subtotal').each(function () {
                            total += parseInt($(this).text());
                        });
                        console.log(total);
                        $('.total-val').text(total);

                        $('.alert-success').text('Cart updated successfully').removeClass('hidden');
                        setTimeout(function () {
                            $('.alert-success').addClass('hidden');
                        }, 2000);
                    } else {
                        $('.alert-danger').text(response.message).removeClass('hidden');
                        setTimeout(function () {
                            $('.alert-danger').addClass('hidden');
                        }, 3000);
                    }
                });
            }
        }
    });

    $('.parent-category').click(function () {
        if ($(this).hasClass('open-parent')) {
            $(this).removeClass('open-parent');
            $(this).find('.glyphicon-chevron-down').removeClass('glyphicon-chevron-down').addClass('glyphicon-chevron-right');
            $(this).next('.sub-categories-block').stop(true, true).slideUp();
        } else {
            $(this).addClass('open-parent');
            $(this).find('.glyphicon-chevron-right').removeClass('glyphicon-chevron-right').addClass('glyphicon-chevron-down');
            $(this).next('.sub-categories-block').stop(true, true).slideDown();
        }
    })

    $('#new-address-btn').click(function () {
        if($('#address-block').css('display') == 'none') {
            $('#shippingAddresses').addClass('disabled').attr('disabled', 'disabled');
            $('#address-block').stop(true, true).slideDown();
        }
        else {
            $('#shippingAddresses').removeClass('disabled').removeAttr('disabled');
            $('#address-block').stop(true, true).slideUp();
        }
    })

    $('#checkout').click(function () {
        var cart = {};
        $('.cart-item').each(function () {
            cart[$(this).find('.productId').val()] = {
                'portion_id': $(this).find('.portionId').val(),
                'quantity': $(this).find('.qty').val(),
            };
        });

        var totalPrice = parseInt($('.total-val').text());
        var currentBalance = parseInt($('#currentBalance').text());
        var shippingAddressId = $('#shippingAddresses').val();

        if (totalPrice <= currentBalance) {

            if($('#address-block').css('display') == 'block' && $('#shippingAddresses').hasClass('disabled')) {
                var deff = $.Deferred();
                var state = $('#states').val();
                var city = $('#city').val();
                var address = $('#address').val();
                var zipcode = $('#zipcode').val();

                if(state != '' && city != '' && address != '' && zipcode != '') {
                    $.ajax({
                        url: baseUrl + '/profile/address/create',
                        dataType: 'json',
                        type: 'post',
                        data: {
                            state: state,
                            city: city,
                            address: address,
                            zip_code: zipcode,
                        },
                        success: function (response) {
                            if(response.status == 'success') {
                                shippingAddressId = response.data;
                                deff.resolve(response);
                            }
                            else {
                                deff.reject(response);
                            }
                        }
                    });
                }
            }
            if(deff) {
                $.when(deff).always(function (res) {
                    if(res.status == 'success') {
                        cartCheckout(cart, shippingAddressId)
                    }
                    else {
                        $('.alert-danger').removeClass('hidden').text(res.message);
                        setTimeout(function () {
                            $('.alert-danger').addClass('hidden').text('')
                        }, 5000);
                    }
                });
            }
            else {
                cartCheckout(cart, shippingAddressId);
            }



        } else {
            $('.alert-danger').text('Your Balance is not enough').removeClass('hidden');
            setTimeout(function () {
                $('.alert-danger').addClass('hidden');
            }, 3000);
        }
    });

    if ($('#payment-form').length > 0) {
        var checkout = new Demo({
            formID: 'payment-form'
        });

        braintree.setup(client_token, "dropin", {
            container: "bt-dropin"
        });
    }


    $('#amount').keyup(function () {
        var amount = $(this).val();
        if (!isNaN(amount)) {
            $('.errors').html('');
            $('.point-count').html(amount);
            $('#amount_value').val(amount);
        } else {
            $('.errors').html('Amount must be numeric');
            $('.point-count').html(0);
            $('#amount_value').val(0);
        }


    })

});

function cartCheckout(cart, shippingAddressId) {
    $.ajax({
        url: baseUrl + '/cart/checkout',
        type: 'post',
        data: {
            cart: cart,
            shipping_address_id: shippingAddressId
        },
        success: function (response) {
            if (response.status == 'success') {
                $('.alert-success').text(response.message).removeClass('hidden');
                setTimeout(function () {
                    window.location.href = baseUrl + '/profile/purchase-history';
                }, 3000);
            } else {
                $('.alert-danger').text(response.message).removeClass('hidden');
                setTimeout(function () {
                    $('.alert-danger').addClass('hidden');
                }, 3000);
            }
        }
    });
}

function updateView() {
    var data = {};
    var appliedFilters = {};
    var appliedOrder = null;
    appliedFilters['category'] = [];
    appliedFilters['strain'] = [];

    var hash = location.hash.replace('#', '');
    if (hash != '') {
        var segments = hash.split('/');

        segments = segments.filter(function (e) {
            return e;
        });

        $.each(segments, function (index, value) {
            if (value.indexOf('category') != -1) {
                var cat = value.split(':').pop();
                appliedFilters['category'].push(cat);
                $('.filter-category[data-id=' + cat + ']').addClass('active');
            }
            if (value.indexOf('strain') != -1) {
                strains = value.split(':');
                strains.shift();

                $.each(strains, function (key, val) { //console.log(val)
                    $('.filter-strain[data-id=' + val + ']').addClass('list-group-item-success');
                    appliedFilters['strain'].push(val);
                });
                
            }
            if (value.indexOf('sort') != -1) {
                appliedOrder = value.split(':').pop();
                $('#sortBy').val(appliedOrder);
            }
        });

//        console.log('appliedFilters: ', appliedFilters);

        data = {
            appliedFilters: appliedFilters,
            appliedOrder: appliedOrder
        };

//        console.log('data: ', data);

    }
    data['locale'] = locale;
   

    $.ajax({
        url: baseUrl + '/products',
        type: 'post',
        data: data,
        success: function (response) {
            $('.product-content').html(response);
        }
    });
}


function updateCart(data, callbackFunc) {
    $.ajax({
        url: baseUrl + '/cart/add-to-cart',
        type: 'post',
        data: data,
        success: function (response) {
            callbackFunc(response);
        }
    });
}

function updateCartMultiple(data, callbackFunc) {
    $.ajax({
        url: baseUrl + '/cart/update-cart',
        type: 'post',
        data: {'cart_data': data},
        success: function (response) {
            callbackFunc(response);
        }
    });
}

function checkStockCount(input) {
    var quantity = input.val()
    var currentPortion = $('.selected-portion.active').find('input');

    if (currentPortion.data('weight') * quantity <= $('#inStock').val()) {
  
        $('.current-total').text((((quantity * currentPortion.data('price'))%1 == 0)?(quantity * currentPortion.data('price')):(quantity * currentPortion.data('price')).toFixed(2)) + ' ' + globalCurrentcy);
        $('#updateCart').removeClass('disabled');
    } else {
        $('.current-total').text('Out of stock');
        $('#updateCart').addClass('disabled');
    }
}

String.prototype.toSlug = function () {
    st = this.toLowerCase();
    st = st.replace(/[\u00C0-\u00C5]/ig, 'a')
    st = st.replace(/[\u00C8-\u00CB]/ig, 'e')
    st = st.replace(/[\u00CC-\u00CF]/ig, 'i')
    st = st.replace(/[\u00D2-\u00D6]/ig, 'o')
    st = st.replace(/[\u00D9-\u00DC]/ig, 'u')
    st = st.replace(/[\u00D1]/ig, 'n')
    st = st.replace(/[^a-z0-9 ]+/gi, '')
    st = st.trim().replace(/ /g, '-');
    st = st.replace(/[\-]{2}/g, '');
    return (st.replace(/[^a-z\- ]*/gi, ''));
}