<?php

/*
  |--------------------------------------------------------------------------
  | Web Routes
  |--------------------------------------------------------------------------
  |
  | Here is where you can register web routes for your application. These
  | routes are loaded by the RouteServiceProvider within a group which
  | contains the "web" middleware group. Now create something great!
  |
 */
Route::get('/login', 'Auth\LoginController@logout');
// Admin Authentication routes...
Route::get('admin/auth/login', 'Admin\AuthController@getLogin');
Route::post('admin/auth/login', 'Admin\AuthController@postLogin');
Route::get('admin/auth/logout', 'Admin\AuthController@getLogout');

Route::group(['prefix' => 'admin', 'middleware' => ['auth.admin']], function () {
    Route::get('/', 'Admin\DashboardController@index');
    Route::get('/patients', 'Admin\PatientsController@index');
    Route::get('/patient/{id}', 'Admin\PatientsController@singleView');
    Route::get('/doctors', 'Admin\DoctorsController@index');
    Route::get('/doctor/{id}', 'Admin\DoctorsController@singleView');
    Route::any('/user/status', 'Admin\UsersController@changeStatus');

    //Blog Category
    Route::get('/categories', 'Admin\CategoriesController@index');
    Route::get('/category/create', 'Admin\CategoriesController@getCreate');
    Route::post('/category/create', 'Admin\CategoriesController@postCreate');
    Route::get('/category/translation/{category_id}', 'Admin\CategoriesController@getTranslation');
    Route::post('/category/translation/{category_id}', 'Admin\CategoriesController@postTranslation');
    Route::get('/category/{id}', 'Admin\CategoriesController@getUpdate');
    Route::post('/category/{id}', 'Admin\CategoriesController@postUpdate');
    Route::get('/category/delete/{id}', 'Admin\CategoriesController@delete');

    //Product Category
    Route::get('/product-categories', 'Admin\ProductCategoriesController@index');
    Route::get('/product-category/create', 'Admin\ProductCategoriesController@getCreate');
    Route::post('/product-category/create', 'Admin\ProductCategoriesController@postCreate');
    Route::get('/product-category/translation/{category_id}', 'Admin\ProductCategoriesController@getTranslation');
    Route::post('/product-category/translation/{category_id}', 'Admin\ProductCategoriesController@postTranslation');
    Route::get('/product-category/{id}', 'Admin\ProductCategoriesController@getUpdate');
    Route::post('/product-category/{id}', 'Admin\ProductCategoriesController@postUpdate');
    Route::get('/product-category/delete/{id}', 'Admin\ProductCategoriesController@delete');

    //Posts
    Route::get('/posts', 'Admin\PostsController@index');
    Route::get('/post/create', 'Admin\PostsController@getCreate');
    Route::post('/post/create', 'Admin\PostsController@postCreate');
    Route::post('/post/upload', 'Admin\PostsController@postUpload');
    Route::post('/post/featured', 'Admin\PostsController@postChangeFeatruedImage');
    Route::get('/post/translation/{post_id}', 'Admin\PostsController@getTranslation');
    Route::post('/post/translation/{post_id}', 'Admin\PostsController@postTranslation');
    Route::post('/post/{id}', 'Admin\PostsController@postUpdate');
    Route::get('/post/{id}', 'Admin\PostsController@getUpdate');
    Route::get('/post/delete/{id}', 'Admin\PostsController@delete');

    // Portions
    Route::get('/portions', 'Admin\PortionsController@index');
    Route::get('/portion/create', 'Admin\PortionsController@getCreate');
    Route::post('/portion/create', 'Admin\PortionsController@postCreate');
    Route::get('/portion/{id}', 'Admin\PortionsController@getUpdate');
    Route::post('/portion/{id}', 'Admin\PortionsController@postUpdate');
    Route::get('/portion/translation/{id}', 'Admin\PortionsController@getTranslation');
    Route::post('/portion/translation/{id}', 'Admin\PortionsController@postTranslation');
    Route::get('/portion/delete/{id}', 'Admin\PortionsController@delete');

    // Strains
    Route::get('/strains', 'Admin\StrainsController@index');
    Route::get('/strain/create', 'Admin\StrainsController@getCreate');
    Route::post('/strain/create', 'Admin\StrainsController@postCreate');
    Route::get('/strain/{id}', 'Admin\StrainsController@getUpdate');
    Route::post('/strain/{id}', 'Admin\StrainsController@postUpdate');
    Route::get('/strain/translation/{id}', 'Admin\StrainsController@getTranslation');
    Route::post('/strain/translation/{id}', 'Admin\StrainsController@postTranslation');
    Route::get('/strain/delete/{id}', 'Admin\StrainsController@delete');

    // Brands
    Route::get('/brands', 'Admin\BrandsController@index');
    Route::get('/brand/create', 'Admin\BrandsController@getCreate');
    Route::post('/brand/create', 'Admin\BrandsController@postCreate');
    Route::get('/brand/{id}', 'Admin\BrandsController@getUpdate');
    Route::post('/brand/{id}', 'Admin\BrandsController@postUpdate');
    Route::get('/brand/translation/{id}', 'Admin\BrandsController@getTranslation');
    Route::post('/brand/translation/{id}', 'Admin\BrandsController@postTranslation');
    Route::get('/brand/delete/{id}', 'Admin\BrandsController@delete');

    // Products
    Route::get('/products', 'Admin\ProductsController@index');
    Route::get('/product/create', 'Admin\ProductsController@getCreate');
    Route::post('/product/create', 'Admin\ProductsController@postCreate');
    Route::get('/product/{id}', 'Admin\ProductsController@getUpdate');
    Route::post('/product/{id}', 'Admin\ProductsController@postUpdate');
    Route::get('/product/translation/{id}', 'Admin\ProductsController@getTranslation');
    Route::post('/product/translation/{id}', 'Admin\ProductsController@postTranslation');
    Route::post('/product/add-video/{id}', 'Admin\ProductsController@addVideo');
    Route::post('/product/add-image/{id}', 'Admin\ProductsController@uploadImage');
    Route::post('/product/remove-media/{id}', 'Admin\ProductsController@removeMedia');
    Route::get('/product/delete/{id}', 'Admin\ProductsController@delete');

    //Orders
    Route::get('/orders', 'Admin\OrdersController@index');
    Route::get('/order/{id}', 'Admin\OrdersController@single');
    Route::post('/order/status-update', 'Admin\OrdersController@postUpdateStatus');
    Route::get('/download-pdf', ['as' => 'download_pdf', 'uses' => 'Admin\OrdersController@downloadPdf']);

    //languages
    Route::get('/languages', 'Admin\LanguagesController@index');
    Route::get('/language/create', 'Admin\LanguagesController@getCreate');
    Route::post('/language/create', 'Admin\LanguagesController@postCreate');
    Route::get('/language/{id}', 'Admin\LanguagesController@getUpdate');
    Route::post('/language/{id}', 'Admin\LanguagesController@postUpdate');
    Route::get('/language/delete/{id}', 'Admin\LanguagesController@delete');
});

Route::group(['prefix' => \UriLocalizer::localeFromRequest(), 'middleware' => 'localize'], function () {

//Doctor and Patient Authentication routes...
    Route::get('/blog', ['as' => 'blog', 'uses' => 'Front\PostController@index']);
    Route::get('/blog/category/{id}/{slug}', ['as' => 'blog', 'uses' => 'Front\PostController@getByCategory']);
    Route::get('/blog/{id}/{slug}', ['as' => 'blog', 'uses' => 'Front\PostController@single']);

    Route::get('/products', ['as' => 'products', 'uses' => 'Front\ProductController@index']);
    Route::post('/products', ['as' => 'products', 'uses' => 'Front\ProductController@filter']);
    Route::get('/product/{id}/{slug}', ['as' => 'products', 'uses' => 'Front\ProductController@single']);
    Route::get('/products/favorites', 'Front\ProductController@favorites');
    Route::post('/products/add-to-favorite', ['as' => 'products', 'uses' => 'Front\ProductController@addToFavorite']);
    Route::post('/products/remove-from-favorite', ['as' => 'products', 'uses' => 'Front\ProductController@removeFromFavorite']);

// Cart
    Route::get('/cart/checkout', 'Front\OrderController@index');
    Route::post('/cart/add-to-cart', 'Front\OrderController@addToCart');
    Route::post('/cart/update-cart', 'Front\OrderController@addToCartMultiple');
    Route::get('/cart/remove-from-cart/{id}', 'Front\OrderController@removeFromCart');
    Route::post('/cart/checkout', 'Front\OrderController@checkout');

    Route::get('/auth/signin', 'Auth\LoginController@showLoginForm');
    Route::post('/auth/signin', 'Auth\LoginController@postLogin');
    Route::get('/auth/signup', 'Auth\RegisterController@showRegistrationForm');
    Route::post('/auth/signup', 'Auth\RegisterController@postRegister');

    Route::group(['domain' => 'miraclecrop.dev'], function () {

        Route::get('/', ['as' => 'patient.home', 'uses' => 'IndexController@index']);
        //step 1
        Route::get('/auth/signup/step-1', 'Auth\RegisterController@getPatientStep1');
        Route::post('/auth/signup/step-1', 'Auth\RegisterController@postPatientStep1');
        //step 2
        Route::get('/auth/signup/step-2', 'Auth\RegisterController@getPatientStep2');
        Route::post('/auth/signup/step-2', 'Auth\RegisterController@postPatientStep2');
        //step 3 
        Route::get('/auth/signup/step-3', 'Auth\RegisterController@getPatientStep3');
        Route::post('/auth/signup/step-3', 'Auth\RegisterController@postPatientStep3');
        //step 4
        Route::get('/auth/signup/step-4', 'Auth\RegisterController@getPatientStep4');
        Route::post('/auth/signup/step-4', 'Auth\RegisterController@postPatientStep4');
        //step 5
        Route::get('/auth/signup/step-5', 'Auth\RegisterController@getPatientStep5');
        Route::post('/auth/signup/step-5', 'Auth\RegisterController@postPatientStep5');
    });

    Route::group(['domain' => 'doctor.miraclecrop.dev'], function () {

        Route::get('/', ['as' => 'doctor.home', 'uses' => 'IndexController@index']);

        //step 1
        Route::get('/auth/signup/step-1', 'Auth\RegisterController@getDoctorStep1');
        Route::post('/auth/signup/step-1', 'Auth\RegisterController@postDoctorStep1');
        //step 2
        Route::get('/auth/signup/step-2', 'Auth\RegisterController@getDoctorStep2');
        Route::post('/auth/signup/step-2', 'Auth\RegisterController@postDoctorStep2');
        //step 3
        Route::get('/auth/signup/step-3', 'Auth\RegisterController@getDoctorStep3');
        Route::post('/auth/signup/step-3', 'Auth\RegisterController@postDoctorStep3');
        Route::post('/doctor/license', 'Auth\RegisterController@postDoctorLicense');
        //step 4
        Route::get('/auth/signup/step-4', 'Auth\RegisterController@getDoctorStep4');
        Route::post('/auth/signup/step-4', 'Auth\RegisterController@postDoctorStep4');

        // doctor profile
        Route::post('/doctor/account/license', 'Front\AccountController@postDoctorProfileLicense');
    });

    Route::get('/auth/logout', 'Auth\LoginController@logout');

    // Password reset link request routes...
    Route::get('password/email', ['as' => 'password.email', 'uses' => 'Auth\ForgotPasswordController@showLinkRequestForm']);
    Route::post('password/email', ['as' => 'password.email.post', 'uses' => 'Auth\ForgotPasswordController@sendResetLinkEmail']);

// Password reset routes...
    Route::get('password/reset/{token}', ['as' => 'password.reset', 'uses' => 'Auth\ResetPasswordController@showResetForm']);
    Route::post('password/reset', ['as' => 'password.reset.post', 'uses' => 'Auth\ResetPasswordController@reset']);

    Route::get('/registration-success', ['as' => 'registration-success', 'uses' => 'Auth\RegisterController@showRegistrationInfo']);
    Route::get('/confirmation-failed', ['as' => 'confirmation-failed', 'uses' => 'Auth\RegisterController@showRegistrationInfo']);
    Route::get('/confirmation-success', ['as' => 'confirmation-success', 'uses' => 'Auth\RegisterController@showRegistrationInfo']);
    Route::get('/confirm-email/{confirmationToken}', ['as' => 'confirm-email', 'uses' => 'Auth\RegisterController@confirmEmail'])->name('confirmationToken');

    Route::get('/profile', 'Front\AccountController@getProfile');
    Route::get('/profile/edit', 'Front\AccountController@getEditProfile');
    Route::post('/profile/edit', 'Front\AccountController@postEditProfile');
    Route::get('/profile/social', 'Front\AccountController@getSocialProfiles');
    Route::get('/profile/social/remove/{id}', 'Front\AccountController@getRemoveSocialProfile');
    Route::post('/profile/social', 'Front\AccountController@postSocialProfiles');
    Route::get('/profile/addresses', 'Front\AccountController@getAddresses');
    Route::get('/profile/address/create', 'Front\AccountController@getAddressesEdit');
    Route::post('/profile/address/create', 'Front\AccountController@postAddressesCreate');
    Route::get('/profile/address/edit/{id}', 'Front\AccountController@getAddressesEdit');
    Route::post('/profile/address/edit', 'Front\AccountController@postAddressesCreate');
    Route::get('/profile/my-wallet', 'Front\AccountController@getMyWallet');
    Route::post('/profile/my-wallet', 'Front\AccountController@postMyWallet');
    Route::get('/profile/purchase-history', 'Front\AccountController@purchaseHistory');
    
    

//Doctors routes...
    Route::get('/doctor', ['as' => 'doctor.account', 'uses' => 'Front\Doctor\IndexController@index'])->middleware('auth.doctor');


//Patient routes...
    Route::get('/patient', ['as' => 'patient.account', 'uses' => 'Front\Patient\IndexController@index'])->middleware('auth.patient');

    Route::post('/signature', 'Auth\RegisterController@postSignature');
});

