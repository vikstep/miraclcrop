<?php

namespace App\Providers;

use Illuminate\Support\ServiceProvider;
use Illuminate\Support\Facades\Validator;
use App\Validators\CustomValidator;
use App\Models\UserCart;
use App\Models\Category;
use App\Models\Strain;

class AppServiceProvider extends ServiceProvider {

    /**
     * Bootstrap any application services.
     *
     * @return void
     */
    public function boot() {
        Validator::resolver(function($translator, $data, $rules, $messages) {
            return new CustomValidator($translator, $data, $rules, $messages);
        });

        view()->composer('front.product', function($view) {
            $categories = Category::where('type', 'product')->where('parent_id', 0)->where('status', 'enabled')->orderBy('sort', 'desc')->get();

            foreach($categories as &$category) {
                $category['sub_categories'] = Category::where('type', 'product')->where('parent_id', $category->id)->where('status', 'enabled')->orderBy('sort', 'desc')->get();
            }
            
            $strains = Strain::orderBy('sort', 'desc')->get();

            $view->with('sidebar', ['categories' => $categories, 'strains' => $strains]);
        });

        view()->composer('front.*', function($view) {
            $view->with('userCart', UserCart::class);
        });
    }

    /**
     * Register any application services.
     *
     * @return void
     */
    public function register() {
        //
    }

}
