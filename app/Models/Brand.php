<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Model;
use Illuminate\Notifications\Notifiable;

class Brand extends Model {

    use Notifiable;

    /**
     * The attributes that are mass assignable.
     *
     * @var array
     */
    protected $table = 'brands';
    protected $filePath = '/files/brands/';
    protected $fillable = [
        'title',
        'description',
        'sort'
    ];
    public static $rules = [
        'title' => 'required',
        'description' => 'required',
        'brand_logo' => 'dimensions:min_width=400,max_width=800|mimes:jpg,jpeg,png|max:1000'
    ];

    public function getFileDir() {
        return public_path() . $this->filePath;
    }

}
