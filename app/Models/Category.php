<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Model;
use Illuminate\Notifications\Notifiable;

class Category extends Model {

    use Notifiable;

    /**
     * The attributes that are mass assignable.
     *
     * @var array
     */
    protected $table = 'categories';
    protected $fillable = [
        'sort',
        'status'
    ];
    public static $rules = [
        'title' => 'required'
    ];

    public function product_category() {
        return $this->hasMany('App\Models\Product', 'category_id');
    }
    
    public function post_category() {
        return $this->hasMany('App\Models\Post', 'category_id');
    }

}
