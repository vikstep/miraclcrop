<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Model;
use Illuminate\Notifications\Notifiable;

class Portion extends Model {

    use Notifiable;

    /**
     * The attributes that are mass assignable.
     *
     * @var array
     */
    protected $table = 'portions';

    protected $fillable = [
        'weight',
        'sort'
       
    ];

    public static $rules = [
        'title' => 'required',
        'weight' => 'required'
    ];

}