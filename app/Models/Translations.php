<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Model;
use Illuminate\Notifications\Notifiable;
use Illuminate\Support\Facades\DB;

class Translations extends Model {

    use Notifiable;

    /**
     * The attributes that are mass assignable.
     *
     * @var array
     */
    protected $table = 'translator_translations';
    protected $fillable = [
        'locale',
        'namespace',
        'group',
        'item',
        'text',
    ];

    public function getTranslationsByGroupAndItem($group, $item) {
        return DB::table($this->table)->where('group', $group)->where('item', $item)->get();
    }

    public function getSingleTranslationsByGroupAndItem($locale, $group, $item) {
        return DB::table($this->table)->where('group', $group)->where('item', $item)->where('locale', $locale)->first();
    }

    public function deleteSingleTranslationsByGroupAndItem($locale, $group, $item) {
        return DB::table($this->table)->where('group', $group)->where('item', $item)->where('locale', $locale)->delete();
    }

    public function deleteTranslationsByGroupAndItem($group, $item) {
        return DB::table($this->table)->where('group', $group)->where('item', $item)->delete();
    }

    public function postTranslations($request, $languages, $post_id, $type, $TranslationRepository) {
        foreach ($request->{ $type } as $language_type => $language_text) {
            $single_translation = $this->getSingleTranslationsByGroupAndItem($language_type, 'post', $post_id . '.' . $type);
            if ($language_type != 'en') {
                if (!empty($language_text[0])) {
                    if (empty($request->{ $type }[$language_type])) {
                        return redirect('admin/post/translation/' . $post_id)
                                        ->withErrors(['The ' . $languages[$language_type] . ' ' . ucfirst($type) . ' can not be empty.']);
                    }
                    if (isset($single_translation) && $single_translation->locale == $language_type) {
                        $TranslationRepository->update($single_translation->id, $language_text[0]);
                    } else {
                        $TranslationRepository->create([
                            'locale' => $language_type,
                            'namespace' => '*',
                            'group' => 'post',
                            'item' => $post_id . '.' . $type,
                            'text' => $language_text[0],
                        ]);
                    }
                } else if (empty($language_text[0]) && isset($single_translation)) {
                    $this->deleteSingleTranslationsByGroupAndItem($language_type, 'post', $post_id . '.' . $type);
                }
            }
        }
    }
    
    public function productTranslations($request, $languages, $product_id, $type, $TranslationRepository) {
        foreach ($request->{ $type } as $language_type => $language_text) {
            $single_translation = $this->getSingleTranslationsByGroupAndItem($language_type, 'product', $product_id . '.' . $type);
            if ($language_type != 'en') {
                if (!empty($language_text[0])) {
                    if (empty($request->{ $type }[$language_type])) {
                        return redirect('admin/product/translation/' . $product_id)
                                        ->withErrors(['The ' . $languages[$language_type] . ' ' . ucfirst($type) . ' can not be empty.']);
                    }
                    if (isset($single_translation) && $single_translation->locale == $language_type) {
                        $TranslationRepository->update($single_translation->id, $language_text[0]);
                    } else {
                        $TranslationRepository->create([
                            'locale' => $language_type,
                            'namespace' => '*',
                            'group' => 'product',
                            'item' => $product_id . '.' . $type,
                            'text' => $language_text[0],
                        ]);
                    }
                } else if (empty($language_text[0]) && isset($single_translation)) {
                    $this->deleteSingleTranslationsByGroupAndItem($language_type, 'product', $product_id . '.' . $type);
                }
            }
        }
    }

}
