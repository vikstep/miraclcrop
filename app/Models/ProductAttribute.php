<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Model;
use Illuminate\Notifications\Notifiable;
use DB;

class ProductAttribute extends Model {

    use Notifiable;

    /**
     * The attributes that are mass assignable.
     *
     * @var array
     */
    protected $table = 'product_attributes';
    protected $fillable = [
        'sort'
    ];
    public static $rules = [
        'type' => 'required',
        'price' => 'required'
    ];
    public static $types = [
        '1' => 'Strain',
        '2' => 'Portion'
    ];

    public function product() {
        return $this->belongsTo('App\Models\Product', 'product_id');
    }
    
    public static function getProductPrice($product_id, $portion_id) {
        return DB::table('product_attributes')->select('attribute_value')->where('product_id', $product_id)->where('attribute_type', '2')->where('attribute_id', $portion_id)->first()->attribute_value;
    }

}
