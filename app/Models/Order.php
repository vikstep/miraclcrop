<?php

namespace App\Models;

use Illuminate\Support\Facades\DB;
use Illuminate\Database\Eloquent\Model;
use Illuminate\Notifications\Notifiable;

class Order extends Model {

    use Notifiable;

    /**
     * The attributes that are mass assignable.
     *
     * @var array
     */
    protected $table = 'orders';

    public function user() {
        return $this->belongsTo('App\Models\User', 'user_id');
    }
    
    public function products() {
        return $this->hasMany('App\Models\OrderProduct');
    }

}
