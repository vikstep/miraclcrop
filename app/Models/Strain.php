<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Model;
use Illuminate\Notifications\Notifiable;

class Strain extends Model {

    use Notifiable;

    /**
     * The attributes that are mass assignable.
     *
     * @var array
     */
    protected $table = 'strains';

    protected $fillable = [
        'sort'
       
    ];

    public static $rules = [
        'title' => 'required',
    ];

}