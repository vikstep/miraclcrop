<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Model;

class DoctorLicense extends Model {


    /**
     * The attributes that are mass assignable.
     *
     * @var array
     */
    protected $table = 'doctor_license';

    protected $fillable = [
        'user_id',
        'registration_state',
        'medical_license',
        'valid_until',
       
    ];

       public function user() {
        return $this->belongsTo('App\Models\User', 'user_id');
    }

}