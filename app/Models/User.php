<?php

namespace App\Models;

use Illuminate\Notifications\Notifiable;
use Illuminate\Foundation\Auth\User as Authenticatable;
use DB;

class User extends Authenticatable {

    use Notifiable;

    /**
     * The attributes that are mass assignable.
     *
     * @var array
     */
    protected $table = 'users';
    protected $fillable = [
        'username',
        'email',
        'password',
        'role',
        'first_name',
        'last_name',
        'gender',
        'dob',
        'image',
        'phone',
        'state',
        'city',
        'address',
        'zip_code',
        'notes',
        'has_recipe',
        'status',
        'status_reason',
        'confirm_token',
        'ip',
        'user_agent',
    ];

    /**
     * The attributes that should be hidden for arrays.
     *
     * @var array
     */
    protected $hidden = [
        'password', 'remember_token',
    ];
    public static $rules = [
        'firstname' => 'required|max:50',
        'lastname' => 'required|max:50',
        'role' => 'required',
        'email' => 'required|email|max:255|unique:users,email',
        'password' => 'required|confirmed|min:3',
        'dob' => 'required|over_21'
    ];

    public function doctorLicense() {
        return $this->hasMany('App\Models\DoctorLicense');
    }

    /**
     * US States
     *
     * @var array
     */
    public static $states = array(
        'AL' => 'Alabama',
        'AK' => 'Alaska',
        'AZ' => 'Arizona',
        'AR' => 'Arkansas',
        'CA' => 'California',
        'CO' => 'Colorado',
        'CT' => 'Connecticut',
        'DE' => 'Delaware',
        'DC' => 'District Of Columbia',
        'FL' => 'Florida',
        'GA' => 'Georgia',
        'HI' => 'Hawaii',
        'ID' => 'Idaho',
        'IL' => 'Illinois',
        'IN' => 'Indiana',
        'IA' => 'Iowa',
        'KS' => 'Kansas',
        'KY' => 'Kentucky',
        'LA' => 'Louisiana',
        'ME' => 'Maine',
        'MD' => 'Maryland',
        'MA' => 'Massachusetts',
        'MI' => 'Michigan',
        'MN' => 'Minnesota',
        'MS' => 'Mississippi',
        'MO' => 'Missouri',
        'MT' => 'Montana',
        'NE' => 'Nebraska',
        'NV' => 'Nevada',
        'NH' => 'New Hampshire',
        'NJ' => 'New Jersey',
        'NM' => 'New Mexico',
        'NY' => 'New York',
        'NC' => 'North Carolina',
        'ND' => 'North Dakota',
        'OH' => 'Ohio',
        'OK' => 'Oklahoma',
        'OR' => 'Oregon',
        'PA' => 'Pennsylvania',
        'RI' => 'Rhode Island',
        'SC' => 'South Carolina',
        'SD' => 'South Dakota',
        'TN' => 'Tennessee',
        'TX' => 'Texas',
        'UT' => 'Utah',
        'VT' => 'Vermont',
        'VA' => 'Virginia',
        'WA' => 'Washington',
        'WV' => 'West Virginia',
        'WI' => 'Wisconsin',
        'WY' => 'Wyoming',
    );

    public function hasRole($input) {
        $roles = explode('|', $input);
        if (in_array($this->role, $roles)) {
            return true;
        }
        return false;
    }

    public static function getDays() {
        $days = [];
        for ($day = 1; $day <= 31; $day++) {
            $days[] = $day;
        }
       
        return $days;
    }

    public static $months = array(
        '01' => 'January',
        '02' => 'February',
        '03' => 'March',
        '04' => 'April',
        '05' => 'May',
        '06' => 'June',
        '07' => 'July',
        '08' => 'August',
        '09' => 'September',
        '10' => 'October',
        '11' => 'November',
        '12' => 'December'
    );

    public static function getYears() {
        $years = [];
        $this_year = date('Y');
        $end_year = $this_year - 21;
        $start_year = $this_year - 100;
        for ($year = $start_year; $year <= $end_year; $year++) {
            $years[] = $year;
        }
        rsort($years);
        
        return $years;
    }
    
    public static function chargeWallet($points) {
        $wallet = DB::table('users')->select('wallet_points')->where('id', auth()->user()->id)->first();
        DB::table('users')->where('id', auth()->user()->id)->update(['wallet_points' => $wallet->wallet_points - $points]);
    }

}
