<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Model;
use Illuminate\Notifications\Notifiable;

class Post extends Model {

    use Notifiable;

    /**
     * The attributes that are mass assignable.
     *
     * @var array
     */
    protected $table = 'posts';
    protected $fillable = [
        'sort',
        'status'
    ];
    protected $filePath = '/files/blog/';
    public static $rules = [
        'title' => 'required|min:3|max:50',
        'category' => 'required',
        'excerpt' => 'required|min:20|max:300',
        'content' => 'required|min:100'
    ];

    public function category() {
        return $this->belongsTo('App\Models\BlogCategory', 'category_id');
    }

    public function attachment() {
        return $this->hasMany('App\Models\Attachment');
    }

    public function getFilePath() {
        return $this->filePath;
    }

    public function getFileDir() {
        return public_path() . $this->filePath;
    }

    public function resizeImage($value, $path, $filename, $widthSize, $type) {
        $extension = $value->guessExtension();

        switch (strtolower($value->guessExtension())) {
            case 'jpeg':
                $image = imagecreatefromjpeg($value->getPathName());
                break;
            case 'png':
                $image = imagecreatefrompng($value->getPathName());
                break;
            case 'gif':
                $image = imagecreatefromgif($value->getPathName());
                break;
            default:
                exit('Unsupported type: ' . $$extension);
        }

        $fn = $value->getPathName();
        $size = getimagesize($fn);

        $ratio = $size[0] / $size[1]; // width/height
        if ($ratio > 1) {
            $new_width = $widthSize;
            $new_height = $widthSize / $ratio;
        } else {
            $new_width = $widthSize * $ratio;
            $new_height = $widthSize;
        }


        $old_width = imagesx($image);
        $old_height = imagesy($image);

        $new = imagecreatetruecolor($new_width, $new_height);
        imagealphablending($new, false);
        imagesavealpha($new, true);
        imagecopyresampled($new, $image, 0, 0, 0, 0, $new_width, $new_height, $old_width, $old_height);

        ob_start();
        imagepng($new, $path . '/' . $filename . '-' . $type . '.' . $extension, 9);
        $data = ob_get_clean();

        imagedestroy($image);
        imagedestroy($new);
        return $data;
    }
    
    public function getAttachmentPath($attachment) {
        return $this->getFilePath() . $attachment->created_at->format('m.Y') . '/' . $attachment->file_name;
    }

}
