<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Model;
use Illuminate\Notifications\Notifiable;

class ProductMedia extends Model {

    use Notifiable;

    /**
     * The attributes that are mass assignable.
     *
     * @var array
     */
    protected $table = 'product_media';
    protected $fillable = [
        'sort'
    ];
    public static $rules = [
        'type' => 'required',
        'media' => 'required'
    ];
    public static $types = [
        '1' => 'Image',
        '2' => 'Video'
    ];

    public function product() {
        return $this->belongsTo('App\Models\Product', 'product_id');
    }

}
