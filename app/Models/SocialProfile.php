<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Model;
use Illuminate\Notifications\Notifiable;

class SocialProfile extends Model
{

    use Notifiable;

    /**
     * The attributes that are mass assignable.
     *
     * @var array
     */
    protected $table = 'social_profiles';
    protected $fillable = [
        'url',
    ];
    public static $rules = [
        'url' => 'required',
    ];
}