<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Model;
use Illuminate\Notifications\Notifiable;

class Address extends Model
{

    use Notifiable;

    /**
     * The attributes that are mass assignable.
     *
     * @var array
     */
    protected $table = 'addresses';
    protected $fillable = [
        'state',
        'city',
        'address',
        'zip_code',
    ];
    public static $rules = [
        'state' => 'required',
        'city' => 'required',
        'address' => 'required',
        'zip_code' => 'required',
    ];
}