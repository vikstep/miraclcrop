<?php

namespace App\Models;

use Illuminate\Support\Facades\DB;
use Illuminate\Database\Eloquent\Model;
use Illuminate\Notifications\Notifiable;

class Product extends Model {

    use Notifiable;

    /**
     * The attributes that are mass assignable.
     *
     * @var array
     */
    protected $table = 'products';
    protected $fillable = [
        'sku',
        'sort',
        'status'
    ];
    protected $filePath = '/files/products/';
    public static $rules = [
        'title' => 'required|min:3|max:100',
        'product_excerpt' => 'required',
        'product_content' => 'required',
        'category' => 'required',
        'strains' => 'required',
        'image' => 'dimensions:width=400,height=400|mimes:jpg,jpeg,png',
        'portions' => 'required',
        'brand' => 'required',
        'weight' => 'required'
    ];
    public static $status = [
        '0' => 'hidden',
        '1' => 'visible'
    ];

    public function category() {
        return $this->belongsTo('App\Models\Category', 'category_id');
    }

    public function brand() {
        return $this->belongsTo('App\Models\Brand', 'brand_id');
    }

    public function attributes() {
        return $this->hasMany('App\Models\ProductAttribute');
    }

    public function media() {
        return $this->hasMany('App\Models\ProductMedia');
    }

    public function getFilePath() {
        return $this->filePath;
    }

    public function getFileDir() {
        return public_path() . $this->filePath;
    }

    public function getMinPortion($product_id) {
        return DB::table('portions')
                ->leftJoin('product_attributes', 'product_attributes.attribute_id', '=', 'portions.id')
                ->where('product_attributes.attribute_type', '=', 2)
                ->where('product_attributes.product_id', '=', $product_id)
                ->orderBy('portions.weight', 'asc')
                ->first();
    }

}
