<?php

namespace App\Models;

use Illuminate\Support\Facades\DB;
use Illuminate\Database\Eloquent\Model;
use Illuminate\Notifications\Notifiable;

class UserCart extends Model {

    use Notifiable;

    /**
     * The attributes that are mass assignable.
     *
     * @var array
     */
    protected $table = 'user_cart';
    protected $fillable = [
        'user_id',
        'product_id',
        'portion_id',
        'quantity'
    ];

    public static function getItemsCount() {
        if (auth()->user()) {
            return DB::table('user_cart')->where('user_id', auth()->user()->id)->count();
        } else {
            if (\Cookie::get('cookie_id')) {
                return DB::table('user_cart')->where('cookie_id', \Cookie::get('cookie_id'))->count();
            }
        }
    }

    public static function updateCart($user_id = null) {
        if (auth()->user()) {
            return DB::table('user_cart')->where('cookie_id', \Cookie::get('cookie_id'))->update(['user_id' => auth()->user()->id, 'cookie_id' => null]);
        }
//        else {
//            if (\Cookie::get('cookie_name')) {
//                 return DB::table('user_cart')->where('user_id', $user_id)->update(['user_id' => null, 'cookie_name' => \Cookie::get('cookie_name')]);
//            }
//        }
    }

}
