<?php

namespace App\Models;

use Illuminate\Support\Facades\DB;
use Illuminate\Database\Eloquent\Model;
use Illuminate\Notifications\Notifiable;


class ProductFavorite extends Model
{
    use Notifiable;

    /**
     * The attributes that are mass assignable.
     *
     * @var array
     */
    protected $table = 'user_favorite_products';
    protected $fillable = [
        'user_id',
        'product_id'
    ];

    public static $rules = [
        'user_id' => 'required',
        'product_id' => 'required'
    ];

    public static function getProducts($user_id) {
        return DB::table('user_favorite_products')
            ->leftJoin('products', 'products.id', '=', 'user_favorite_products.product_id')
            ->leftJoin('product_attributes', 'product_attributes.product_id', '=', 'products.id')
            ->where('user_favorite_products.user_id', '=', $user_id)
            ->orderBy('user_favorite_products.created_at', 'desc')
            ->get();
    }
}