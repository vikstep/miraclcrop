<?php

use App\Models\Translations;
use Sunra\PhpSimple\HtmlDomParser;

if (!function_exists('translate')) {

    function translate($url) {
        $url_array = explode('/', $url);

        if (count($url_array) == 5) {
            $translation_text = app(Translations::class)->getSingleTranslationsByGroupAndItem($url_array[1], (($url_array[2] == 'blog')?'post': $url_array[2]), $url_array[3] . '.title');
          
            if ($translation_text) {
                return '/' . $url_array[1] . '/' . $url_array[2] . '/' . $url_array[3] . '/' . slugify($translation_text->text);
            } else {
                return $url;
            }
        } else {
            return $url;
        }
    }

}

if (!function_exists('has_trans')) {

    function has_trans($locale, $group, $item) {
        
            $translation_text = app(Translations::class)->getSingleTranslationsByGroupAndItem($locale, $group, $item);
            if ($translation_text) {
                return true;
            } else {
                return false;
            }
    }

}

if (!function_exists('slugify')) {

    function slugify($text) {
        $text = preg_replace('~[^\\pL\d]+~u', '-', $text);
        $text = trim($text, '-');
        $text = iconv('utf-8', 'us-ascii//TRANSLIT', $text);
        $text = strtolower($text);
        $text = preg_replace('~[^-\w]+~', '', $text);
        if (empty($text)) {
            return 'n-a';
        }

        return $text;
    }

}

if (!function_exists('filterContent')) {

    function filterContent($content) {
        $dom = HtmlDomParser::str_get_html($content);

        if ($dom->find('.youtube-video')) {
            foreach ($dom->find('.youtube-video') as $key => $element) {
                $video = $dom->find('.youtube-video', $key);
                if ($video->rel) {
                    $video->outertext = '<figure class="iframe-cont"><iframe width="800" height="450" src="http://www.youtube.com/embed/' . $video->rel . '?wmode=opaque&autoplay=0&showinfo=0&autohide=1&rel=0" frameborder="0" allowfullscreen></iframe></figure>';
                }
            }
            $content = $dom->save();
        }
        return $content;
    }

}

if (!function_exists('resizeImage')) {

    function resizeImage($value, $path, $filename, $widthSize, $type) {
        $extension = $value->guessExtension();

        switch (strtolower($value->guessExtension())) {
            case 'jpeg':
                $image = imagecreatefromjpeg($value->getPathName());
                break;
            case 'png':
                $image = imagecreatefrompng($value->getPathName());
                break;
            case 'gif':
                $image = imagecreatefromgif($value->getPathName());
                break;
            default:
                exit('Unsupported type: ' . $$extension);
        }

        $fn = $value->getPathName();
        $size = getimagesize($fn);

        $ratio = $size[0] / $size[1]; // width/height
        if ($ratio > 1) {
            $new_width = $widthSize;
            $new_height = $widthSize / $ratio;
        } else {
            $new_width = $widthSize * $ratio;
            $new_height = $widthSize;
        }


        $old_width = imagesx($image);
        $old_height = imagesy($image);

        $new = imagecreatetruecolor($new_width, $new_height);
        imagealphablending($new, false);
        imagesavealpha($new, true);
        imagecopyresampled($new, $image, 0, 0, 0, 0, $new_width, $new_height, $old_width, $old_height);

        ob_start();
        imagepng($new, $path . '/' . $filename . '-' . $type . '.' . $extension, 9);
        $data = ob_get_clean();

        imagedestroy($image);
        imagedestroy($new);
        return $data;
    }

}

if (!function_exists('generateSku')) {

    function generateSku($text) {

        $words = explode(" ", $text);
        $sku = "";

        for ($i = 0; $i < 2; $i++) {
            $sku .= isset($words[$i][0]) ? ucfirst($words[$i][0]) : chr(rand(65, 90));
        }

        return $sku . '-' . time();
    }

}