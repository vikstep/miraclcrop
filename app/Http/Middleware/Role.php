<?php

namespace App\Http\Middleware;

use Closure;
use Illuminate\Contracts\Auth\Guard;

class Role {

    protected $auth;

    /**
     * Create a new filter instance.
     *
     * @param  Guard  $auth
     * @return void
     */
    public function __construct(Guard $auth) {
        $this->auth = $auth;
    }

    /**
     * Run the request filter.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  \Closure  $next
     * @param  string  $role
     * @return mixed
     */
    public function handle($request, Closure $next) {

        if ($this->auth->guest()) {
            if ($request->ajax()) {
                return response('Unauthorized.', 401);
            } else {
                return redirect()->guest('/');
            }
        }
        $role = $this->auth->user()->role;

        $doctor_pages = [
            'profile',
        ];
        $pieces = explode('.', $request->getHost());

        if (in_array($request->segment(2), $doctor_pages)) {
            if ($role == 'doctor' && $pieces[0] != 'doctor') {
                return redirect()->route('doctor.home');
            }
        }


        if (!$request->user()->hasRole($role)) {
            return redirect()->route($role . 'account')->with('status', 'You are not authorized.');
        }

        return $next($request);
    }

}
