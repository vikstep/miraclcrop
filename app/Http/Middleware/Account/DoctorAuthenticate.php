<?php

//not used

namespace App\Http\Middleware\Account;

use Closure;
use Illuminate\Contracts\Auth\Guard;

class DoctorAuthenticate {

    /**
     * The Guard implementation.
     *
     * @var Guard
     */
    protected $auth;

    /**
     * Create a new filter instance.
     *
     * @param  Guard  $auth
     * @return void
     */
    public function __construct(Guard $auth) {
        $this->auth = $auth;
    }

    /**
     * Handle an incoming request.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  \Closure  $next
     * @return mixed
     */
    public function handle($request, Closure $next) {
        if ($this->auth->guest()) {
            if ($request->ajax()) {
                return response('Unauthorized.', 401);
            } else {
                return redirect()->guest('/');
            }
        }

        $role = $this->auth->user()->role;
        $pieces = explode('.', $request->getHost());

        if ($role == 'doctor' && $pieces[0] != 'doctor') {
            return redirect()->route('doctor.home');
        }

        if ($role != config('roles.doctor')) {
            return redirect()->route($role . '.account')->with('status', 'You are not authorized.');
        }

        return $next($request);
    }

}
