<?php

namespace App\Http\Controllers\Auth;

use App\Models\User;
use Validator;
use Config;
use App\Http\Controllers\Controller;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Auth;
use Illuminate\Support\Facades\Lang;
use App\Models\UserCart;
use Illuminate\Foundation\Auth\AuthenticatesUsers;

class LoginController extends Controller {
    /*
      |--------------------------------------------------------------------------
      | Login Controller
      |--------------------------------------------------------------------------
      |
      | This controller handles authenticating users for the application and
      | redirecting them to your home screen. The controller uses a trait
      | to conveniently provide its functionality to your applications.
      |
     */

use AuthenticatesUsers;

    /**
     * Where to redirect users after login.
     *
     * @var string
     */

    protected $loginPath = '/';
    protected $redirectTo = '/account';

    /**
     * Create a new controller instance.
     *
     * @return void
     */
    public function __construct() {
        $this->middleware('guest', ['except' => 'logout']);
    }

    protected function authenticated(Request $request, User $user) {
        $roles = config('roles');

        if ($user->role == $roles['patient'] || $user->role == $roles['doctor']) {
            if ($user->status != 'approved') {
                Auth::logout();
                
                $message = '';
                
                switch ($user->status) {
                    case 'confirmed': 
                        $message = 'Your account is in stage of review, We will inform you soon via email';
                        break;
                    case 'pending': 
                        $message = 'Please confirm your email first';
                        break;
                    case 'blocked':
                        $message = 'Your account is blocked at the moment, contact us via email';
                        break;
                    case 'inreview':
                        $message = 'Your account is on review at the moment, we will let you know once approved. Thanks for your patience';
                        break;
                    case 'rejected':
                        $message = 'Your account is rejectd at the moment, contact us via email';
                        break;
                }
                
                return redirect()->action('Auth\LoginController@showLoginForm')
                                ->withErrors([$message]);
            }
            UserCart::updateCart();
            if($user->role == $roles['patient']){
                 return redirect()->route('patient.home');
            }else{
                return redirect()->route('doctor.home');
            }
        }

        Auth::logout();

        return redirect()->action('Auth\LoginController@showLoginForm')
                        ->withErrors(['Incorrect login data']);
    }

    public function showLoginForm() {
        return view('front.auth.login');
    }

    public function postLogin(Request $request) {

        $this->validate($request, [
            $this->username() => 'required', 'password' => 'required',
        ]);

        // If the class is using the ThrottlesLogins trait, we can automatically throttle
        // the login attempts for this application. We'll key this by the username and
        // the IP address of the client making these requests into this application.
        if ($this->hasTooManyLoginAttempts($request)) {
            $this->fireLockoutEvent($request);

            return $this->sendLockoutResponse($request);
        }

        if ($this->attemptLogin($request)) {
            return $this->sendLoginResponse($request);
        }

        // If the login attempt was unsuccessful we will increment the number of attempts
        // to login and redirect the user back to the login form. Of course, when this
        // user surpasses their maximum number of attempts they will get locked out.
        $this->incrementLoginAttempts($request);

        return $this->sendFailedLoginResponse($request);
    }

    protected function attemptLogin(Request $request) {
        return $this->guard()->attempt(
                        $this->credentials($request), $request->has('remember')
        );
    }

    public function username() {
        return property_exists($this, 'username') ? $this->username : 'email';
    }

    protected function getFailedLoginMessage() {
        return Lang::has('auth.failed') ? Lang::get('auth.failed') : 'These credentials do not match our records.';
    }

    public function redirectPath() {
        if (property_exists($this, 'loginPath')) {
            return $this->loginPath;
        }
    }

    public function logout(Request $request) {

        Auth::logout();
            $request->session()->flush();

        $request->session()->regenerate();

        return redirect($this->redirectPath().$request->segment(1));
    }

}
