<?php

namespace App\Http\Controllers\Auth;

use App;
use App\Models\User;
use App\Models\Address;
use App\Models\DoctorLicense;
use App\Http\Controllers\Controller;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Mail;
use Illuminate\Support\Facades\URL;
use Illuminate\Support\Facades\Validator;
use Illuminate\Support\Facades\Auth;
use Illuminate\Foundation\Auth\RegistersUsers;
use Illuminate\Auth\Events\Registered;

class RegisterController extends Controller {
    /*
      |--------------------------------------------------------------------------
      | Register Controller
      |--------------------------------------------------------------------------
      |
      | This controller handles the registration of new users as well as their
      | validation and creation. By default this controller uses a trait to
      | provide this functionality without requiring any additional code.
      |
     */

use RegistersUsers;

    /**
     * Where to redirect users after registration.
     *
     * @var string
     */
    protected $redirectTo = '/home';

    /**
     * Create a new controller instance.
     *
     * @return void
     */
    public function __construct() {
        $this->middleware('guest');
    }

    public function showRegistrationForm() {

        return view('front.auth.register', ['states' => User::$states]);
    }

    /**
     * Get a validator for an incoming registration request.
     *
     * @param  array  $data
     * @return \Illuminate\Contracts\Validation\Validator
     */
    protected function validator(array $data) {
        $rules = User::$rules;
        $messages = [
            'username.regex' => 'The username has lowercase characters only.',
        ];
        return Validator::make($data, $rules, $messages);
    }

    /**
     * Create a new user instance after a valid registration.
     *
     * @param  array  $data
     * @return User
     */
    public function showRegistrationInfo() {
        return view('front.info');
    }

    public function confirmEmail($confirmation_token) {
        $user = User::where(['confirm_token' => $confirmation_token])->first();

        if (is_null($user)) {
            return redirect()->route('confirmation-failed')->with('info', 'Invalid confirmation token');
        }

        $user->confirm_token = null;
        $user->status = 'confirmed';

        if ($user->save()) {
            return redirect()->route('confirmation-success')->with('info', 'Your email address successfully confirmed. You will receive email within 2 business days after we review your account details.');
        }

        return redirect()->route('registration-success')->with('info', 'Invalid action Please try again');
    }

    /*
     * patient registration
     */

    public function getPatientStep1() {
        session()->forget('user_id');
        session()->forget('patientStep1');
        session()->forget('patientStep2');
        session()->forget('patientStep3');
        session()->forget('patientStep4');
        session()->forget('patientStep5');

        return view('front.auth.patient_register_step1');
    }

    public function postPatientStep1(Request $request) {
        $confirm_token = str_random(30); //todo - make unique
        $get_user = User::where('email', $request->email)->first();

        $rules = [
            'username' => 'required',
            'firstname' => 'required|max:50',
            'lastname' => 'required|max:50',
            'email' => ($get_user && $get_user->status == 'pending') ? 'required|email|max:255' : 'required|email|max:255|unique:users,email',
            'password' => 'required|confirmed|min:3',
        ];

        $validator = Validator::make($request->all(), $rules);

        if ($validator->fails()) {
            $this->throwValidationException(
                    $request, $validator
            );
        }
        if ($get_user && $get_user->status == 'incomplete') {
            $destinationPath = public_path() . '/files/profiles/patient-' . $get_user->id;
            if (is_dir($destinationPath)) {
                array_map('unlink', glob("$destinationPath/*.*"));
                rmdir($destinationPath);
            }

            $get_user->username = $request->username;
            $get_user->first_name = $request->firstname;
            $get_user->last_name = $request->lastname;
            $get_user->email = $request->email;
            $get_user->password = bcrypt($request->password);
            $get_user->confirm_token = $confirm_token;
            $get_user->role = 'patient';
            $get_user->ip = $request->ip();
            $get_user->user_agent = $request->header('User-Agent');
            $get_user->face_photo = '';
            $get_user->registration_step = 1;
            $get_user->id_card = '';
            $get_user->id_card_image = '';
            $get_user->recommendation_image = '';
            $get_user->driver_license = '';
            $get_user->driver_license_image = '';
            $get_user->signature_image = '';

            $get_user->save();
            session()->put('user_id', $get_user->id);
        } else {
            $user = User::create([
                        'username' => $request->username,
                        'first_name' => $request->firstname,
                        'last_name' => $request->lastname,
                        'email' => $request->email,
                        'role' => 'patient',
                        'confirm_token' => $confirm_token,
                        'password' => bcrypt($request->password),
                        'ip' => $request->ip(),
                        'user_agent' => $request->header('User-Agent')
            ]);
            $user->registration_step = 1;
            $user->save();
            session()->put('user_id', $user->id);
        }

        session()->put('patientStep1', $request->all());


        return redirect()->action('Auth\RegisterController@postPatientStep2');
    }

    public function getPatientStep2() {
        if (session()->has('user_id')) {
            return view('front.auth.patient_register_step2', ['states' => User::$states]);
        } else {
            return redirect('/auth/signup/step-1');
        }
    }

    public function postPatientStep2(Request $request) {
        if ($request->user_id) {
            $user_data = User::find($request->user_id);

            $user_data->state = $request->state;
            $user_data->city = $request->city;
            $user_data->address = $request->address;
            $user_data->zip_code = $request->zip_code;
            $user_data->phone = $request->phone;
            $user_data->registration_step = 2;

            $user_data->save();

            //Add Address in address table

            $address = Address::where('user_id', $request->user_id)->first();
            if (!$address) {
                $address = new Address();
            }

            $address->user_id = $request->user_id;
            $address->state = $request->state;
            $address->city = $request->city;
            $address->address = $request->address;
            $address->zip_code = $request->zip_code;
            $address->save();
        } else {
            return redirect('/auth/signup/step-1');
        }
        session()->put('patientStep2', $request->all());


        return redirect()->action('Auth\RegisterController@getPatientStep3');
    }

    public function getPatientStep3() {
        if (session()->has('user_id')) {
            $user = User::find(session()->get('user_id'));
            return view('front.auth.patient_register_step3', ['user' => $user]);
        } else {
            return redirect('/auth/signup/step-1');
        }
    }

    public function postPatientStep3(Request $request) {
        if ($request->user_id) {
            $user_data = User::find($request->user_id);
            $destinationPath = public_path() . '/files/profiles';

            $driver_license_file_name = 'driverlicense' . $request->user_id . '-' . time();
            $id_card_file_name = 'idcard-' . $request->user_id . '-' . time();
            $recommendation_file_name = 'recommendation-' . $request->user_id . '-' . time();

            $destinationPath = $destinationPath . '/patient-' . $request->user_id;
            if (!is_dir($destinationPath)) {
                @mkdir($destinationPath);
            };

            if ($request->hasFile('driver_license_file') && $request->file('driver_license_file')->isValid()) {
                $extension = $request->file('driver_license_file')->guessExtension();
                $driver_license_file = $driver_license_file_name . '.' . $extension;

                $request->file('driver_license_file')->move($destinationPath, $driver_license_file);
                $user_data->driver_license_image = $driver_license_file;
            }

            if ($request->hasFile('id_file') && $request->file('id_file')->isValid()) {
                $extension = $request->file('id_file')->guessExtension();
                $id_card_image = $id_card_file_name . '.' . $extension;

                $request->file('id_file')->move($destinationPath, $id_card_image);
                $user_data->id_card_image = $id_card_image;
            }

            if ($request->hasFile('patient_recommendation') && $request->file('patient_recommendation')->isValid()) {
                $extension = $request->file('patient_recommendation')->guessExtension();
                $recommendation_image = $recommendation_file_name . '.' . $extension;

                $request->file('patient_recommendation')->move($destinationPath, $recommendation_image);
                $user_data->recommendation_image = $recommendation_image;
            }

            $user_data->driver_license = $request->driver_license;
            $user_data->id_card = $request->id_card;
            $user_data->registration_step = 3;

            $user_data->save();
        } else {
            return redirect('/auth/signup/step-1');
        }
        session()->put('patientStep3', ['driver_license' => $request->driver_license, 'id_card' => $request->id_card]);


        return redirect()->action('Auth\RegisterController@getPatientStep4');
    }

    public function getPatientStep4() {
        if (session()->has('user_id')) {
            $user = User::find(session()->get('user_id'));
            return view('front.auth.patient_register_step4', ['user' => $user, 'days' => User::getDays(), 'months' => User::$months, 'years' => User::getYears()]);
        } else {
            return redirect('/auth/signup/step-1');
        }
    }

    public function postPatientStep4(Request $request) {
        if ($request->user_id) {
            $user_data = User::find($request->user_id);
            $destinationPath = public_path() . '/files/profiles';

            $profile_picture_file_name = 'profile-' . $request->user_id . '-' . time();

            $destinationPath = $destinationPath . '/patient-' . $request->user_id;
            if (!is_dir($destinationPath)) {
                @mkdir($destinationPath);
            };

            if ($request->hasFile('profile_picture') && $request->file('profile_picture')->isValid()) {
                $extension = $request->file('profile_picture')->guessExtension();
                $profile_image = $profile_picture_file_name . '.' . $extension;

                $request->file('profile_picture')->move($destinationPath, $profile_image);
                $user_data->face_photo = $profile_image;
            }

            $user_data->dob = $request->dob_year . '-' . $request->dob_month . '-' . $request->dob_day;
            $user_data->gender = $request->gender;
            $user_data->registration_step = 4;

            $user_data->save();
        } else {
            return redirect('/auth/signup/step-1');
        }
        session()->put('patientStep4', ['gender' => $request->gender, 'day' => $request->dob_day, 'month' => $request->dob_month, 'year' => $request->dob_year]);


        return redirect()->action('Auth\RegisterController@getPatientStep5');
    }

    public function getPatientStep5() {
        if (session()->has('user_id')) {
            $user = User::find(session()->get('user_id'));
            return view('front.auth.patient_register_step5', ['user' => $user, 'states' => User::$states]);
        } else {
            return redirect('/auth/signup/step-1');
        }
    }

    public function postPatientStep5(Request $request) {
        if ($request->signature_image == '') {
            return redirect()->back()->withErrors(['The signature is required']);
        }

        $user = User::find(session()->get('user_id'));
        $user->registration_step = 5;
        $user->status = 'pending';


        $href = URL::to('/confirm-email/' . $user->confirm_token);

        try {
            Mail::send('emails.verify', ['href' => $href], function ($message) use ($user) {
                $message->to($user->email, $user->first_name . ' ' . $user->last_name)->subject('Verify your email address');
            });
        } catch (\Exception $e) {
            return redirect($this->loginPath)
                            ->withInput($request->all())
                            ->withErrors(['Something went wrong']);
        }
        $user->save();

        session()->forget('user_id');
        session()->forget('patientStep1');
        session()->forget('patientStep2');
        session()->forget('patientStep3');
        session()->forget('patientStep4');
        session()->forget('patientStep5');

        return redirect()->route('registration-success')->with('info', 'Thanks for signing up! Please check your email.');
    }

    /*
     * doctor registration
     */

    public function getDoctorStep1() {
        session()->forget('user_id');
        session()->forget('doctorStep1');
        session()->forget('doctorStep2');
        session()->forget('doctorStep3');
        session()->forget('doctorStep4');

        return view('front.auth.doctor_register_step1', ['days' => User::getDays(), 'months' => User::$months, 'years' => User::getYears()]);
    }

    public function postDoctorStep1(Request $request) {
        $confirm_token = str_random(30); //todo - make unique
        $get_user = User::where('email', $request->email)->first();

        $rules = [
            'username' => 'required',
            'firstname' => 'required|max:50',
            'lastname' => 'required|max:50',
            'email' => ($get_user && $get_user->status == 'pending') ? 'required|email|max:255' : 'required|email|max:255|unique:users,email',
            'password' => 'required|confirmed|min:3',
        ];
        $validator = Validator::make($request->all(), $rules);

        if ($validator->fails()) {
            $this->throwValidationException(
                    $request, $validator
            );
        }
        if ($get_user && $get_user->status == 'incomplete') {
            // $user_data = User::find($request->user_id);
            $doctorLicenses = DoctorLicense::where('user_id', $get_user->id)->get();
            if ($doctorLicenses) {
                foreach ($doctorLicenses as $doctorLicense) {
                    $doctorLicense->delete();
                }
            }

            $destinationPath = public_path() . '/files/profiles/doctor-' . $get_user->id;
            if (is_dir($destinationPath)) {
                array_map('unlink', glob("$destinationPath/*.*"));
                rmdir($destinationPath);
            }

            $get_user->username = $request->username;
            $get_user->first_name = $request->firstname;
            $get_user->last_name = $request->lastname;
            $get_user->email = $request->email;
            $get_user->password = bcrypt($request->password);
            $get_user->confirm_token = $confirm_token;
            $get_user->role = 'doctor';
            $get_user->dob = $request->dob_year . '-' . $request->dob_month . '-' . $request->dob_day;
            $get_user->ip = $request->ip();
            $get_user->user_agent = $request->header('User-Agent');
            $get_user->face_photo = '';
            $get_user->registration_step = 1;
            $get_user->driver_license = '';
            $get_user->driver_license_image = '';
            $get_user->signature_image = '';

            $get_user->save();
            session()->put('user_id', $get_user->id);
        } else {
            $user = User::create([
                        'username' => $request->username,
                        'first_name' => $request->firstname,
                        'last_name' => $request->lastname,
                        'email' => $request->email,
                        'role' => 'doctor',
                        'confirm_token' => $confirm_token,
                        'dob' => $request->dob_year . '-' . $request->dob_month . '-' . $request->dob_day,
                        'password' => bcrypt($request->password),
                        'ip' => $request->ip(),
                        'user_agent' => $request->header('User-Agent')
            ]);

            $user->registration_step = 1;
            $user->save();
            session()->put('user_id', $user->id);
        }
        session()->put('doctorStep1', $request->all());


        return redirect()->action('Auth\RegisterController@getDoctorStep2');
    }

    public function getdoctorStep2() {
        if (session()->has('user_id')) {
            return view('front.auth.doctor_register_step2', ['states' => User::$states]);
        } else {
            return redirect('/auth/signup/step-1');
        }
    }

    public function postDoctorStep2(Request $request) {

        if ($request->user_id) {
            $user_data = User::find($request->user_id);

            $user_data->state = $request->state;
            $user_data->city = $request->city;
            $user_data->address = $request->address;
            $user_data->zip_code = $request->zip_code;
            $user_data->phone = $request->phone;
            $user_data->registration_step = 2;

            $user_data->save();


            //Add Address in address table
            $address = Address::where('user_id', $request->user_id)->first();
            if (!$address) {
                $address = new Address();
            }

            $address->user_id = $request->user_id;
            $address->state = $request->state;
            $address->city = $request->city;
            $address->address = $request->address;
            $address->zip_code = $request->zip_code;
            $address->save();
        } else {
            return redirect('/auth/signup/step-1');
        }
        session()->put('doctorStep2', $request->all());


        return redirect()->action('Auth\RegisterController@getDoctorStep3');
    }

    public function getDoctorStep3() {
        if (session()->has('user_id')) {
            $user = User::find(session()->get('user_id'));
            return view('front.auth.doctor_register_step3', ['states' => User::$states, 'user' => $user]);
        } else {
            return redirect('/auth/signup/step-1');
        }
    }

    public function postDoctorStep3(Request $request) {

        if ($request->user_id) {
            $user_data = User::find($request->user_id);

            if (count($user_data->doctorLicense) == 0) {
                return redirect()->back()->withErrors(['You must add more then 1 medical license']);
            }
            $destinationPath = public_path() . '/files/profiles';

            $driver_license_file_name = 'driverlicense' . $request->user_id . '-' . time();
            $face_photo_file_name = 'profile-' . $request->user_id . '-' . time();

            $destinationPath = $destinationPath . '/doctor-' . $request->user_id;
            if (!is_dir($destinationPath)) {
                @mkdir($destinationPath);
            };

            if ($request->hasFile('driver_license_file') && $request->file('driver_license_file')->isValid()) {
                $extension = $request->file('driver_license_file')->guessExtension();
                $driver_license_file = $driver_license_file_name . '.' . $extension;

                $request->file('driver_license_file')->move($destinationPath, $driver_license_file);
                $user_data->driver_license_image = isset($driver_license_file) ? $driver_license_file : NULL;
            }

            if ($request->hasFile('face_photo') && $request->file('face_photo')->isValid()) {
                $extension = $request->file('face_photo')->guessExtension();
                $face_photo = $face_photo_file_name . '.' . $extension;

                $request->file('face_photo')->move($destinationPath, $face_photo);
                $user_data->face_photo = isset($face_photo) ? $face_photo : NULL;
            }



            $user_data->driver_license = $request->driver_license;
            $user_data->gender = $request->gender;
            $user_data->registration_step = 3;


            $user_data->save();
        } else {
            return redirect('/auth/signup/step-1');
        }
        session()->put('doctorStep3', ['driver_license' => $request->driver_license, 'gender' => $request->gender]);


        return redirect()->action('Auth\RegisterController@getDoctorStep4');
    }

    public function postDoctorLicense(Request $request) {
        if (session()->has('user_id')) {
            $user_id = session()->get('user_id');

            $rules = ['license_image' => 'dimensions:min_width=50,max_width=1000|mimes:jpg,jpeg,png|max:4000'];

            $validator = Validator::make($request->all(), $rules);

            if ($validator->fails()) {
                return redirect()->action('Auth\RegisterController@getDoctorStep3')
                                ->withErrors($validator)
                                ->withInput();
            }

            $doctorLicense = DoctorLicense::where('user_id', $user_id)->first();
            if ($doctorLicense) {
                $destinationPath = public_path() . '/files/profiles/doctor-' . $user_id;
                if (file_exists($destinationPath . '/' . $doctorLicense->license_image)) {
                    unlink($destinationPath . '/' . $doctorLicense->license_image);
                }
            } else {
                $doctorLicense = new DoctorLicense();
            }


            $doctorLicense->user_id = (int) $user_id;
            $doctorLicense->registration_state = $request->registration_state;
            $doctorLicense->medical_license = $request->medical_license;
            $doctorLicense->valid_until = date('Y-m-d', strtotime($request->valid_until));

            if ($request->hasFile('license_image')) {
                $destinationPath = public_path() . '/files/profiles/doctor-' . $user_id;
                if (!is_dir($destinationPath)) {
                    @mkdir($destinationPath);
                };

                $fileName = 'license-' . $user_id . '-' . time();

                if ($request->file('license_image')->isValid()) {
                    $extension = $request->file('license_image')->guessExtension();
                    $file = $request->file('license_image')->move($destinationPath, $fileName . '.' . $extension);
                    $doctorLicense->license_image = $fileName . '.' . $extension;
                }
            }
            if ($doctorLicense->save()) {
                return redirect()->back();
            }
            //return redirect()->action('Auth\RegisterController@getDoctorStep3');
        }
    }

    public function postSignature(Request $request) {

        if (session()->has('user_id')) {
            $user_id = session()->get('user_id');
            $user_data = User::find($user_id);
            $destinationPath = public_path() . '/files/profiles';
            $sig_file_name = 'signature-' . $user_id . '-' . time() . '.png';

            if ($user_data->role == 'patient') {
                $destinationPath = $destinationPath . '/patient-' . $user_id;
                session()->put('patientStep5', ['signature_image' => $sig_file_name]);
            } else if ($user_data->role == 'doctor') {
                $destinationPath = $destinationPath . '/doctor-' . $user_id;
                session()->put('doctorStep4', ['signature_image' => $sig_file_name]);
            }

            if (!is_dir($destinationPath)) {
                @mkdir($destinationPath);
            };

            $decode_img = base64_decode($request->sig_image);
            file_put_contents($destinationPath . '/' . $sig_file_name, $decode_img);


            $user_data->signature_image = $sig_file_name;

            $user_data->save();

            return response()->json([
                        'status' => 'success'
            ]);
        }
    }

    public function getDoctorStep4() {
        if (session()->has('user_id')) {
            $user = User::find(session()->get('user_id'));
            return view('front.auth.doctor_register_step4', ['user' => $user, 'states' => User::$states]);
        } else {
            return redirect('/auth/signup/step-1');
        }
    }

    public function postDoctorStep4(Request $request) {
        if ($request->signature_image == '') {
            return redirect()->back()->withErrors(['The signature is required']);
        }

        $user = User::find(session()->get('user_id'));
        $user->registration_step = 4; 
        $user->status = 'pending';
        $href = URL::to('/confirm-email/' . $user->confirm_token);

        try {
            Mail::send('emails.verify', ['href' => $href], function ($message) use ($user) {
                $message->to($user->email, $user->first_name . ' ' . $user->last_name)->subject('Verify your email address');
            });
        } catch (\Exception $e) {
            return redirect($this->loginPath)
                            ->withInput($request->all())
                            ->withErrors(['Something went wrong']);
        }
        $user->save();

        session()->forget('user_id');
        session()->forget('doctorStep1');
        session()->forget('doctorStep2');
        session()->forget('doctorStep3');
        session()->forget('doctorStep4');

        return redirect()->route('registration-success')->with('info', 'Thanks for signing up! Please check your email.');
    }

}
