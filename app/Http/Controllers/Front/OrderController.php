<?php

namespace App\Http\Controllers\Front;

use App\Http\Controllers\Controller;
use App\Models\Product;
use App\Models\Portion;
use App\Models\User;
use App\Models\UserCart;
use App\Models\Address;
use App\Models\ProductAttribute;
use App\Models\Order;
use App\Models\OrderProduct;
use App\Models\Translations;
use Illuminate\Http\Request;
use Illuminate\View\View;

class OrderController extends Controller {

    public function __construct() {
        //$this->middleware('role');
    }

    public function index() {

        if (auth()->user()) {
            $cart = UserCart::where('user_id', auth()->user()->id)->get();
        } else {
            $cart = UserCart::where('cookie_id', \Cookie::get('cookie_id'))->get();
        }

        $portions = [];
        foreach (Portion::all()->toArray() as $portion) {
            $portions[$portion['id']] = $portion['weight'];
        }

        $addresses = (auth()->user()) ? Address::where('user_id', auth()->user()->id)->get() : [];
        session()->put('from_checkout', 'cart');

        return view('front.cart', [
            'cart' => $cart,
            'product' => new Product,
            'portions' => $portions,
            'addresses' => $addresses,
            'states' => User::$states
        ]);
    }

    public function addToCart(Request $request) {

        $portions = [];
        foreach (Portion::all()->toArray() as $portion) {
            $portions[$portion['id']] = $portion['weight'];
        }

        $product = Product::find($request->product_id);

        if ($portions[$request->portion_id] * $request->quantity <= (int) $product->weight_in_stock) {
            $userCart = new UserCart();
            if (auth()->user()) {
                $userCart->user_id = auth()->user()->id;
                UserCart::where('user_id', auth()->user()->id)->where('product_id', $request->product_id)->delete();
            } else {
                if (\Cookie::get('cookie_id')) {
                    UserCart::where('cookie_id', \Cookie::get('cookie_id'))->where('product_id', $request->product_id)->delete();
                    $userCart->cookie_id = \Cookie::get('cookie_id');
                } else {
                    $cooke_id = uniqid();
                    $cookie = \Cookie::make('cookie_id', $cooke_id, 2678400);
                    $userCart->cookie_id = $cooke_id;
                }
            }


            $userCart->product_id = $request->product_id;
            $userCart->portion_id = $request->portion_id;
            $userCart->quantity = $request->quantity;
            $userCart->save();
            if (\Cookie::get('cookie_id') || auth()->user()) {
                return response()->json([
                            'status' => 'success',
                            'count' => UserCart::getItemsCount(),
                ]);
            } else {
                return response()->json([
                            'status' => 'success',
                            'count' => 1,
                        ])->withCookie($cookie);
            }
        } else {
            return response()->json([
                        'status' => 'error',
                        'message' => 'The product with request count is out of stock',
                    ]);
        }
    }

    public function addToCartMultiple(Request $request) {
        $error = false;
        foreach ($request->get('cart_data') as $cart_data) {
            $portions = [];
            foreach (Portion::all()->toArray() as $portion) {
                $portions[$portion['id']] = $portion['weight'];
            }

            $product = Product::find($cart_data['product_id']);

            if ($portions[$cart_data['portion_id']] * $cart_data['quantity'] <= (int) $product->weight_in_stock) {
                $userCart = new UserCart();
                if (auth()->user()) {
                    $userCart->user_id = auth()->user()->id;
                    $userCart = UserCart::where('user_id', auth()->user()->id)->where('product_id', $cart_data['product_id'])->delete();
                } else {
                    if (\Cookie::get('cookie_id')) {
                        UserCart::where('cookie_id', \Cookie::get('cookie_id'))->where('product_id', $cart_data['product_id'])->delete();
                        $userCart->cookie_id = \Cookie::get('cookie_id');
                    } else {
                        $cooke_id = uniqid();
                        \Cookie::make('cookie_id', $cooke_id, 2678400);
                        $userCart->cookie_id = $cooke_id;
                    }
                }

                $userCart->product_id = $cart_data['product_id'];
                $userCart->portion_id = $cart_data['portion_id'];
                $userCart->quantity = $cart_data['quantity'];
                $userCart->save();
            } else {
                $error = true;
                return response()->json([
                            'status' => 'error',
                            'message' => 'The product with request count is out of stock',
                ]);
            }
        }

        if (!$error) {
            return response()->json([
                        'status' => 'success',
                        'count' => UserCart::getItemsCount(),
            ]);
        }
    }

    public function removeFromCart($id) {
        if (auth()->user()) {
            if (auth()->user()->recommendation_image) {
                UserCart::where('user_id', auth()->user()->id)->where('product_id', $id)->delete();
            }
        } else {
            UserCart::where('cookie_id', \Cookie::get('cookie_id'))->where('product_id', $id)->delete();
        }
        return redirect()->back();
    }

    public function checkout(Request $request) {
        if (auth()->user()) {
            if (auth()->user()->recommendation_image) {
                $total = 0;
                $order_products = [];
                $order_number = time();
                $translations = new Translations();

                foreach ($request->get('cart') as $product_id => $item) {
                    $portions = [];
                    foreach (Portion::all()->toArray() as $portion) {
                        $portions[$portion['id']] = $portion['weight'];
                    }

                    $product = Product::find($product_id);
                    if ($portions[$item['portion_id']] * $item['quantity'] <= $product->weight_in_stock) {
                        $total += ProductAttribute::getProductPrice($product_id, $item['portion_id']) * $item['quantity'];

                        $order_products[] = [
                            'product_id' => $product->id,
                            'product_sku' => $product->sku,
                            'product_title' => $translations->getSingleTranslationsByGroupAndItem('en', 'product', $product->id . '.title')->text,
                            'product_image' => $product->image . '.' . $product->extension,
                            'portion' => $translations->getSingleTranslationsByGroupAndItem('en', 'portion', $item['portion_id'] . '.title')->text,
                            'quantity' => $item['quantity'],
                            'price' => ProductAttribute::getProductPrice($product_id, $item['portion_id']),
                            'ordered_weight' => $portions[$item['portion_id']] * $item['quantity']
                        ];
                    } else {
                        return response()->json([
                                    'status' => 'error',
                                    'message' => 'The product with request count is out of stock',
                        ]);
                    }
                }


                $address = Address::where('user_id', auth()->user()->id)->where('id', $request->get('shipping_address_id'))->first();
                $shipping_address = [
                    'state' => $address->state,
                    'city' => $address->city,
                    'address' => $address->address,
                    'zip_code' => $address->zip_code
                ];

                if ($total <= auth()->user()->wallet_points) {
                    $order = new Order();
                    $order->user_id = auth()->user()->id;
                    $order->order_number = $order_number;
                    $order->shipping_address = serialize($shipping_address);
                    $order->status = 'pending';
                    $order->save();

                    foreach ($order_products as $product) {
                        $orderProduct = new OrderProduct();
                        $orderProduct->order_id = $order->id;
                        $orderProduct->product_id = $product['product_id'];
                        $orderProduct->product_sku = $product['product_sku'];
                        $orderProduct->product_title = $product['product_title'];
                        $orderProduct->product_image = $product['product_image'];
                        $orderProduct->portion = $product['portion'];
                        $orderProduct->quantity = $product['quantity'];
                        $orderProduct->price = $product['price'];
                        $orderProduct->save();

                        $current_product = Product::find($product['product_id']);
                        $current_product->weight_in_stock = $current_product->weight_in_stock - $product['ordered_weight'];

                        $current_product->save();


                        UserCart::where('user_id', auth()->user()->id)->where('product_id', $product['product_id'])->delete();
                    }

                    User::chargeWallet($total);

                    return response()->json([
                                'status' => 'success',
                                'message' => 'Your products successfully ordered',
                    ]);
                } else {
                    return response()->json([
                                'status' => 'error',
                                'message' => 'Your Balance is not enough',
                    ]);
                }
            } else {
                return response()->json([
                            'status' => 'error',
                            'message' => 'You do not have patient recommendation',
                ]);
            }
        }
    }

}
