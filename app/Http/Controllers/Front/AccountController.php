<?php

namespace App\Http\Controllers\Front;

use Illuminate\Http\Request;
use App\Http\Controllers\Controller;
use Validator;
use App\Models\User;
use App\Models\Address;
use App\Models\SocialProfile;
use App\Models\Transaction;
use App\Models\DoctorLicense;
use App\Models\Order;
use Illuminate\Support\Facades\Auth;

class AccountController extends Controller {

    public function __construct() {
        $this->middleware('role');
    }

    public function changeStatus(Request $request) {
        if ($request->id) {
            $user = User::find($request->id);
            $user->status = $request->status;

            if (isset($request->status_reason))
                $user->status_reason = $request->status_reason;

            $user->save();
            return response()->json([
                        'status' => $request->status
            ]);
        }
    }

    public function getProfile(User $user) {
        $doctorLicense = DoctorLicense::where('user_id', Auth::id())->get();
        session()->forget('from_checkout');
        
        return view('front.profile_page', ['doctorLicense' => $doctorLicense, 'states' => User::$states]);
    }

    public function getEditProfile(User $user) {
        $doctorLicense = DoctorLicense::where('user_id', Auth::id())->get();

        return view('front.profile_page_edit', ['doctorLicense' => $doctorLicense, 'states' => User::$states, 'days' => User::getDays(), 'months' => User::$months, 'years' => User::getYears()]);
    }

    public function getSocialProfiles(User $user) {
        $socialProfiles = SocialProfile::where('user_id', Auth::id())->get();

        return view('front.profile_social', ['socialProfiles' => $socialProfiles]);
    }

    public function getRemoveSocialProfile($id) {
        SocialProfile::where('user_id', auth()->user()->id)->where('id', $id)->delete();

        return redirect()->back()->with('success', 'Social profile removed successfully');
    }

    public function getAddresses(User $user) {

        $addresses = Address::where('user_id', Auth::id())->get();

        return view('front.profile_addresses', ['addresses' => $addresses]);
    }

    public function getAddressesEdit($id = null, User $user) {

        $addressTypes = array('billing' => true, 'shipping' => true);
        $addresses = Address::where('user_id', Auth::id())->get();

        if (count($addresses) > 0) {
            foreach ($addresses as $address) {
                if ($address->default_billing == 'yes')
                    $addressTypes['billing'] = ($id && $id == $address->id) ? true : false;
                if ($address->default_shipping == 'yes')
                    $addressTypes['shipping'] = ($id && $id == $address->id) ? true : false;
            }
        }

        $data = array('states' => User::$states, 'addressTypes' => $addressTypes);

        if ($id) {
            $address = Address::find($id);
            if (count($address) > 0)
                $data['address'] = $address;
        }

        return view('front.profile_address_create', $data);
    }

    public function postEditProfile(Request $request) {
        $user = User::find(auth()->user()->id);
        $destinationPath = public_path() . '/files/profiles';
        $user_blocked_check = false;

        $user->first_name = $request->firstname;
        $user->last_name = $request->lastname;
        $user->dob = $request->dob_year . '-' . $request->dob_month . '-' . $request->dob_day;
        $user->gender = $request->gender;

        if ($user->driver_license != $request->driver_license)
            $user_blocked_check = true;

        $user->driver_license = $request->driver_license;
//        $user->has_recipe = isset($request->has_recipe) ? 'yes' : 'no';

        $driver_license_file_name = 'driverlicense' . $user->id . '-' . time();
        $profile_picture_file_name = 'profile-' . $user->id . '-' . time();
        $destinationPath = $destinationPath . ( $user->role == 'patient' ? '/patient-' : '/doctor-' ) . $user->id;

        if (!is_dir($destinationPath)) {
            @mkdir($destinationPath);
        };

        if ($request->hasFile('driver_license_file') && $request->file('driver_license_file')->isValid()) {
            $extension = $request->file('driver_license_file')->guessExtension();
            $driver_license_file = $driver_license_file_name . '.' . $extension;

            $request->file('driver_license_file')->move($destinationPath, $driver_license_file);
            if (isset($user->driver_license_image) && file_exists($destinationPath . '/' . $user->driver_license_image)) {
                unlink($destinationPath . '/' . $user->driver_license_image);
                $user_blocked_check = true;
            }
            $user->driver_license_image = $driver_license_file;
        }

        if ($user->role == 'patient') {
            $id_card_file_name = 'idcard-' . $user->id . '-' . time();
            $recommendation_file_name = 'recommendation-' . $user->id . '-' . time();

            if ($request->hasFile('id_file') && $request->file('id_file')->isValid()) {
                $extension = $request->file('id_file')->guessExtension();
                $id_card_image = $id_card_file_name . '.' . $extension;

                $request->file('id_file')->move($destinationPath, $id_card_image);
                if (isset($user->id_card_image) && file_exists($destinationPath . '/' . $user->id_card_image)) {
                    unlink($destinationPath . '/' . $user->id_card_image);
                    $user_blocked_check = true;
                }
                $user->id_card_image = $id_card_image;
            }

            if ($request->hasFile('patient_recommendation') && $request->file('patient_recommendation')->isValid()) {
                $extension = $request->file('patient_recommendation')->guessExtension();
                $recommendation_image = $recommendation_file_name . '.' . $extension;

                $request->file('patient_recommendation')->move($destinationPath, $recommendation_image);
                if (isset($user->recommendation_image) && file_exists($destinationPath . '/' . $user->recommendation_image)) {
                    //dd($user->recommendation_image);
                    unlink($destinationPath . '/' . $user->recommendation_image);
                }
                $user->recommendation_image = $recommendation_image;
            }
            if ($user->id_card != $request->id_card)
                $user_blocked_check = true;

            $user->id_card = $request->id_card;
        }
        elseif ($user->role == 'doctor') {
            $user->phone = $request->phone;

            if (isset($request->license) && !empty($request->license)) {
                foreach ($request->license as $key => $license) {
                    $license_data = DoctorLicense::find($key);
                    if ($license_data) {
                        $license_data->registration_state = $license['registration_state'] ? $license['registration_state'] : '';
                        $license_data->medical_license = $license['medical_license'] ? $license['medical_license'] : '';
                        $license_data->valid_until = $license['valid_until'] ? $license['valid_until'] : '';

                        if ($request->hasFile('license_image_' . $license_data->id)) {

                            $fileName = 'license-' . $user->id . '-' . time() . $license_data->id;

                            if ($request->file('license_image_' . $license_data->id)->isValid()) {
                                $extension = $request->file('license_image_' . $license_data->id)->guessExtension();
                                $file = $request->file('license_image_' . $license_data->id)->move($destinationPath, $fileName . '.' . $extension);
                                if (isset($license_data->license_image) && file_exists($destinationPath . '/' . $license_data->license_image)) {
                                    unlink($destinationPath . '/' . $license_data->license_image);
                                }
                                $license_data->license_image = $fileName . '.' . $extension;
                            }
                        }

                        $license_data->save();
                    }
                }
            }
        }

        if ($request->hasFile('profile_picture') && $request->file('profile_picture')->isValid()) {
            $extension = $request->file('profile_picture')->guessExtension();
            $profile_image = $profile_picture_file_name . '.' . $extension;

            $request->file('profile_picture')->move($destinationPath, $profile_image);
            if (isset($user->face_photo) && file_exists($destinationPath . '/' . $user->face_photo))
                unlink($destinationPath . '/' . $user->face_photo);
            $user->face_photo = $profile_image;
        }

        if ($user_blocked_check) {
            $user->status = 'inreview';
        }

        $user->save();

        return redirect()->back()->with('success', 'Your data updated successfully');
    }

    public function postSocialProfiles(Request $request) {
        SocialProfile::where('user_id', auth()->user()->id)->delete();

        $socials = $request->all();

        foreach ($socials['type'] as $key => $type) {
            if(!empty($type) && !empty($socials['url'][$key])) {
                $socialProfiles = new SocialProfile();
                $socialProfiles->user_id = auth()->user()->id;
                $socialProfiles->type = $type;
                $socialProfiles->url = $socials['url'][$key];

                $socialProfiles->save();
            }
        }

        return redirect()->back()->with('success', 'Your data updated successfully');
    }

    public function postAddressesCreate(Request $request) {
        if ($request->id)
            $address = Address::find($request->id);
        else
            $address = new Address();

        $validator = Validator::make($request->all(), $address::$rules);

        if ($validator->fails()) {
            if($request->ajax()){
                return response()->json([
                    'status' => 'error',
                    'message' => 'Please fill all address fields'
                ]);
            }
            else {
                return redirect('profile/address/' . ($request->id ? 'edit/' . $request->id : 'create'))
                    ->withErrors($validator)
                    ->withInput();
            }
        }

        $address->user_id = Auth::id();
        $address->state = $request->state;
        $address->city = $request->city;
        $address->address = $request->address;
        $address->zip_code = $request->zip_code;
        $address->default_billing = $request->billing ? 'yes' : 'no';
        $address->default_shipping = $request->shipping ? 'yes' : 'no';
        $address->save();

        if($request->ajax()){
            return response()->json([
                'status' => 'success',
                'data' => $address->id
            ]);
        }
        else
            return redirect('/profile/addresses');
    }

    public function getMyWallet(Request $request) {

        \Braintree\Configuration::environment(env('BT_ENVIRONMENT'));
        \Braintree\Configuration::merchantId(env('BT_MERCHANT_ID'));
        \Braintree\Configuration::publicKey(env('BT_PUBLIC_KEY'));
        \Braintree\Configuration::privateKey(env('BT_PRIVATE_KEY'));

        return view('front.profile_my_wallet', ['generate' => \Braintree\ClientToken::generate()]);
    }

    public function postMyWallet(Request $request) {
        \Braintree\Configuration::environment(env('BT_ENVIRONMENT'));
        \Braintree\Configuration::merchantId(env('BT_MERCHANT_ID'));
        \Braintree\Configuration::publicKey(env('BT_PUBLIC_KEY'));
        \Braintree\Configuration::privateKey(env('BT_PRIVATE_KEY'));

        $result = \Braintree\Transaction::sale([
                    'amount' => $request->amount,
                    'paymentMethodNonce' => $request->payment_method_nonce,
                    'options' => [
                        'submitForSettlement' => true
                    ]
        ]);

        if ($result->success || !is_null($result->transaction)) {
            $transaction_info = $result->transaction;
           // dd($transaction_info);
            $transaction = new Transaction();
            $user = User::find(auth()->user()->id);

            $user->wallet_points = $user->wallet_points + $transaction_info->amount;
            $user->save();

            $transaction->user_id = auth()->user()->id;
            $transaction->transaction_id = $transaction_info->id;
            $transaction->status = $transaction_info->status;
            $transaction->amount = $transaction_info->amount;
            $transaction->currency = $transaction_info->currencyIsoCode;

            $transaction->save();
            if (session()->has('from_checkout')) {
                session()->forget('from_checkout');
                return redirect()->action('Front\OrderController@index');
            } else {
                return redirect()->back()->with('success', 'Transaction completed successfully');
            }
        } else {
            $errorString = "";

            foreach ($result->errors->deepAll() as $error) {
                $errorString .= $error->message . "\n";
            }
            return redirect()->back()->withErrors($errorString);
        }

        return view('front.profile_my_wallet', ['generate' => \Braintree\ClientToken::generate()]);
    }

    public function purchaseHistory(Request $request) {
        $orders = Order::where('user_id', auth()->user()->id)->orderBy('created_at', 'desc')->get();
         session()->forget('from_checkout');

        return view('front.profile_purchase_history', ['orders' => $orders]);
    }

    public function postDoctorProfileLicense(Request $request) {
        $destinationPath = public_path() . '/files/profiles';
        $destinationPath = $destinationPath . ( auth()->user()->role == 'patient' ? '/patient-' : '/doctor-' ) . auth()->user()->id;

        if (!is_dir($destinationPath)) {
            @mkdir($destinationPath);
        };

        if ($request->hasFile('license_image')) {

            $fileName = 'license-' . auth()->user()->id . '-' . time();

            if ($request->file('license_image')->isValid()) {
                $extension = $request->file('license_image')->guessExtension();
                $file = $request->file('license_image')->move($destinationPath, $fileName . '.' . $extension);

                $licenseFile = $fileName . '.' . $extension;
            }
        }
        $doctor_license = new DoctorLicense();

        $doctor_license->user_id = auth()->user()->id;
        $doctor_license->registration_state = $request->registration_state;
        $doctor_license->medical_license = $request->medical_license;
        $doctor_license->valid_until = date('Y-m-d', strtotime($request->valid_until));
        $doctor_license->license_image = isset($licenseFile) ? $licenseFile : '';

        $doctor_license->save();

        return response()->json([
                    'status' => 'success'
        ]);
    }

}
