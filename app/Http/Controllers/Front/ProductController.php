<?php

namespace App\Http\Controllers\Front;

use App\Http\Controllers\Controller;
use App\Models\Product;
use App\Models\ProductFavorite;
use App\Models\Portion;
use App\Models\Category;
use App\Models\Strain;
use App\Models\UserCart;
use App\Models\Brand;
use App\Models\Translations;
use Illuminate\Http\Request;
use Illuminate\View\View;
use DB;

class ProductController extends Controller {

    public function index() {

        $products = Product::where('status', '1')->orderBy('sort', 'desc')->get();
        $favorites = [];

        if(auth()->check())
            $favorites = ProductFavorite::where('user_id', auth()->user()->id)->get();


        if($favorites) {
            foreach ($products as &$product) {
                foreach ($favorites as $favorite) {
                    if($product->id == $favorite->product_id)
                        $product->favorite = 'yes';
                }
            }
        }

        return view('front.product', ['products' => $products]);
    }

    public function filter(Request $request) {
        $products = Product::select('products.*', 'product_attributes.attribute_value')->where('status', '1');
        $products->leftJoin('product_attributes', 'products.id', '=', 'product_attributes.product_id');

        foreach ($request->all() as $filters) {
            if (isset($filters['category'])) {
                $products->where('category_id', $filters['category'][0]);
                $sub_categories = Category::select('id')->where('parent_id', $filters['category'][0])->where('status', 'enabled')->get();
                $sub_categoriesIds = [];
                if(count($sub_categories) > 0) {
                    foreach ($sub_categories as $category) {
                        array_push($sub_categoriesIds, $category->id);
                    }
                }

                $products->orWhereIn('category_id', $sub_categoriesIds);
            }
            if (isset($filters['strain'])) {
                $products->where('attribute_type', '1')->whereIn('attribute_id', $filters['strain']);
            }
        }

        $products->groupBy('products.id');

        $products = $products->orderBy('sort', 'desc')->get();

        $sorted_products = $products;
        
        if (isset($request->appliedOrder) && $request->appliedOrder != 0) {

            $sorted_products = [];
            foreach ($products as $product) {
                $max = (int) $product->attributes[0]->attribute_value;
                for ($i = 1; $i < count($product->attributes); $i++) {
                    if ((int) $product->attributes[$i]->attribute_value > $max) {
                        $max = (int) $product->attributes[$i]->attribute_value;
                    }
                }
                $sorted_products[$max] = $product;

            }

            switch ($request->appliedOrder) {
                case '1':   //Newness
                    arsort($sorted_products);
                    break;
                case '2':   //Price Low to High
                    ksort($sorted_products);
                    break;
                case '3':   //Price High to Low
                    krsort($sorted_products);
                    break;
                case '4':   //Popularity
                    //$products->orderBy('id', 'desc');
                    break;
            }
        }

        $favorites = [];

        if(auth()->check())
            $favorites = ProductFavorite::where('user_id', auth()->user()->id)->get();


        if($favorites) {
            foreach ($sorted_products as &$product) {
                foreach ($favorites as $favorite) {
                    if($product->id == $favorite->product_id)
                        $product->favorite = 'yes';
                }
            }
        }
//dd($sorted_products);
        return view('front._products', ['products' => $sorted_products, 'current_locale' => $request->locale]);
    }

    public function single(Request $request, $id, $slug) {
        $data = [];

        $product = Product::where('products.id', $id)->where('status', '1')->first();
        $favorite = [];

        if(auth()->check())
            $favorite = ProductFavorite::where('user_id', auth()->user()->id)->where('product_id', $product->id)->first();
        $data['product'] = $product;
        $data['favorite'] = $favorite ? 'yes' : 'no';

        $trans = new Translations();
        $has_translation = $trans->getSingleTranslationsByGroupAndItem($request->segment(1), 'product', $id . '.title');
        $data['has_translation'] = $has_translation;

        $portions = [];
        foreach (Portion::all()->toArray() as $portion) {
            $portions[$portion['id']] = $portion['weight'];
        }
        $data['portions'] = $portions;
        
        $data['brand'] = Brand::find($product->brand_id);

        if (auth()->check()) {
            $user_cart = UserCart::where('user_id', auth()->user()->id)->where('product_id', $id)->first();
            $data['user_cart'] = $user_cart;
        }

        return view('front.product_single', $data);
    }

    public function getByCategory(Request $request, $id, $slug) {
        $posts = Post::where('posts.category_id', $id)->where('status', 'published')->where('is_featured', 'yes')
                        ->join('attachments', 'attachments.post_id', '=', 'posts.id')
                        ->select('posts.*', 'attachments.file_name', 'attachments.file_type')->get();

        return view('front.blog_by_category', ['posts' => $posts, 'category_id' => $id]);
    }

    public function favorites() {
        $products = ProductFavorite::getProducts(auth()->user()->id);
        $favorite_products = [];

        foreach ($products as $product) {
            $favorite_products[$product->product_id]['product'] = $product;

            $favorite_products[$product->product_id]['attributes'][$product->attribute_id] =  [
                'attribute_type' => $product->attribute_type,
                'attribute_id' => $product->attribute_id,
                'attribute_value' => $product->attribute_value,
            ];


        }

        return view('front.favorites', ['products' => $favorite_products]);
    }

    public function addToFavorite(Request $request) {
        $product_id = $request->get('product_id');
        if($product_id) {
            $product_favorite = new ProductFavorite();
            $product_favorite->user_id = auth()->user()->id;
            $product_favorite->product_id = $product_id;

            $product_favorite->save();

            return response()->json([
                'status' => 'success'
            ]);
        }
    }

    public function removeFromFavorite(Request $request) {
        $product_id = $request->get('product_id');
        if($product_id) {
            $product_favorite = ProductFavorite::where('user_id', auth()->user()->id)->where('product_id', $product_id)->first();
            if($product_favorite) {
                $product_favorite->delete();
                return response()->json([
                    'status' => 'success'
                ]);
            }
            else {
                return response()->json([
                    'status' => 'error'
                ]);
            }
        }
    }

}
