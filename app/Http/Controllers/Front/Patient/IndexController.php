<?php

namespace App\Http\Controllers\Front\Patient;

use App\Http\Controllers\Controller;

class IndexController extends Controller {

    public function __construct() {
        $this->middleware('auth.patient');
        
    }

    public function index() {
        return view('front.patient.index');
    }

}
