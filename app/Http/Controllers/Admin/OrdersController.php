<?php

namespace App\Http\Controllers\Admin;

use App\Http\Controllers\Controller;
use Waavi\Translation\Repositories\TranslationRepository;
use Illuminate\Http\Request;
use Validator;
use App\Models\Order;
use App\Models\User;
use App\Models\Language;
use App\Models\Translations;
use PDF;

class OrdersController extends Controller {

    protected $translation;

    public function __construct() {
        $this->middleware('auth.admin');
    }

    public function index() {
        $orders = Order::all();

        return view('admin.orders', ['orders' => $orders, 'user' => new User()]);
    }

    public function single($id) {
        $order = Order::find($id);

        $user = User::find($order->user_id);

        return view('admin.orderSingle', ['order' => $order, 'user' => $user]);
    }

    public function postUpdateStatus(Request $request) {
        $order_status = $request->get('order_status');
        $order_id = $request->get('order_id');
        $user_id = $request->get('user_id');

        if ($order_status && $order_id && $user_id) {
            Order::where('id', $order_id)->where('user_id', $user_id)->update(['status' => $order_status]);

            return response()->json([
                        'status' => 'success',
                        'message' => 'Order status updated successfully',
            ]);
        }
    }

    public function downloadPdf(Request $request) {
        if ($request->order_id) {
            $order = Order::find($request->order_id);
            $user = User::find($order->user_id);

            $pdf = PDF::loadView('admin.downloadPdf', ['order' => $order, 'user' => $user]);

            return $pdf->download('invoice-'.$order->order_number.'.pdf');
        }
        return redirect()->back();
    }

}
