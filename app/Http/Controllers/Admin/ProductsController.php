<?php

namespace App\Http\Controllers\Admin;

use App\Http\Controllers\Controller;
use Waavi\Translation\Repositories\TranslationRepository;
use Illuminate\Http\Request;
use Illuminate\Http\Response;
use Validator;
use App\Models\Category;
use App\Models\Brand;
use App\Models\Strain;
use App\Models\Portion;
use App\Models\Product;
use App\Models\ProductAttribute;
use App\Models\ProductMedia;
use App\Models\Language;
use App\Models\Translations;

class ProductsController extends Controller {

    protected $translation;

    public function __construct() {
        $this->middleware('auth.admin');
        $this->translation = new Translations();
    }

    public function index() {
        $products = Product::all();

        foreach ($products as $key => $product) {
            $products[$key]['titles'] = $this->translation->getTranslationsByGroupAndItem('product', $product->id . '.title');
        }

        return view('admin.products', ['products' => $products]);
    }

    public function getCreate(Product $product, TranslationRepository $TranslationRepository) {
        $categories = Category::where('type', 'product')->where('parent_id', 0)->get();
        $brands = Brand::all();
        $strains = Strain::all();
        $portions = Portion::all();

        foreach ($categories as &$category) {
            $category['translation'] = $this->translation->getSingleTranslationsByGroupAndItem('en', 'product_category', $category->id . '.title');
            $category['sub_categories'] = Category::where('type', 'product')->where('parent_id', $category->id)->get();
            if (count($category['sub_categories']) > 0) {
                foreach ($category['sub_categories'] as &$sub) {
                    $sub['translation'] = $this->translation->getSingleTranslationsByGroupAndItem('en', 'product_category', $sub->id . '.title');
                }
            }
        }

        foreach ($brands as $key => $brand) {
            $brands[$key]['translation'] = $this->translation->getSingleTranslationsByGroupAndItem('en', 'brand', $brand->id . '.title');
        }

        foreach ($strains as $key => $strain) {
            $strains[$key]['translation'] = $this->translation->getSingleTranslationsByGroupAndItem('en', 'strain', $strain->id . '.title');
        }

        foreach ($portions as $key => $portion) {
            $portions[$key]['translation'] = $this->translation->getSingleTranslationsByGroupAndItem('en', 'portion', $portion->id . '.title');
        }

        return view('admin.productCreate', [
            'categories' => $categories,
            'brands' => $brands,
            'strains' => $strains,
            'portions' => $portions,
            'product' => $product
        ]);
    }

    public function postCreate(Request $request, Product $product, TranslationRepository $TranslationRepository) {
//        dd($request->all());

        $validator = Validator::make($request->all(), Product::$rules);

        if ($validator->fails()) {
            return redirect('admin/product/create')
                            ->withErrors($validator)
                            ->withInput();
        }
        
          $category = Category::find($request->category);
           
            if($category->status == 'disabled'){
                return redirect('admin/product/create')
                                ->withErrors(['Selected Category is Disabled']); 
            }

        $product = new Product();
        $product->sku = generateSku($request->title);
        $product->category_id = $request->category;
        $product->brand_id = $request->brand;
        $product->weight_in_stock = $request->weight;
        $product->status = $request->status;
        $product->sort = $request->sort;
        $product->save();

        if ($request->hasFile('image')) {
            $destinationPath = $product->getFileDir();
            $fileName = slugify($request->title) . '-' . $product->id . '-' . time();

            if ($request->file('image')->isValid()) {
                $extension = $request->file('image')->guessExtension();

                resizeImage($request->file('image'), $destinationPath, $fileName, '300', 'm');
                resizeImage($request->file('image'), $destinationPath, $fileName, '100', 's');
                $file = $request->file('image')->move($destinationPath, $fileName . '-o.' . $extension);

                $product = Product::find($product->id);
                $product->image = $fileName;
                $product->extension = $extension;
                $product->save();
            }
        }

        $TranslationRepository->create([
            'locale' => 'en',
            'namespace' => '*',
            'group' => 'product',
            'item' => $product->id . '.title',
            'text' => $request->title,
        ]);

        $TranslationRepository->create([
            'locale' => 'en',
            'namespace' => '*',
            'group' => 'product',
            'item' => $product->id . '.excerpt',
            'text' => !empty($request->product_excerpt) ? $request->product_excerpt : '',
        ]);

        $TranslationRepository->create([
            'locale' => 'en',
            'namespace' => '*',
            'group' => 'product',
            'item' => $product->id . '.content',
            'text' => !empty($request->product_content) ? strip_tags($request->product_content, '<p><div><b><i><u><table><tbody><tr><td><ul><ol><li><a><img><sub><sup><span>') : '',
        ]);

        if ($request->strains) {
            foreach ($request->strains as $strain) {
                $productAttr = new ProductAttribute();
                $productAttr->product_id = $product->id;
                $productAttr->attribute_type = 1;
                $productAttr->attribute_id = $strain;
                $productAttr->save();
            }
        }

        if ($request->portions) {
            foreach ($request->portions as $key => $portion) {
                $productAttr = new ProductAttribute();
                $productAttr->product_id = $product->id;
                $productAttr->attribute_type = 2;
                $productAttr->attribute_id = $portion;
                $productAttr->attribute_value = $request->prices[$key];
                $productAttr->save();
            }
        }

        return redirect('/admin/products');
    }

    public function getUpdate($id, TranslationRepository $TranslationRepository) {
        if (isset($id) && !empty($id)) {
            $product = Product::find($id);

            $categories = Category::where('type', 'product')->where('parent_id', 0)->get();

            foreach ($categories as &$category) {
                $category['translation'] = $this->translation->getSingleTranslationsByGroupAndItem('en', 'product_category', $category->id . '.title');
                $category['sub_categories'] = Category::where('type', 'product')->where('parent_id', $category->id)->get();
                if (count($category['sub_categories']) > 0) {
                    foreach ($category['sub_categories'] as &$sub) {
                        $sub['translation'] = $this->translation->getSingleTranslationsByGroupAndItem('en', 'product_category', $sub->id . '.title');
                    }
                }
            }
            $brands = Brand::all();
            $strains = Strain::all();
            $portions = Portion::all();

            foreach ($categories as $key => $category) {
                $categories[$key]['translation'] = $this->translation->getSingleTranslationsByGroupAndItem('en', 'product_category', $category->id . '.title');
            }

            foreach ($brands as $key => $brand) {
                $brands[$key]['translation'] = $this->translation->getSingleTranslationsByGroupAndItem('en', 'brand', $brand->id . '.title');
            }

            foreach ($strains as $key => $strain) {
                $strains[$key]['translation'] = $this->translation->getSingleTranslationsByGroupAndItem('en', 'strain', $strain->id . '.title');
            }

            foreach ($portions as $key => $portion) {
                $portions[$key]['translation'] = $this->translation->getSingleTranslationsByGroupAndItem('en', 'portion', $portion->id . '.title');
            }

            $languages = Language::where('deleted_at', NULL)->pluck('name', 'locale');
            $productTranslationTitle = $this->translation->getSingleTranslationsByGroupAndItem('en', 'product', $product->id . '.title');
            $productTranslationExcerpt = $this->translation->getSingleTranslationsByGroupAndItem('en', 'product', $product->id . '.excerpt');
            $productTranslationContent = $this->translation->getSingleTranslationsByGroupAndItem('en', 'product', $product->id . '.content');

            $attrStrains = ProductAttribute::where('product_id', $id)->where('attribute_type', 1)->get();

            $selectedStrains = [];
            foreach ($attrStrains as $attrStrain) {
                array_push($selectedStrains, $attrStrain->attribute_id);
            }



            $selectedPortions = [];
            $attrPortions = ProductAttribute::where('product_id', $id)->where('attribute_type', 2)->get();
            foreach ($attrPortions as $key => $attrPortion) {
                $selectedPortions[$attrPortion->attribute_id] = $attrPortion->attribute_value;
            }

            return view('admin.productCreate', [
                'product' => $product,
                'categories' => $categories,
                'brands' => $brands,
                'strains' => $strains,
                'portions' => $portions,
                'languages' => $languages,
                'title' => $productTranslationTitle->text,
                'excerpt' => isset($productTranslationExcerpt->text) ? $productTranslationExcerpt->text : '',
                'content' => isset($productTranslationContent->text) ? $productTranslationContent->text : '',
                'selectedStrains' => $selectedStrains,
                'selectedPortions' => $selectedPortions
            ]);
        }
    }

    public function postUpdate(Request $request, $id, TranslationRepository $TranslationRepository) {
        if (isset($id) && !empty($id)) {
            $product = Product::find($id);

            $validator = Validator::make($request->all(), Product::$rules);

            if ($validator->fails()) {
                return redirect('admin/product/' . $id)
                                ->withErrors($validator)
                                ->withInput();
            }
            $category = Category::find($request->category);
           
            if($category->status == 'disabled'){
                return redirect('admin/product/' . $id)
                                ->withErrors(['Selected Category is Disabled']); 
            }
            
            $product->sku = generateSku($request->title);
            $product->category_id = $request->category;
            $product->brand_id = $request->brand;
            $product->weight_in_stock = $request->weight;
            $product->status = $request->status;
            $product->sort = $request->sort;
            $product->save();

            if ($request->hasFile('image')) {
                $destinationPath = $product->getFileDir();
                $fileName = slugify($request->title) . '-' . $product->id . '-' . time();

                if ($request->file('image')->isValid()) {
                    $extension = $request->file('image')->guessExtension();

                    @unlink($destinationPath . '/' . $fileName . '-o.' . $extension);
                    @unlink($destinationPath . '/' . $fileName . '-m.' . $extension);
                    @unlink($destinationPath . '/' . $fileName . '-s.' . $extension);

                    resizeImage($request->file('image'), $destinationPath, $fileName, '300', 'm');
                    resizeImage($request->file('image'), $destinationPath, $fileName, '100', 's');
                    $file = $request->file('image')->move($destinationPath, $fileName . '-o.' . $extension);

                    $product->image = $fileName;
                    $product->extension = $extension;
                    $product->save();
                }
            }

            $postTranslationTitle = $this->translation->getSingleTranslationsByGroupAndItem('en', 'product', $product->id . '.title');
            $TranslationRepository->update($postTranslationTitle->id, $request->title);

            $postTranslationExcerpt = $this->translation->getSingleTranslationsByGroupAndItem('en', 'product', $product->id . '.excerpt');
            $TranslationRepository->update($postTranslationExcerpt->id, $request->product_excerpt);

            $postTranslationContent = $this->translation->getSingleTranslationsByGroupAndItem('en', 'product', $product->id . '.content');
            $TranslationRepository->update($postTranslationContent->id, $request->product_content);

            if ($request->strains) {
                ProductAttribute::where('product_id', $id)->where('attribute_type', 1)->delete();
                foreach ($request->strains as $strain) {
                    $productAttr = new ProductAttribute();
                    $productAttr->product_id = $product->id;
                    $productAttr->attribute_type = 1;
                    $productAttr->attribute_id = $strain;
                    $productAttr->save();
                }
            }

            if ($request->portions) {
                ProductAttribute::where('product_id', $id)->where('attribute_type', 2)->delete();
                foreach ($request->portions as $key => $portion) {
                    $productAttr = new ProductAttribute();
                    $productAttr->product_id = $product->id;
                    $productAttr->attribute_type = 2;
                    $productAttr->attribute_id = $portion;
                    $productAttr->attribute_value = $request->prices[$key];
                    $productAttr->save();
                }
            }

            return redirect('/admin/products');
        }
    }

    public function getTranslation($product_id) {
        $product = Product::find($product_id);
        $translations['title'] = $this->translation->getTranslationsByGroupAndItem('product', $product->id . '.title');
        $translations['excerpt'] = $this->translation->getTranslationsByGroupAndItem('product', $product->id . '.excerpt');
        $translations['content'] = $this->translation->getTranslationsByGroupAndItem('product', $product->id . '.content');
//        dd($translations);
        $languages = Language::where('deleted_at', NULL)->pluck('name', 'locale');

        return view('admin.productTranslation', ['product' => $product, 'languages' => $languages, 'translations' => $translations]);
    }

    public function postTranslation($product_id, Request $request, TranslationRepository $TranslationRepository) {
        $languages = Language::where('deleted_at', NULL)->pluck('name', 'locale');

        // title
        $this->translation->productTranslations($request, $languages, $product_id, 'title', $TranslationRepository);

        // content
        $this->translation->productTranslations($request, $languages, $product_id, 'content', $TranslationRepository);

        // excerpt
        $this->translation->productTranslations($request, $languages, $product_id, 'excerpt', $TranslationRepository);

        return redirect('/admin/products');
    }

    public function addVideo(Request $request, $product_id) {
        if ($request->media) {
            $productMedia = new ProductMedia();
            $productMedia->product_id = $product_id;
            $productMedia->media_type = '2';
            $productMedia->media = $request->media;
            $productMedia->save();

            return response()->json([
                        'status' => 'success',
                        'data' => ['id' => $productMedia->id]
            ]);
        }

        return response()->json([
                    'status' => 'error',
                    'message' => 'Invalid data',
        ]);
    }

    public function uploadImage(Request $request, Product $product, $product_id) {
        $rules = [
            'media_info' => 'required',
            'image' => 'dimensions:width=400,height=400|mimes:jpg,jpeg,png'
        ];
        $validator = Validator::make($request->all(), $rules);

        if ($validator->fails()) {
            return response()->json([
                        'status' => 'error',
                        'message' => 'Image sizes should be 400x400 px',
            ]);
        }

        if ($request->hasFile('image')) {
            $media_info = json_decode($request->media_info);

            $destinationPath = $product->getFileDir();
            $fileName = slugify($media_info->title) . '-' . $product_id . '-' . uniqid();

            if ($request->file('image')->isValid()) {
                $extension = $request->file('image')->guessExtension();

                $productMedia = new ProductMedia();
                $productMedia->product_id = $product_id;
                $productMedia->media_type = '1';
                $productMedia->media = $fileName;
                $productMedia->extension = $extension;
                $productMedia->save();

                resizeImage($request->file('image'), $destinationPath, $fileName, '300', 'm');
                resizeImage($request->file('image'), $destinationPath, $fileName, '100', 's');
                $file = $request->file('image')->move($destinationPath, $fileName . '-o.' . $extension);

                return response()->json([
                            'id' => $productMedia->id,
                            'file_path' => $product->getFilePath() . $fileName . '-s.' . $extension,
                ]);
            }
        }

        return response()->json([
                    'status' => 'error',
                    'message' => 'Invalid data',
        ]);
    }

    public function removeMedia($media_id, Product $product) {
        if (isset($media_id) && !empty($media_id)) {
            $productMedia = ProductMedia::find($media_id);

            if ($productMedia->media_type == 1) {
                $destinationPath = $product->getFileDir();
                @unlink($destinationPath . '/' . $productMedia->media . '-o.' . $productMedia->extension);
                @unlink($destinationPath . '/' . $productMedia->media . '-m.' . $productMedia->extension);
                @unlink($destinationPath . '/' . $productMedia->media . '-s.' . $productMedia->extension);
            }

            $productMedia->delete();

            return response()->json([
                        'status' => 'success',
            ]);
        }
        return response()->json([
                    'status' => 'error',
                    'message' => 'soemthing went wrong',
        ]);
    }

    public function delete($id) {
        if (isset($id) && !empty($id)) {
            $product = Product::find($id);

            $this->translation->deleteTranslationsByGroupAndItem('product', $id . '.title');
            $this->translation->deleteTranslationsByGroupAndItem('product', $id . '.content');
            $this->translation->deleteTranslationsByGroupAndItem('product', $id . '.excerpt');

            $destinationPath = $product->getFileDir();
            @unlink($destinationPath . '/' . $product->image . '-o.' . $product->extension);
            @unlink($destinationPath . '/' . $product->image . '-m.' . $product->extension);
            @unlink($destinationPath . '/' . $product->image . '-s.' . $product->extension);

            $product->delete();

            return redirect('/admin/products');
        }
    }

}
