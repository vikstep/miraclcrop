<?php

namespace App\Http\Controllers\Admin;

use App\Http\Controllers\Controller;
use Waavi\Translation\Repositories\TranslationRepository;
use Illuminate\Http\Request;
use Illuminate\Http\Response;
use Validator;
use App\Models\Post;
use App\Models\Attachment;
use App\Models\Category;
use App\Models\Language;
use App\Models\Translations;

class PostsController extends Controller {

    protected $translation;

    public function __construct() {
        $this->middleware('auth.admin');
        $this->translation = new Translations();
    }

    public function index() {
        $posts = Post::all();

        foreach ($posts as $key => $post) {
            $posts[$key]['titles'] = $this->translation->getTranslationsByGroupAndItem('post', $post->id . '.title');
        }

        return view('admin.posts', ['posts' => $posts]);
    }

    public function getCreate(Post $post, TranslationRepository $TranslationRepository) {
        $categories = Category::where('type', 'blog')->get();

        foreach ($categories as $key => $category) {
            $categories[$key]['translation'] = $this->translation->getSingleTranslationsByGroupAndItem('en', 'blog_category', $category->id . '.title');
        }

        return view('admin.postCreate', ['categories' => $categories, 'post' => $post]);
    }

    public function postCreate(Request $request, Post $post, TranslationRepository $TranslationRepository) {
        $post_id = isset($request->post_id) ? $request->post_id : '';
        if ($post_id) {
            $translationsTitle = $this->translation->getSingleTranslationsByGroupAndItem('en', 'post', $post_id . '.title');
            $TranslationRepository->update($translationsTitle->id, $request->title);
        } else {
            $post->status = "draft";
            $post->category_id = '1';
            $post->defined_created_date = date('Y-m-d', time());
            $post->save();

            $TranslationRepository->create([
                'locale' => 'en',
                'namespace' => '*',
                'group' => 'post',
                'item' => $post->id . '.title',
                'text' => $request->title,
            ]);

            $post_id = $post->id;
        }

        return response()->json([
                    'id' => $post_id,
                    'slug' => slugify($request->title),
        ]);
    }

    public function getUpdate($id, TranslationRepository $TranslationRepository) {
        if (isset($id) && !empty($id)) {
            $post = Post::find($id);
            $categories = Category::where('type', 'blog')->get();

            foreach ($categories as $key => $category) {
                $categories[$key]['translation'] = $this->translation->getSingleTranslationsByGroupAndItem('en', 'blog_category', $category->id . '.title');
            }

            $languages = Language::where('deleted_at', NULL)->pluck('name', 'locale');
            $postTranslationTitle = $this->translation->getSingleTranslationsByGroupAndItem('en', 'post', $post->id . '.title');
            $postTranslationContent = $this->translation->getSingleTranslationsByGroupAndItem('en', 'post', $post->id . '.content');
            $postTranslationExcerpt = $this->translation->getSingleTranslationsByGroupAndItem('en', 'post', $post->id . '.excerpt');
           
            $attachments = [];
            $featured_image = NULL;

            foreach ($post->attachment as $attachment) {
                if ($attachment->is_featured == 'yes') {
                    $featured_image = url($post->getAttachmentPath($attachment) . '-s.' . $attachment->file_type);
                }
                array_push($attachments, array(
                    'id' => $attachment->id,
                    'file_path' => $post->getAttachmentPath($attachment),
                    'type' => $attachment->file_type,
                    'is_featured' => $attachment->is_featured)
                );
            }

//            dd($attachments);
//            dd($featured_image);

            return view('admin.postCreate', [
                'post' => $post,
                'categories' => $categories,
                'languages' => $languages,
                'title' => $postTranslationTitle->text,
                'excerpt' => isset($postTranslationExcerpt->text) ? $postTranslationExcerpt->text : '',
                'content' => isset($postTranslationContent->text) ? $postTranslationContent->text : '',
                'featured_image' => $featured_image,
                'attachments' => json_encode($attachments)
            ]);
        }
    }

    public function postUpdate(Request $request, $id, TranslationRepository $TranslationRepository) {
        if (isset($id) && !empty($id)) {
            $post = Post::find($id);
            $data = array(
                'title' => $request->title,
                'category' => $request->category,
                'excerpt' => $request->post_excerpt,
                'content' => $request->post_content
            );

            if ($request->status == 'published') { 
                $validator = Validator::make($data, Post::$rules);

                if ($validator->fails()) {
                    return redirect('admin/post/' . $id)
                                    ->withErrors($validator)
                                    ->withInput();
                }

                if (empty($request->attachments) || $request->attachments == '[]') {
                    return redirect('admin/post/' . $id)
                                    ->withErrors(['The published posts must have featured image'])
                                    ->withInput();
                }
            }
            
             $category = Category::find($request->category);
           
            if($category->status == 'disabled'){
                return redirect('admin/post/' . $id)
                                ->withErrors(['Selected Category is Disabled']); 
            }


            $post->category_id = $request->category;
            $post->sort = $request->sort;
            $post->status = $request->status;
            $post->defined_created_date = $request->defined_created_date ? date('Y-m-d', strtotime($request->defined_created_date)) : date('Y-m-d', time());
            $post->save();
           
            //title 
            $translationsTitle = $this->translation->getSingleTranslationsByGroupAndItem('en', 'post', $post->id . '.title');
            $TranslationRepository->update($translationsTitle->id, $request->title);

            //excerpt
            $translationsExcerpt = $this->translation->getSingleTranslationsByGroupAndItem('en', 'post', $post->id . '.excerpt');

            if ($translationsExcerpt) {
                $TranslationRepository->update($translationsExcerpt->id, !empty($request->post_excerpt) ? $request->post_excerpt : '');
            } else {
                $TranslationRepository->create([
                    'locale' => 'en',
                    'namespace' => '*',
                    'group' => 'post',
                    'item' => $post->id . '.excerpt',
                    'text' => !empty($request->post_excerpt) ? $request->post_excerpt : '',
                ]);
            }

            //content
            $translationsContent = $this->translation->getSingleTranslationsByGroupAndItem('en', 'post', $post->id . '.content');

            if ($translationsContent) {
                $TranslationRepository->update($translationsContent->id, !empty($request->post_content) ? $request->post_content : '');
            } else {
                $TranslationRepository->create([
                    'locale' => 'en',
                    'namespace' => '*',
                    'group' => 'post',
                    'item' => $post->id . '.content',
                    'text' => !empty($request->post_content) ? strip_tags($request->post_content, '<p><div><b><i><u><table><tbody><tr><td><ul><ol><li><a><img><sub><sup><span>') : '',
                ]);
            }


            return redirect('/admin/posts');
        }
    }

    public function getTranslation($post_id) {
        $post = Post::find($post_id);
        $translations['title'] = $this->translation->getTranslationsByGroupAndItem('post', $post->id . '.title');
        $translations['excerpt'] = $this->translation->getTranslationsByGroupAndItem('post', $post->id . '.excerpt');
        $translations['content'] = $this->translation->getTranslationsByGroupAndItem('post', $post->id . '.content');
        //dd($translations['excerpt']);
        $languages = Language::where('deleted_at', NULL)->pluck('name', 'locale');

        return view('admin.postTranslation', ['post' => $post, 'languages' => $languages, 'translations' => $translations]);
    }

    public function postTranslation($post_id, Request $request, TranslationRepository $TranslationRepository) {
        $languages = Language::where('deleted_at', NULL)->pluck('name', 'locale');

        // title
        $this->translation->postTranslations($request, $languages, $post_id, 'title', $TranslationRepository);

        // content
        $this->translation->postTranslations($request, $languages, $post_id, 'content', $TranslationRepository);

        // excerpt
        $this->translation->postTranslations($request, $languages, $post_id, 'excerpt', $TranslationRepository);

        return redirect('/admin/posts');
    }

    public function delete($id) {
        if (isset($id) && !empty($id)) {
            $post = Post::find($id);

            $this->translation->deleteTranslationsByGroupAndItem('post', $id . '.title');
            $this->translation->deleteTranslationsByGroupAndItem('post', $id . '.content');
            $this->translation->deleteTranslationsByGroupAndItem('post', $id . '.excerpt');
             foreach ($post->attachment as $attachment) {
                    @unlink(public_path().$post->getAttachmentPath($attachment) . '-o.' . $attachment->file_type);
                    @unlink(public_path().$post->getAttachmentPath($attachment) . '-m.' . $attachment->file_type);
                    @unlink(public_path().$post->getAttachmentPath($attachment) . '-s.' . $attachment->file_type);
             }

            $post->delete();

            return redirect('/admin/posts');
        }
    }

    public function postUpload(Request $request, Post $post, Attachment $attachment) {
        $destinationPath = $post->getFileDir();

        $post_info = json_decode($request->post_info);

        $fileName = slugify($post_info->title) . '-' . $post_info->id . '-' . time();

        if ($request->hasFile('image')) {

            $rules = ['image' => 'dimensions:min_width=400,max_width=800|mimes:jpg,jpeg,png|max:4000'];

            $validator = Validator::make($request->all(), $rules);

            if ($validator->fails()) {
                return response()->json([ 'status' => 'error', 'messages' => $validator->messages()->get('image') ]);
            }

            if ($request->file('image')->isValid()) {
                $extension = $request->file('image')->guessExtension();

                $destinationPath = $destinationPath . '/' . date('m.Y');

                if (!is_dir($destinationPath)) {
                    @mkdir($destinationPath);
                }
                try {
                    //save attachment
                    $attachment->post_id = $post_info->id;
                    $attachment->is_featured = Attachment::where('post_id', $post_info->id)->count() > 0 ? "no" : "yes";
                    $attachment->file_name = $fileName;
                    $attachment->file_type = $request->file('image')->guessExtension();

                    $attachment->save();
                    $post->resizeImage($request->file('image'), $destinationPath, $fileName, '300', 'm');
                    $post->resizeImage($request->file('image'), $destinationPath, $fileName, '50', 's');
                    $file = $request->file('image')->move($destinationPath, $fileName . '-o.' . $extension);

                    return response()->json([
                                'id' => $attachment->id,
                                'file_path' => $post->getFilePath() . date('m.Y') . '/' . $fileName,
                                'type' => $extension,
                                'is_featured' => $attachment->is_featured
                    ]);
                } catch (FileException $ex) {
                    dd($ex);
                }
            }
        }
    }

    public function postChangeFeatruedImage(Request $request) {
        if (isset($request->post_id) && isset($request->attachment_id)) {
            Attachment::where('post_id', $request->post_id)->update(['is_featured' => 'no']);

            $attachment = Attachment::find($request->attachment_id);
            $attachment->is_featured = 'yes';
            $attachment->save();

            return response()->json([
                        'status' => 'ok'
            ]);
        }
    }

}
