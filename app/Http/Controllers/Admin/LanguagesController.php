<?php

namespace App\Http\Controllers\Admin;

use App\Http\Controllers\Controller;
use Validator;
use App\Models\Language;
use Illuminate\Http\Request;

class LanguagesController extends Controller {

    public function __construct() {
        $this->middleware('auth.admin');
    }

    public function index() {
        $languages = Language::all();

        return view('admin.languages', ['languages' => $languages]);
    }

    public function getCreate() {
        return view('admin.languageCreate');
    }

    public function postCreate(Request $request) {

        $language = new Language();

        $validator = Validator::make(['name' => $request->name, 'locale' => $request->language_locale], Language::$rules);

        if ($validator->fails()) {
            return redirect('admin/language/create')
                ->withErrors($validator)
                ->withInput();
        }

        $language->locale = $request->language_locale;
        $language->name = $request->name;
        $language->deleted_at = $request->status == 'disabled' ? date('Y-m-d H:i:s') : NULL;

        $language->save();

        return redirect('/admin/languages');
    }

    public function getUpdate($id) {
        if(isset($id) && !empty($id)) {
            $language = Language::find($id);

            return view('admin.languageCreate', ['language' => $language]);
        }
    }

    public function postUpdate(Request $request, $id) {
        if(isset($id) && !empty($id)) {
            $language = Language::find($id);

            $validator = Validator::make(['name' => $request->name, 'locale' => $request->language_locale], Language::$rules);

            if ($validator->fails()) {
                return redirect('admin/language/' . $id)
                    ->withErrors($validator)
                    ->withInput();
            }


            $language->locale = $request->language_locale;
            $language->name = $request->name;
            $language->deleted_at = $request->status == 'disabled' ? date('Y-m-d H:i:s') : NULL;

            $language->save();

            return redirect('/admin/languages');
        }
    }

    public function delete($id) {
        if(isset($id) && !empty($id)) {
            $language = Language::find($id);

            $language->delete();

            return redirect('/admin/languages');
        }
    }

}