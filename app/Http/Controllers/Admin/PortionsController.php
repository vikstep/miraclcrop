<?php

namespace App\Http\Controllers\Admin;

use App\Http\Controllers\Controller;
use Waavi\Translation\Repositories\TranslationRepository;
use Illuminate\Http\Request;
use Validator;
use App\Models\Portion;
use App\Models\ProductAttribute;
use App\Models\Language;
use App\Models\Translations;

class PortionsController extends Controller {

    protected $translation;

    public function __construct() {
        $this->middleware('auth.admin');
        $this->translation = new Translations();
    }

    public function index() {
        $portions = Portion::all();

        foreach ($portions as $key => $portion) {
            $portions[$key]['translations'] = $this->translation->getTranslationsByGroupAndItem('portion', $portion->id . '.title');
        }

        return view('admin.portions', ['portions' => $portions]);
    }

    public function getCreate() {

        return view('admin.portionCreate');
    }

    public function postCreate(Request $request, TranslationRepository $TranslationRepository) {
        $portion = new Portion();

        $validator = Validator::make(['title' => $request->title, 'weight' => $request->weight], Portion::$rules);

        if ($validator->fails()) {
            return redirect('admin/portion/create')
                            ->withErrors($validator)
                            ->withInput();
        }

        $portion->weight = $request->weight;
//        $portion->sort = $request->sort;
        $portion->save();

        $TranslationRepository->create([
            'locale' => 'en',
            'namespace' => '*',
            'group' => 'portion',
            'item' => $portion->id . '.title',
            'text' => $request->title,
        ]);

        return redirect('/admin/portions');
    }

    public function getUpdate($id, TranslationRepository $TranslationRepository) {
        if (isset($id) && !empty($id)) {
            $portion = Portion::find($id);
            $languages = Language::where('deleted_at', NULL)->pluck('name', 'locale');
              $portionTranslation = $this->translation->getSingleTranslationsByGroupAndItem('en', 'portion', $portion->id . '.title');
         
              return view('admin.portionCreate', ['portion' => $portion, 'languages' => $languages, 'title' => $portionTranslation->text]);
        }
    }

    public function postUpdate(Request $request, $id, TranslationRepository $TranslationRepository) {
        if (isset($id) && !empty($id)) {
            $portion = Portion::find($id);
            $validator = Validator::make(['title' => $request->title, 'weight' => $request->weight], Portion::$rules);

            if ($validator->fails()) {
                return redirect('admin/portion/' . $id)
                                ->withErrors($validator)
                                ->withInput();
            }

            $portion->weight = $request->weight;
//            $portion->sort = $request->sort;
            $portion->save();
            
             $portionTranslation = $this->translation->getSingleTranslationsByGroupAndItem('en', 'portion', $portion->id . '.title');
            
             $TranslationRepository->update($portionTranslation->id, $request->title);

            return redirect('/admin/portions');
        }
    }

    public function getTranslation($id) {
        $portion = Portion::find($id);
        $translations = $this->translation->getTranslationsByGroupAndItem('portion', $portion->id . '.title');

        $languages = Language::where('deleted_at', NULL)->pluck('name', 'locale');

        return view('admin.portionTranslation', ['portion' => $portion, 'languages' => $languages, 'translations' => $translations]);
    }

    public function postTranslation($id, Request $request, TranslationRepository $TranslationRepository) {

        foreach ($request->title as $language_type => $language_text) {
            $single_translation = $this->translation->getSingleTranslationsByGroupAndItem($language_type, 'portion', $id . '.title');
            if ($language_type != 'en') {
                if (!empty($language_text[0])) {
                    if (isset($single_translation) && $single_translation->locale == $language_type) {
                        $TranslationRepository->update($single_translation->id, $language_text[0]);
                    } else {
                        $TranslationRepository->create([
                            'locale' => $language_type,
                            'namespace' => '*',
                            'group' => 'portion',
                            'item' => $id . '.title',
                            'text' => $language_text[0],
                        ]);
                    }
                } else if (empty($language_text[0]) && isset($single_translation)) {
                    $this->translation->deleteSingleTranslationsByGroupAndItem($language_type, 'portion', $id . '.title');
                }
            }
        }

        return redirect('/admin/portions');
    }

    public function delete($id) {
        if (isset($id) && !empty($id)) {
            $portion = Portion::find($id);
            
            $product_attribute = ProductAttribute::where('attribute_id', $id)->where('attribute_type', 2)->first();
            if($product_attribute){
                 return redirect()->back()->with('error', 'This portion is associated with one or more products, in order to remove this portion you need to remove it from product "'. $this->translation->getSingleTranslationsByGroupAndItem('en', 'product', $product_attribute['product_id'] . '.title')->text.'"');
            }
           
            $this->translation->deleteTranslationsByGroupAndItem('portion', $id . '.title');

            $portion->delete();

            return redirect('/admin/portions');
        }
    }

}
