<?php

namespace App\Http\Controllers\Admin;

use App\Http\Controllers\Controller;
use Waavi\Translation\Repositories\TranslationRepository;
use Illuminate\Http\Request;
use Validator;
use App\Models\Strain;
use App\Models\Language;
use App\Models\ProductAttribute;
use App\Models\Translations;

class StrainsController extends Controller {

    protected $translation;

    public function __construct() {
        $this->middleware('auth.admin');
        $this->translation = new Translations();
    }

    public function index() {
        $strains = Strain::all();

        foreach ($strains as $key => $strain) {
            $strains[$key]['translations'] = $this->translation->getTranslationsByGroupAndItem('strain', $strain->id . '.title');
        }

        return view('admin.strains', ['strains' => $strains]);
    }

    public function getCreate() {

        return view('admin.strainCreate');
    }

    public function postCreate(Request $request, TranslationRepository $TranslationRepository) {
        $strain = new Strain();

        $validator = Validator::make(['title' => $request->title], Strain::$rules);

        if ($validator->fails()) {
            return redirect('admin/strain/create')
                            ->withErrors($validator)
                            ->withInput();
        }

        $strain->sort = $request->sort;
        $strain->save();

        $TranslationRepository->create([
            'locale' => 'en',
            'namespace' => '*',
            'group' => 'strain',
            'item' => $strain->id . '.title',
            'text' => $request->title,
        ]);

        return redirect('/admin/strains');
    }

    public function getUpdate($id, TranslationRepository $TranslationRepository) {
        if (isset($id) && !empty($id)) {
            $strain = Strain::find($id);
            $languages = Language::where('deleted_at', NULL)->pluck('name', 'locale');
            $strainTranslation = $this->translation->getSingleTranslationsByGroupAndItem('en', 'strain', $strain->id . '.title');

            return view('admin.strainCreate', ['strain' => $strain, 'languages' => $languages, 'title' => $strainTranslation->text]);
        }
    }

    public function postUpdate(Request $request, $id, TranslationRepository $TranslationRepository) {
        if (isset($id) && !empty($id)) {
            $strain = Strain::find($id);
            $validator = Validator::make(['title' => $request->title], Strain::$rules);

            if ($validator->fails()) {
                return redirect('admin/strain/' . $id)
                                ->withErrors($validator)
                                ->withInput();
            }

            $strain->sort = $request->sort;
            $strain->save();

            $strainTranslation = $this->translation->getSingleTranslationsByGroupAndItem('en', 'strain', $strain->id . '.title');
            $TranslationRepository->update($strainTranslation->id, $request->title);

            return redirect('/admin/strains');
        }
    }

    public function getTranslation($id) {
        $strain = Strain::find($id);
        $translations = $this->translation->getTranslationsByGroupAndItem('strain', $strain->id . '.title');

        $languages = Language::where('deleted_at', NULL)->pluck('name', 'locale');

        return view('admin.strainTranslation', ['strain' => $strain, 'languages' => $languages, 'translations' => $translations]);
    }

    public function postTranslation($id, Request $request, TranslationRepository $TranslationRepository) {

        foreach ($request->title as $language_type => $language_text) {
            $single_translation = $this->translation->getSingleTranslationsByGroupAndItem($language_type, 'strain', $id . '.title');
            if ($language_type != 'en') {
                if (!empty($language_text[0])) {
                    if (isset($single_translation) && $single_translation->locale == $language_type) {
                        $TranslationRepository->update($single_translation->id, $language_text[0]);
                    } else {
                        $TranslationRepository->create([
                            'locale' => $language_type,
                            'namespace' => '*',
                            'group' => 'strain',
                            'item' => $id . '.title',
                            'text' => $language_text[0],
                        ]);
                    }
                } else if (empty($language_text[0]) && isset($single_translation)) {
                    $this->translation->deleteSingleTranslationsByGroupAndItem($language_type, 'strain', $id . '.title');
                }
            }
        }

        return redirect('/admin/strains');
    }

    public function delete($id) {
        if (isset($id) && !empty($id)) {
            $strain = Strain::find($id);

            $product_attribute = ProductAttribute::where('attribute_id', $id)->where('attribute_type', 1)->first();
            if ($product_attribute) {
                return redirect()->back()->with('error', 'This strain is associated with one or more products, in order to remove this strain you need to remove it from product "' . $this->translation->getSingleTranslationsByGroupAndItem('en', 'product', $product_attribute['product_id'] . '.title')->text . '"');
            }

            $this->translation->deleteTranslationsByGroupAndItem('strain', $id . '.title');

            $strain->delete();

            return redirect('/admin/strains');
        }
    }

}
