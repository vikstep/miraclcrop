<?php

namespace App\Http\Controllers\Admin;

use App\Http\Controllers\Controller;
use Waavi\Translation\Repositories\TranslationRepository;
use Illuminate\Http\Request;
use Validator;
use App\Models\Category;
use App\Models\Product;
use App\Models\Language;
use App\Models\Translations;

class ProductCategoriesController extends Controller {

    protected $translation;

    public function __construct() {
        $this->middleware('auth.admin');
        $this->translation = new Translations();
    }

    public function index() {
        $categories = Category::where('type', 'product')->where('parent_id', 0)->orderBy('sort', 'desc')->get();

        foreach ($categories as $key => &$category) {
            $category['translations'] = $this->translation->getTranslationsByGroupAndItem('product_category', $category->id . '.title');

            $category['sub_categories'] = Category::where('type', 'product')->where('parent_id', $category->id)->get();
            if (count($category['sub_categories']) > 0) {
                foreach ($category['sub_categories'] as &$sub) {
                    $sub['translations'] = $this->translation->getTranslationsByGroupAndItem('product_category', $sub->id . '.title');
                }
            }
        }

        return view('admin.productCategories', ['categories' => $categories]);
    }

    public function getCreate() {
        $categories = Category::where('type', 'product')->where('parent_id', 0)->get();

        foreach ($categories as $key => $category) {
            $categories[$key]['translation'] = $this->translation->getSingleTranslationsByGroupAndItem('en', 'product_category', $category->id . '.title');
        }

        return view('admin.productCategoryCreate', ['categories' => $categories]);
    }

    public function postCreate(Request $request, TranslationRepository $TranslationRepository) {
        $category = new Category();

        $validator = Validator::make(['title' => $request->title], Category::$rules);

        if ($validator->fails()) {
            return redirect('admin/product-category/create')
                            ->withErrors($validator)
                            ->withInput();
        }

        $category->sort = $request->sort;
        $category->parent_id = ($request->parent_category) ? $request->parent_category : 0;
        $category->status = $request->status;
        $category->type = 'product';

        $category->save();

        $TranslationRepository->create([
            'locale' => 'en',
            'namespace' => '*',
            'group' => 'product_category',
            'item' => $category->id . '.title',
            'text' => $request->title,
        ]);

        return redirect('/admin/product-categories');
    }

    public function getUpdate($id, TranslationRepository $TranslationRepository) {
        if (isset($id) && !empty($id)) {
            $categories = Category::where('type', 'product')->where('parent_id', 0)->where('id', '!=', $id)->get();


            foreach ($categories as $key => $category) {
                $categories[$key]['translation'] = $this->translation->getSingleTranslationsByGroupAndItem('en', 'product_category', $category->id . '.title');
            }
            $category = Category::find($id);
            $languages = Language::where('deleted_at', NULL)->pluck('name', 'locale');
            $categoryTranslation = $this->translation->getSingleTranslationsByGroupAndItem('en', 'product_category', $category->id . '.title');

            return view('admin.productCategoryCreate', ['categories' => $categories, 'category' => $category, 'languages' => $languages, 'title' => $categoryTranslation->text]);
        }
    }

    public function postUpdate(Request $request, $id, TranslationRepository $TranslationRepository) {
        if (isset($id) && !empty($id)) {
            $category = Category::find($id);
            $validator = Validator::make(['title' => $request->title], Category::$rules);

            if ($validator->fails()) {
                return redirect('admin/product-category/' . $id)
                                ->withErrors($validator)
                                ->withInput();
            }
            if($request->status == 'disabled' && count($category->product_category) > 0){
               foreach($category->product_category as $product_category){
                   $product_category->status = 0;
                   $product_category->save();
               } 
            }
            $category->sort = $request->sort;
            $category->status = $request->status;
            $category->parent_id = ($request->parent_category) ? $request->parent_category : 0;
            $categoryTranslation = $this->translation->getSingleTranslationsByGroupAndItem('en', 'product_category', $category->id . '.title');
            
            $translations = $this->translation->getTranslationsByGroupAndItem('product_category', $category->id . '.title');
            foreach ($translations as $translation) {
                if ($translation->locale != 'en') {
                    $this->translation->deleteSingleTranslationsByGroupAndItem($translation->locale, 'product_category', $category->id . '.title');
                }
            }
            $category->save();

            $TranslationRepository->update($categoryTranslation->id, $request->title);

            return redirect('/admin/product-categories');
        }
    }

    public function getTranslation($category_id) {
        $category = Category::find($category_id);
        $translations = $this->translation->getTranslationsByGroupAndItem('product_category', $category->id . '.title');

        $languages = Language::where('deleted_at', NULL)->pluck('name', 'locale');

        return view('admin.productCategoryTranslation', ['category' => $category, 'languages' => $languages, 'translations' => $translations]);
    }

    public function postTranslation($category_id, Request $request, TranslationRepository $TranslationRepository) {

        foreach ($request->title as $language_type => $language_text) {
            $single_translation = $this->translation->getSingleTranslationsByGroupAndItem($language_type, 'product_category', $category_id . '.title');
            if ($language_type != 'en') {
                if (!empty($language_text[0])) {
                    if (isset($single_translation) && $single_translation->locale == $language_type) {
                        $TranslationRepository->update($single_translation->id, $language_text[0]);
                    } else {
                        $TranslationRepository->create([
                            'locale' => $language_type,
                            'namespace' => '*',
                            'group' => 'product_category',
                            'item' => $category_id . '.title',
                            'text' => $language_text[0],
                        ]);
                    }
                } else if (empty($language_text[0]) && isset($single_translation)) {
                    $this->translation->deleteSingleTranslationsByGroupAndItem($language_type, 'product_category', $category_id . '.title');
                }
            }
        }

        return redirect('/admin/product-categories');
    }

    public function delete($id) {
        if (isset($id) && !empty($id)) {
            $category = Category::find($id);
            $product = Product::where('category_id', $id)->first();
            if ($product) {
                return redirect()->back()->with('error', 'This category is associated with one or more products, in order to remove this category you need to remove it from product "' . $this->translation->getSingleTranslationsByGroupAndItem('en', 'product', $product->id . '.title')->text . '"');
            }
            $this->translation->deleteTranslationsByGroupAndItem('product_category', $id . '.title');

            $category->delete();

            return redirect('/admin/product-categories');
        }
    }

}
