<?php

namespace App\Http\Controllers\Admin;

use App\Http\Controllers\Controller;
use Waavi\Translation\Repositories\TranslationRepository;
use Illuminate\Http\Request;
use Validator;
use App\Models\Brand;
use App\Models\Product;
use App\Models\Language;
use App\Models\Translations;

class BrandsController extends Controller {

    protected $translation;

    public function __construct() {
        $this->middleware('auth.admin');
        $this->translation = new Translations();
    }

    public function index() {
        $brands = Brand::all();

        foreach ($brands as $key => $brand) {
            $brands[$key]['translations'] = $this->translation->getTranslationsByGroupAndItem('brand', $brand->id . '.title');
        }

        return view('admin.brands', ['brands' => $brands]);
    }

    public function getCreate() {

        return view('admin.brandCreate');
    }

    public function postCreate(Request $request, TranslationRepository $TranslationRepository) {
        $brand = new Brand();

        $validation_attrs = [];

        if ($request->hasFile('brand_logo')) {
            $validation_attrs['brand_logo'] = $request->brand_logo;
        }

        $validation_attrs['title'] = $request->title;
        $validation_attrs['description'] = $request->description;

        $validator = Validator::make($validation_attrs, Brand::$rules);

        if ($validator->fails()) {
            return redirect('admin/brand/create')
                            ->withErrors($validator)
                            ->withInput();
        }

        $brand->sort = $request->sort;
        $brand->save();

        if ($request->hasFile('brand_logo')) {
            $destinationPath = $brand->getFileDir();
            $fileName = slugify($request->title) . '-' . $brand->id;

            if ($request->file('brand_logo')->isValid()) {
                $extension = $request->file('brand_logo')->guessExtension();
                $file = $request->file('brand_logo')->move($destinationPath, $fileName . '.' . $extension);

                $brand = Brand::find($brand->id);
                $brand->brand_logo = $fileName . '.' . $extension;
                $brand->save();
            }
        }

        $TranslationRepository->create([
            'locale' => 'en',
            'namespace' => '*',
            'group' => 'brand',
            'item' => $brand->id . '.title',
            'text' => $request->title,
        ]);

        $TranslationRepository->create([
            'locale' => 'en',
            'namespace' => '*',
            'group' => 'brand',
            'item' => $brand->id . '.content',
            'text' => !empty($request->description) ? strip_tags($request->description, '<p><div><b><i><u><table><tbody><tr><td><ul><ol><li><a><img><sub><sup><span>') : '',
        ]);

        return redirect('/admin/brands');
    }

    public function getUpdate($id, TranslationRepository $TranslationRepository) {
        if (isset($id) && !empty($id)) {
            $brand = Brand::find($id);
            $languages = Language::where('deleted_at', NULL)->pluck('name', 'locale');
            $brandTranslationTitle = $this->translation->getSingleTranslationsByGroupAndItem('en', 'brand', $brand->id . '.title');
            $brandTranslationContent = $this->translation->getSingleTranslationsByGroupAndItem('en', 'brand', $brand->id . '.content');

            return view('admin.brandCreate', ['brand' => $brand, 'languages' => $languages, 'title' => $brandTranslationTitle->text, 'description' => $brandTranslationContent->text]);
        }
    }

    public function postUpdate(Request $request, $id, TranslationRepository $TranslationRepository) {
        if (isset($id) && !empty($id)) {
            $brand = Brand::find($id);
            $validator = Validator::make(['title' => $request->title, 'description' => $request->description], Brand::$rules);

            if ($validator->fails()) {
                return redirect('admin/brand/' . $id)
                                ->withErrors($validator)
                                ->withInput();
            }

            $brand->sort = $request->sort;


            if ($request->hasFile('brand_logo')) {
                $destinationPath = $brand->getFileDir();

                @unlink($destinationPath . '/' . $brand->brand_logo);

                $fileName = slugify($request->title) . '-' . $brand->id;

                if ($request->file('brand_logo')->isValid()) {
                    $extension = $request->file('brand_logo')->guessExtension();
                    $file = $request->file('brand_logo')->move($destinationPath, $fileName . '.' . $extension);
                    $brand->brand_logo = $fileName . '.' . $extension;
                }
            }

            $brand->save();

            $postTranslationTitle = $this->translation->getSingleTranslationsByGroupAndItem('en', 'brand', $brand->id . '.title');
            $TranslationRepository->update($postTranslationTitle->id, $request->title);

            $postTranslationContent = $this->translation->getSingleTranslationsByGroupAndItem('en', 'brand', $brand->id . '.content');
            $TranslationRepository->update($postTranslationContent->id, $request->description);

            return redirect('/admin/brands');
        }
    }

    public function postUpload(Request $request, Brand $brand) {
        $destinationPath = $brand->getFileDir();

        $post_info = json_decode($request->post_info);

        $fileName = slugify($request->title) . '-' . $request->id . '-' . time();

        if ($request->hasFile('image')) {

            $rules = ['image' => 'dimensions:min_width=400,max_width=800|mimes:jpg,jpeg,png|max:100'];

            $validator = Validator::make($request->all(), $rules);

            if ($validator->fails()) {
                return response()->json([ 'status' => 'error', 'messages' => $validator->messages()->get('image')]);
            }

            if ($request->file('image')->isValid()) {
                $extension = $request->file('image')->guessExtension();

                $destinationPath = $destinationPath . '/' . date('m.Y');

                if (!is_dir($destinationPath)) {
                    @mkdir($destinationPath);
                }
                try {
                    //save attachment
                    $attachment->post_id = $post_info->id;
                    $attachment->is_featured = Attachment::where('post_id', $post_info->id)->count() > 0 ? "no" : "yes";
                    $attachment->file_name = $fileName;
                    $attachment->file_type = $request->file('image')->guessExtension();

                    $attachment->save();
                    $post->resizeImage($request->file('image'), $destinationPath, $fileName, '300', 'm');
                    $post->resizeImage($request->file('image'), $destinationPath, $fileName, '50', 's');
                    $file = $request->file('image')->move($destinationPath, $fileName . '-o.' . $extension);

                    return response()->json([
                                'id' => $attachment->id,
                                'file_path' => $post->getFilePath() . date('m.Y') . '/' . $fileName,
                                'type' => $extension,
                                'is_featured' => $attachment->is_featured
                    ]);
                } catch (FileException $ex) {
                    dd($ex);
                }
            }
        }
    }

    public function getTranslation($id) {
        $brand = Brand::find($id);
        $translations['title'] = $this->translation->getTranslationsByGroupAndItem('brand', $brand->id . '.title');
        $translations['description'] = $this->translation->getTranslationsByGroupAndItem('brand', $brand->id . '.content');

        $languages = Language::where('deleted_at', NULL)->pluck('name', 'locale');

        return view('admin.brandTranslation', ['brand' => $brand, 'languages' => $languages, 'translations' => $translations]);
    }

    public function postTranslation($id, Request $request, TranslationRepository $TranslationRepository) {

        foreach ($request->title as $language_type => $language_text) {
            $single_translation = $this->translation->getSingleTranslationsByGroupAndItem($language_type, 'brand', $id . '.title');
            if ($language_type != 'en') {
                if (!empty($language_text[0])) {
                    if (isset($single_translation) && $single_translation->locale == $language_type) {
                        $TranslationRepository->update($single_translation->id, $language_text[0]);
                    } else {
                        $TranslationRepository->create([
                            'locale' => $language_type,
                            'namespace' => '*',
                            'group' => 'brand',
                            'item' => $id . '.title',
                            'text' => $language_text[0],
                        ]);
                    }
                } else if (empty($language_text[0]) && isset($single_translation)) {
                    $this->translation->deleteSingleTranslationsByGroupAndItem($language_type, 'brand', $id . '.title');
                }
            }
        }

        foreach ($request->description as $language_type => $language_text) {
            $single_translation = $this->translation->getSingleTranslationsByGroupAndItem($language_type, 'brand', $id . '.content');
            if ($language_type != 'en') {
                if (!empty($language_text[0])) {
                    if (isset($single_translation) && $single_translation->locale == $language_type) {
                        $TranslationRepository->update($single_translation->id, $language_text[0]);
                    } else {
                        $TranslationRepository->create([
                            'locale' => $language_type,
                            'namespace' => '*',
                            'group' => 'brand',
                            'item' => $id . '.content',
                            'text' => $language_text[0],
                        ]);
                    }
                } else if (empty($language_text[0]) && isset($single_translation)) {
                    $this->translation->deleteSingleTranslationsByGroupAndItem($language_type, 'brand', $id . '.content');
                }
            }
        }

        return redirect('/admin/brands');
    }

    public function delete($id) {
        if (isset($id) && !empty($id)) {
            $brand = Brand::find($id);
            $product = Product::where('brand_id', $id)->first();
            if ($product) {

                return redirect()->back()->with('error', 'This brand is associated with one or more products, in order to remove this brand you need to remove it from product "' . $this->translation->getSingleTranslationsByGroupAndItem('en', 'product', $product->id . '.title')->text . '"');
            }
            $destinationPath = $brand->getFileDir();

            @unlink($destinationPath . '/' . $brand->brand_logo);
            $this->translation->deleteTranslationsByGroupAndItem('brand', $id . '.title');
            $this->translation->deleteTranslationsByGroupAndItem('brand', $id . '.content');

            $brand->delete();

            return redirect('/admin/brands');
        }
    }

}
