<?php

namespace App\Http\Controllers\Admin;

use Illuminate\Http\Request;
use Illuminate\Support\Facades\Mail;
use Illuminate\Support\Facades\URL;

use App\Http\Controllers\Controller;
use App\Models\User;

class UsersController extends Controller {

    public function __construct() {
        $this->middleware('auth.admin');
    }

    public function changeStatus(Request $request) {
        if ($request->id) {
            $user = User::find($request->id);
            $user->status = $request->status;

            $status_reason = '';
            if (isset($request->status_reason)) {
                $user->status_reason = $request->status_reason;
                $status_reason = $request->status_reason;
            }

            if ($user->save()) {
                Mail::send('emails.notification', ['href' => URL::to('/auth/signin'), 'status' => $request->status, 'reason' => $status_reason], function ($message) use ($user) {
                    $message->to($user->email, $user->first_name . ' ' . $user->last_name)->subject('Account status');
                });
            }

            return response()->json([
                        'status' => $request->status
            ]);
        }
    }

    public function profile() {
        return view('front.profile_page');
    }

}
