<?php

namespace App\Http\Controllers\Admin;

use App\Http\Controllers\Controller;
use App\Models\User;
use App\Models\DoctorLicense;
use Illuminate\Support\Facades\Auth;

class DoctorsController extends UsersController {

    public function index() {
        $doctors = User::all()->where('role', 'doctor');

        return view('admin.doctors', ['doctors' => $doctors]);
    }

    public function singleView($id) {
        $doctor = User::find($id);
        $doctorLicense = DoctorLicense::where('user_id', $doctor->id)->get();

        return view('admin.doctorsSingle', ['doctor' => $doctor, 'doctorLicense' => $doctorLicense, 'states' => User::$states]);
    }

}
