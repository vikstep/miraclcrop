<?php

namespace App\Http\Controllers\Admin;

use App\Http\Controllers\Controller;
use App\Models\User;
use App\Models\Order;

class PatientsController extends UsersController {

    public function index() {
        $patients = User::orderBy('created_at', 'desc')->where('role', 'patient')->get();

        return view('admin.patients', ['patients' => $patients]);
    }

    public function singleView($id) {
        $patient = User::find($id);
        
        $orders = Order::where('user_id', $id)->get();

        return view('admin.patientsSingle', ['patient' => $patient, 'orders' => $orders]);
    }

}
