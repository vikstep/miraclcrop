<!DOCTYPE html>
<html lang="{{ App::getLocale() }}">

    <head>

        <meta charset="utf-8">
        <meta http-equiv="X-UA-Compatible" content="IE=edge">
        <meta name="viewport" content="width=device-width, initial-scale=1">
        <meta name="description" content="">
        <meta name="author" content="">
        <meta name="csrf-token" content="{{ csrf_token() }}">

        <title>Miracle Crop @yield('title')</title>

        <!-- Bootstrap Core CSS -->
        <link href="{{ asset ("/template/vendor/bootstrap/css/bootstrap.min.css") }}" rel="stylesheet">

        <!-- Theme CSS -->
        <link href="{{ asset ("/template/css/freelancer.min.css") }}" rel="stylesheet">
        <link href="{{ asset ("/css/styles.css") }}" rel="stylesheet">
        <link href="{{ asset ("/template/css/bootstrap-datetimepicker.css") }}" rel="stylesheet">

        <!-- Custom Fonts -->
        <link href="{{ asset ("/template/vendor/font-awesome/css/font-awesome.min.css") }}" rel="stylesheet" type="text/css">
        <link href="https://fonts.googleapis.com/css?family=Montserrat:400,700" rel="stylesheet" type="text/css">
        <link href="https://fonts.googleapis.com/css?family=Lato:400,700,400italic,700italic" rel="stylesheet" type="text/css">

        <!-- HTML5 Shim and Respond.js IE8 support of HTML5 elements and media queries -->
        <!-- WARNING: Respond.js doesn't work if you view the page via file:// -->
        <!--[if lt IE 9]>
        <script src="https://oss.maxcdn.com/libs/html5shiv/3.7.0/html5shiv.js"></script>
        <script src="https://oss.maxcdn.com/libs/respond.js/1.4.2/respond.min.js"></script>
        <![endif]-->
        <script>
            var globalCurrentcy = "{{ Config::get('custom.currency') }}";
        </script>
    </head>

    <body id="page-top" class="index">

        <div id="skipnav"><a href="#maincontent">Skip to main content</a></div>

        <!-- Navigation -->
        <nav id="mainNav" class="navbar navbar-default navbar-fixed-top navbar-custom">
            <div class="container">
                <!-- Brand and toggle get grouped for better mobile display -->
                <div class="navbar-header page-scroll">
                    <button type="button" class="navbar-toggle" data-toggle="collapse" data-target="#bs-example-navbar-collapse-1">
                        <span class="sr-only">Toggle navigation</span> Menu <i class="fa fa-bars"></i>
                    </button>
                    <a class="navbar-brand" href="{{ url('/') }}">{{trans('title.miracle')}}</a>
                </div>

                <!-- Collect the nav links, forms, and other content for toggling -->
                <div class="collapse navbar-collapse" id="bs-example-navbar-collapse-1">
                    <ul class="nav navbar-nav navbar-right">
                        <li class="hidden">
                            <a href="#page-top"></a>
                        </li>
                        <li class="dropdown">
                            <a href="#" class="dropdown-toggle" data-toggle="dropdown">{{ $currentLanguage->name }} <b class="caret"></b></a>
                            <ul class="dropdown-menu localizeDropDown">
                                @foreach ($altLocalizedUrls as $alt)
                                <li><a href="{{ translate($alt['url']) }}" hreflang="{{ $alt['locale'] }}" >{{ $alt['name'] }}</a></li>
                                @endforeach
                            </ul>
                        </li>
                        <li class="dropdown">
                            <a href="#" class="dropdown-toggle" data-toggle="dropdown">{{auth()->user()->first_name}} {{auth()->user()->last_name}} <b class="caret"></b></a>
                            <ul class="dropdown-menu localizeDropDown">
                                <li class="page-scroll">
                                    <a href="{{ url('/profile') }}">{{trans('title.my_profile')}}</a>
                                </li>
                                <li class="page-scroll">
                                    <a href="{{ action('Auth\LoginController@logout') }}">{{trans('title.logout')}}</a>
                                </li>
                            </ul>
                        </li>
                    </ul>
                </div>
                <!-- /.navbar-collapse -->
            </div>
            <!-- /.container-fluid -->
        </nav>

        @yield('content')

        <!-- jQuery -->
        <script src="{{ asset ("/template/vendor/jquery/jquery.min.js") }}"></script>

        <!-- Bootstrap Core JavaScript -->
        <script src="{{ asset ("/template/vendor/bootstrap/js/bootstrap.min.js") }}"></script>

        <!-- Plugin JavaScript -->
        <script src="https://cdnjs.cloudflare.com/ajax/libs/jquery-easing/1.3/jquery.easing.min.js"></script>

        <!-- Contact Form JavaScript -->
        <script src="{{ asset ("/template/js/jqBootstrapValidation.js") }}"></script>
        <script src="{{ asset ("/template/js/moment.min.js") }}"></script>
        <script src="{{ asset ("/template/js/script.js") }}"></script>

        <!-- Theme JavaScript -->
        <script src="{{ asset ("/template/js/freelancer.min.js") }}"></script>
        <script src="{{ asset ("/template/js/bootstrap-datetimepicker.js") }}"></script>

    </body>

</html>