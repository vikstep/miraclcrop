<!DOCTYPE html>
<!--
This is a starter template page. Use this page to start your new project from
scratch. This page gets rid of all links and provides the needed markup only.
-->
<html>
<head>
    <meta charset="UTF-8">
    <title>Miracle Crop Admin @yield('title')</title>
    <meta content='width=device-width, initial-scale=1, maximum-scale=1, user-scalable=no' name='viewport'>
    <meta name="csrf-token" content="{{ csrf_token() }}">
    <!-- Bootstrap 3.3.2 -->
    <link href="{{ asset("/admin-components/bootstrap/css/bootstrap.min.css") }}" rel="stylesheet" type="text/css" />
    <link href="{{ asset("/admin-components/plugins/datepicker/datepicker3.css") }}" rel="stylesheet" type="text/css" />
    <link href="{{ asset("/admin-components/plugins/datatables/dataTables.bootstrap.css") }}" rel="stylesheet" type="text/css" />
    <!-- Font Awesome Icons -->
    <link href="https://maxcdn.bootstrapcdn.com/font-awesome/4.3.0/css/font-awesome.min.css" rel="stylesheet" type="text/css" />
    <!-- Ionicons -->
    <link href="http://code.ionicframework.com/ionicons/2.0.0/css/ionicons.min.css" rel="stylesheet" type="text/css" />
    <!-- Theme style -->
    <link href="{{ asset("/admin-components/plugins/select2/select2.min.css")}}" rel="stylesheet" type="text/css" />
    <link href="{{ asset("/admin-components/dist/css/AdminLTE.min.css")}}" rel="stylesheet" type="text/css" />
    <!-- AdminLTE Skins. We have chosen the skin-blue for this starter
          page. However, you can choose any other skin. Make sure you
          apply the skin class to the body tag so the changes take effect.
    -->
    <link href="{{ asset("/admin-components/dist/css/skins/skin-blue.min.css")}}" rel="stylesheet" type="text/css" />
    <link href="{{ asset("/admin-components/plugins/bootstrap-wysihtml5/bootstrap-wysihtml5.css")}}" rel="stylesheet" type="text/css" />
    <link href="{{ asset("/admin-components/plugins/bootstrap-wysihtml5/wysihtml5-image-upload.css")}}" rel="stylesheet" type="text/css" />
    <link href="{{ asset("/admin-components/plugins/uploader/uploadfile.css")}}" rel="stylesheet" type="text/css" />
    <link href="{{ asset("/css/hierarchy-select.min.css")}}" rel="stylesheet" type="text/css" />
    <link href="{{ asset("/css/styles.css")}}" rel="stylesheet" type="text/css" />
   
    <!-- HTML5 Shim and Respond.js IE8 support of HTML5 elements and media queries -->
    <!-- WARNING: Respond.js doesn't work if you view the page via file:// -->
    <!--[if lt IE 9]>
    <script src="https://oss.maxcdn.com/libs/html5shiv/3.7.0/html5shiv.js"></script>
    <script src="https://oss.maxcdn.com/libs/respond.js/1.3.0/respond.min.js"></script>
    <![endif]-->
</head>
<body class="skin-blue">
<div class="wrapper">

    <!-- Header -->
@include('admin.header')

<!-- Sidebar -->
@include('admin.sidebar')

@yield('content')

<!-- Footer -->
    @include('admin.footer')

</div><!-- ./wrapper -->

</body>
</html>