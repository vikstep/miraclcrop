@extends('layouts.front.index')
@section('title', '| ' . trans('title.register'))
@section('content')
<div class="login-content">
    <div class="login-box">
        <div class="wizard-progress">
            <div class="step complete">
                Basic Info
                <div class="node"></div>
            </div>
            <div class="step complete">
                Addresses
                <div class="node"></div>
            </div>
            <div class="step complete">
                Licensing
                <div class="node"></div>
            </div>
            <div class="step complete">
                Profile details
                <div class="node"></div>
            </div>
            <div class="step in-progress">
                Signing
                <div class="node"></div>
            </div>
        </div>
        <div class="login-box-body">
            <p class="login-box-msg">Signing</p>
            @if (count($errors) > 0)
            <div class="alert alert-danger">
                <ul>
                    @foreach ($errors->all() as $error)
                    <li>{{ $error }}</li>
                    @endforeach
                </ul>
            </div>
            @endif
            <form method="POST" action="{{ action('Auth\RegisterController@postPatientStep5') }}" data-toggle="validator" role="form" id="registrationForm" class="custom-validation-form">
                {!! csrf_field() !!}
                <div class='form-group'>
                    <div id="signature-pad" class="m-signature-pad">
                        <div class="m-signature-pad--body">
                            <canvas class="signature-class"></canvas>
                        </div>
                        <div class="m-signature-pad--footer">
                            <div class="description">Sign above</div>
                            <button type="button" class="button clear" data-action="clear">Clear</button>
                            <button type="button" class="button save" data-action="save">Sign</button>
                        </div>
                    </div>
                </div>
                <div class="form-group bg-white">
                    <div class="row">
                        <div class="col-xs-12 m-t-10">
                            <div class="col-xs-4"><strong>User Name :</strong></div>
                            <div class="col-xs-8 account-info">{{$user->username}}</div>
                        </div>
                        <div class="col-xs-12 m-t-5">
                            <div class="col-xs-4"><strong>First Name :</strong></div>
                            <div class="col-xs-8 account-info">{{$user->first_name}}</div>
                        </div>
                        <div class="col-xs-12 m-t-5">
                            <div class="col-xs-4"><strong>Last Name :</strong></div>
                            <div class="col-xs-8 account-info">{{$user->last_name}}</div>
                        </div>
                        <div class="col-xs-12 m-t-5">
                            <div class="col-xs-4"><strong>Email :</strong></div>
                            <div class="col-xs-8 account-info">{{$user->email}}</div>
                        </div>
                        <div class="col-xs-12 m-t-5">
                            <div class="col-xs-4"><strong>State :</strong></div>
                            <div class="col-xs-8 account-info">{{$states[$user->state]}}</div>
                        </div>
                        <div class="col-xs-12 m-t-5">
                            <div class="col-xs-4"><strong>City :</strong></div>
                            <div class="col-xs-8 account-info">{{$user->city}}</div>
                        </div>
                        <div class="col-xs-12 m-t-5">
                            <div class="col-xs-4"><strong>Address :</strong></div>
                            <div class="col-xs-8 account-info">{{$user->address}}</div>
                        </div>
                        <div class="col-xs-12 m-t-5">
                            <div class="col-xs-4"><strong>Zip Code :</strong></div>
                            <div class="col-xs-8 account-info">{{$user->zip_code}}</div>
                        </div>
                        <div class="col-xs-12 m-t-5">
                            <div class="col-xs-4"><strong>Driver License :    </strong></div>
                            <div class="col-xs-8 account-info">{{$user->driver_license}}</div>
                        </div>
                        <div class="col-xs-12 m-t-5">
                            <div class="col-xs-4"><strong>ID number :</strong></div>
                            <div class="col-xs-8 account-info">{{$user->id_card}}</div>
                        </div>
                        <div class="col-xs-12 m-t-5">
                            <div class="col-xs-4"><strong>Date of Birth :</strong></div>
                            <div class="col-xs-8 account-info">{{isset($user->dob) ? date('m/d/Y', strtotime($user->dob)) : '' }}</div>
                        </div>
                        <div class="col-xs-12 m-t-5">
                            <div class="col-xs-4"><strong>Gender :</strong></div>
                            <div class="col-xs-8 account-info">{{$user->gender}}</div>
                        </div>
                        <div class="col-xs-12 m-t-5 m-b-20">
                            <div class="col-xs-12 "> <strong>Signature :</strong>
                                @if($user->signature_image)
                                <img src='{{ asset ("/files/profiles/patient-" .$user->id ."/". $user->signature_image)}}' width="160" height="80">
                                @endif
                                <input type="hidden" name="signature_image" value="{{session()->has('patientStep5')? session()->get('patientStep5')['signature_image']: ''}}" required> 
                            </div>
                        </div>
                           <div class="col-xs-12 m-t-5">
                            <div class="col-xs-8"></div>
                        </div>
                    </div>
                </div>
                <div class="row m-t-20">
                    <div class="col-xs-12 text-right">
                        <input type="hidden" name="user_id" value="{{session()->has('user_id')? session()->get('user_id'): ''}}">
                        <div class="col-xs-8">
                            <a href="{{ url('/auth/signup/step-4') }}" class="btn btn-primary btn-flat">Previous</a>
                        </div>
                        <button type="submit" class="btn btn-primary btn-flat">Complete Signup</button>
                    </div>
                    <!-- /.col -->
                </div>
            </form>
        </div>
    </div>
</div>
@endsection
