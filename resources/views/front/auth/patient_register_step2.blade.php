@extends('layouts.front.index')
@section('title', '| ' . trans('title.register'))
@section('content')
<div class="login-content">
    <div class="login-box">
        <div class="wizard-progress">
            <div class="step complete">
                Basic Info
                <div class="node"></div>
            </div>
            <div class="step in-progress">
                Addresses
                <div class="node"></div>
            </div>
            <div class="step">
                Licensing
                <div class="node"></div>
            </div>
            <div class="step">
                Profile details
                <div class="node"></div>
            </div>
            <div class="step">
                Signing
                <div class="node"></div>
            </div>
        </div>
        <div class="login-box-body">
            <p class="login-box-msg">{{trans('form.fill_your_data')}}</p>
            @if (count($errors) > 0)
            <div class="alert alert-danger">
                <ul>
                    @foreach ($errors->all() as $error)
                    <li>{{ $error }}</li>
                    @endforeach
                </ul>
            </div>
            @endif
            <form method="POST" action="{{ action('Auth\RegisterController@postPatientStep2') }}" data-toggle="validator" role="form" id="registrationForm" class="custom-validation-form">
                {!! csrf_field() !!}
                <div class="form-group">
                    <select class="form-control" id="states" name="state" required>
                        @foreach($states as $key => $state)
                        <option value="{{ $key  }}" {{(session()->has('patientStep2') && session()->get('patientStep2')['state'] == $key) ? 'selected': ''}}>{{ $state }}</option>
                        @endforeach
                    </select>
                    <div class="help-block with-errors"></div>
                </div>
                <div class="form-group">
                    <input type="text" name="city" value="{{session()->has('patientStep2')? session()->get('patientStep2')['city']: ''}}" class="form-control" id="city" placeholder="City" required>
                    <div class="help-block with-errors"></div>
                </div>
                <div class="form-group">
                    <input type="text" name="address" value="{{session()->has('patientStep2')? session()->get('patientStep2')['address']: ''}}" class="form-control" id="address" placeholder="Address" required>
                    <div class="help-block with-errors"></div>
                </div>
                <div class="form-group">
                    <input type="text" name="zip_code" value="{{session()->has('patientStep2')? session()->get('patientStep2')['zip_code']: ''}}" class="form-control" id="zipcode" placeholder="Zip Code" required>
                    <div class="help-block with-errors"></div>
                </div>
                <div class="form-group">
                    <input type="text" name="phone"  class="form-control" id="phone"  value="{{session()->has('patientStep2')? session()->get('patientStep2')['phone']: ''}}" placeholder="Phone" required>
                    <div class="help-block with-errors"></div>
                </div>
                <div class="row m-t-20">
                    <div class="col-xs-12 text-right">
                        <input type="hidden" name="user_id" value="{{session()->has('user_id')? session()->get('user_id'): ''}}">
                        <button type="submit" class="btn btn-primary btn-flat">Next</button>
                    </div>
                    <!-- /.col -->
                </div>
            </form>
        </div>
    </div>
</div>
@endsection
