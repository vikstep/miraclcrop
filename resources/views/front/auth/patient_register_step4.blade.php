@extends('layouts.front.index')
@section('title', '| ' . trans('title.register'))
@section('content')
<div class="login-content">
    <div class="login-box">
        <div class="wizard-progress">
            <div class="step complete">
                Basic Info
                <div class="node"></div>
            </div>
            <div class="step complete">
                Addresses
                <div class="node"></div>
            </div>
            <div class="step complete">
                Licensing
                <div class="node"></div>
            </div>
            <div class="step in-progress">
                Profile details
                <div class="node"></div>
            </div>
            <div class="step">
                Signing
                <div class="node"></div>
            </div>
        </div>
        <div class="login-box-body">
            <p class="login-box-msg">{{trans('form.fill_your_data')}}</p>
            @if (count($errors) > 0)
            <div class="alert alert-danger">
                <ul>
                    @foreach ($errors->all() as $error)
                    <li>{{ $error }}</li>
                    @endforeach
                </ul>
            </div>
            @endif
            <form method="POST" action="{{ action('Auth\RegisterController@postPatientStep4') }}" data-toggle="validator" role="form" id="registrationForm" enctype="multipart/form-data" class="custom-validation-form">
                {!! csrf_field() !!}
                <div class="form-group">
                    <label>Profile Picture</label>
                    <input type="file" name="profile_picture" value="" class="m-t-5" id="profile_picture" {{$user->face_photo? '':'required'}}>
                    <div class="help-block with-errors"></div>
                    @if($user->face_photo)
                    <img src='{{ asset ("/files/profiles/patient-" .$user->id ."/". $user->face_photo)}}' width="160" height="160">
                    @endif
                </div>
                 <div class="form-group">
                    <div class="row">
                        <div class="form-group col-xs-4 p-r-0">
                            <select name="dob_month" class="form-control" required>
                                <option value=''>-- Month --</option>
                                @foreach($months as $key => $month)
                                <option value="{{ $key }}" {{(session()->has('patientStep4') && session()->get('patientStep4')['month'] == $key)? 'selected': ''}}>{{ $month }}</option>
                                @endforeach
                            </select>
                            <div class="help-block with-errors"></div>
                        </div>
                        <div class="form-group col-xs-4 p-r-0">
                            <select name="dob_day" class="form-control" required>
                                <option value=''>-- Day --</option>
                                @foreach($days as $day)
                                <option value="{{ $day }}" {{(session()->has('patientStep4') && session()->get('patientStep4')['day'] == $day)? 'selected': ''}}>{{ $day }}</option>
                                @endforeach
                            </select>
                            <div class="help-block with-errors"></div>
                        </div>
                        <div class="form-group col-xs-4">
                            <select name="dob_year" class="form-control" required>
                                <option value=''>-- Year --</option>
                                @foreach($years as $year)
                                <option value="{{ $year }}" {{(session()->has('patientStep4') && session()->get('patientStep4')['year'] == $year)? 'selected': ''}} >{{ $year }}</option>
                                @endforeach
                            </select>
                            <div class="help-block with-errors"></div>
                        </div>
                    </div>
                </div>
                <div class="form-group">
                    <div class="radio radio-inline p-0">
                        <label>
                            <input type="radio" name="gender" value="male" id="genderMale" required {{(session()->has('patientStep4') && session()->get('patientStep4')['gender'] == 'male')? 'checked': ''}}>
                                   Male
                        </label>
                        <label class="m-l-20">
                            <input type="radio" name="gender" value="female" id="genderFemale" required {{(session()->has('patientStep4') && session()->get('patientStep4')['gender'] == 'female')? 'checked': ''}} >
                                   Female
                        </label>
                    </div>
                    <div class="help-block with-errors"></div>
                </div>
                <div class="row m-t-20">
                    <div class="col-xs-12 text-right">
                        <input type="hidden" name="user_id" value="{{session()->has('user_id')? session()->get('user_id'): ''}}">
                        <div class="col-xs-10">
                            <a href="{{ url('/auth/signup/step-3') }}" class="btn btn-primary btn-flat">Previous</a>
                        </div>
                        <button type="submit" class="btn btn-primary btn-flat">Next</button>
                    </div>
                    <!-- /.col -->
                </div>
            </form>
        </div>
    </div>
</div>
@endsection
