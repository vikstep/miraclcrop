@extends('layouts.front.index')
@section('title', '| ' . trans('title.register'))
@section('content')
<div class="login-content">
    <div class="login-box">
        <div class="login-box-body">
            <div class="row">
                <div class="col-xs-12 text-center">
                    <p>Are you</p>
                </div>
                <div class="col-xs-12 m-t-30">
                    <div class="col-xs-4">
                        <a href="{{ action('Auth\RegisterController@getPatientStep1') }}" class="btn btn-danger pull-left f-s-21">Patient</a>
                    </div>
                    <div class="col-xs-4 text-center">
                        <p>OR</p>
                    </div>
                    <div class="col-xs-4">
                         <a href="{{ action('Auth\RegisterController@getDoctorStep1') }}" class="btn btn-danger pull-right f-s-21">Doctor</a>
                    </div>
                </div>
            </div>
        </div>
    </div>
</div>
@endsection
