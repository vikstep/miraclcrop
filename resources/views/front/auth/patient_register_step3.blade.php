@extends('layouts.front.index')
@section('title', '| ' . trans('title.register'))
@section('content')
<div class="login-content">
    <div class="login-box">
        <div class="wizard-progress">
            <div class="step complete">
                Basic Info
                <div class="node"></div>
            </div>
            <div class="step complete">
                Addresses
                <div class="node"></div>
            </div>
            <div class="step in-progress">
                Licensing
                <div class="node"></div>
            </div>
            <div class="step">
                Profile details
                <div class="node"></div>
            </div>
            <div class="step">
                Signing
                <div class="node"></div>
            </div>
        </div>
        <div class="login-box-body">
            <p class="login-box-msg">{{trans('form.fill_your_data')}}</p>
            @if (count($errors) > 0)
            <div class="alert alert-danger">
                <ul>
                    @foreach ($errors->all() as $error)
                    <li>{{ $error }}</li>
                    @endforeach
                </ul>
            </div>
            @endif
            <form method="POST" action="{{ action('Auth\RegisterController@postPatientStep3') }}" data-toggle="validator" role="form" id="registrationForm" enctype="multipart/form-data" class="custom-validation-form">
                {!! csrf_field() !!}
                <div class="form-group">
                    <input type="text" name="driver_license" value="{{session()->has('patientStep3')? session()->get('patientStep3')['driver_license']: ''}}" class="form-control" id="driver_license" placeholder="Driver License" required>
                    <div class="help-block with-errors"></div>
                </div>
                <div class="form-group">
                    <input type="file" name="driver_license_file" value="" class="m-t-5" id="driver_license_file" {{$user->driver_license_image ? '':'required'}}>
                    <div class="help-block with-errors"></div>
                    @if($user->driver_license_image)
                    <img src='{{ asset ("/files/profiles/patient-" .$user->id ."/". $user->driver_license_image)}}' width="160" height="160">
                    @endif
                </div>
                <div class="form-group">
                    <input type="text" name="id_card" value="{{session()->has('patientStep3')? session()->get('patientStep3')['id_card']: ''}}" class="form-control" id="id" placeholder="ID" required>
                    <div class="help-block with-errors"></div>
                </div>
                <div class="form-group">
                    <input type="file" name="id_file" value="" class="m-t-5" id="id_file" {{$user->id_card_image ? '':'required'}}>
                    <div class="help-block with-errors"></div>
                    @if($user->id_card_image)
                    <img src='{{ asset ("/files/profiles/patient-" .$user->id ."/". $user->id_card_image)}}' width="160" height="160">
                    @endif
                </div>
                <div class="form-group">
                    <label>Patient Recommendation</label>
                    <input type="file" name="patient_recommendation" value="" class="m-t-5" id="patient_recommendation">
                    @if($user->recommendation_image)
                    <img src='{{ asset ("/files/profiles/patient-" .$user->id ."/". $user->recommendation_image)}}' width="160" height="160">
                    @endif
                </div>
                <div class="row m-t-20">
                    <div class="col-xs-12 text-right">
                        <input type="hidden" name="user_id" value="{{session()->has('user_id')? session()->get('user_id'): ''}}">
                        <div class="col-xs-10">
                            <a href="{{ url('/auth/signup/step-2') }}" class="btn btn-primary btn-flat">Previous</a>
                        </div>
                        <button type="submit" class="btn btn-primary btn-flat">Next</button>
                    </div>
                    <!-- /.col -->
                </div>
            </form>
        </div>
    </div>
</div>
@endsection
