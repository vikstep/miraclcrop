@extends('layouts.front.index')
@section('title', '| ' . trans('title.password_forgot'))
@section('content')
<div class="login-content">
    <div class="login-box">
        <div class="login-box-body">
            @if (session('status'))
            <div class="alert alert-success">
                {{ session('status') }}
            </div>
            @endif
            <p class="login-box-msg">Fill in your email to reset your password</p>
            @if (count($errors) > 0)
            <div class="alert alert-danger">
                <ul>
                    @foreach ($errors->all() as $error)
                    <li>{{ $error }}</li>
                    @endforeach
                </ul>
            </div>
            @endif
            <form method="POST" action="{{ action('Auth\ForgotPasswordController@sendResetLinkEmail') }}" data-toggle="validator">
                {!! csrf_field() !!}
                <div class="form-group has-feedback">
                    <input type="email" name="email"  class="form-control" id="email" placeholder="E-mail"  required >
                    <div class="help-block with-errors"></div>
                </div>
                <div class="row">
                    <!-- /.col -->
                    <div class="col-xs-4 pull-right">
                        <button type="submit" class="btn btn-primary btn-block btn-flat">Send</button>
                    </div>
                    <!-- /.col -->
                </div>
            </form>
        </div>
    </div>
</div>
@endsection
