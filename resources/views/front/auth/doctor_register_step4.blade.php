@extends('layouts.front.index')
@section('title', '| ' . trans('title.register'))
@section('content')
<div class="login-content">
    <div class="login-box">
        <div class="wizard-progress">
            <div class="step complete">
                {{trans('content.basic_info')}}
                <div class="node"></div>
            </div>
            <div class="step complete">
                {{trans('content.more_info')}}
                <div class="node"></div>
            </div>
            <div class="step complete">
                {{trans('content.licensing')}}
                <div class="node"></div>
            </div>
            <div class="step in-progress">
                {{trans('content.signing')}}
                <div class="node"></div>
            </div>
        </div>
        <div class="login-box-body">
            @if (count($errors) > 0)
            <div class="alert alert-danger">
                <ul>
                    @foreach ($errors->all() as $error)
                    <li>{{ $error }}</li>
                    @endforeach
                </ul>
            </div>
            @endif
            <form method="POST" action="{{ action('Auth\RegisterController@postDoctorStep4') }}" data-toggle="validator" role="form" id="form_sig_plus" class="custom-validation-form">
                {!! csrf_field() !!}
                <div class='form-group'>
                    <div id="signature-pad" class="m-signature-pad">
                        <div class="m-signature-pad--body">
                            <canvas class="signature-class"></canvas>
                        </div>
                        <div class="m-signature-pad--footer">
                            <div class="description">{{trans('content.sign')}} {{trans('content.above')}}</div>
                            <button type="button" class="button clear" data-action="clear">{{trans('content.clear')}}</button>
                            <button type="button" class="button save" data-action="save">{{trans('content.sign')}}</button>
                        </div>
                    </div>
                </div>
                <div class="form-group bg-white">
                    <div class="row">
                        <div class="col-xs-12 m-t-10">
                            <div class="col-xs-4"><strong>{{trans('content.user_name')}} :</strong></div>
                            <div class="col-xs-8 account-info">{{$user->username}}</div>
                        </div>
                        <div class="col-xs-12 m-t-5">
                            <div class="col-xs-4"><strong>{{trans('content.first_name')}} :</strong></div>
                            <div class="col-xs-8 account-info">{{$user->first_name}}</div>
                        </div>
                        <div class="col-xs-12 m-t-5">
                            <div class="col-xs-4"><strong>{{trans('content.last_name')}} :</strong></div>
                            <div class="col-xs-8 account-info">{{$user->last_name}}</div>
                        </div>
                        <div class="col-xs-12 m-t-5">
                            <div class="col-xs-4"><strong>{{trans('content.email')}} :</strong></div>
                            <div class="col-xs-8 account-info">{{$user->email}}</div>
                        </div>
                        <div class="col-xs-12 m-t-5">
                            <div class="col-xs-4"><strong>{{trans('content.state')}} :</strong></div>
                            <div class="col-xs-8 account-info">{{$states[$user->state]}}</div>
                        </div>
                        <div class="col-xs-12 m-t-5">
                            <div class="col-xs-4"><strong>{{trans('content.city')}} :</strong></div>
                            <div class="col-xs-8 account-info">{{$user->city}}</div>
                        </div>
                        <div class="col-xs-12 m-t-5">
                            <div class="col-xs-4"><strong>{{trans('content.address')}} :</strong></div>
                            <div class="col-xs-8 account-info">{{$user->address}}</div>
                        </div>
                        <div class="col-xs-12 m-t-5">
                            <div class="col-xs-4"><strong>{{trans('content.zip_code')}} :</strong></div>
                            <div class="col-xs-8 account-info">{{$user->zip_code}}</div>
                        </div>
                        <div class="col-xs-12 m-t-5">
                            <div class="col-xs-4"><strong>{{trans('content.phone')}} :</strong></div>
                            <div class="col-xs-8 account-info">{{$user->phone}}</div>
                        </div>
                        <div class="col-xs-12 m-t-5">
                            <div class="col-xs-4"><strong>{{trans('content.driver_license')}} :</strong></div>
                            <div class="col-xs-8 account-info">{{$user->driver_license}}</div>
                        </div>
                        <div class="col-xs-12 m-t-5">
                            <div class="col-xs-4"><strong>{{trans('content.date_of_birth')}} :</strong></div>
                            <div class="col-xs-8 account-info">{{isset($user->dob) ? date('m/d/Y', strtotime($user->dob)) : '' }}</div>
                        </div>
                        <div class="col-xs-12 m-t-5">
                            <div class="col-xs-4"><strong>{{trans('content.medical_license')}}:</strong></div>
                            <div class="col-xs-8"> 
                                <div class="row">
                                    <table class="table table-bordered">
                                        <thead>
                                        <th>{{trans('content.registration_state')}}</th>
                                        <th>{{trans('content.medical_license')}}</th>
                                        <th>{{trans('content.valid_until')}}</th>
                                        </thead>
                                        <tbody class="license-body">
                                            @if($user->doctorLicense)
                                            @foreach($user->doctorLicense as $license)
                                            <tr>

                                                <td>{{$states[$license->registration_state]}}</td>
                                                <td>{{$license->medical_license}}</td>
                                                <td>{{$license->valid_until}}</td>
                                            </tr>
                                            @endforeach
                                            @endif
                                        </tbody>
                                    </table>
                                </div>
                            </div>
                        </div>
                        <div class="col-xs-12 m-t-5 m-b-20">
                            <div class="col-xs-12 "> <strong>{{trans('content.signature')}} :</strong>
                                @if($user->signature_image)
                                <img src='{{ asset ("/files/profiles/doctor-". $user->id ."/".$user->signature_image)}}' width="160" height="80">
                                @endif
                                <input type="hidden" name="signature_image" value="{{session()->has('doctorStep4')? session()->get('doctorStep4')['signature_image']: ''}}" required> 
                            </div>
                        </div>
                        <div class="col-xs-12 m-t-5">
                            <div class="col-xs-8"></div>
                        </div>
                    </div>
                </div>
                <div class="row m-t-20">
                    <div class="col-xs-12 text-right">
                        <input type="hidden" name="user_id" value="{{session()->has('user_id')? session()->get('user_id'): ''}}">
                        <div class="col-xs-8">
                            <a href="{{ url('/auth/signup/step-3') }}" class="btn btn-primary btn-flat">{{trans('content.previous')}}</a>
                        </div>
                        <button type="submit" class="btn btn-primary btn-flat">{{trans('content.complete_signup')}}</button>
                    </div>
                </div>
                <!-- /.col -->

            </form>
        </div>
    </div>
</div>
@endsection
