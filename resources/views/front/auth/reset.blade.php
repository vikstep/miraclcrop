@extends('layouts.front.index')
@section('title', '| ' . trans('title.password_reset'))
@section('content')
<div class="login-content">
    <div class="login-box">
        <div class="login-box-body">
            <p class="login-box-msg">Fill your password</p>
            @if (count($errors) > 0)
            <div class="alert alert-danger">
                <ul>
                    @foreach ($errors->all() as $error)
                    <li>{{ $error }}</li>
                    @endforeach
                </ul>
            </div>
            @endif
            <form method="POST" action="{{ action('Auth\ResetPasswordController@reset') }}" data-toggle="validator">
                {!! csrf_field() !!}
                <input type="hidden" name="token" value="{{ $token }}">
                <div class="form-group">
                    <input type="email" name="email" value="{{ old('email') }}" class="form-control" id="inputEmail" placeholder="Email" required>
                    <div class="help-block with-errors"></div>
                </div>
                <div class="form-group">
                    <input type="password" name="password" data-minlength="3"  class="form-control" id="inputPassword" placeholder="Password" required>
                    <div class="help-block with-errors"></div>
                </div>
                <div class="form-group">
                    <input type="password" name="password_confirmation"  class="form-control" id="inputConfirmPassword" placeholder="Confirm Password" data-match="#inputPassword" data-match-error="Whoops, these don't match" required>
                    <div class="help-block with-errors"></div>
                </div>
                <div class="row">
                    <!-- /.col -->
                    <div class="col-xs-4 pull-right">
                        <button type="submit" class="btn btn-primary btn-block btn-flat">Reset</button>
                    </div>
                    <!-- /.col -->
                </div>
            </form>
        </div>
    </div>
</div>
@endsection
