@extends('layouts.front.index')
@section('title', '| ' . trans('title.register'))
@section('content')
<div class="login-content">
    <div class="login-box">
        <div class="wizard-progress">
            <div class="step complete">
                {{trans('content.basic_info')}}
                <div class="node"></div>
            </div>
            <div class="step complete">
               {{trans('content.more_info')}}
                <div class="node"></div>
            </div>
            <div class="step in-progress">
                {{trans('content.licensing')}}
                <div class="node"></div>
            </div>
            <div class="step">
                 {{trans('content.signing')}}
                <div class="node"></div>
            </div>
        </div>
        <div class="login-box-body">
            <p class="login-box-msg">{{trans('form.fill_your_data')}}</p>
            @if (session('error'))

            <div class="alert alert-danger">
                <ul>
                    @foreach (session('error') as $error)
                    <li>{{ $error }}</li>
                    @endforeach
                </ul>
            </div>
            @endif
            @if (count($errors) > 0)
            <div class="alert alert-danger">
                <ul>
                    @foreach ($errors->all() as $error)
                    <li>{{ $error }}</li>
                    @endforeach
                </ul>
            </div>
            @endif
            <form method="POST" action="{{ action('Auth\RegisterController@postDoctorLicense') }}" data-toggle="validator" role="form" id="licenseForm" enctype="multipart/form-data" class="custom-validation-form">
                {!! csrf_field() !!}
                <div class="form-group">
                    <div class="row">
                        <div class="form-group col-md-3 p-0">
                            <label>Registration State</label>
                            <select class="form-control" id="states" name="registration_state" required>
                                @foreach($states as $key => $state)
                                <option value="{{ $key  }}">{{ $state }}</option>
                                @endforeach
                            </select>
                            <div class="help-block with-errors"></div>
                        </div>

                        <div class="form-group col-md-4 p-0">
                            <label>{{trans('content.medical_license')}}</label>
                            <input type="text"  value="" class="form-control" name="medical_license" id="medical_license" data-profile_license="" placeholder="{{trans('content.medical_license')}}" required>
                            <div class="help-block with-errors"></div>
                        </div>

                        <div class="form-group col-md-3 p-0">
                            <label>{{trans('content.valid_until')}}</label>
                            <input type="text" name="valid_until" value="" class="form-control" name="valid_until" id="valid_until" placeholder="{{trans('content.valid_until')}}" required>
                            <div class="help-block with-errors"></div>
                        </div>

                        <div class="col-md-1 p-0">
                            <label>Upload</label>
                            <label for="valid_license" class="btn btn-primary valid_license_lable"><span class="glyphicon glyphicon-upload"></span></label>
                            <input type="file" name="license_image" id="valid_license" class="hidden">
                        </div>
                        <div class="col-md-1 m-t-23">
                            <button type="submit" class="btn btn-danger add-license"><span class="glyphicon glyphicon-plus"></span></button>
                        </div>
                    </div>
                </div>
            </form>
            <div class="form-group">
                <table class="table table-bordered">
                    <thead class="alert-info">
                    <th>{{trans('content.registration_state')}}</th>
                    <th>{{trans('content.medical_license')}}</th>
                    <th>{{trans('content.valid_until')}}</th>
                    <th>{{trans('content.license_image')}}</th>
                    </thead>
                    <tbody class="bg-white license-body">
                        @if($user->doctorLicense)
                        @foreach($user->doctorLicense as $license)
                        <tr>

                            <td>{{$states[$license->registration_state]}}</td>
                            <td>{{$license->medical_license}}</td>
                            <td>{{$license->valid_until}}</td>
                            <td>
                                @if($license->license_image)
                                <img src='{{ asset ("/files/profiles/doctor-". $user->id ."/".$license->license_image)}}' width="50" >
                                @endif
                            </td>
                        </tr>
                        @endforeach
                        @endif
                    </tbody>
                </table>
            </div>
            <form method="POST" action="{{ action('Auth\RegisterController@postDoctorStep3') }}" data-toggle="validator" role="form" id="registrationForm" enctype="multipart/form-data" class="custom-validation-form">
                {!! csrf_field() !!}
                <div class="form-group">
                    <input type="text" name="driver_license" value="{{session()->has('doctorStep3')? session()->get('doctorStep3')['driver_license']: ''}}" class="form-control" id="driver_license" placeholder="{{trans('content.driver_license')}}" required>
                    <div class="help-block with-errors"></div>
                </div>
                <div class="form-group">
                    <input type="file" name="driver_license_file" class="m-t-5" id="driver_license_file" required>
                    <div class="help-block with-errors"></div>
                    @if($user->driver_license_image)
                    <img src='{{ asset ("/files/profiles/doctor-". $user->id ."/".$user->driver_license_image)}}' width="160" height="160">
                    @endif
                </div>
                <div class="form-group">
                    <label>{{trans('content.face_photo')}}</label>
                    <input type="file" name="face_photo"  class="m-t-5" id="face_photo" required>
                    <div class="help-block with-errors"></div>
                    @if($user->face_photo)
                    <img src='{{ asset ("/files/profiles/doctor-". $user->id ."/".$user->face_photo)}}' width="160" height="160">
                    @endif
                </div>
                <div class="form-group">
                    <div class="radio radio-inline p-0">
                        <label>
                            <input type="radio" name="gender" value="male" id="genderMale" required {{(session()->has('doctorStep3') && session()->get('doctorStep3')['gender'] == 'male')? 'checked': ''}}>
                                  {{trans('content.male')}}
                        </label>
                        <label class="m-l-20">
                            <input type="radio" name="gender" value="female" id="genderFemale" required {{(session()->has('doctorStep3') && session()->get('doctorStep3')['gender'] == 'female')? 'checked': ''}} >
                                   {{trans('content.female')}}
                        </label>
                    </div>
                    <div class="help-block with-errors"></div>
                </div>
                <div class="row m-t-20">
                    <div class="col-xs-12 text-right">
                        <input type="hidden" name="user_id" value="{{session()->has('user_id')? session()->get('user_id'): ''}}">
                        <div class="col-xs-10">
                            <a href="{{ url('/auth/signup/step-2') }}" class="btn btn-primary btn-flat">{{trans('content.previous')}}</a>
                        </div>
                        <button type="submit" class="btn btn-primary btn-flat">{{trans('content.next')}}</button>
                    </div>
                    <!-- /.col -->
                </div>
            </form>
        </div>
    </div>
</div>
@endsection
