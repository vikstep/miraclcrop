@extends('layouts.front.index')
@section('title', '| ' . trans('title.login'))
@section('content')
<div class="login-content">
    <div class="login-box">
        <div class="login-box-body">
            @if (session('status'))
            <div class="alert alert-success">
                {{ session('status') }}
            </div>
            @endif
            <p class="login-box-msg">{{trans('content.start_session')}}</p>
            @if (count($errors) > 0)
            <div class="alert alert-danger">
                <ul>
                    @foreach ($errors->all() as $error)
                    <li>{{ $error }}</li>
                    @endforeach
                </ul>
            </div>
            @endif
            <form method="POST" action="{{ action('Auth\LoginController@postLogin') }}" data-toggle="validator">
                {!! csrf_field() !!}
                <div class="form-group has-feedback">
                    <input type="email" name="email" value="" class="form-control" id="inputEmail" placeholder="Email" required>
                    <span class="glyphicon glyphicon-envelope form-control-feedback"></span>
                    <div class="help-block with-errors"></div>
                </div>
                <div class="form-group has-feedback">
                    <input type="password" name="password" class="form-control" id="inputPassword" placeholder="{{trans('form.password')}}" required>
                    <span class="glyphicon glyphicon-lock form-control-feedback"></span>
                    <div class="help-block with-errors"></div>
                </div>
                <div class="form-group has-feedback text-right">
                    <a href="{{ action('Auth\ForgotPasswordController@showLinkRequestForm') }}" class="forgot-password">{{trans('title.password_forgot')}}?</a>
                </div>
                <div class="row">
                    <div class="col-xs-8">
                        <div class="checkbox icheck">
                            <label>
                                <input type="checkbox"> {{trans('content.remember_me')}}
                            </label>
                        </div>
                    </div>
                    <!-- /.col -->
                    <div class="col-xs-4">
                        <button type="submit" class="btn btn-primary btn-block btn-flat">{{trans('content.sign_in')}}</button>
                    </div>
                    <!-- /.col -->
                </div>
            </form>
        </div>
    </div>
</div>
@endsection
