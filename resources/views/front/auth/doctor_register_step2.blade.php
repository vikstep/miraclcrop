@extends('layouts.front.index')
@section('title', '| ' . trans('title.register'))
@section('content')
<div class="login-content">
    <div class="login-box">
        <div class="wizard-progress">
            <div class="step complete">
               {{trans('content.basic_info')}}
                <div class="node"></div>
            </div>
            <div class="step in-progress">
                {{trans('content.more_info')}}
                <div class="node"></div>
            </div>
            <div class="step">
                {{trans('content.licensing')}}
                <div class="node"></div>
            </div>
            <div class="step">
               {{trans('content.signing')}}
                <div class="node"></div>
            </div>
        </div>
        <div class="login-box-body">
            <p class="login-box-msg">{{trans('form.fill_your_data')}}</p>
            @if (count($errors) > 0)
            <div class="alert alert-danger">
                <ul>
                    @foreach ($errors->all() as $error)
                    <li>{{ $error }}</li>
                    @endforeach
                </ul>
            </div>
            @endif
            <form method="POST" action="{{ action('Auth\RegisterController@postDoctorStep2') }}" data-toggle="validator" role="form" id="registrationForm" class="custom-validation-form">
                {!! csrf_field() !!}
                <div class="form-group">
                    <select class="form-control" id="states" name="state" required>
                        @foreach($states as $key => $state)
                        <option value="{{ $key  }}" {{(session()->has('doctorStep2') && session()->get('doctorStep2')['state'] == $key) ? 'selected': ''}}>{{ $state }}</option>
                        @endforeach
                    </select>
                    <div class="help-block with-errors"></div>
                </div>
                <div class="form-group">
                    <input type="text" name="city"  class="form-control" id="city" value="{{session()->has('doctorStep2')? session()->get('doctorStep2')['city']: ''}}" placeholder="{{trans('content.city')}}" required>
                    <div class="help-block with-errors"></div>
                </div>
                <div class="form-group">
                    <input type="text" name="address"  class="form-control" id="address" value="{{session()->has('doctorStep2')? session()->get('doctorStep2')['address']: ''}}"  placeholder="{{trans('content.address')}}" required>
                    <div class="help-block with-errors"></div>
                </div>
                <div class="form-group">
                    <input type="text" name="zip_code"  class="form-control" id="zipcode" value="{{session()->has('doctorStep2')? session()->get('doctorStep2')['zip_code']: ''}}" placeholder="{{trans('content.zip_code')}}" required>
                    <div class="help-block with-errors"></div>
                </div>
                <div class="form-group">
                    <input type="text" name="phone"  class="form-control" id="phone"  value="{{session()->has('doctorStep2')? session()->get('doctorStep2')['phone']: ''}}" placeholder="{{trans('content.phone')}}" required>
                    <div class="help-block with-errors"></div>
                </div>
                <div class="row m-t-20">
                    <input type="hidden" name="user_id" value="{{session()->has('user_id')? session()->get('user_id'): ''}}">
                    <div class="col-xs-12 text-right">
                        <button type="submit" class="btn btn-primary btn-flat">{{trans('content.next')}}</button>
                    </div>
                    <!-- /.col -->
                </div>
            </form>
        </div>
    </div>
</div>
@endsection
