@extends((Auth::check() && auth()->user()->role !='admin')  ? config('layout.'.auth()->user()->role): 'layouts.front.index')
@section('title', '| ' . trans('title.my_wallet'))
@section('content')
<!-- Portfolio Grid Section -->
<section id="portfolio" class="blog-list">
    <div class="container">
        <ul class="nav nav-tabs" role="tablist">
            <li role="presentation"><a href="{{ URL::to('/profile') }}">{{trans('title.profile_info')}}</a></li>
            <li role="presentation"><a href="{{ URL::to('/profile/edit') }}">{{trans('title.edit_profile')}}</a></li>
            <li role="presentation"><a href="{{ URL::to('/profile/social') }}" >{{trans('title.profile_social')}}</a></li>
            <li role="presentation"><a href="{{ URL::to('/profile/addresses') }}">{{trans('title.addresses')}}</a></li>
            <li role="presentation" class="active"><a href="javscript:void(0)" >{{trans('title.my_wallet')}}</a></li>
            <li role="presentation"><a href="{{ URL::to('/profile/purchase-history') }}" >{{trans('title.purchase_history')}}</a></li>
        </ul>
        <div class="tab-content">
            <div role="tabpanel" class="tab-pane active">
                <h3>{{trans('content.my_wallet')}}</h3>
                <div class="text-right m-t-10 m-b-10"><h6 style="display: inline-block">{{trans('content.current_balance')}}: </h6> <h4 style="display: inline-block">{{ auth()->user()->wallet_points }}</h4> <h6 style="display: inline-block">{{trans('content.points')}}</h6></div>
                <div class="row">
                    <div class="checkout">
                        @if (session('success'))
                        <div class="alert alert-success">
                            {{ session('success') }}
                        </div>
                        @endif
                        
                        @if (count($errors) > 0)
                        <div class="alert alert-danger">
                            <ul>
                                @foreach ($errors->all() as $error)
                                <li>{{ $error }}</li>
                                @endforeach
                            </ul>
                        </div>
                        @endif
                        <form method="post" id="payment-form" action="{{ action('Front\AccountController@postMyWallet') }}" data-toggle="validator" role="form" class="custom-validation-form">
                            {!! csrf_field() !!}
                            <div class="form-group m-b-20">
                                <div class="col-md-3 input-wrapper amount-wrapper">
                                    <label for="amount">{{trans('content.point')}}</label>
                                    <input id="amount" type="text"   placeholder="Amount" class="form-control input-number" required>
                                    <div class="text-danger errors"></div>
                                    <div class="help-block with-errors"></div>
                                </div>
                                <div class="col-md-3 m-t-23">
                                    <h6  style="display: inline-block"> {{trans('content.points')}}</h6> <h4 style="display: inline-block">  =  <span class="point-count">0</span> $</h4>
                                    <input  name="amount" id="amount_value" type="hidden" >
                                </div>
                            </div>
                            <div class="clearfix"></div>
                            <div class="form-group m-t-20">
                                <div class="bt-drop-in-wrapper">
                                    <div id="bt-dropin"></div>
                                </div>
                            </div>

                            <div class="form-group">
                                <button class="btn btn-success" type="submit"><span>{{trans('content.add_point')}}</span></button>
                            </div>
                        </form>
                    </div>

                    <script>
                        var client_token = "{{ $generate }}";
                    </script>
                </div>
            </div>
        </div>
</section>

@endsection

