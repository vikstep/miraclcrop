@extends((Auth::check() && auth()->user()->role !='admin')  ? config('layout.'.auth()->user()->role): 'layouts.front.index')
@section('title', '| Blog - ' . trans('title.newest'))
@section('content')



<!-- Portfolio Grid Section -->
<section id="portfolio" class="blog-list">
    <div class="container">
        <h3>Category: {{ trans('blog_category.' . $category_id . '.title') }}</h3>
        @if(count($posts) > 0)
        @foreach($posts as $post)
        <div class="media">
            <div class="media-left">
                <a href="#">
                    <img class="media-object" src="{{ asset('/files/blog/'.date('m.Y').'/' . $post->file_name . '-s.' . $post->file_type) }}" alt="{{ trans('post.' . $post->id . '.title') }}">
                </a>
            </div>
            <div class="media-body">
                <h4 class="media-heading">{{ trans('post.' . $post->id . '.title') }}</h4>
                {!! trans('post.' . $post->id . '.excerpt') !!}
                <div class="text-left"><small>{{ \Carbon\Carbon::parse($post->defined_created_date)->format('m/d/Y') }}</small></div>
                <div class="text-right"><a href="{{ URL::to('/blog/' . $post->id . '/' . slugify(trans('post.' . $post->id . '.title'))) }}">{{trans('content.read_more')}}</a></div>
            </div>
        </div>
        @endforeach
        @endif
    </div>
</section>

@endsection