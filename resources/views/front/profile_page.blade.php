@extends((Auth::check() && auth()->user()->role !='admin')  ? config('layout.'.auth()->user()->role): 'layouts.front.index')
@section('title', '| ' . trans('title.my_profile'))
@section('content')

<!-- Portfolio Grid Section -->
<section id="portfolio" class="blog-list">
    <div class="container">
        <ul class="nav nav-tabs" role="tablist">
            <li role="presentation" class="active"><a href="javascript:void(0)" >{{trans('title.profile_info')}}</a></li>
            <li role="presentation"><a href="{{ URL::to('/profile/edit') }}" >{{trans('title.edit_profile')}}</a></li>
            <li role="presentation"><a href="{{ URL::to('/profile/social') }}" >{{trans('title.profile_social')}}</a></li>
            <li role="presentation"><a href="{{ URL::to('/profile/addresses') }}" >{{trans('title.addresses')}}</a></li>
            <li role="presentation"><a href="{{ URL::to('/profile/my-wallet') }}" >{{trans('title.my_wallet')}}</a></li>
            <li role="presentation"><a href="{{ URL::to('/profile/purchase-history') }}" >{{trans('title.purchase_history')}}</a></li>
        </ul>
        <div class="tab-content">
            <div role="tabpanel" class="tab-pane active" id="profile-info">
                <h3>{{trans('title.profile_info')}}</h3>
                <div class="x">
                    <div class="">

                        <div class="row">
                            <div class="col-xs-12 col-md-3">
                                <div><img src="{{ auth()->user()->face_photo ? '/files/profiles/' . auth()->user()->role . '-' . auth()->user()->id . '/' . auth()->user()->face_photo : '/files/avatar/default-avatar.png' }}" class="profile-image img-thumbnail"></div>
                            </div>
                            <div class="col-xs-12 col-md-9">
                                <div class="row profile-info-row">
                                    <div class="col-xs-12 col-md-4 profile-info-title">{{trans('content.email')}}</div>
                                    <div class="col-xs-12 col-md-8 profile-info-text">{{ auth()->user()->email }}</div>
                                </div>
                                <div class="row profile-info-row">
                                    <div class="col-xs-12 col-md-4 profile-info-title">{{trans('content.first_name')}}</div>
                                    <div class="col-xs-12 col-md-8 profile-info-text">{{ auth()->user()->first_name }}</div>
                                </div>
                                <div class="row profile-info-row">
                                    <div class="col-xs-12 col-md-4 profile-info-title">{{trans('content.last_name')}}</div>
                                    <div class="col-xs-12 col-md-8 profile-info-text">{{ auth()->user()->last_name }}</div>
                                </div>
                                <div class="row profile-info-row">
                                    <div class="col-xs-12 col-md-4 profile-info-title">{{trans('content.date_of_birth')}}</div>
                                    <div class="col-xs-12 col-md-8 profile-info-text">{{ date('m/d/Y', strtotime(auth()->user()->dob)) }}</div>
                                </div>
                                <div class="row profile-info-row">
                                    <div class="col-xs-12 col-md-4 profile-info-title">{{trans('content.gender')}}</div>
                                    <div class="col-xs-12 col-md-8 profile-info-text ">{{ ucfirst(auth()->user()->gender) }}</div>
                                </div>
                                <div class="row profile-info-row">
                                    <div class="col-xs-12 col-md-4 profile-info-title">{{trans('content.driver_license')}}</div>
                                    <div class="col-xs-12 col-md-8 profile-info-text">
                                        {{ auth()->user()->driver_license }}
                                        {!!  auth()->user()->driver_license_image ? '<img class="right-img-x" src="/files/profiles/' . auth()->user()->role . '-' . auth()->user()->id . '/' . auth()->user()->driver_license_image . '">' : '' !!}
                                    </div>
                                </div>
                                @if(auth()->user()->role == 'doctor')
                                <div class="row profile-info-row">
                                    <div class="col-xs-12 col-md-4 profile-info-title">{{trans('content.phone')}}</div>
                                    <div class="col-xs-12 col-md-8 profile-info-text">{{ auth()->user()->phone }}</div>
                                </div>
                                <div class="row">
                                    <div class="col-xs-12 profile-info-title">{{trans('content.licenses')}}</div>
                                    <div class="col-xs-12">
                                        <div class="table-responsive">
                                            <table class="table table-stripped">
                                                <thead>
                                                    <tr>
                                                        <th>{{trans('content.state')}}</th>
                                                        <th>{{trans('content.license_number')}}</th>
                                                        <th>{{trans('content.valid_until')}}</th>
                                                        <th>{{trans('content.license_image')}}</th>
                                                    </tr>
                                                </thead>
                                                <tbody>
                                                    @if(isset($doctorLicense) && !empty($doctorLicense))
                                                    @foreach ($doctorLicense as $license)
                                                    <tr>
                                                        <td>{{ isset($states[$license->registration_state]) ? $states[$license->registration_state] : $license->registration_state }}</td>
                                                        <td>{{ $license->medical_license }}</td>
                                                        <td>{{ $license->valid_until }}</td>
                                                        <td>
                                                            @if($license->license_image)
                                                            <img src='{{ asset ("/files/profiles/doctor-". $license->user_id ."/".$license->license_image)}}' width="100" >
                                                            @endif
                                                        </td>
                                                    </tr>
                                                    @endforeach
                                                    @else
                                                    <tr><td colspan="3">{{trans('content.no_data_available')}}</td></tr>
                                                    @endif
                                                </tbody>
                                            </table>
                                        </div>
                                    </div>
                                </div>
                                @elseif(auth()->user()->role == 'patient')
                                <div class="row profile-info-row">
                                    <div class="col-xs-12 col-md-4 profile-info-title">ID</div>
                                    <div class="col-xs-12 col-md-8 profile-info-text">
                                        {{ auth()->user()->id_card }}
                                        {!! auth()->user()->id_card_image ? '<img class="right-img-x" src="/files/profiles/' . auth()->user()->role . '-' . auth()->user()->id . '/' . auth()->user()->id_card_image . '">' : '' !!}
                                    </div>
                                </div>
                                <div class="row profile-info-row">
                                    <div class="col-xs-12 col-md-4 profile-info-title">{{trans('content.recommendation')}}</div>
                                    <div class="col-xs-12 col-md-8 profile-info-text">
                                        {!! auth()->user()->recommendation_image ? '<img class="right-img-x" src="/files/profiles/' . auth()->user()->role . '-' . auth()->user()->id . '/' . auth()->user()->recommendation_image . '">' : '' !!}
                                    </div>
                                </div>
                                @endif

                            </div>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </div>
</section>

@endsection