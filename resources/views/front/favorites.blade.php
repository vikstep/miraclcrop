@extends((Auth::check() && auth()->user()->role !='admin') ? config('layout.'.auth()->user()->role) : 'layouts.front.index')
@section('title', '| ' . trans('title.favorite'))
@section('content')

    <section id="portfolio">
        <div class="container m-t-10">
            <div class="row product-item-row">
                @if(count($products) > 0)
                    @foreach($products as $product)
                        <div class="col-xs-6 col-md-4 product-item-block">
                            <span class="glyphicon glyphicon-remove user-favorite-remove" title="Remove from favorite list" rel="{{ $product['product']->product_id }}"></span>
                            <a href="{{ URL::to('product/' . $product['product']->product_id . '/' . slugify(trans('product.' . $product['product']->product_id . '.title'))) }}" class="thumbnail product-item">
                                <img src="{{ asset('files/products/' . $product['product']->image . '-s.' . $product['product']->extension) }}" class="img-responsive">
                                <div>{{ trans('product.' . $product['product']->product_id . '.title') }}</div>

                                <span class="label label-info">{{ trans('product_category.' . $product['product']->category_id . '.title') }}</span>
                                @foreach($product['attributes'] as $attribute)
                                    @if($attribute['attribute_type'] == 1)
                                        <span class="label label-warning">{{ trans('strain.' . $attribute['attribute_id'] . '.title') }}</span>
                                    @endif
                                @endforeach
                            </a>
                        </div>
                    @endforeach
                @else
                    <h3>{{trans('content.nothing_to_show')}}</h3>
                @endif
            </div>

        </div>
    </section>
@endsection