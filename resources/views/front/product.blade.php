@extends('layouts.front.product')
@section('title', '| ' . trans('title.products'))
@section('content')

<section id="portfolio">
    @if(Auth::check() && auth()->user()->role == 'patient' && !auth()->user()->recommendation_image)
    <div class="reminder text-center">
        {{trans('content.you_dont_have_patient')}}  
        <a href="#" class="btn btn-success">{{trans('content.patient_recommandation')}}</a>
    </div>
    @endif
    <div class="container" style="margin: 20px 0 20px 183px;">
        <div class="row">
            <div class="col-md-offset-10">
                <select id="sortBy" class="form-control pull-right">
                    <option value="0">{{trans('content.default_sorting')}}</option>
                    <option value="1">{{trans('content.sort_by_newess')}}</option>
                    <option value="2">{{trans('content.low_to_high')}}</option>
                    <option value="3">{{trans('content.high_to_low')}}</option>
                    <option value="3">{{trans('content.sort_by_popularity')}}</option>
                </select>
            </div>
        </div>
    </div>
    <div class="container product-content">
        @include('front._products')
    </div>
</section>
@endsection