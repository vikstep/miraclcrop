@extends((Auth::check() && auth()->user()->role !='admin') ? config('layout.'.auth()->user()->role) : 'layouts.front.index')
@section('title', '| ' . trans('title.cart'))
@section('content')

<!-- Portfolio Grid Section -->

<section id="portfolio" class="blog-list">
    @if(Auth::check() && auth()->user()->role == 'patient' && !auth()->user()->recommendation_image)
        <div class="reminder text-center">
                {{trans('content.you_dont_have_patient')}}  
                <a href="#" class="btn btn-success"> {{trans('content.patient_recommandation')}}</a>
        </div>
    @endif
    <div class="container">
        <div class="alert alert-success hidden" role="alert"></div>
        <div class="alert alert-danger hidden" role="alert"></div>
        @if(!$cart->isEmpty())
        <div class="box-header with-border">
            <h3 class="box-title">{{trans('content.your_cart')}}</h3>
        </div>
        <div class="row">
            <div class="col-md-4">
                <div class="box box-info cart-box">
                    <div class="box-body">
                        <div style="margin: 0 15px">
                            <?php $total = 0; ?>
                            @foreach($cart as $item)
                            <div class="row cart-item">
                                <?php $product = $product::find($item->product_id); ?>
                                <div class="col-md-4">
                                    <img src="{{ asset('/files/products/' . $product->image . '-s.' . $product->extension) }}" />
                                </div>
                                <div class="col-md-8">
                                    <a href="{{ URL::to('product/' . $item->product_id . '/' . slugify(trans('product.' . $item->product_id . '.title'))) }}">{{ trans('product.' . $item->product_id . '.title') }}</a>
                                    <div>
                                        <?php $price = 0; $portion_id; ?>
                                        @foreach($product->attributes as $attribute)
                                            @if($attribute->attribute_type == 2 && $attribute->attribute_id == $item->portion_id)
                                                <div>
                                                    <div class="label label-success">{{ trans('portion.' . $attribute->attribute_id . '.title') }}</div>
                                                    <?php $price = $attribute->attribute_value; $portion_id = $attribute->attribute_id; ?>
                                                    <span class="">{{ floatval($attribute->attribute_value) }} {{ Config::get('custom.currency') }}</span>
                                                </div>
                                            @endif
                                        @endforeach
                                    </div>
                                    <div>
                                        <input type="hidden" class="inStock" value="{{ $product->weight_in_stock }}" />
                                        <input type="hidden" class="weight" value="{{ $portions[$portion_id] }}" />
                                        <input type="hidden" class="productId" value="{{ $product->id }}" />
                                        <input type="hidden" class="portionId" value="{{ $portion_id }}" />
                                        <input type="hidden" class="price" value="{{ $price }}" />
                                        <input type="number" class="form-control qty" value="{{ $item->quantity }}" min="0" step="1" style="width: 100px; display: inline-block" />
                                        <span class="current-subtotal">{{ ($item->quantity * $price) }}</span> {{ Config::get('custom.currency') }}
                                    </div>
                                </div>
                                <a href="{{ URL::to('/cart/remove-from-cart/' . $item->product_id) }}" title="Remove from cart"><span class="glyphicon glyphicon-remove" style="position:absolute; right: 5px; top: 5px;"></span></a>
                            </div>
                            <?php $total += $item->quantity * $price ?>
                            @endforeach
                        </div>
                    </div>
                </div>
                <button class="btn btn-primary updateCart" style="margin-top: 10px;">{{trans('content.update_cart')}}</button>
                <div class="pull-right"><h3>Total: <span class="total-val">{{ $total }}</span> {{ Config::get('custom.currency') }}</h3></div>
            </div>
            <div class="col-md-4">
                @if($addresses)
                <select id="shippingAddresses" class="form-control">
                    @foreach($addresses as $address)
                        <option value="{{ $address->id }}" {{ $address->default_shipping == 'yes' ? 'selected' : '' }}>{{ $address->address . ' ' . $address->zip_code . ' ' . $address->city . ' ' . $address->state . ' ' . ($address->default_shipping == 'yes' ? '(Default shipping address)' : '') }}</option>
                    @endforeach
                </select>
                @endif
                  @if(Auth::check())
                <div class="m-t-10"><a role="button" id="new-address-btn">New Address</a></div>
                <div id="address-block">
                    <div class="form-group">
                        <select class="form-control" id="states" name="state" required>
                            @foreach($states as $key => $state)
                                <option value="{{ $key  }}">{{ $state }}</option>
                            @endforeach
                        </select>
                        <div class="help-block with-errors"></div>
                    </div>
                    <div class="form-group">
                        <input type="text" name="city" class="form-control" id="city" placeholder="{{trans('content.city')}}" required>
                        <div class="help-block with-errors"></div>
                    </div>
                    <div class="form-group">
                        <input type="text" name="address" class="form-control" id="address" placeholder="{{trans('content.address')}}" required>
                        <div class="help-block with-errors"></div>
                    </div>
                    <div class="form-group">
                        <input type="text" name="zip_code" class="form-control" id="zipcode" placeholder="{{trans('content.zip_code')}}" required>
                        <div class="help-block with-errors"></div>
                    </div>
                </div>
            </div>
          
            <div class="col-md-4 text-right">
                <h6 style="display: inline-block">Balance: </h6> <h4 id="currentBalance" style="display: inline-block">{{ auth()->user()->wallet_points }}</h4> <h6 style="display: inline-block">{{ Config::get('custom.currency') }}</h6>
                <div><a href="{{URL::to('/profile/my-wallet') }}" class="btn btn-success">{{trans('content.add_point')}}</a></div>
            </div>
            @endif
        </div>
        <div class="row">
            <div class="col-md-12">
                <div class="box-footer clearfix">
                      @if(Auth::check())
                        <a href="javascript:void(0)" class="btn btn-sm btn-info btn-flat pull-right" id="checkout">{{trans('content.checkout')}}</a>
                     @else 
                         <a href="{{URL::to('/auth/signin') }}" class="btn btn-sm btn-info btn-flat pull-right">{{trans('content.checkout')}}</a>
                     @endif
                    <a href="{{ URL::to('/products') }}" class="btn btn-sm btn-default btn-flat pull-right">{{trans('content.continue_shopping')}}</a>
                </div>
            </div>
        </div>
        @else
        <h2>{{trans('content.cart_is_empty')}}</h2>
        @endif
    </div>
</div>
</div>
</section>

@endsection
