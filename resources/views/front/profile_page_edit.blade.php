@extends((Auth::check() && auth()->user()->role !='admin')  ? config('layout.'.auth()->user()->role): 'layouts.front.index')
@section('title', '| ' . trans('title.edit_profile'))
@section('content')

<!-- Portfolio Grid Section -->
<section id="portfolio" class="blog-list">
    <div class="container">
        <ul class="nav nav-tabs" role="tablist">
            <li role="presentation"><a href="{{ URL::to('/profile') }}">{{trans('title.profile_info')}}</a></li>
            <li role="presentation" class="active"><a href="javascript:void(0)">{{trans('title.edit_profile')}}</a></li>
            <li role="presentation"><a href="{{ URL::to('/profile/social') }}" >{{trans('title.profile_social')}}</a></li>
            <li role="presentation"><a href="{{ URL::to('/profile/addresses') }}">{{trans('title.addresses')}}</a></li>
            <li role="presentation"><a href="{{ URL::to('/profile/my-wallet') }}" >{{trans('title.my_wallet')}}</a></li>
            <li role="presentation"><a href="{{ URL::to('/profile/purchase-history') }}" >{{trans('title.purchase_history')}}</a></li>
        </ul>
        <div class="tab-content">
            <div role="tabpanel" class="tab-pane active" id="edit-profile">
                <h3>{{trans('content.profile_edit')}}</h3>
                <div class="x">
                    <div class="">
                        @if (session('success'))
                        <div class="alert alert-success">
                            {{ session('success') }}
                        </div>
                        @endif
                        @if (count($errors) > 0)
                        <div class="alert alert-danger">
                            <ul>
                                @foreach ($errors->all() as $error)
                                <li>{{ $error }}</li>
                                @endforeach
                            </ul>
                        </div>
                        @endif
                        <form method="POST" action="{{ action('Front\AccountController@postEditProfile') }}" enctype="multipart/form-data" data-toggle="validator" role="form" class="custom-validation-form">
                            {!! csrf_field() !!}
                            <div class="form-group has-feedback">
                                <input type="email" value="{{ auth()->user()->email }}" class="form-control disabled" disabled="disabled">
                            </div>

                            <div class="form-group has-feedback">
                                <input type="text" name="firstname" value="{{ auth()->user()->first_name }}" class="form-control" id="firstname" placeholder="{{trans('content.first_name')}}" required>
                                <div class="help-block with-errors"></div>
                            </div>
                            <div class="form-group">
                                <input type="text" name="lastname" value="{{ auth()->user()->last_name }}" class="form-control" id="lastname" placeholder="{{trans('content.last_name')}}" required>
                                <div class="help-block with-errors"></div>
                            </div>
                            <div class="form-group">
                                <label>{{trans('content.profile_image')}}</label>
                                {!!  auth()->user()->face_photo ? '<img class="right-img-x" src="/files/profiles/' . auth()->user()->role . '-' . auth()->user()->id . '/' . auth()->user()->face_photo . '">' : '' !!}
                                <input type="file" name="profile_picture">
                                <div class="help-block with-errors"></div>
                            </div>
                            <div class="form-group">
                                <input type="text" name="driver_license" value="{{ auth()->user()->driver_license }}" class="form-control" id="driver_license" placeholder="{{trans('content.driver_license')}}">
                                {!!  auth()->user()->driver_license_image ? '<img class="right-img-x m-t-5" src="/files/profiles/' . auth()->user()->role . '-' . auth()->user()->id . '/' . auth()->user()->driver_license_image . '">' : '' !!}
                                <input type="file" name="driver_license_file">
                                <div class="help-block with-errors"></div>
                            </div>
                            <?php $dob = explode('-', auth()->user()->dob) ?>
                            <div class="form-group m-t-20">
                                <label>Date of Birthday</label>
                                <div class="row">
                                    <div class="form-group col-xs-4 p-r-0">
                                        <select name="dob_month" class="form-control" required>
                                            <option value=''>-- Month --</option>
                                            @foreach($months as $key => $month)
                                            <option value="{{ $key }}" {{ $dob[1] == $key ? 'selected': ''}}>{{ $month }}</option>
                                            @endforeach
                                        </select>
                                        <div class="help-block with-errors"></div>
                                    </div>
                                    <div class="form-group col-xs-4 p-r-0">
                                        <select name="dob_day" class="form-control" required>
                                            <option value=''>-- Day --</option>
                                            @foreach($days as $day)
                                            <option value="{{ $day }}" {{$dob[2] == $day ? 'selected': ''}}>{{ $day }}</option>
                                            @endforeach
                                        </select>
                                        <div class="help-block with-errors"></div>
                                    </div>
                                    <div class="form-group col-xs-4">
                                        <select name="dob_year" class="form-control" required>
                                            <option value=''>-- Year --</option>
                                            @foreach($years as $year)
                                            <option value="{{ $year }}" {{$dob[0] == $year ? 'selected': ''}} >{{ $year }}</option>
                                            @endforeach
                                        </select>
                                        <div class="help-block with-errors"></div>
                                    </div>
                                </div>
                            </div>
                            <div class="form-group">
                                <div class="radio radio-inline p-0">
                                    <label>
                                        <input type="radio" name="gender" value="male" id="genderMale" {{ auth()->user()->gender == 'male' ? 'checked' : '' }}>
                                               {{trans('content.male')}}
                                    </label>
                                    <label class="m-l-20">
                                        <input type="radio" name="gender" value="female" id="genderFemale" {{ auth()->user()->gender == 'female' ? 'checked' : '' }}>
                                               {{trans('content.female')}}
                                    </label>
                                </div>
                            </div>
                            @if(auth()->user()->role == 'doctor')
                            <div class="form-group">
                                <input type="text" name="phone" value="{{ auth()->user()->phone }}" class="form-control" id="phone" placeholder="{{trans('content.phone')}}">
                                <div class="help-block with-errors"></div>
                            </div>
                            <div class="line license">{{trans('content.licenses')}}</div>
                                <div class="form-group">
                                    <div class="row">
                                        <div class="col-xs-12 col-md-3">
                                            <select class="form-control" id="states">
                                                @foreach($states as $key => $state)
                                                <option value="{{ $key  }}">{{ $state }}</option>
                                                @endforeach
                                            </select>
                                        </div>
                                        <div class="col-xs-12 col-md-3">
                                            <input type="text" class="form-control" id="medical_license" data-profile_license="profile" placeholder="{{trans('content.medical_license')}}">
                                            <div class="license-error text-danger with-errors"></div>
                                        </div>
                                        <div class="col-xs-12 col-md-3">
                                            <input type="text" class="form-control" id="valid_until" placeholder="{{trans('content.valid_until')}}">
                                            <div class="until-error text-danger with-errors"></div>
                                        </div>
                                        <div class="col-xs-12 col-md-3 text-right">
                                            <label for="valid_license" class="btn btn-primary valid_license_lable" style="display: inline-block !important;"><span class="glyphicon glyphicon-upload"></span></label>
                                            <input type="file" name="license_image" id="valid_license" class="hidden">
                                            <button type="button" class="btn btn-primary btn-flat add-license"><span class="glyphicon glyphicon-plus"></span></button>
                                        </div>
                                    </div>
                                </div>
                                <hr>
                                @if($doctorLicense && !empty($doctorLicense))
                                @foreach ($doctorLicense as $license)
                                <div class="form-group">
                                    <div class="row">
                                        <div class="col-xs-12 col-md-3">
                                            <select class="form-control" name="license[{{ $license->id }}][registration_state]">
                                                @foreach($states as $key => $state)
                                                <option value="{{ $key  }}" {{ (isset( $license->registration_state) &&  $license->registration_state == $key) ? 'selected' : '' }}>{{ $state }}</option>
                                                @endforeach
                                            </select>
                                        </div>
                                        <div class="col-xs-12 col-md-3">
                                            <input type="text" class="form-control" name="license[{{ $license->id }}][medical_license]" value="{{ $license->medical_license }}" placeholder="{{trans('content.medical_license')}}">
                                        </div>
                                        <div class="col-xs-12 col-md-3">
                                            <input type="text" class="form-control" name="license[{{ $license->id }}][valid_until]" value="{{ $license->valid_until }}" placeholder="{{trans('content.valid_until')}}">
                                        </div>
                                        <div class="col-xs-12 col-md-3 text-right">
                                            @if($license->license_image)
                                                <img src='{{ asset ("/files/profiles/doctor-". $license->user_id ."/".$license->license_image)}}' width="100" >
                                            @endif
                                            <label for="license_image_{{ $license->id }}" class="btn btn-primary valid_license_lable" style="display: inline-block !important;"><span class="glyphicon glyphicon-upload"></span></label>
                                            <input type="file" class="hidden" id="license_image_{{ $license->id }}" name="license_image_{{ $license->id }}">
                                        </div>
                                    </div>
                                </div>
                                @endforeach
                                @endif
                                @elseif(auth()->user()->role == 'patient')
                                <div class="form-group">
                                    <input type="text" name="id_card" value="{{ auth()->user()->id_card }}" class="form-control" id="id_card" placeholder="ID">
                                    {!!  auth()->user()->id_card_image ? '<img class="right-img-x m-t-5 m-b-5" src="/files/profiles/' . auth()->user()->role . '-' . auth()->user()->id . '/' . auth()->user()->id_card_image . '">' : '' !!}
                                    <input type="file" name="id_file">
                                    <div class="help-block with-errors"></div>
                                    <div class="clearfix"></div>
                                </div>
                                <div class="form-group">
                                    <label>{{trans('content.recommendation')}}</label>
                                    {!!  auth()->user()->recommendation_image ? '<img class="right-img-x" src="/files/profiles/' . auth()->user()->role . '-' . auth()->user()->id . '/' . auth()->user()->recommendation_image . '">' : '' !!}
                                    <input type="file" name="patient_recommendation">
                                    <div class="help-block with-errors"></div>
                                </div>
                                @endif
                                <input type="hidden" name="user_id" value="{{ auth()->user()->id }}">
                                <div class="row m-t-20">
                                    <hr>
                                    <div class="col-xs-12 text-right">
                                        <button type="submit" class="btn btn-primary btn-flat">Save</button>
                                    </div>
                                    <!-- /.col -->
                                </div>
                            </form>
                    </div>
                </div>
            </div>
        </div>
    </div>
</section>

@endsection