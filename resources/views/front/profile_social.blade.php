@extends((Auth::check() && auth()->user()->role !='admin')  ? config('layout.'.auth()->user()->role): 'layouts.front.index')
@section('title', '| ' . trans('title.addresses'))
@section('content')

    <!-- Portfolio Grid Section -->
    <section id="portfolio" class="blog-list">
        <div class="container">
            <ul class="nav nav-tabs" role="tablist">
                <li role="presentation"><a href="{{ URL::to('/profile') }}">{{trans('title.profile_info')}}</a></li>
                <li role="presentation"><a href="{{ URL::to('/profile/edit') }}">{{trans('title.edit_profile')}}</a></li>
                <li role="presentation" class="active"><a href="javascript:void(0)" >{{trans('title.profile_social')}}</a></li>
                <li role="presentation"><a href="{{ URL::to('/profile/addresses') }}">{{trans('title.addresses')}}</a></li>
                <li role="presentation"><a href="{{ URL::to('/profile/my-wallet') }}" >{{trans('title.my_wallet')}}</a></li>
                <li role="presentation"><a href="{{ URL::to('/profile/purchase-history') }}" >{{trans('title.purchase_history')}}</a></li>
            </ul>
            <div class="tab-content">
                <div role="tabpanel" class="tab-pane active">
                    <h3>{{trans('title.profile_social')}}</h3>
                    @if (session('success'))
                        <div class="alert alert-success">
                            {{ session('success') }}
                        </div>
                    @endif
                    <form action="{{ action('Front\AccountController@postSocialProfiles') }}" method="post">
                        {!! csrf_field() !!}
                        @if(count($socialProfiles) > 0)
                            @foreach($socialProfiles as $socialProfile)
                                <div class="row m-b-10">
                                    <div class="col-xs-12 col-md-3">
                                        <select class="form-control" name="type[]">
                                            <option value="facebook" {{ $socialProfile->type == 'facebook' ? 'selected' : ''}}>Facebook</option>
                                            <option value="twitter" {{ $socialProfile->type == 'twitter' ? 'selected' : ''}}>Twitter</option>
                                            <option value="google" {{ $socialProfile->type == 'google' ? 'selected' : ''}}>Google plus</option>
                                        </select>
                                    </div>
                                    <div class="col-xs-12 col-md-8"><input type="text" class="form-control" name="url[]" value="{{ $socialProfile->url }}"></div>
                                    <div class="col-xs-12 col-md-1"><a href="{{ URL::to('/profile/social/remove/' . $socialProfile->id) }}"><span class="glyphicon glyphicon-remove"></span></a></div>
                                </div>
                            @endforeach
                        @endif
                        <hr>
                        <div class="row">
                            <div class="col-xs-12 col-md-3">
                                <select class="form-control" name="type[]">
                                    <option value="facebook">Facebook</option>
                                    <option value="twitter">Twitter</option>
                                    <option value="google">Google plus</option>
                                </select>
                            </div>
                            <div class="col-xs-12 col-md-9"><input type="text" class="form-control" name="url[]" placeholder="Social url..."></div>
                        </div>
                        <div class="row m-t-20">
                            <div class="col-xs-12  text-right">
                                <button type="submit" class="btn btn-primary">{{trans('content.save')}}</button>
                            </div>
                        </div>
                    </form>
                </div>
            </div>
        </div>
    </section>

@endsection