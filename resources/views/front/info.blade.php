@extends((Auth::check() && auth()->user()->role !='admin')  ? config('layout.'.auth()->user()->role): 'layouts.front.index')
@section('title', '| ' . session('info'))
@section('content')
    <div class="info-content">
        <div class="info-box">
            <div class="info-box-body">
                <p class="info-box-msg">{{ session('info') }}</p>
            </div>
        </div>
    </div>
@endsection
