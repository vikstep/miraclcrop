@extends((Auth::check() && auth()->user()->role !='admin') ? config('layout.'.auth()->user()->role) : 'layouts.front.index')
@section('title', '| ' . trans('title.products') . ' - ' . trans('product.' . $product->id . '.title'))
@section('content')

<!-- Portfolio Grid Section -->

<section id="portfolio" class="blog-list">
    @if(Auth::check() && auth()->user()->role == 'patient' && !auth()->user()->recommendation_image)
        <div class="reminder text-center">
                {{trans('content.you_dont_have_patient')}}  
                <a href="#" class="btn btn-success">{{trans('content.patient_recommandation')}}</a>
        </div>
    @endif
    
    <div class="container">
    <div class="alert alert-success hidden" role="alert">Cart updated successfully</div>
    <div class="alert alert-danger hidden" role="alert"></div>
        <div>
            @if($has_translation)
                @if(Auth::check())
                    <span class="{{ $favorite == 'yes' ? 'glyphicon-star remove-from-favorite' : 'glyphicon-star-empty add-to-favorite' }} glyphicon  static user-favorite" title="{{ $favorite == 'yes' ? 'Remove from favorite list' : 'Add to favorite list' }}" rel="{{ $product->id }}"></span>
                @endif
                <h2>
                    {{ trans('product.' . $product->id . '.title') }}
                </h2>
                <div><a href="{{ URL::to('/products/category/' . $product->category_id . '/' . strtolower(slugify(trans('product_category.' . $product->category_id . '.title')))) }}">{{ trans('product_category.' . $product->category_id . '.title') }}</a> </div>
                <img src="{{ asset('/files/products/' . $product->image . '-m.' . $product->extension) }}" class="product-default-img" />
                <div><small class="badge">Sku: {{ $product->sku }}</small></div>
                <div>{!! filterContent(trans('product.' . $product->id . '.content')) !!}</div>
                <div>
                    <img src="{{ asset('files/products/' . $product->image . '-s.' . $product->extension ) }}" rel="{{ $product->image }}" data-ext="{{ $product->extension }}" class="img-thumbnail product-img">
                    @foreach($product->media as $media)
                        @if($media->media_type == 1)
                            <img src="{{ asset('files/products/' . $media->media . '-s.' . $media->extension ) }}" rel="{{ $media->media }}" data-ext="{{ $media->extension }}" class="img-thumbnail product-img">
                        @endif
                    @endforeach
                </div>
                <div>
                    @foreach($product->media as $media)
                        @if($media->media_type == 2)
                            <figure class="iframe-cont">
                                <iframe width="350" height="200" src="http://www.youtube.com/embed/{{ $media->media }}?wmode=opaque&amp;autoplay=0&amp;showinfo=0&amp;autohide=1&amp;rel=0" frameborder="0" allowfullscreen=""></iframe>
                            </figure>
                        @endif
                    @endforeach
                </div>
                <div>
                    @foreach($product->attributes as $attribute)
                        @if($attribute->attribute_type == 1)
                            <span class="label label-warning">{{ trans('strain.' . $attribute->attribute_id . '.title') }}</span>
                        @endif
                    @endforeach
                </div>
                <div>
                    <?php $product_portions = []; ?>
                    @foreach($product->attributes as $attribute)
                        @if($attribute->attribute_type == 2)
                        <?php
                            $product_portions[$attribute->attribute_value] = $attribute;
                            ksort($product_portions);
                        ?>
                        @endif
                    @endforeach
                    
                    <div id="portions" class="btn-group" data-toggle="buttons" style="margin-top: 30px">
                        <?php $price = 0; $index = 0; ?>
                        @foreach($product_portions as $portion)
                            <?php $index++ ?>
                            @if(isset($user_cart) && $user_cart->portion_id == $portion->attribute_id ? 'active' : '')
                                <?php $price = $portion->attribute_value ?>
                            @endif
                            <label class="btn btn-success selected-portion {{ (!isset($user_cart) && $index == 1) ? 'active' : (isset($user_cart) && $user_cart->portion_id == $portion->attribute_id ? 'active' : '') }}">
                                <input type="radio" name="options" data-id="{{ $portion->attribute_id }}" data-price="{{ floatval($portion->attribute_value) }}" data-weight="{{ $portions[$portion->attribute_id] }}" autocomplete="off"><div class="label label-success">{{ trans('portion.' . $portion->attribute_id . '.title') }}</div> {{ floatval($portion->attribute_value) }} {{ Config::get('custom.currency') }}
                            </label>
                        @endforeach
                    </div>
                    <div style="margin-top: 30px">
                        <input type="hidden" id="inStock" value="{{ $product->weight_in_stock }}" />
                        <input type="hidden" id="productId" value="{{ $product->id }}" />
                        <input type="number" class="form-control" id="qty" value="{{ isset($user_cart) ? $user_cart->quantity : '1' }}" min="0" step="1" style="width: 100px; display: inline-block;" /> <span class="current-total">{{ isset($user_cart) ? floatval($user_cart->quantity * $price) . ' ' . Config::get('custom.currency') : '' }}</span>
                    </div>
                    <div>
                            <button type="button" class="btn btn-primary" id="updateCart" style="margin-top: 10px">{{ isset($user_cart) ? 'Update Cart' : 'Add to Cart' }}</button>
                  
                    </div>
                    <hr />
                    <div>
                        <img src="{{ asset('files/brands/' . $brand->brand_logo ) }}" height="100" />
                        <h5>{{ trans('brand.' . $brand->id . '.title') }}</h5>
                        <div>{{ trans('brand.' . $brand->id . '.content') }}</div>
                    </div>
                </div>
            @else
                <h2>{{trans('content.there_is_no_content')}}</h2>
            @endif
        </div>
    </div>
</section>

@endsection
