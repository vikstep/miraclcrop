@extends((Auth::check() && auth()->user()->role !='admin')  ? config('layout.'.auth()->user()->role): 'layouts.front.index')
@section('title', '| Blog - ' . trans('post.' . $post->id . '.title'))
@section('content')

<!-- Portfolio Grid Section -->
<section id="portfolio" class="blog-list">
    <div class="container">
            <div>
                @if($has_translation)
                    <h2>{{ trans('post.' . $post->id . '.title') }}</h2>
                    <div class="text-left"><small>{{ \Carbon\Carbon::parse($post->defined_created_date)->format('m/d/Y') }}</small></div>
                    <div><a href="{{ URL::to('/blog/category/' . $post->category_id . '/' . strtolower(slugify(trans('blog_category.' . $post->category_id . '.title')))) }}">{{ trans('blog_category.' . $post->category_id . '.title') }}</a> </div>
                    <img src="{{ asset('/files/blog/'.date('m.Y').'/' . $post->file_name . '-m.' . $post->file_type) }}" />
                    <div>{!! filterContent(trans('post.' . $post->id . '.content')) !!}</div>
                @else
                    <h2>{{trans('content.there_is_no_content')}}</h2>
                @endif
            </div>
    </div>
</section>

@endsection
