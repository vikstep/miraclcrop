@extends((Auth::check() && auth()->user()->role !='admin')  ? config('layout.'.auth()->user()->role): 'layouts.front.index')
@section('title', '| ' . trans('title.purchase_history'))
@section('content')
<!-- Portfolio Grid Section -->
<section id="portfolio" class="blog-list">
    <div class="container">
        <ul class="nav nav-tabs" role="tablist">
            <li role="presentation"><a href="{{ URL::to('/profile') }}">{{trans('title.profile_info')}}</a></li>
            <li role="presentation"><a href="{{ URL::to('/profile/edit') }}">{{trans('title.edit_profile')}}</a></li>
            <li role="presentation"><a href="{{ URL::to('/profile/social') }}" >{{trans('title.profile_social')}}</a></li>
            <li role="presentation"><a href="{{ URL::to('/profile/addresses') }}">{{trans('title.addresses')}}</a></li>
            <li role="presentation"><a href="{{ URL::to('/profile/my-wallet') }}">{{trans('title.my_wallet')}}</a></li>
            <li role="presentation" class="active"><a href="javscript:void(0)" >{{trans('title.purchase_history')}}</a></li>
        </ul>
        <div class="tab-content">
            <div role="tabpanel" class="tab-pane active">
                <h3>{{trans('title.purchase_history')}}</h3>
                
                @foreach($orders as $order)
                <div class="box-group" id="accordion">
                    <div class="panel box box-primary">
                        <div class="box-header with-border">
                            <h4 class="box-title">
                                <?php 
                                    $class = 'warning';
                                    if($order->status == 'shipped')
                                        $class = 'primary';
                                    elseif($order->status == 'delivered')
                                        $class = 'success';
                                ?>
                                <a data-toggle="collapse" data-parent="#accordion" href="#order_{{ $order->order_number }}" aria-expanded="false" class="collapsed">
                                    #{{ $order->order_number }} 
                                </a>
                                <small>{{ \Carbon\Carbon::parse($order->created_at)->format('m/d/Y H:i') }}</small>
                                <span class="label label-{{ $class }} pull-right">{{ $order->status }}</span>
                            </h4>
                        </div>
                        <div id="order_{{ $order->order_number }}" class="panel-collapse collapse" aria-expanded="false">
                            <div class="box-body">
                                <table class="table no-margin">
                                    <thead>
                                        <th>{{trans('content.product_name')}}</th>
                                        <th>Sku</th>
                                        <th>{{trans('content.quantity')}}</th>
                                        <th>{{trans('content.portion')}}</th>
                                        <th>{{trans('content.price')}}</th>
                                        <th>{{trans('content.subtotal')}}</th>
                                    </thead>
                                    <tbody>
                                        <?php $current_total = 0; ?>
                                       
                                        @foreach($order->products as $product)
                                        <tr>
                                            <td><a href="{{ URL::to('product/' . $product->product_id . '/' . slugify(trans('product.' . $product->product_id . '.title'))) }}" target="_blank">{{ trans('product.' . $product->product_id . '.title') . ' (' . $product->product_sku . ')' }}</a></td>
                                            <td>{{ $product->product_sku }}</td>
                                            <td>{{ $product->quantity }}</td>
                                            <td>{{ $product->portion }}</td>
                                            <td>{{ floatval($product->price) }}</td>
                                            <td>{{ $product->price * $product->quantity }}</td>
                                            <?php $current_total += ($product->price * $product->quantity)  ?>
                                        </tr>
                                        @endforeach
                                        <tr><td colspan="6" class="text-right">{{trans('content.total')}}: {{ $current_total }}</td></tr>
                                    </tbody>
                                </table>
                            </div>
                        </div>
                    </div>
                </div>
                @endforeach
            </div>
        </div>
</section>

@endsection

