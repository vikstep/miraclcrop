@extends((Auth::check() && auth()->user()->role !='admin')  ? config('layout.'.auth()->user()->role): 'layouts.front.index')
@section('title', '| Blog')
@section('content')

<!-- Portfolio Grid Section -->
<section id="portfolio" class="blog-list">
    <div class="container">
        @if(count($posts) > 0)
        @foreach($posts as $post)
            <div class="media">
                <div class="media-left">
                    <a href="#">
                        <img class="media-object" src="{{ asset('/files/blog/'.date('m.Y').'/' . $post->file_name . '-s.' . $post->file_type) }}" alt="{{ trans('post.' . $post->id . '.title') }}">
                    </a>
                </div>
                <div class="media-body">
                    <h4 class="media-heading">{{ trans('post.' . $post->id . '.title') }}</h4>
                    {!! trans('post.' . $post->id . '.excerpt') !!}
                    <div class="text-left"><small>{{ \Carbon\Carbon::parse($post->defined_created_date)->format('m/d/Y') }}</small></div>
                    <div class="text-right"><a href="{{ URL::to('/blog/' . $post->id . '/' . slugify(trans('post.' . $post->id . '.title'))) }}">Read more</a></div>
                </div>
            </div>
        @endforeach
        @endif
    </div>
</section>


<!-- Footer -->
<footer class="text-center">
    <div class="footer-above">
        <div class="container">
            <div class="row">
                <div class="footer-col col-md-4">
                    <h3>{{trans('content.location')}}</h3>
                    <p>3481 Melrose Place
                        <br>Beverly Hills, CA 90210</p>
                </div>
                <div class="footer-col col-md-4">
                    <h3>{{trans('content.around_the_web')}}</h3>
                    <ul class="list-inline">
                        <li>
                            <a href="#" class="btn-social btn-outline"><span class="sr-only">Facebook</span><i class="fa fa-fw fa-facebook"></i></a>
                        </li>
                        <li>
                            <a href="#" class="btn-social btn-outline"><span class="sr-only">Google Plus</span><i class="fa fa-fw fa-google-plus"></i></a>
                        </li>
                        <li>
                            <a href="#" class="btn-social btn-outline"><span class="sr-only">Twitter</span><i class="fa fa-fw fa-twitter"></i></a>
                        </li>
                        <li>
                            <a href="#" class="btn-social btn-outline"><span class="sr-only">Linked In</span><i class="fa fa-fw fa-linkedin"></i></a>
                        </li>
                        <li>
                            <a href="#" class="btn-social btn-outline"><span class="sr-only">Dribble</span><i class="fa fa-fw fa-dribbble"></i></a>
                        </li>
                    </ul>
                </div>
                <div class="footer-col col-md-4">
                    <h3>{{trans('content.about_frelanser')}}</h3>
                    <p>{{trans('content.frelanser_open')}} <a href="http://startbootstrap.com">{{trans('content.start_bootstrap')}}</a>.</p>
                </div>
            </div>
        </div>
    </div>
    <div class="footer-below">
        <div class="container">
            <div class="row">
                <div class="col-lg-12">
                    Copyright &copy; Your Website 2016
                </div>
            </div>
        </div>
    </div>
</footer>

<!-- Scroll to Top Button (Only visible on small and extra-small screen sizes) -->
<div class="scroll-top page-scroll hidden-sm hidden-xs hidden-lg hidden-md">
    <a class="btn btn-primary" href="#page-top">
        <i class="fa fa-chevron-up"></i>
    </a>
</div>

<!-- Portfolio Modals -->
<div class="portfolio-modal modal fade" id="portfolioModal1" tabindex="-1" role="dialog" aria-hidden="true">
    <div class="modal-content">
        <div class="close-modal" data-dismiss="modal">
            <div class="lr">
                <div class="rl">
                </div>
            </div>
        </div>
        <div class="container">
            <div class="row">
                <div class="col-lg-8 col-lg-offset-2">
                    <div class="modal-body">
                        <h2>{{trans('content.prolect_title')}}</h2>
                        <hr class="star-primary">
                        <img src="{{ asset ("/template/img/portfolio/cabin.png")}}" class="img-responsive img-centered" alt="">
                        <p>{{trans('content.use_this_area')}}</p>
                        <ul class="list-inline item-details">
                            <li>Client:
                                <strong><a href="http://startbootstrap.com">{{trans('content.start_bootstrap')}}</a>
                                </strong>
                            </li>
                            <li>Date:
                                <strong><a href="http://startbootstrap.com">April 2014</a>
                                </strong>
                            </li>
                            <li>Service:
                                <strong><a href="http://startbootstrap.com">{{trans('content.web_development')}}</a>
                                </strong>
                            </li>
                        </ul>
                        <button type="button" class="btn btn-default" data-dismiss="modal"><i class="fa fa-times"></i> {{trans('content.close')}}</button>
                    </div>
                </div>
            </div>
        </div>
    </div>
</div>
@endsection
