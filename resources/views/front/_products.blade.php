<div class="row">
    <?php $locale = isset($current_locale)? $current_locale: App::getLocale();
            App::setLocale($locale);
            ?>
    @if(count($products) > 0)
        @foreach($products as $product)
        @if(has_trans($locale,'product', $product->id . '.title'))
            <div class="col-xs-4">
                @if(Auth::check())
                    <span class="{{ isset($product->favorite) ? 'glyphicon-star remove-from-favorite' : 'glyphicon-star-empty add-to-favorite' }} glyphicon  user-favorite" title="{{ isset($product->favorite) ? 'Remove from favorite list' : 'Add to favorite list' }}" rel="{{ $product->id }}"></span>
                @endif
                <a href="{{ URL::to('product/' . $product->id . '/' . slugify(trans('product.' . $product->id . '.title'))) }}" class="thumbnail product-item">
                    <img src="{{ asset('files/products/' . $product->image . '-s.' . $product->extension) }}" class="img-responsive">
                    <div>{{ trans('product.' . $product->id . '.title') }}</div>
                    <?php $portion_attr = $product->getMinPortion($product->id); /*dd($portion_attr);*/ ?>

                    <span class="label label-info">{{ trans('product_category.' . $product->category_id . '.title') }}</span>
                    @foreach($product->attributes as $attribute)
                        @if($attribute->attribute_type == 1)
                            <span class="label label-warning">{{ trans('strain.' . $attribute->attribute_id . '.title') }}</span>
                        @endif
                    @endforeach
                </a>
            </div>
         @endif
        @endforeach
    @else
        <h3>{{trans('content.nothing_to_show')}}</h3>
    @endif
</div>
