@extends((Auth::check() && auth()->user()->role !='admin')  ? config('layout.'.auth()->user()->role): 'layouts.front.index')
@section('title', '| ' . trans('title.addresses'))
@section('content')

    <!-- Portfolio Grid Section -->
    <section id="portfolio" class="blog-list">
        <div class="container">
            <ul class="nav nav-tabs" role="tablist">
                <li role="presentation"><a href="{{ URL::to('/profile') }}">{{trans('title.profile_info')}}</a></li>
                <li role="presentation"><a href="{{ URL::to('/profile/edit') }}">{{trans('title.edit_profile')}}</a></li>
                <li role="presentation"><a href="{{ URL::to('/profile/social') }}" >{{trans('title.profile_social')}}</a></li>
                <li role="presentation" class="active"><a href="javascript:void(0)">{{trans('title.addresses')}}</a></li>
                <li role="presentation"><a href="{{ URL::to('/profile/my-wallet') }}" >{{trans('title.my_wallet')}}</a></li>
                <li role="presentation"><a href="{{ URL::to('/profile/purchase-history') }}" >{{trans('title.purchase_history')}}</a></li>
            </ul>
            <div class="tab-content">
                <div role="tabpanel" class="tab-pane active">
                    <h3>{{trans('content.profile_addresses')}}</h3>
                    <div class="text-right m-t-10 m-b-10"><a class="btn btn-success" href="{{ URL::to('/profile/address/create') }}">{{trans('content.create')}}</a></div>
                    <div class="row">
                        @if(count($addresses) > 0)
                            @foreach($addresses as $address)
                                <div class="col-xs-12 col-md-6 m-b-30">
                                    <div class="address">
                                        <div class="text-right m-b-10"><a href="{{ URL::to('/profile/address/edit/' . $address->id) }}">{{trans('content.edit')}}</a></div>
                                        <div class="row">
                                            <div class="col-xs-12 col-md-3 profile-info-title">{{trans('content.address')}}</div>
                                            <div class="col-xs-12 col-md-9 profile-info-text">{{ $address->address }}</div>
                                        </div>
                                        <div class="row">
                                            <div class="col-xs-12 col-md-3 profile-info-title">{{trans('content.city')}}</div>
                                            <div class="col-xs-12 col-md-9 profile-info-text">{{ $address->city }}</div>
                                        </div>
                                        <div class="row">
                                            <div class="col-xs-12 col-md-3 profile-info-title">{{trans('content.state')}}</div>
                                            <div class="col-xs-12 col-md-9 profile-info-text">{{ $address->state }}</div>
                                        </div>
                                        <div class="row">
                                            <div class="col-xs-12 col-md-3 profile-info-title">{{trans('content.zip_code')}}</div>
                                            <div class="col-xs-12 col-md-9 profile-info-text">{{ $address->zip_code }}</div>
                                        </div>
                                        @if($address->default_billing == 'yes')
                                            <div class="address-type">{{trans('content.billing')}}</div>
                                        @endif
                                        @if($address->default_shipping == 'yes')
                                            <div class="address-type">{{trans('content.shipping')}}</div>
                                        @endif
                                        <div class="clearfix"></div>
                                    </div>
                                </div>
                            @endforeach
                        @else
                            <h4>{{trans('content.data_not_found')}}</h4>
                        @endif
                    </div>
                </div>
            </div>
        </div>
    </section>

@endsection