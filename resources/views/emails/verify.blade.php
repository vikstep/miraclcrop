<p>Thanks for creating an account with the MiracleCrop. Please follow the link to <a href="{{ $href }}">verify your email address</a>.</p>
