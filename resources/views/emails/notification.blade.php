<p>Your account has been {{ $status }} at the MiracleCrop. <br />

    @if ($status == 'approved') 
    
        Now you can <a href="{{ $href }}">login to your account</a>.
        
    @elseif($status == 'rejected' || $status == 'blocked')
        @if($status == 'rejected')
            Reject reason: {{ $reason }}
        @endif
        <br />
        Contact us for more information.
    @endif
    
</p>

