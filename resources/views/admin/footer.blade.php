<!-- Main Footer -->
<footer class="main-footer">
    <!-- To the right -->
    <div class="pull-right hidden-xs">
        Anything you want
    </div>
    <!-- Default to the left -->
    <strong>Copyright © 2017 <a href="#">Company</a>.</strong> All rights reserved.
</footer>

</div><!-- ./wrapper -->

<!-- REQUIRED JS SCRIPTS -->

<!-- jQuery 2.1.3 -->
<script src="{{ asset ("/admin-components/plugins/jQuery/jquery-2.2.3.min.js") }}"></script>
<!-- Bootstrap 3.3.2 JS -->
<script src="{{ asset ("/admin-components/bootstrap/js/bootstrap.min.js") }}" type="text/javascript"></script>
<script src="{{ asset ("/admin-components/plugins/datepicker/bootstrap-datepicker.js") }}" type="text/javascript"></script>
<script src="{{ asset("/admin-components/plugins/datatables/jquery.dataTables.min.js") }}" type="text/javascript"></script>
<script src="{{ asset("/admin-components/plugins/datatables/dataTables.bootstrap.min.js") }}" type="text/javascript"></script>

<script src="//cdnjs.cloudflare.com/ajax/libs/underscore.js/1.4.2/underscore-min.js"></script>
<script src="{{ asset ("/admin-components/dist/js/app.min.js") }}" type="text/javascript"></script>
<script src="{{ asset ("/admin-components/plugins/bootstrap-wysihtml5/bootstrap-wysihtml5.js") }}" type="text/javascript"></script>
<script src="{{ asset ("/admin-components/plugins/bootstrap-wysihtml5/bootstrap-wysihtml5-0.0.2.js") }}" type="text/javascript"></script>
<script src="{{ asset ("/admin-components/plugins/bootstrap-wysihtml5/custom_image_and_upload_wysihtml5.js") }}" type="text/javascript"></script>
<script src="{{ asset ("/admin-components/plugins/uploader/jquery.uploadfile.js") }}" type="text/javascript"></script>
<script src="{{ asset ("/admin-components/plugins/select2/select2.full.min.js") }}" type="text/javascript"></script>
<script src="{{ asset ("/js/hierarchy-select.min.js") }}" type="text/javascript"></script>
<script src="{{ asset ("/js/functions.js") }}" type="text/javascript"></script>
<script src="{{ asset ("/js/script.js") }}" type="text/javascript"></script>
