@extends('layouts.admin.index')
@section('title', '| Brands')
@section('content')
<!-- Content Wrapper. Contains page content -->
<div class="content-wrapper">
    <!-- Content Header (Page header) -->
    <section class="content-header">
        <h1>{{ isset($brand) ? "Update Brand" : "Create Brand" }}</h1>
        <!-- You can dynamically generate breadcrumbs here -->
        <ol class="breadcrumb">
            <li><a href="/admin/brands"><i class="fa fa-dashboard"></i> Brands</a></li>
            <li class="active">Here</li>
        </ol>
    </section>

    <!-- Main content -->
    <section class="content">
        <div class="row">
            <div class="col-xs-12">
                <div class="box">
                    <div class="row">
                        <div class="col-xs-12 col-md-6 col-md-offset-3">
                            @if (count($errors) > 0)
                            <div class="alert alert-danger alert-dismissible m-t-30">
                                <button type="button" class="close" data-dismiss="alert" aria-hidden="true">×</button>
                                <h4><i class="icon fa fa-ban"></i> Error!</h4>
                                <ul>
                                    @foreach ($errors->all() as $error)
                                    <li>{{ $error }}</li>
                                    @endforeach
                                </ul>
                            </div>
                            @endif

                            <form role="form" action="{{ isset($brand) ? action('Admin\BrandsController@postUpdate', ['id' => $brand->id]) : action('Admin\BrandsController@postCreate') }}" method="post" enctype="multipart/form-data">
                                {!! csrf_field() !!}
                                <div class="box-body">
                                    <div class="form-group">
                                        <label for="name">Title</label>
                                        <input type="text" class="form-control" name="title" value="{{ (isset($brand) && isset($title))  ? $title : '' }}" placeholder="Title">
                                    </div>
                                    <div class="form-group">
                                        <label for="name">Description</label>
                                        <textarea class="form-control" name="description" placeholder="Description">{{ (isset($brand) && isset($description))  ? $description : '' }}</textarea>
                                    </div>
                                    <div class="form-group">
                                        <label id="image_uploader">Upload</label>
                                        <div id="image_fields">
                                            @if (isset($brand->brand_logo) && $brand->brand_logo != "")
                                                <img src="{{ asset ("/files/brands/" . $brand->brand_logo) }}" height="100"/>
                                            @endif
                                            <input type="file" class="form-control" name="brand_logo"/>
                                        </div>
                                        <div id="image_status"></div>
                                    </div>
                                    <div class="form-group">
                                        <label for="name">Sort</label>
                                        <input type="text" class="form-control" name="sort" value="{{ isset($brand) ? $brand->sort : '' }}" id="sort" placeholder="The sort must be a number">
                                    </div>
                                </div>
                                <div class="box-footer text-right">
                                    <button type="submit" class="btn btn-primary">Submit</button>
                                </div>
                            </form>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </section><!-- /.content -->
</div><!-- /.content-wrapper -->
@endsection