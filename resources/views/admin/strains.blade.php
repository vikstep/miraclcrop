@extends('layouts.admin.index')
@section('title', '| Strains')
@section('content')
<!-- Content Wrapper. Contains page content -->
<div class="content-wrapper">
    <!-- Content Header (Page header) -->
    <section class="content-header">
        <h1>Strains</h1>
        <!-- You can dynamically generate breadcrumbs here -->
        <ol class="breadcrumb">
            <li><a href="#"><i class="fa fa-dashboard"></i> Strains</a></li>
        </ol>
    </section>

    <!-- Main content -->
    <section class="content">
        <div class="row">
            <div class="col-xs-12">
                <div class="box">
                    <div class="box-body">
                        @if (session('error'))
                        <div class="alert alert-danger">
                            {{ session('error') }}
                        </div>
                        @endif
                        <div class="text-right m-b-30"><a href="{{ URL::to('/admin/strain/create') }}" class="btn btn-primary">Add new Strain</a></div>
                        <table class="table table-bordered table-hover datatable middle" data-order="[[ 2, &quot;desc&quot; ]]">
                            <thead>
                                <tr>
                                    <th>Title</th>
                                    <th>Locales</th>
                                    <th>Sort</th>
                                    <th data-orderable="false" style="width: 50px" class="text-center">Action</th>
                                </tr>
                            </thead>
                            <tbody>
                                @foreach ($strains as $strain)
                                <tr>
                                    <td>{{ trans('strain.' . $strain->id . '.title') }}</td>
                                    <td>
                                        @foreach($strain->translations as $translation)
                                        <span class="label label-info m-r-10">{{ $translation->locale }}</span>
                                        @endforeach
                                        <a href="/admin/strain/translation/{{ $strain->id }}" class="btn btn-primary translation-edit">Edit</a>
                                    </td>
                                    <td>{{ $strain->sort }}</td>
                                    <td>
                                        <a href="/admin/strain/{{ $strain->id }}" class="m-l-r-15"><i class="fa fa-edit"></i></a>
                                        <a href="javascript:void(0)" rel="{{ $strain->id }}" data-type="strain" class="recordDelete"><i class="fa fa-remove"></i></a>
                                    </td>
                                </tr>
                                @endforeach
                            </tbody>
                        </table>
                    </div>
                </div>
            </div>
        </div>
    </section><!-- /.content -->
</div><!-- /.content-wrapper -->
@endsection