@extends('layouts.admin.index')
@section('title', '| Posts')
@section('content')
    <!-- Content Wrapper. Contains page content -->
    <div class="content-wrapper">
        <!-- Content Header (Page header) -->
        <section class="content-header">
            <h1>Create Post Translations</h1>
            <!-- You can dynamically generate breadcrumbs here -->
            <ol class="breadcrumb">
                <li><a href="/admin/posts"><i class="fa fa-dashboard"></i> Posts</a></li>
                <li class="active">Here</li>
            </ol>
        </section>

        <!-- Main content -->
        <section class="content">
            <div class="row">
                <div class="col-xs-12">
                    <div class="box">
                        <div class="col-md-12">
                            <hr class="divider">
                            @if (count($errors) > 0)
                                <div class="alert alert-danger alert-dismissible m-t-30">
                                    <button type="button" class="close" data-dismiss="alert" aria-hidden="true">×</button>
                                    <h4><i class="icon fa fa-ban"></i> Error!</h4>
                                    <ul>
                                        @foreach ($errors->all() as $error)
                                            <li>{{ $error }}</li>
                                        @endforeach
                                    </ul>
                                </div>
                            @endif

                        </div>
                        <div class="clearfix"></div>
                        <div class="col-md-12">
                            <div class="nav-tabs-custom">
                                <ul class="nav nav-tabs custom-tab" id="detail_tab">
                                    @foreach($languages as $language_type => $language)
                                        <li class="{{$language_type == 'en' ? 'active':''}}"><a href="#{{$language}}" data-toggle="tab" aria-expanded="{{$language_type == 'en' ? 'true':'false'}}">{{ ucfirst($language) }}</a></li>
                                    @endforeach
                                </ul>
                                <form role="form" action="{{action('Admin\PostsController@postTranslation', ['post_id' => $post->id]) }}" method="post">
                                    {!! csrf_field() !!}
                                    <div class="tab-content">
                                        @foreach($languages as $language_type => $language)
                                            <div class="tab-pane {{$language_type == 'en' ? 'active':''}}" id="{{$language}}">
                                                <h3>{{ ucfirst($language) }}</h3>
                                                <hr class="divider">
                                                <div class="col-xs-12 col-md-6 col-md-offset-3">
                                                    <div class="form-group">
                                                        <label for="name">Title</label>
                                                        <?php $check_title_translation = ''; ?>
                                                        @foreach($translations['title'] as  $translation)
                                                            @if($translation->locale == $language_type )
                                                                <input type="text" class="form-control" name="title[{{$language_type}}][]" value="{{ $translation->text }}" {{$language_type == 'en' ? 'disabled': ''}} placeholder="Title">
                                                                <?php $check_title_translation = $translation->locale; ?>
                                                            @endif
                                                        @endforeach
                                                        @if(!$check_title_translation)
                                                            <input type="text" class="form-control" name="title[{{$language_type}}][]" value=""  placeholder="Title">
                                                        @endif
                                                        <?php $check_title_translation = ''; ?>
                                                    </div>

                                                    <div class="form-group">
                                                        <label for="name">Excerpt</label>
                                                        <?php $check_excerpt_translation = ''; ?>
                                                        @foreach($translations['excerpt'] as $translation)
                                                            @if($translation->locale == $language_type)
                                                                <textarea class="form-control excerpt-editor" name="excerpt[{{$language_type}}][]" {{$language_type == 'en' ? 'disabled': ''}} >{{ $translation->text }}</textarea>
                                                                <?php $check_excerpt_translation = $translation->locale; ?>
                                                            @endif
                                                        @endforeach
                                                        @if(!$check_excerpt_translation)
                                                            <textarea class="form-control excerpt-editor" name="excerpt[{{$language_type}}][]" {{$language_type == 'en' ? 'disabled': ''}}></textarea>
                                                        @endif
                                                        <?php $check_excerpt_translation = ''; ?>
                                                    </div>

                                                    <div class="form-group">
                                                        <label for="name">Content</label>
                                                        <?php $check_content_translation = ''; ?>
                                                        @foreach($translations['content'] as $translation)
                                                            @if($translation->locale == $language_type)
                                                                <textarea class="form-control editor" name="content[{{$language_type}}][]" {{$language_type == 'en' ? 'disabled': ''}} >{{ $translation->text }}</textarea>
                                                                <?php $check_content_translation = $translation->locale; ?>
                                                            @endif
                                                        @endforeach
                                                        @if(!$check_content_translation)
                                                            <textarea class="form-control editor" name="content[{{$language_type}}][]" {{$language_type == 'en' ? 'disabled': ''}}></textarea>
                                                        @endif
                                                        <?php $check_content_translation = ''; ?>
                                                    </div>
                                                </div>
                                            </div>
                                        @endforeach
                                    </div>
                                    <div class="clearfix"></div>
                                    <div class="col-xs-12 col-md-6 col-md-offset-3">
                                        <div class="pull-right p-t-b-10">
                                            <button type="submit" class="btn btn-primary">Submit</button>
                                        </div>
                                    </div>
                                </form>
                            </div>
                        </div>
                        <div class="clearfix"></div>
                    </div>
                    <div class="clearfix"></div>
                </div>
        </section><!-- /.content -->
    </div><!-- /.content-wrapper -->
    
@endsection



