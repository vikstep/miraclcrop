@extends('layouts.admin.index')
@section('title', '| Brands')
@section('content')
<!-- Content Wrapper. Contains page content -->
<div class="content-wrapper">
    <!-- Content Header (Page header) -->
    <section class="content-header">
        <h1>Brands</h1>
        <!-- You can dynamically generate breadcrumbs here -->
        <ol class="breadcrumb">
            <li><a href="#"><i class="fa fa-dashboard"></i> Brands</a></li>
        </ol>
    </section>

    <!-- Main content -->
    <section class="content">
        <div class="row">
            <div class="col-xs-12">
                <div class="box">
                    <div class="box-body">
                        @if (session('error'))
                        <div class="alert alert-danger">
                            {{ session('error') }}
                        </div>
                        @endif
                        <div class="text-right m-b-30"><a href="{{ URL::to('/admin/brand/create') }}" class="btn btn-primary">Add new Brand</a></div>
                        <table class="table table-bordered table-hover datatable middle" data-order="[[ 2, &quot;desc&quot; ]]">
                            <thead>
                                <tr>
                                    <th>Title</th>
                                    <th>Locales</th>
                                    <th>Sort</th>
                                    <th data-orderable="false" style="width: 50px" class="text-center">Action</th>
                                </tr>
                            </thead>
                            <tbody>
                                @foreach ($brands as $brand)
                                <tr>
                                    <td>{{ trans('brand.' . $brand->id . '.title') }}</td>
                                    <td>
                                        @foreach($brand->translations as $translation)
                                        <span class="label label-info m-r-10">{{ $translation->locale }}</span>
                                        @endforeach
                                        <a href="/admin/brand/translation/{{ $brand->id }}" class="btn btn-primary translation-edit">Edit</a>
                                    </td>
                                    <td>{{ $brand->sort }}</td>
                                    <td>
                                        <a href="/admin/brand/{{ $brand->id }}" class="m-l-r-15"><i class="fa fa-edit"></i></a>
                                        <a href="javascript:void(0)" rel="{{ $brand->id }}" data-type="brand" class="recordDelete"><i class="fa fa-remove"></i></a>
                                    </td>
                                </tr>
                                @endforeach
                            </tbody>
                        </table>
                    </div>
                </div>
            </div>
        </div>
    </section><!-- /.content -->
</div><!-- /.content-wrapper -->
@endsection