<aside class="main-sidebar">

  <section class="sidebar">

    <div class="user-panel">
      <div class="pull-left image">
        <img src="/files/avatar/{{ Auth::user()->image ? $patient->image : 'default-avatar.png' }}" class="img-circle" alt="User Image" />
      </div>
      <div class="pull-left info sidebar-username">
        <p>{{ Auth::user()->first_name . ' ' . Auth::user()->last_name  }}</p>
        {{--<a href="#"><i class="fa fa-circle text-success"></i> Online</a>--}}
      </div>
    </div>


    <ul class="sidebar-menu">
      <li class="header"></li>
      <!-- Optionally, you can add icons to the links -->
      <li class="mcrop_dashboard"><a href="{{ URL::to('/admin/') }}"><span>Dashboard</span></a></li>
      <li class="mcrop_languages mcrop_language"><a href="{{ URL::to('/admin/languages') }}"><span>Languages</span></a></li>

      <li class="treeview mcrop_users">
        <a href="#"><span>Users</span> <i class="fa fa-angle-left pull-right"></i></a>
        <ul class="treeview-menu">
          <li class="mcrop_patients mcrop_patient"><a href="{{ URL::to('/admin/patients') }}"><span>Patients</span></a></li>
          <li class="mcrop_doctors mcrop_doctor"><a href="{{ URL::to('/admin/doctors') }}"><span>Doctors</span></a></li>
        </ul>
      </li>
      <li class="treeview mcrop_blog">
        <a href="#"><span>Blog</span> <i class="fa fa-angle-left pull-right"></i></a>
        <ul class="treeview-menu">
          <li class="mcrop_post_create"><a href="{{ URL::to('/admin/post/create') }}"><span>Add New Posts</span></a></li>
          <li class="mcrop_posts mcrop_post mcrop_post_translation"><a href="{{ URL::to('/admin/posts') }}"><span>Posts</span></a></li>
          <li class="mcrop_categories mcrop_category mcrop_category_create mcrop_category_translation"><a href="{{ URL::to('/admin/categories') }}"><span>Categories</span></a></li>
        </ul>
      <li class="treeview mcrop_product">
        <a href="#"><span>Product</span> <i class="fa fa-angle-left pull-right"></i></a>
        <ul class="treeview-menu">
          <li class="mcrop_product_create"><a href="{{ URL::to('/admin/product/create') }}"><span>Add New Products</span></a></li>
          <li class="mcrop_products"><a href="{{ URL::to('/admin/products') }}"><span>Products</span></a></li>
          <li class="mcrop_product-categories mcrop_product-category_create mcrop_product-category mcrop_product-category_translation"><a href="{{ URL::to('/admin/product-categories') }}"><span>Categories</span></a></li>
          <li class="mcrop_portions mcrop_portion_create mcrop_portion mcrop_portion_translation"><a href="{{ URL::to('/admin/portions') }}"><span>Portions</span></a></li>
          <li class="mcrop_strains mcrop_strain mcrop_strain_create mcrop_strain_translation"><a href="{{ URL::to('/admin/strains') }}"><span>Strains</span></a></li>
          <li class="mcrop_brands mcrop_brand mcrop_brand_create mcrop_brand_translation"><a href="{{ URL::to('/admin/brands') }}"><span>Brands</span></a></li>
        </ul>
      </li>
      <li class="mcrop_languages mcrop_orders mcrop_order"><a href="{{ URL::to('/admin/orders') }}"><span>Orders</span></a></li>
    </ul>
  </section>

</aside>