@extends('layouts.admin.index')
@section('title', '| Products')
@section('content')
<!-- Content Wrapper. Contains page content -->
<div class="content-wrapper">
    <!-- Content Header (Page header) -->
    <section class="content-header">
        <h1>{{ isset($product->id) ? "Update Product" : "Create Product" }}</h1>
        <!-- You can dynamically generate breadcrumbs here -->
        <ol class="breadcrumb">
            <li><a href="/admin/products"><i class="fa fa-dashboard"></i> Products</a></li>
            <li class="active">Here</li>
        </ol>
    </section>

    <!-- Main content -->
    <section class="content">
        <div class="row">
            <div class="col-xs-12">
                <div class="box">
                    <div class="row">
                        <div class="col-xs-12 col-md-6 col-md-offset-3">
                            @if (count($errors) > 0)
                            <div class="alert alert-danger alert-dismissible m-t-30">
                                <button type="button" class="close" data-dismiss="alert" aria-hidden="true">×</button>
                                <h4><i class="icon fa fa-ban"></i> Error!</h4>
                                <ul>
                                    @foreach ($errors->all() as $error)
                                    <li>{{ $error }}</li>
                                    @endforeach
                                </ul>
                            </div>
                            @endif
                            <form role="form" action="{{ isset($product->id) ? action('Admin\ProductsController@postUpdate', ['id' => $product->id]) : action('Admin\ProductsController@postCreate') }}" method="post" enctype="multipart/form-data">
                                {!! csrf_field() !!}
                                <div class="box-body">
                                    <div class="form-group">
                                        <label for="name">Title</label>
                                        <input type="text" class="form-control" name="title" value="{{ (isset($product) && isset($title))  ? $title : '' }}" placeholder="Title">
                                    </div>
                                    <div class="form-group">
                                        <label for="name">Excerpt</label>
                                        <textarea class="form-control excerpt-editor validate-excerpt" name="product_excerpt">{{ (isset($product) && isset($excerpt)) ? $excerpt : '' }}</textarea>
                                    </div>
                                    <div class="form-group">
                                        <label for="name">Content</label>
                                        <textarea class="form-control product_editor validate-content " name="product_content">{{ (isset($product) && isset($content))  ? $content : '' }}</textarea>
                                    </div>
                                    <div class="form-group">
                                        <label for="name">Categories</label>
                                        <div class="btn-group hierarchy-select" data-resize="auto" id="example-one">
                                            <button type="button" class="btn btn-default dropdown-toggle" data-toggle="dropdown">
                                                <span class="selected-label pull-left">&nbsp;</span>
                                                <span class="caret"></span>
                                            </button>
                                            <div class="dropdown-menu open">
                                                <div class="hs-searchbox">
                                                    <input type="text" class="form-control" autocomplete="off">
                                                </div>
                                                <ul class="dropdown-menu inner" role="menu">
                                                    <li data-value="" data-level="1" {{ isset($product->id) ? '' : 'class=active' }}>
                                                        <a href="#">All categories</a>
                                                    </li>
                                                    @foreach($categories as $category)
                                                        <li data-value="{{ $category->id }}" data-level="1" {{ (isset($product) && $product->category_id == $category->id) ? 'class=active' : '' }}>
                                                            <a href="#">{{ $category->translation->text }}</a>
                                                        </li>
                                                        @if(count($category['sub_categories']) > 0)
                                                            @foreach($category['sub_categories'] as $sub_category)
                                                                <li data-value="{{ $sub_category['id'] }}" data-level="2" {{ (isset($product) && $product->category_id == $sub_category['id']) ? 'class=active' : '' }}>
                                                                    <a href="#">{{ $sub_category->translation->text }}</a>
                                                                </li>
                                                            @endforeach
                                                        @endif
                                                    @endforeach
                                                    
                                                </ul>
                                            </div>
                                            <input class="hidden hidden-field" name="category" readonly="readonly" aria-hidden="true" type="text"/>
                                        </div>
                                    </div>
                                    <div class="form-group">
                                        <label for="name">Brands</label>
                                        <select class="form-control" name="brand">
                                            @foreach($brands as $brand)
                                            <option value="{{ $brand->id }}" {{ (isset($product) && $product->brand_id == $brand->id) ? 'selected' : '' }}>{{ $brand->translation->text }}</option>
                                            @endforeach
                                        </select>
                                    </div>
                                    <div class="form-group">
                                        <label id="image_uploader">Main Image</label>
                                        <p class="text-red">Image sizes should be 400x400 px</p>
                                        <div id="image_fields">
                                            @if (isset($product->image) && $product->image != "")
                                            <img src="{{ asset ("/files/products/" . $product->image . '-s.' . $product->extension) }}" height="100"/>
                                            @endif
                                            <input type="file" class="form-control" name="image"/>
                                        </div>
                                        <div id="image_status"></div>
                                    </div>
                                    <div class="form-group">
                                        <label for="name">Weight in stock (g)</label>
                                        <input type="text" class="form-control" name="weight" value="{{ (isset($product) && $product->weight_in_stock)  ? $product->weight_in_stock : '' }}" placeholder="Weight">
                                    </div>
                                    <div class="form-group">
                                        <label for="strains">Strains</label>
                                        <select class="form-control select2" name="strains[]" id="sttains" multiple="multiple" style="width: 100%;">
                                            @foreach($strains as $strain)
                                            <option value="{{ $strain->id }}" {{ (isset($selectedStrains) && in_array($strain->id, $selectedStrains)) ? 'selected' : '' }}>{{ $strain->translation->text }}</option>
                                            @endforeach
                                        </select>
                                    </div>
                                    <div class="form-group">
                                        <div class="row">
                                            <div class="col-md-12">
                                                <label for="portions">Portions</label>
                                            </div>
                                        </div>
                                        <div class="row">
                                            <div class="col-md-6">
                                                <select class="form-control" id="portions">
                                                    @foreach($portions as $portion)
                                                    <option value="{{ $portion->id }}">{{ $portion->translation->text }} ({{ $portion->weight }})</option>
                                                    @endforeach
                                                </select>
                                            </div>
                                            <div class="col-md-4">
                                                <input type="number" class="form-control" step=0.01 id="price" placeholder="Price" />
                                            </div>
                                            <div class="col-md-2">
                                                <button type="button" class="btn btn-success add-portion pull-right"><span class="glyphicon glyphicon-plus"></span></button>
                                            </div>
                                        </div>
                                    </div>
                                    <div class="form-group">
                                        <table class="table table-bordered">
                                            <thead class="alert-info">
                                            <th>Portion</th>
                                            <th>Price</th>
                                            <th></th>
                                            </thead>
                                            <tbody class="bg-white portions-body">
                                                @if($product->id && $selectedPortions) 
                                                @foreach ($portions as $portion)
                                                @if(key_exists($portion->id, $selectedPortions))
                                                <tr><td>{{ $portion->translation->text }} ({{ $portion->weight }})</td><td>{{ floatval($selectedPortions[$portion->id]) }}</td><td class="text-center"><i class="fa fa-times remove-portion" role="button" data-id="{{ $portion->id }}" title="Remove" aria-hidden="true"></i></td></tr>
                                                @endif
                                                @endforeach
                                                @endif
                                            </tbody>
                                        </table>
                                        <div id="portions-hidden">
                                            @if($product->id && $selectedPortions) 
                                            @foreach ($portions as $portion)
                                            @if(key_exists($portion->id, $selectedPortions))
                                            <div id="p_{{ $portion->id }}">
                                                <input type="hidden" class="added-portions" name="portions[]" value="{{ $portion->id }}">
                                                <input type="hidden" name="prices[]" value="{{ $selectedPortions[$portion->id] }}">
                                            </div>
                                            @endif
                                            @endforeach
                                            @endif
                                        </div>
                                    </div>
                                    <div class="form-group">
                                        <label for="name">Status</label>
                                        <select class="form-control" name="status">
                                            @foreach($product::$status as $key => $status)
                                            <option value="{{ $key }}" {{ (isset($product) && $product->status == $key) ? 'selected' : '' }}>{{ $status }}</option>
                                            @endforeach
                                        </select>
                                    </div>

                                    @if(isset($product->id))
                                    @include('admin._productMedia')
                                    @endif

                                    <div class="form-group">
                                        <label for="name">Sort</label>
                                        <input type="text" class="form-control" name="sort" value="{{ isset($product) ? $product->sort : '' }}" id="sort" placeholder="The sort must be a number">
                                    </div>
                                </div>
                                <div class="box-footer text-right">
                                    <button type="submit" class="btn btn-primary">Submit</button>
                                </div>
                            </form>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </section><!-- /.content -->
</div><!-- /.content-wrapper -->
@endsection