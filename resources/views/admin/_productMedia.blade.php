<div class="box box-primary">
    <div class="box-header with-border">
        <h3 class="box-title">Product Media</h3>
        <div class="box-tools pull-right">
            <button type="button" class="btn btn-info add-image" data-toggle="modal" data-target="#imageModal" title="Add image"><i class="glyphicon glyphicon-picture"></i></button>
            <button type="button" class="btn btn-danger add-video" data-toggle="modal" data-target="#videoModal" title="Add youtube video"><i class="glyphicon glyphicon-expand"></i></button>
        </div>
    </div>
    <div class="box-body media-container">
        <ul class="products-list product-list-in-box">
            @foreach ($product->media as $product_media)
            <li class="item">
                <div class="product-img">
                    <img src="{{ $product_media->media_type == '2' ? 'https://img.youtube.com/vi/' . $product_media->media . '/default.jpg' : asset('/files/products/' . $product_media->media . '-s.' . $product_media->extension) }}" />
                </div>

                <div class="product-info">
                    <span class="fa fa-times remove-media pull-right" role="button" data-id="{{ $product_media->id }}" title="Remove" aria-hidden="true"></span>
                    <span class="label label-{{ $product_media->media_type == '2' ? 'danger' : 'info' }} media-type">{{ $product_media::$types[$product_media->media_type] }}</span>
                </div>
                <div class="clearfix"></div>
            </li>
            @endforeach
        </ul>
    </div>
</div>

<div class="modal" id="imageModal">
    <div class="modal-dialog">
        <div class="modal-content">
            <div class="modal-header">
                <button type="button" class="close" data-dismiss="modal" aria-label="Close">
                    <span aria-hidden="true">×</span></button>
                <h4 class="modal-title">Add Image</h4>
            </div>
            <div class="modal-body">
                <div class="alert alert-danger alert-dismissible m-t-30 image-error hidden">
                    <button type="button" class="close" data-dismiss="alert" aria-hidden="true">×</button>
                    <h4><i class="icon fa fa-ban"></i> Error!</h4>
                    <ul>
                        <li>Image sizes should be 400x400 px</li>
                       
                    </ul>
                </div>
                <p class="text-red">Image sizes should be 400x400 px</p>
                <div class="fileUpload">
                    <div id="media_uploader">Upload</div>
                    <div id="image_fields">
                    </div>
                    <div id="image_status"></div>
                </div>    
            </div>
            <div class="modal-footer">
                <button type="button" class="btn btn-default pull-left" data-dismiss="modal">Close</button>
                <button type="button" class="btn btn-primary" id="saveImage">Save changes</button>
            </div>
        </div>
    </div>
</div>

<div class="modal" id="videoModal">
    <div class="modal-dialog">
        <div class="modal-content">
            <div class="modal-header">
                <button type="button" class="close" data-dismiss="modal" aria-label="Close">
                    <span aria-hidden="true">×</span></button>
                <h4 class="modal-title">Add Youtube Video</h4>
            </div>
            <div class="modal-body">
                <p>
                    <input type="text" id="videourl" class="form-control" placeholder="Youtube video url" />
                    <input type="hidden" id="product_id" value="{{ $product->id }}" />
                </p>
            </div>
            <div class="modal-footer">
                <button type="button" class="btn btn-default pull-left" data-dismiss="modal">Close</button>
                <button type="button" class="btn btn-primary" id="saveVideo">Save changes</button>
            </div>
        </div>
    </div>
</div>
