@extends('layouts.admin.index')
@section('title', '| Orders')
@section('content')
    <!-- Content Wrapper. Contains page content -->
    <div class="content-wrapper">
        <!-- Content Header (Page header) -->
        <section class="content-header">
            <h1>Orders</h1>
            <!-- You can dynamically generate breadcrumbs here -->
            <ol class="breadcrumb">
                <li><a href="#"><i class="fa fa-dashboard"></i> Orders</a></li>
            </ol>
        </section>

        <!-- Main content -->
        <section class="content">
            <div class="row">
                <div class="col-xs-12">
                    <div class="box">
                        <div class="box-body">
                            <table class="table table-bordered table-hover datatable middle" data-order="[[ 3, &quot;desc&quot; ]]">
                                <thead>
                                <tr>
                                    <th>Order Number</th>
                                    <th>User</th>
                                    <th>Status</th>
                                    <th>Order Date</th>
                                    <th data-orderable="false" style="width: 50px" class="text-right">Action</th>
                                </tr>
                                </thead>
                                <tbody>
                                @foreach ($orders as $order)
                                    <?php $ordered_by = $user->find($order->user_id); ?>
                                    <tr>
                                        <td class="col-md-4">#{{ $order->order_number }}</td>
                                        <td class="col-md-4"><a href="{{ URL::to('admin/patient/' . $ordered_by->id) }}" target="_blank">{{ $ordered_by->first_name . ' ' . $ordered_by->last_name }}</a></td>
                                        <td class="col-md-1">{{ $order->status }}</td>
                                        <td class="col-md-2">{{ \Carbon\Carbon::parse($order->created_at)->format('m/d/Y H:i') }}</td>
                                        <td class="col-md-1 text-right">
                                            <a href="/admin/order/{{ $order->id }}" class="m-l-r-15" title="Show details"><i class="fa fa-list"></i></a>
                                        </td>
                                    </tr>
                                @endforeach
                                </tbody>
                            </table>
                        </div>
                    </div>
                </div>
            </div>
        </section><!-- /.content -->
    </div><!-- /.content-wrapper -->
@endsection