@extends('layouts.admin.index')
@section('title', '| Patients')
@section('content')
<!-- Content Wrapper. Contains page content -->
<div class="content-wrapper">
    <!-- Content Header (Page header) -->
    <section class="content-header">
        <h1>&nbsp;</h1>
        <!-- You can dynamically generate breadcrumbs here -->
        <ol class="breadcrumb">
            <li><a href="/admin/patients"><i class="fa fa-dashboard"></i> Patients</a></li>
            <li class="active">Here</li>
        </ol>
    </section>

    <!-- Main content -->
    <section class="content">
        <div class="row">
            <div class="col-xs-12">
                <div class="col-md-3">
                    <!-- Profile Image -->
                    <div class="box box-primary">
                        <div class="box-body box-profile">
                            <img class="profile-user-img img-responsive" src="{{ $patient->face_photo ? '/files/profiles/' . $patient->role . '-' . $patient->id . '/' . $patient->face_photo : '/files/avatar/default-avatar.png' }}" alt="User profile picture">
                            {{--<img class="profile-user-img img-responsive img-circle" src="/files/avatar/{{$patient->image ? $patient->image : 'default-avatar.png' }}" alt="User profile picture">--}}
                            <h3 class="profile-username text-center">{{ $patient->first_name . ' ' . $patient->last_name  }}</h3>
                            <p class="text-muted text-center">{{ $patient->role  }}</p>
                        </div>
                        <!-- /.box-body -->
                    </div>
                    <!-- /.box -->
                </div>
                <div class="col-md-9">
                    <div class="nav-tabs-custom">
                        <ul class="nav nav-tabs">
                            <li class="active"><a href="#user-info" data-toggle="tab" aria-expanded="true">User Info</a></li>
                            <li class=""><a href="#pages-history" data-toggle="tab" aria-expanded="false">Pages history</a></li>
                            <li class=""><a href="#order-history" data-toggle="tab" aria-expanded="false">Order history</a></li>
                        </ul>
                        <div class="tab-content">
                            <div class="tab-pane active" id="user-info">
                                <div class="row user-info-item">
                                    <div class="col-xs-12 col-md-3 user-info-title">E-mail</div>
                                    <div class="col-xs-12 col-md-9 user-info-value">{{ $patient->email }}</div>
                                </div>
                                <div class="row user-info-item">
                                    <div class="col-xs-12 col-md-3 user-info-title">Has recipe</div>
                                    <div class="col-xs-12 col-md-9 user-info-value">{{ $patient->has_recipe }}</div>
                                </div>
                                <div class="row user-info-item">
                                    <div class="col-xs-12 col-md-3 user-info-title">Gender</div>
                                    <div class="col-xs-12 col-md-9 user-info-value">{{ $patient->gender }}</div>
                                </div>
                                <div class="row user-info-item">
                                    <div class="col-xs-12 col-md-3 user-info-title">Date of birth</div>
                                    <div class="col-xs-12 col-md-9 user-info-value">{{ $patient->dob }}</div>
                                </div>
                                <div class="row user-info-item">
                                    <div class="col-xs-12 col-md-3 user-info-title">Phone</div>
                                    <div class="col-xs-12 col-md-9 user-info-value">{{ $patient->phone }}</div>
                                </div>
                                <div class="row user-info-item">
                                    <div class="col-xs-12 col-md-3 user-info-title">Address</div>
                                    <div class="col-xs-12 col-md-9 user-info-value">{{ $patient->address }}</div>
                                </div>
                                <div class="row user-info-item">
                                    <div class="col-xs-12 col-md-3 user-info-title">State</div>
                                    <div class="col-xs-12 col-md-9 user-info-value">{{ $patient->state }}</div>
                                </div>
                                <div class="row user-info-item">
                                    <div class="col-xs-12 col-md-3 user-info-title">City</div>
                                    <div class="col-xs-12 col-md-9 user-info-value">{{ $patient->city }}</div>
                                </div>
                                <div class="row user-info-item">
                                    <div class="col-xs-12 col-md-3 user-info-title">Zip code</div>
                                    <div class="col-xs-12 col-md-9 user-info-value">{{ $patient->zip_code }}</div>
                                </div>
                                <div class="row user-info-item m-t-5">
                                    <div class="col-xs-12 col-md-3 user-info-title">Driver License</div>
                                    <div class="col-xs-12 col-md-9 user-info-value">
                                        {{ $patient->driver_license }}
                                        {!!  $patient->driver_license_image ? '<img class="right-img-x" src="/files/profiles/' . $patient->role . '-' . $patient->id . '/' . $patient->driver_license_image . '">' : '' !!}
                                    </div>
                                </div>
                                <div class="row user-info-item">
                                    <div class="col-xs-12 col-md-3 user-info-title">ID</div>
                                    <div class="col-xs-12 col-md-9 user-info-value">
                                        {{ $patient->id_card }}
                                        {!!  $patient->id_card_image ? '<img class="right-img-x" src="/files/profiles/' . $patient->role . '-' . $patient->id . '/' . $patient->id_card_image . '">' : '' !!}
                                    </div>
                                </div>
                                <div class="row user-info-item m-t-5">
                                    <div class="col-xs-12 col-md-3 user-info-title">Patient recommendation</div>
                                    <div class="col-xs-12 col-md-9 user-info-value">
                                        {!!  $patient->recommendation_image ? '<img class="right-img-x" src="/files/profiles/' . $patient->role . '-' . $patient->id . '/' . $patient->recommendation_image . '">' : '' !!}
                                    </div>
                                </div>
                                <div class="row user-info-item">
                                    <div class="col-xs-12 col-md-3 user-info-title">Notes</div>
                                    <div class="col-xs-12 col-md-9 user-info-value">{{ $patient->notes }}</div>
                                </div>
                                <div class="row user-info-item">
                                    <div class="col-xs-12 col-md-3 user-info-title">Status</div>
                                    <div class="col-xs-12 col-md-5 user-info-value">{{ $patient->status == 'rejected' ? $patient->status . ' ( ' . $patient->status_reason . ' ) ' : $patient->status }}</div>
                                    <div class="col-xs-12 col-md-4 user-info-value">
                                        <select id="statuses" class="form-control">
                                            <option value="0">-- Change status --</option>
                                            <option value="approved">Aprove</option>
                                            <option value="blocked">Block</option>
                                            <option value="rejected">Reject</option>
                                        </select>
                                        <textarea id="status_reason" class="form-control hidden"></textarea>
                                        <input type="button" class="btn btn-default" id="changeStatus" data-id="{{ $patient->id }}" value="Save" />
                                    </div>
                                </div>
                                <div class="row user-info-item">
                                    <div class="col-xs-12 col-md-3 user-info-title">Ip</div>
                                    <div class="col-xs-12 col-md-9 user-info-value">{{ $patient->ip }}</div>
                                </div>
                                <div class="row user-info-item">
                                    <div class="col-xs-12 col-md-3 user-info-title">User agent</div>
                                    <div class="col-xs-12 col-md-9 user-info-value">{{ $patient->user_agent }}</div>
                                </div>
                                <div class="row user-info-item">
                                    <div class="col-xs-12 col-md-3 user-info-title">Registration date</div>
                                    <div class="col-xs-12 col-md-9 user-info-value">{{ $patient->created_at }}</div>
                                </div>
                            </div>
                            <div class="tab-pane" id="pages-history">
                                pages history content
                            </div>
                            <div class="tab-pane" id="order-history">
                                <section class="content">
                                    <div class="row">
                                        <div class="col-xs-12">
                                            <div class="box">
                                                <div class="box-body">
                                                    <table class="table table-bordered table-hover datatable middle">
                                                        <thead>
                                                            <tr>
                                                                <th>Order Number</th>
                                                                <th>Status</th>
                                                                <th data-orderable="false" style="width: 50px" class="text-right">Action</th>
                                                            </tr>
                                                        </thead>
                                                        <tbody>
                                                            @foreach ($orders as $order)
                                                            <tr>
                                                                <td class="col-md-4">#{{ $order->order_number }}</td>
                                                                <td class="col-md-2">{{ $order->status }}</td>
                                                                <td class="col-md-2 text-right">
                                                                    <a href="/admin/order/{{ $order->id }}" class="m-l-r-15" title="Show details"><i class="fa fa-list"></i></a>
                                                                </td>
                                                            </tr>
                                                            @endforeach
                                                        </tbody>
                                                    </table>
                                                </div>
                                            </div>
                                        </div>
                                    </div>
                                </section>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </section><!-- /.content -->
</div><!-- /.content-wrapper -->
@endsection