@extends('layouts.admin.index')
@section('title', '| Languages')
@section('content')
    <!-- Content Wrapper. Contains page content -->
    <div class="content-wrapper">
        <!-- Content Header (Page header) -->
        <section class="content-header">
            <h1>{{ isset($language) ? "Update Language" : "Create Language" }}</h1>
            <!-- You can dynamically generate breadcrumbs here -->
            <ol class="breadcrumb">
                <li><a href="/admin/languages"><i class="fa fa-dashboard"></i> Languages</a></li>
                <li class="active">Here</li>
            </ol>
        </section>

        <!-- Main content -->
        <section class="content">
            <div class="row">
                <div class="col-xs-12">
                    <div class="box">
                        <div class="row">
                            <div class="col-xs-12 col-md-6 col-md-offset-3">
                                @if (count($errors) > 0)
                                    <div class="alert alert-danger alert-dismissible m-t-30">
                                        <button type="button" class="close" data-dismiss="alert" aria-hidden="true">×</button>
                                        <h4><i class="icon fa fa-ban"></i> Error!</h4>
                                        <ul>
                                            @foreach ($errors->all() as $error)
                                                <li>{{ $error }}</li>
                                            @endforeach
                                        </ul>
                                    </div>
                                @endif

                                <form role="form" action="{{ isset($language) ? action('Admin\LanguagesController@postUpdate', ['id' => $language->id]) : action('Admin\LanguagesController@postCreate') }}" method="post">
                                    {!! csrf_field() !!}
                                    <div class="box-body">
                                        <div class="form-group">
                                            <label for="name">Name</label>
                                            <input type="text" class="form-control" name="name" value="{{ isset($language) ? $language->name : '' }}" id="name" placeholder="Name">
                                        </div>
                                        <div class="form-group">
                                            <label for="locale">Locale</label>
                                            <input type="text" class="form-control" name="language_locale" value="{{ isset($language) ? $language->locale : '' }}" id="locale" placeholder="Locale">
                                        </div>
                                        <div class="form-group">
                                            <label for="name">Status</label>
                                            <select class="form-control" name="status">
                                                <option value="enabled" {{ (isset($language) && empty($language->deleted_at)) ? 'selected' : '' }}>Enabled</option>
                                                <option value="disabled" {{ (isset($language) && !empty($language->deleted_at)) ? 'selected' : '' }}>Disabled</option>
                                            </select>
                                        </div>
                                    </div>
                                    <div class="box-footer text-right">
                                        <button type="submit" class="btn btn-primary">Submit</button>
                                    </div>
                                </form>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
        </section><!-- /.content -->
    </div><!-- /.content-wrapper -->
@endsection