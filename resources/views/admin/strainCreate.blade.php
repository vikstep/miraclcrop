@extends('layouts.admin.index')
@section('title', '| Strains')
@section('content')
<!-- Content Wrapper. Contains page content -->
<div class="content-wrapper">
    <!-- Content Header (Page header) -->
    <section class="content-header">
        <h1>{{ isset($strain) ? "Update Strain" : "Create Strain" }}</h1>
        <!-- You can dynamically generate breadcrumbs here -->
        <ol class="breadcrumb">
            <li><a href="/admin/strains"><i class="fa fa-dashboard"></i> Strains</a></li>
            <li class="active">Here</li>
        </ol>
    </section>

    <!-- Main content -->
    <section class="content">
        <div class="row">
            <div class="col-xs-12">
                <div class="box">
                    <div class="row">
                        <div class="col-xs-12 col-md-6 col-md-offset-3">
                            @if (count($errors) > 0)
                            <div class="alert alert-danger alert-dismissible m-t-30">
                                <button type="button" class="close" data-dismiss="alert" aria-hidden="true">×</button>
                                <h4><i class="icon fa fa-ban"></i> Error!</h4>
                                <ul>
                                    @foreach ($errors->all() as $error)
                                    <li>{{ $error }}</li>
                                    @endforeach
                                </ul>
                            </div>
                            @endif

                            <form role="form" action="{{ isset($strain) ? action('Admin\StrainsController@postUpdate', ['id' => $strain->id]) : action('Admin\StrainsController@postCreate') }}" method="post">
                                {!! csrf_field() !!}
                                <div class="box-body">
                                    <div class="form-group">
                                        <label for="name">Title</label>
                                        <input type="text" class="form-control" name="title" value="{{ (isset($strain) && isset($title))  ? $title : '' }}" placeholder="Title">
                                    </div>
                                    <div class="form-group">
                                        <label for="name">Sort</label>
                                        <input type="text" class="form-control" name="sort" value="{{ isset($strain) ? $strain->sort : '' }}" id="sort" placeholder="The sort must be a number">
                                    </div>
                                </div>
                                <div class="box-footer text-right">
                                    <button type="submit" class="btn btn-primary">Submit</button>
                                </div>
                            </form>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </section><!-- /.content -->
</div><!-- /.content-wrapper -->
@endsection