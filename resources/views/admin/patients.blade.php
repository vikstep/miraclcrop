@extends('layouts.admin.index')
@section('title', '| Patients')
@section('content')
    <!-- Content Wrapper. Contains page content -->
    <div class="content-wrapper">
        <!-- Content Header (Page header) -->
        <section class="content-header">
            <h1>Patients</h1>
            <!-- You can dynamically generate breadcrumbs here -->
            <ol class="breadcrumb">
                <li><a href="#"><i class="fa fa-dashboard"></i> Patients</a></li>
            </ol>
        </section>

        <!-- Main content -->
        <section class="content">
            <div class="row">
                <div class="col-xs-12">
                    <div class="box">
                        <div class="box-body">
                            <table class="table table-bordered table-hover datatable middle" data-order="[[ 5, &quot;desc&quot; ]]">
                                <thead>
                                <tr>
                                    <th data-orderable="false" style="width: 26px">Avatar</th>
                                    <th>Full Name</th>
                                    <th>E-mail</th>
                                    <th>Gender</th>
                                    <th>Date of birth</th>
                                    <th>Reg Date</th>
                                    <th>Reg Step</th>
                                    <th>Status</th>
                                </tr>
                                </thead>
                                <tbody>
                                @foreach ($patients as $patient)
                                    <tr>
                                        <td><img src="{{ $patient->face_photo ? '/files/profiles/' . $patient->role . '-' . $patient->id . '/' . $patient->face_photo : '/files/avatar/default-avatar.png' }}" class="img-thumbnail" style="width: 50px"></td>
                                        {{--<td><img src="/files/avatar/{{$patient->image ? $patient->image : 'default-avatar.png' }}" class="img-thumbnail" style="width: 50px"></td>--}}
                                        <td><a href="{{ URL::to('/admin/patient/' . $patient->id) }}">{{ $patient->first_name . ' ' . $patient->last_name }}</a></td>
                                        <td>{{ $patient->email }}</td>
                                        <td>{{ $patient->gender }}</td>
                                        <td>{{ $patient->dob ? \Carbon\Carbon::parse($patient->dob)->format('m/d/Y') : '' }}</td>
                                        <td>{{ \Carbon\Carbon::parse($patient->created_at)->format('m/d/Y') }}</td>
                                        <td>{{ $patient->registration_step }}</td>
                                        <td>{{ $patient->status }}</td>
                                    </tr>
                                @endforeach
                                </tbody>
                            </table>
                        </div>
                    </div>
                </div>
            </div>
        </section><!-- /.content -->
    </div><!-- /.content-wrapper -->
@endsection