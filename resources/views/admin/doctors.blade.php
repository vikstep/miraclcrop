@extends('layouts.admin.index')
@section('title', '| Doctors')
@section('content')
    <!-- Content Wrapper. Contains page content -->
    <div class="content-wrapper">
        <!-- Content Header (Page header) -->
        <section class="content-header">
            <h1>Doctors</h1>
            <!-- You can dynamically generate breadcrumbs here -->
            <ol class="breadcrumb">
                <li><a href="#"><i class="fa fa-dashboard"></i> Doctors</a></li>
            </ol>
        </section>

        <!-- Main content -->
        <section class="content">
            <div class="row">
                <div class="col-xs-12">
                    <div class="box">
                        <div class="box-body">
                            <table class="table table-bordered table-hover datatable middle" data-order="[[ 5, &quot;asc&quot; ]]">
                                <thead>
                                <tr>
                                    <th data-orderable="false" style="width: 26px">Avatar</th>
                                    <th>Full Name</th>
                                    <th>E-mail</th>
                                    <th>Gender</th>
                                    <th>Date of birth</th>
                                    <th>Reg Date</th>
                                    <th>Reg Step</th>
                                    <th>Status</th>
                                </tr>
                                </thead>
                                <tbody>
                                @foreach ($doctors as $doctors)
                                    <tr>
                                        <td><img src="{{ $doctors->face_photo ? '/files/profiles/' . $doctors->role . '-' . $doctors->id . '/' . $doctors->face_photo : '/files/avatar/default-avatar.png' }}" class="img-thumbnail" style="width: 50px"></td>
                                        {{--<td><img src="/files/avatar/{{$doctors->image ? $doctors->image : 'default-avatar.png' }}" class="img-thumbnail" style="width: 50px"></td>--}}
                                        <td><a href="{{ URL::to('/admin/doctor/' . $doctors->id) }}">{{ $doctors->first_name . ' ' . $doctors->last_name }}</a></td>
                                        <td>{{ $doctors->email }}</td>
                                        <td>{{ $doctors->gender }}</td>
                                        <td>{{ \Carbon\Carbon::parse($doctors->dob)->format('m/d/Y') }}</td>
                                        <td>{{ \Carbon\Carbon::parse($doctors->created_at)->format('m/d/Y') }}</td>
                                        <td>{{ $doctors->registration_step }}</td>
                                        <td>{{ $doctors->status }}</td>
                                    </tr>
                                @endforeach
                                </tbody>
                            </table>
                        </div>
                    </div>
                </div>
            </div>
        </section><!-- /.content -->
    </div><!-- /.content-wrapper -->
@endsection