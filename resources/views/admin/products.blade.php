@extends('layouts.admin.index')
@section('title', '| Products')
@section('content')
    <!-- Content Wrapper. Contains page content -->
    <div class="content-wrapper">
        <!-- Content Header (Page header) -->
        <section class="content-header">
            <h1>Products</h1>
            <!-- You can dynamically generate breadcrumbs here -->
            <ol class="breadcrumb">
                <li><a href="#"><i class="fa fa-dashboard"></i> Products</a></li>
            </ol>
        </section>

        <!-- Main content -->
        <section class="content">
            <div class="row">
                <div class="col-xs-12">
                    <div class="box">
                        <div class="box-body">
                            <div class="text-right m-b-30"><a href="{{ URL::to('/admin/product/create') }}" class="btn btn-primary">Add new Product</a></div>
                            <table class="table table-bordered table-hover datatable middle" data-order="[[ 5, &quot;desc&quot; ]]">
                                <thead>
                                <tr>
                                    <th>Sku Number</th>
                                    <th>Title</th>
                                    <th data-orderable="false">Locales</th>
                                    <th>Brand</th>
                                    <th>Status</th>
                                    <th>Sort</th>
                                    <th data-orderable="false" style="width: 50px" class="text-center">Action</th>
                                </tr>
                                </thead>
                                <tbody>
                                @foreach ($products as $product)
                                    <tr>
                                        <td class="col-md-2">{{ $product->sku }}</td>
                                        <td class="col-md-4">{{ trans('product.' . $product->id . '.title') }}</td>
                                        <td class="col-md-2">
                                            @foreach($product->titles as $title)
                                                <span class="label label-info m-r-10">{{ $title->locale }}</span>
                                            @endforeach
                                            <a href="/admin/product/translation/{{ $product->id }}" class="btn btn-primary translation-edit">Edit</a>
                                        </td>
                                        <td class="col-md-2">{{ trans('brand.' . $product->brand_id . '.title') }}</td>
                                        <td class="col-md-1">{{ $product::$status[$product->status] }}</td>
                                        <td class="col-md-1">{{ $product->sort }}</td>
                                        <td class="col-md-2 text-center">
                                            <a href="/admin/product/{{ $product->id }}" class="m-l-r-15"><i class="fa fa-edit"></i></a>
                                            <a href="javascript:void(0)" rel="{{ $product->id }}" data-type="product" class="recordDelete"><i class="fa fa-remove"></i></a>
                                        </td>
                                    </tr>
                                @endforeach
                                </tbody>
                            </table>
                        </div>
                    </div>
                </div>
            </div>
        </section><!-- /.content -->
    </div><!-- /.content-wrapper -->
@endsection