@extends('layouts.admin.index')
@section('title', '| Categories')
@section('content')
<!-- Content Wrapper. Contains page content -->
<div class="content-wrapper">
    <!-- Content Header (Page header) -->
    <section class="content-header">
        <h1>Create Category Translations</h1>
        <!-- You can dynamically generate breadcrumbs here -->
        <ol class="breadcrumb">
            <li><a href="/admin/categories"><i class="fa fa-dashboard"></i> Categories</a></li>
            <li class="active">Here</li>
        </ol>
    </section>

    <!-- Main content -->
    <section class="content">
        <div class="row">
            <div class="col-xs-12">
                <div class="box">
                    <div class="col-md-12">
                        <hr class="divider">
                    </div>
                    <div class="clearfix"></div>
                    <div class="col-md-12">
                        <div class="nav-tabs-custom">
                            <ul class="nav nav-tabs custom-tab" id="detail_tab">
                                @foreach($languages as $language_type => $language)
                                <li class="{{$language_type == 'en' ? 'active':''}}"><a href="#{{$language}}" data-toggle="tab" aria-expanded="{{$language_type == 'en' ? 'true':'false'}}">{{ ucfirst($language) }}</a></li>
                                @endforeach
                            </ul>
                            <form role="form" action="{{action('Admin\CategoriesController@postTranslation', ['category_id' => $category->id]) }}" method="post">
                                {!! csrf_field() !!}
                                <div class="tab-content">
                                    @foreach($languages as $language_type => $language)
                                    <div class="tab-pane {{$language_type == 'en' ? 'active':''}}" id="{{$language}}">
                                        <h3>{{ ucfirst($language) }}</h3>
                                        <hr class="divider">
                                        <div class="col-xs-12 col-md-6 col-md-offset-3">
                                            <div class="form-group">
                                                <label for="name">Title</label>
                                                <?php $check_translation = ''; ?>
                                                @foreach($translations as $translation)
                                                @if($translation->locale == $language_type)
                                                <input type="text" class="form-control" name="title[{{$language_type}}][]" value="{{ $translation->text }}" id="title" {{$language_type == 'en' ? 'disabled': ''}} placeholder="Title">
                                                <?php $check_translation = $translation->locale; ?>
                                                @endif
                                                @endforeach
                                                @if(!$check_translation && $language_type != 'en')
                                                <input type="text" class="form-control" name="title[{{$language_type}}][]" value="" id="title"  placeholder="Title">
                                                @endif
                                                <?php $check_translation = ''; ?>
                                            </div>
                                        </div>
                                    </div>
                                    @endforeach
                                </div>
                                <div class="clearfix"></div>
                                <div class="col-xs-12 col-md-6 col-md-offset-3">
                                    <div class="pull-right p-t-b-10">
                                        <button type="submit" class="btn btn-primary">Submit</button>
                                    </div>
                                </div>
                            </form>
                        </div>
                    </div>
                    <div class="clearfix"></div>
                </div>
                <div class="clearfix"></div>
            </div>
    </section><!-- /.content -->
</div><!-- /.content-wrapper -->
@endsection



