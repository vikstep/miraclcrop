@extends('layouts.admin.index')
@section('title', '| Posts')
@section('content')
    <!-- Content Wrapper. Contains page content -->
    <div class="content-wrapper">
        <!-- Content Header (Page header) -->
        <section class="content-header">
            <h1>{{ isset($post) ? "Update Post" : "Create Post" }}</h1>
            <!-- You can dynamically generate breadcrumbs here -->
            <ol class="breadcrumb">
                <li><a href="/admin/posts"><i class="fa fa-dashboard"></i> Posts</a></li>
                <li class="active">Here</li>
            </ol>
        </section>

        <!-- Main content -->
        <section class="content">
            <div class="row">
                <div class="col-xs-12">
                    <div class="box">
                        <div class="row">
                            <div class="col-xs-12 col-md-6 col-md-offset-3">
                                @if (count($errors) > 0)
                                    <div class="alert alert-danger alert-dismissible m-t-30">
                                        <button type="button" class="close" data-dismiss="alert" aria-hidden="true">×</button>
                                        <h4><i class="icon fa fa-ban"></i> Error!</h4>
                                        <ul>
                                            @foreach ($errors->all() as $error)
                                                <li>{{ $error }}</li>
                                            @endforeach
                                        </ul>
                                    </div>
                                @endif

                                <form role="form" action="{{ action('Admin\PostsController@postUpdate', ['id' => $post->id]) }}" method="post">
                                    {!! csrf_field() !!}
                                    <div class="box-body post-body">
                                        <div class="form-group">
                                            <label for="name">Title <i class="fa fa-info-circle validate-title-icon validataion-invalid" aria-hidden="true"></i></label>
                                            <input type="text" class="form-control validate-title" name="title" value="{{ (isset($post) && isset($title))  ? $title : '' }}" id="title" placeholder="Title">
                                        </div>
                                        <div class="form-group {{ isset($post->id) ? '' : 'hidden' }}" id="slug-group">
                                            <label for="name">Slug</label>
                                            <span id="post-slug">{{ isset($post->id) ? (URL::to('/blog') . '/' . $post->id . '/' . slugify($title)) : '' }}</span>
                                        </div>
                                        <div class="form-group">
                                            <label for="name">Excerpt <i class="fa fa-info-circle validate-excerpt-icon validataion-invalid" aria-hidden="true"></i></label>
                                            <textarea class="form-control excerpt-editor validate-excerpt" name="post_excerpt">{{ (isset($post) && isset($excerpt)) ? $excerpt : '' }}</textarea>
                                        </div>
                                        <div class="form-group">
                                            <img id="featuredImage" class="img-thumbnail" src="{{ !empty($featured_image) ? $featured_image : 'data:image/svg+xml;base64,PD94bWwgdmVyc2lvbj0iMS4wIiBlbmNvZGluZz0iVVRGLTgiIHN0YW5kYWxvbmU9InllcyI/PjxzdmcgeG1sbnM9Imh0dHA6Ly93d3cudzMub3JnLzIwMDAvc3ZnIiB3aWR0aD0iMTQwIiBoZWlnaHQ9IjE0MCIgdmlld0JveD0iMCAwIDE0MCAxNDAiIHByZXNlcnZlQXNwZWN0UmF0aW89Im5vbmUiPjwhLS0KU291cmNlIFVSTDogaG9sZGVyLmpzLzE0MHgxNDAKQ3JlYXRlZCB3aXRoIEhvbGRlci5qcyAyLjYuMC4KTGVhcm4gbW9yZSBhdCBodHRwOi8vaG9sZGVyanMuY29tCihjKSAyMDEyLTIwMTUgSXZhbiBNYWxvcGluc2t5IC0gaHR0cDovL2ltc2t5LmNvCi0tPjxkZWZzPjxzdHlsZSB0eXBlPSJ0ZXh0L2NzcyI+PCFbQ0RBVEFbI2hvbGRlcl8xNWIzZTZhYTJmNyB0ZXh0IHsgZmlsbDojQUFBQUFBO2ZvbnQtd2VpZ2h0OmJvbGQ7Zm9udC1mYW1pbHk6QXJpYWwsIEhlbHZldGljYSwgT3BlbiBTYW5zLCBzYW5zLXNlcmlmLCBtb25vc3BhY2U7Zm9udC1zaXplOjEwcHQgfSBdXT48L3N0eWxlPjwvZGVmcz48ZyBpZD0iaG9sZGVyXzE1YjNlNmFhMmY3Ij48cmVjdCB3aWR0aD0iMTQwIiBoZWlnaHQ9IjE0MCIgZmlsbD0iI0VFRUVFRSIvPjxnPjx0ZXh0IHg9IjQ0LjA2MjUiIHk9Ijc0LjU1NjI1Ij4xNDB4MTQwPC90ZXh0PjwvZz48L2c+PC9zdmc+' }}" />
                                        </div>
                                        <div class="form-group">
                                            <label for="name">Content <i class="fa fa-info-circle validate-content-icon validataion-invalid" aria-hidden="true"></i></label>
                                            <textarea class="form-control editor validate-content " name="post_content">{{ (isset($post) && isset($content))  ? $content : '' }}</textarea>
                                        </div>
                                        <div class="form-group">
                                            <label for="name">Categories</label>
                                            <select class="form-control" name="category">
                                                @foreach($categories as $category)
                                                    <option value="{{ $category->id }}" {{ (isset($post) && $post->category_id == $category->id) ? 'selected' : '' }}>{{ $category->translation->text }}</option>
                                                @endforeach
                                            </select>
                                        </div>
                                        <div class="form-group">
                                            <label for="name">Status</label>
                                            <select class="form-control" name="status">
                                                <option value="draft" {{ (isset($post) && $post->status == 'draft') ? 'selected' : '' }}>Draft</option>
                                                <option value="published" {{ (isset($post) && $post->status == 'published') ? 'selected' : '' }}>Published</option>
                                                <option value="disabled" {{ (isset($post) && $post->status == 'disabled') ? 'selected' : '' }}>Disabled</option>
                                            </select>
                                        </div>
                                        <div class="form-group date" data-provider="datepicker">
                                            <label for="defined_created_date">Date</label>
                                            <input type="text" class="form-control post_datepicker" name="defined_created_date" value="{{ isset($post->id) ? date('m/d/Y', strtotime($post->defined_created_date)) : date('m/d/Y', time()) }}" id="defined_created_date">
                                        </div>
                                        <div class="form-group">
                                            <label for="sort">Sort</label>
                                            <input type="text" class="form-control" name="sort" value="{{ isset($post) ? $post->sort : '' }}" id="sort" placeholder="The sort must be a number">
                                        </div>
                                    </div>
                                    <div class="box-footer text-right">
                                        <input type="hidden" id="attachments" name="attachments" value="{{ isset($post->id) ? $attachments : '' }}" />
                                        <input type="hidden" id="post_id" value="{{ isset($post->id) ? $post->id : '' }}" />
                                        <button type="submit" class="btn btn-primary" id="submit-btn" {{ isset($post->id) ? '' : 'disabled' }}>Submit</button>
                                    </div>
                                </form>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
        </section><!-- /.content -->
    </div><!-- /.content-wrapper -->
@endsection