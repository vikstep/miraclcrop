@extends('layouts.admin.index')
@section('title', '| Doctors')
@section('content')
<!-- Content Wrapper. Contains page content -->
<div class="content-wrapper">
    <!-- Content Header (Page header) -->
    <section class="content-header">
        <h1>&nbsp;</h1>
        <!-- You can dynamically generate breadcrumbs here -->
        <ol class="breadcrumb">
            <li><a href="/admin/doctors"><i class="fa fa-dashboard"></i> Doctors</a></li>
            <li class="active">Here</li>
        </ol>
    </section>

    <!-- Main content -->
    <section class="content">
        <div class="row">
            <div class="col-xs-12">
                <div class="col-md-3">
                    <!-- Profile Image -->
                    <div class="box box-primary">
                        <div class="box-body box-profile">
                            <img class="profile-user-img img-responsive" src="{{ $doctor->face_photo ? '/files/profiles/' . $doctor->role . '-' . $doctor->id . '/' . $doctor->face_photo : '/files/avatar/default-avatar.png' }}" alt="User profile picture">
                            {{--<img class="profile-user-img img-responsive img-circle" src="/files/avatar/{{$doctor->image ? $doctor->image : 'default-avatar.png' }}" alt="User profile picture">--}}
                            <h3 class="profile-username text-center">{{ $doctor->first_name . ' ' . $doctor->last_name  }}</h3>
                            <p class="text-muted text-center">{{ $doctor->role  }}</p>
                        </div>
                        <!-- /.box-body -->
                    </div>
                    <!-- /.box -->
                </div>
                <div class="col-md-9">
                    <div class="nav-tabs-custom">
                        <ul class="nav nav-tabs">
                            <li class="active"><a href="#user-info" data-toggle="tab" aria-expanded="true">User Info</a></li>
                            <li class=""><a href="#pages-history" data-toggle="tab" aria-expanded="false">Pages history</a></li>
                            <li class=""><a href="#order-history" data-toggle="tab" aria-expanded="false">Order history</a></li>
                        </ul>
                        <div class="tab-content">
                            <div class="tab-pane active" id="user-info">
                                <div class="row user-info-item">
                                    <div class="col-xs-12 col-md-3 user-info-title">E-mail</div>
                                    <div class="col-xs-12 col-md-9 user-info-value">{{ $doctor->email }}</div>
                                </div>
                                <div class="row user-info-item">
                                    <div class="col-xs-12 col-md-3 user-info-title">Phone</div>
                                    <div class="col-xs-12 col-md-9 user-info-value">{{ $doctor->phone }}</div>
                                </div>
                                <div class="row user-info-item">
                                    <div class="col-xs-12 col-md-3 user-info-title">Has recipe</div>
                                    <div class="col-xs-12 col-md-9 user-info-value">{{ $doctor->has_recipe }}</div>
                                </div>
                                <div class="row user-info-item">
                                    <div class="col-xs-12 col-md-3 user-info-title">Gender</div>
                                    <div class="col-xs-12 col-md-9 user-info-value">{{ $doctor->gender }}</div>
                                </div>
                                <div class="row user-info-item">
                                    <div class="col-xs-12 col-md-3 user-info-title">Date of birth</div>
                                    <div class="col-xs-12 col-md-9 user-info-value">{{ $doctor->dob }}</div>
                                </div>
                                <div class="row user-info-item">
                                    <div class="col-xs-12 col-md-3 user-info-title">Phone</div>
                                    <div class="col-xs-12 col-md-9 user-info-value">{{ $doctor->phone }}</div>
                                </div>
                                <div class="row user-info-item">
                                    <div class="col-xs-12 col-md-3 user-info-title">Address</div>
                                    <div class="col-xs-12 col-md-9 user-info-value">{{ $doctor->address }}</div>
                                </div>
                                <div class="row user-info-item">
                                    <div class="col-xs-12 col-md-3 user-info-title">State</div>
                                    <div class="col-xs-12 col-md-9 user-info-value">{{ $doctor->state }}</div>
                                </div>
                                <div class="row user-info-item">
                                    <div class="col-xs-12 col-md-3 user-info-title">City</div>
                                    <div class="col-xs-12 col-md-9 user-info-value">{{ $doctor->city }}</div>
                                </div>
                                <div class="row user-info-item">
                                    <div class="col-xs-12 col-md-3 user-info-title">Zip code</div>
                                    <div class="col-xs-12 col-md-9 user-info-value">{{ $doctor->zip_code }}</div>
                                </div>
                                <div class="row user-info-item m-t-5">
                                    <div class="col-xs-12 col-md-3 user-info-title">Driver License</div>
                                    <div class="col-xs-12 col-md-9 user-info-value">
                                        {{ $doctor->driver_license }}
                                        {!!  $doctor->driver_license_image ? '<img class="right-img-x" src="/files/profiles/' . $doctor->role . '-' . $doctor->id . '/' . $doctor->driver_license_image . '">' : '' !!}
                                    </div>
                                </div>
                                <div class="row user-info-item m-t-5">
                                    <div class="col-xs-12 profile-info-title">Licenses</div>
                                    <div class="col-xs-12">
                                        <div class="table-responsive">
                                            <table class="table table-stripped">
                                                <thead>
                                                    <tr>
                                                        <th>State</th>
                                                        <th>License Number</th>
                                                        <th>Valid Until</th>
                                                        <th>License Image</th>
                                                    </tr>
                                                </thead>
                                                <tbody>
                                                    @if(isset($doctorLicense) && !empty($doctorLicense))
                                                    @foreach ($doctorLicense as $license)
                                                    <tr>
                                                        <td>{{ isset($states[$license->registration_state]) ? $states[$license->registration_state] : $license->registration_state }}</td>
                                                        <td>{{ $license->medical_license }}</td>
                                                        <td>{{ $license->valid_until }}</td>
                                                        <td>
                                                            @if($license->license_image)
                                                            <img src='{{ asset ("/files/profiles/doctor-". $license->user_id ."/".$license->license_image)}}' width="50" >
                                                            @endif
                                                        </td>
                                                    </tr>
                                                    @endforeach
                                                    @else
                                                    <tr><td colspan="3">No data available</td></tr>
                                                    @endif
                                                </tbody>
                                            </table>
                                        </div>
                                    </div>
                                </div>
                                <div class="row user-info-item">
                                    <div class="col-xs-12 col-md-3 user-info-title">Notes</div>
                                    <div class="col-xs-12 col-md-9 user-info-value">{{ $doctor->notes }}</div>
                                </div>
                                <div class="row user-info-item">
                                    <div class="col-xs-12 col-md-3 user-info-title">Status</div>
                                    <div class="col-xs-12 col-md-5 user-info-value">{{ $doctor->status == 'rejected' ? $doctor->status . ' ( ' . $doctor->status_reason . ' ) ' : $doctor->status }}</div>
                                    <div class="col-xs-12 col-md-4 user-info-value">
                                        <select id="statuses" class="form-control">
                                            <option value="0">-- Change status --</option>
                                            <option value="approved">Aprove</option>
                                            <option value="blocked">Block</option>
                                            <option value="rejected">Reject</option>
                                        </select>
                                        <textarea id="status_reason" class="form-control hidden"></textarea>
                                        <input type="button" class="btn btn-default" id="changeStatus" data-id="{{ $doctor->id }}" value="Save" />
                                    </div>
                                </div>
                                <div class="row user-info-item">
                                    <div class="col-xs-12 col-md-3 user-info-title">Ip</div>
                                    <div class="col-xs-12 col-md-9 user-info-value">{{ $doctor->ip }}</div>
                                </div>
                                <div class="row user-info-item">
                                    <div class="col-xs-12 col-md-3 user-info-title">User agent</div>
                                    <div class="col-xs-12 col-md-9 user-info-value">{{ $doctor->user_agent }}</div>
                                </div>
                                <div class="row user-info-item">
                                    <div class="col-xs-12 col-md-3 user-info-title">Registration date</div>
                                    <div class="col-xs-12 col-md-9 user-info-value">{{ $doctor->created_at }}</div>
                                </div>
                            </div>
                            <div class="tab-pane" id="pages-history">
                                pages history content
                            </div>
                            <div class="tab-pane" id="order-history">
                                order history content
                            </div>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </section><!-- /.content -->
</div><!-- /.content-wrapper -->
@endsection