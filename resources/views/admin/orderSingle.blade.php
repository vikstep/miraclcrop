@extends('layouts.admin.index')
@section('title', '| Orders Details')
@section('content')
<!-- Content Wrapper. Contains page content -->
<div class="content-wrapper">
    <!-- Content Header (Page header) -->
    <section class="content-header">
        <h1>
            Invoice 
            <small>#{{ $order->order_number }}</small>
        </h1>
        <!-- You can dynamically generate breadcrumbs here -->
        <ol class="breadcrumb">
            <li><a href="/admin/orders"><i class="fa fa-dashboard"></i> Orders</a></li>
            <li class="active">Here</li>
        </ol>
    </section>


    <!-- Main content -->
    <section class="invoice">
        <div class="row">
            <div class="col-xs-12">
                <h2 class="page-header">
                    <i class="fa fa-globe"></i> Miracle Crop, ltd.
                    <small class="pull-right">Date: {{ \Carbon\Carbon::now()->format('m/d/Y H:i') }}</small>
                </h2>
            </div>
        </div>
        <div class="row invoice-info">
            <div class="col-sm-4 invoice-col">
                To
                <address>
                    <strong>{{ $user->first_name . ' ' . $user->last_name }}</strong><br>
                    <?php $shipping_address = unserialize($order->shipping_address) ?>
                    {{ $shipping_address['address'] }}<br>
                    {{ $shipping_address['city'] . ', ' . $shipping_address['state'] . ' ' . $shipping_address['zip_code'] }}<br>
                    Phone: {{ $user->phone }}<br>
                    Email: {{ $user->email }}
                </address>
            </div>
            <div class="col-sm-4 invoice-col">
                <b>Invoice #{{ $order->order_number }}</b><br>
                <br>
                <b>Payment Due:</b> {{ \Carbon\Carbon::parse($order->created_at)->format('m/d/Y H:i') }}<br>
            </div>
        </div>

        <div class="row">
            <div class="col-xs-12 table-responsive">
                <table class="table table-striped">
                    <thead>
                        <tr>
                            <th>Product</th>
                            <th>Sku #</th>
                            <th>Qty</th>
                            <th>Portion</th>
                            <th>Price</th>
                            <th>Subtotal</th>
                        </tr>
                    </thead>
                    <tbody>
                        <?php $total = 0; ?>
                        @foreach($order->products as $product)
                        <tr>
                            <td>{{ $product->product_title }}</td>
                            <td>{{ $product->product_sku }}</td>
                            <td>{{ $product->quantity }}</td>
                            <td>{{ $product->portion }}</td>
                            <td>{{ floatval($product->price) }}</td>
                            <td>{{ $product->quantity * $product->price }}</td>
                            <?php $total += ($product->quantity * $product->price); ?>
                        </tr>
                        @endforeach
                        <tr>
                            <td colspan="6" class="text-right">Total: {{ $total }}</td>
                        </tr>
                    </tbody>
                </table>
            </div>
            <!-- /.col -->
        </div>
        <!-- /.row -->


        <!-- this row will not appear when printing -->
        <div class="row no-print">
            <div class="row">
                <div class="col-xs-2">
                    <button class="btn btn-default" id="order_print"><i class="fa fa-print"></i> Print</button>
                </div>
                <div class="col-xs-6">
                    <select id="orderStatus" class="form-control inline" style="width: 200px">
                        <option value="pending" {{ $order->status == 'pending' ? 'selected' : '' }}>Pending</option>
                        <option value="shipped" {{ $order->status == 'shipped' ? 'selected' : '' }}>Shipped</option>
                        <option value="delivered" {{ $order->status == 'delivered' ? 'selected' : '' }}>Delivered</option>
                    </select>
                    <button type="button" id="updateOrderStatus" class="btn btn-success">Update Status</button>
                    <input type="hidden" id="orderId" value="{{ $order->id }}" />
                    <input type="hidden" id="userId" value="{{ $user->id }}" />
                </div>
                <div class="col-xs-4">
                    <a href="{{ route('download_pdf',['download'=>'pdf', 'order_id' => $order->id]) }}" type="button" class="btn btn-primary pull-right" style="margin-right: 5px;">
                        <i class="fa fa-download"></i> Generate PDF
                    </a>
                </div>
            </div>
        </div>
    </section>
    <!-- /.content -->
    <div class="clearfix"></div>

</div><!-- /.content-wrapper -->
@endsection