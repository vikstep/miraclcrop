@extends('layouts.admin.index')
@section('title', '| Product Categories')
@section('content')
<!-- Content Wrapper. Contains page content -->
<div class="content-wrapper">
    <!-- Content Header (Page header) -->
    <section class="content-header">
        <h1>Category</h1>
        <!-- You can dynamically generate breadcrumbs here -->
        <ol class="breadcrumb">
            <li><a href="#"><i class="fa fa-dashboard"></i> Categories</a></li>
        </ol>
    </section>

    <!-- Main content -->
    <section class="content">
        <div class="row">

            <div class="col-xs-12">
                <div class="box">
                    <div class="box-body">
                        @if (session('error'))
                        <div class="alert alert-danger">
                            {{ session('error') }}
                        </div>
                        @endif
                        <div class="text-right m-b-30"><a href="{{ URL::to('/admin/product-category/create') }}" class="btn btn-primary">Add new Category</a></div>
                        <table class="table table-bordered table-hover category-datatable middle">
                            <thead>
                                <tr>
                                    <th>Title</th>
                                    <th>Parent Category</th>
                                    <th>Locales</th>
                                    <th>Status</th>
                                    <th data-orderable="false" style="width: 50px" class="text-center">Action</th>
                                </tr>
                            </thead>
                            <tbody>
                                @foreach ($categories as $category)
                                    <tr>
                                        <td class="col-md-3">{{ trans('product_category.' . $category->id . '.title') }}</td>
                                        <td class="col-md-3">{{ ($category->parent_id != 0)? trans('product_category.' . $category->parent_id . '.title'):'' }}</td>
                                        <td class="col-md-3">
                                            @foreach($category->translations as $translation)
                                            <span class="label label-info m-r-10">{{ $translation->locale }}</span>
                                            @endforeach
                                            <a href="/admin/product-category/translation/{{ $category->id }}" class="btn btn-primary translation-edit">Edit</a>
                                        </td>
                                        <td class="col-md-2">{{ $category->status }}</td>
                                        <td class="col-md-1">
                                            <a href="/admin/product-category/{{ $category->id }}" class="m-l-r-15"><i class="fa fa-edit"></i></a>
                                            <a href="javascript:void(0)" rel="{{ $category->id }}" data-type="product-category" class="recordDelete"><i class="fa fa-remove"></i></a>
                                        </td>
                                    </tr>
                                    @if(count($category['sub_categories']) > 0)
                                        @foreach($category['sub_categories'] as $sub_category)
                                            <tr>
                                                <td class="col-md-3">{{ trans('product_category.' . $sub_category->id . '.title') }}</td>
                                                <td class="col-md-3">{{ ($sub_category->parent_id != 0)? trans('product_category.' . $sub_category->parent_id . '.title'):'' }}</td>
                                                <td class="col-md-3">
                                                    @foreach($sub_category->translations as $translation)
                                                        <span class="label label-info m-r-10">{{ $translation->locale }}</span>
                                                    @endforeach
                                                    <a href="/admin/product-category/translation/{{ $sub_category->id }}" class="btn btn-primary translation-edit">Edit</a>
                                                </td>
                                                <td class="col-md-2">{{ $sub_category->status }}</td>
                                                <td class="col-md-1">
                                                    <a href="/admin/product-category/{{ $sub_category->id }}" class="m-l-r-15"><i class="fa fa-edit"></i></a>
                                                    <a href="javascript:void(0)" rel="{{ $sub_category->id }}" data-type="product-category" class="recordDelete"><i class="fa fa-remove"></i></a>
                                                </td>
                                            </tr>
                                        @endforeach
                                    @endif
                                @endforeach
                            </tbody>
                        </table>
                    </div>
                </div>
            </div>
        </div>
    </section><!-- /.content -->
</div><!-- /.content-wrapper -->
@endsection