@extends('layouts.admin.index')
@section('title', '| Portions')
@section('content')
<!-- Content Wrapper. Contains page content -->
<div class="content-wrapper">
    <!-- Content Header (Page header) -->
    <section class="content-header">
        <h1>Portions</h1>
        <!-- You can dynamically generate breadcrumbs here -->
        <ol class="breadcrumb">
            <li><a href="#"><i class="fa fa-dashboard"></i> Portions</a></li>
        </ol>
    </section>

    <!-- Main content -->
    <section class="content">
        <div class="row">
            <div class="col-xs-12">
                <div class="box">
                    <div class="box-body">
                        @if (session('error'))
                        <div class="alert alert-danger">
                            {{ session('error') }}
                        </div>
                        @endif
                        <div class="text-right m-b-30"><a href="{{ URL::to('/admin/portion/create') }}" class="btn btn-primary">Add new Portion</a></div>
                        <table class="table table-bordered table-hover datatable middle" data-order="[[ 2, &quot;asc&quot; ]]">
                            <thead>
                                <tr>
                                    <th>Title</th>
                                    <th>Locales</th>
                                    <th>Weight (g)</th>
                                    <th data-orderable="false" style="width: 50px" class="text-center">Action</th>
                                </tr>
                            </thead>
                            <tbody>
                                @foreach ($portions as $portion)
                                 <tr>
                                    <td>{{ trans('portion.' . $portion->id . '.title') }}</td>
                                    <td>
                                        @foreach($portion->translations as $translation)
                                        <span class="label label-info m-r-10">{{ $translation->locale }}</span>
                                        @endforeach
                                        <a href="/admin/portion/translation/{{ $portion->id }}" class="btn btn-primary translation-edit">Edit</a>
                                    </td>
                                    <td>{{ $portion->weight }}</td>
                                    <td>
                                        <a href="/admin/portion/{{ $portion->id }}" class="m-l-r-15"><i class="fa fa-edit"></i></a>
                                        <a href="javascript:void(0)" rel="{{ $portion->id }}" data-type="portion" class="recordDelete"><i class="fa fa-remove"></i></a>
                                    </td>
                                 </tr>
                                 @endforeach
                            </tbody>
                        </table>
                    </div>
                </div>
            </div>
        </div>
    </section><!-- /.content -->
</div><!-- /.content-wrapper -->
@endsection
