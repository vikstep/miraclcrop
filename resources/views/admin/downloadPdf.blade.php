
<!-- Content Wrapper. Contains page content -->
<style>
    .content-header {
        position: relative;
        padding: 15px 15px 0 15px;
    }
    .invoice {
        position: relative;
        background: #fff;
        border: 1px solid #f4f4f4;
        padding: 20px;
        margin: 10px 15px;
    }
    .content-header>h1 {
        margin: 0;
        font-size: 24px;
    }
    .text-center {
        text-align: center;
    }
    .row {
        margin-right: -15px;
        margin-left: -15px;
    }
    .col-xs-4{
        position: relative;
        min-height: 1px;
        padding-right: 15px;
        padding-left: 15px;
        width: 33.33333333%;
        float: left;
    }
    address {
        margin-bottom: 20px;
        font-style: normal;
        line-height: 1.42857143;
    }
    .pull-right {
        float: right!important;
    }
    .row:after {
        clear: both;
    }
    .row:before {
        display: table;
        content: " ";
    }

    .clearfix::after {
        display: block;
        content: "";
        clear: both;
    }
    .page-header {
        margin: 10px 0 20px 0;
        font-size: 22px;
    }

    .page-header {
        padding-bottom: 9px;
        margin: 5px 0 20px;
        border-bottom: 1px solid #eee;
    }

    .table {
        width: 100%;
        max-width: 100%;
        margin-bottom: 20px;
    }

    table {
        background-color: transparent;
    }
    table {
        border-spacing: 0;
        border-collapse: collapse;
    }
    thead {
        display: table-header-group;
        vertical-align: middle;
        border-color: inherit;
    }
    tr {
        display: table-row;
        vertical-align: inherit;
        border-color: inherit;
    }
    .table>caption+thead>tr:first-child>td, .table>caption+thead>tr:first-child>th, .table>colgroup+thead>tr:first-child>td, .table>colgroup+thead>tr:first-child>th, .table>thead:first-child>tr:first-child>td, .table>thead:first-child>tr:first-child>th {
        border-top: 0;
    }
    .table>thead>tr>th {
        border-bottom: 2px solid #f4f4f4;
    }
    tbody {
        display: table-row-group;
        vertical-align: middle;
        border-color: inherit;
    }
    .table-striped>tbody>tr:nth-of-type(odd) {
        background-color: #f9f9f9;
    }
    .table>thead>tr>th, .table>tbody>tr>th, .table>thead>tr>td, .table>tbody>tr>td {
        border-top: 1px solid #f4f4f4;
    }
    td, th {
        display: table-cell;
        vertical-align: inherit;
    }
    th {
        text-align: left;
    }
    .table>tbody>tr>td{
        padding: 8px;
        line-height: 1.42857143;
        vertical-align: top;
        border-top: 1px solid #ddd;
    }
    .text-right {
        text-align: right;
    }
</style>
<div class="">
    <!-- Content Header (Page header) -->
    <section class="content-header">
        <h1 class="text-center">
            Invoice <span style="font-size: 14px"> #{{ $order->order_number }}</span>
        </h1>
    </section>
    <section class="invoice">
        <div class="row">
            <div class="col-xs-12">
                <h2 class="page-header">
                    <i class="fa fa-globe"></i> Miracle Crop, ltd.
                    <small class="pull-right">Date: {{ \Carbon\Carbon::now()->format('m/d/Y H:i') }}</small>
                </h2>
            </div>
        </div>
        <div class="row invoice-info">
            <span class="col-xs-4 invoice-col">
                To
                <address>
                    <strong>{{ $user->first_name . ' ' . $user->last_name }}</strong><br>
                    <?php $shipping_address = unserialize($order->shipping_address) ?>
                    {{ $shipping_address['address'] }}<br>
                    {{ $shipping_address['city'] . ', ' . $shipping_address['state'] . ' ' . $shipping_address['zip_code'] }}<br>
                    Phone: {{ $user->phone }}<br>
                    Email: {{ $user->email }}
                </address>
            </span>
            <span class="col-xs-4 invoice-col pull-right">
                <b>Invoice #{{ $order->order_number }}</b><br>
                <br>
                <b>Payment Due:</b> {{ \Carbon\Carbon::parse($order->created_at)->format('m/d/Y H:i') }}<br>
            </span>
        </div>
        <div class="clearfix"></div>
        <div class="row">
            <div class="col-xs-12 table-responsive">
                <table class="table table-striped">
                    <thead>
                        <tr>
                            <th>Product</th>
                            <th>Sku #</th>
                            <th>Qty</th>
                            <th>Portion</th>
                            <th>Price</th>
                            <th>Subtotal</th>
                        </tr>
                    </thead>
                    <tbody>
                        <?php $total = 0; ?>
                        @foreach($order->products as $product)
                        <tr>
                            <td>{{ $product->product_title }}</td>
                            <td>{{ $product->product_sku }}</td>
                            <td>{{ $product->quantity }}</td>
                            <td>{{ $product->portion }}</td>
                            <td>{{ $product->price }}</td>
                            <td>{{ $product->quantity * $product->price }}</td>
                            <?php $total += ($product->quantity * $product->price); ?>
                        </tr>
                        @endforeach
                        <tr>
                            <td colspan="6" class="text-right">Total: {{ $total }}</td>
                        </tr>
                    </tbody>
                </table>
            </div>
            <!-- /.col -->
        </div>
        <!-- /.row -->

    </section>
</div>
<!-- /.content -->
<div class="clearfix"></div>

</div><!-- /.content-wrapper -->
