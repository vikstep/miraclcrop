@extends('layouts.admin.index')
@section('title', '| Languages')
@section('content')
    <!-- Content Wrapper. Contains page content -->
    <div class="content-wrapper">
        <!-- Content Header (Page header) -->
        <section class="content-header">
            <h1>Languages</h1>
            <!-- You can dynamically generate breadcrumbs here -->
            <ol class="breadcrumb">
                <li><a href="#"><i class="fa fa-dashboard"></i> Languages</a></li>
                <li class="active">Here</li>
            </ol>
        </section>

        <!-- Main content -->
        <section class="content">
            <div class="row">
                <div class="col-xs-12">
                    <div class="box">
                        <div class="box-body">
                            <div class="text-right m-b-30"><a href="{{ URL::to('/admin/language/create') }}" class="btn btn-primary">Add new language</a></div>
                            <table class="table table-bordered table-hover datatable middle">
                                <thead>
                                <tr>
                                    <th>Name</th>
                                    <th>Locale</th>
                                    <th>Status</th>
                                    <th data-orderable="false" style="width: 50px" class="text-center">Action</th>
                                </tr>
                                </thead>
                                <tbody>
                                @foreach ($languages as $language)
                                    <tr>
                                        <td>{{ $language->name }}</td>
                                        <td>{{ $language->locale }}</td>
                                        <td>{{ !empty($language->deleted_at) ? 'Disabled' : 'Enabled' }}</td>
                                        <td>
                                            @if($language->locale != 'en')
                                                <a href="/admin/language/{{ $language->id }}" class="m-l-r-15"><i class="fa fa-edit"></i></a>
                                                <a href="javascript:void(0)" rel="{{ $language->id }}" data-type="language" class="recordDelete"><i class="fa fa-remove"></i></a>
                                            @endif
                                        </td>
                                    </tr>
                                @endforeach
                                </tbody>
                            </table>
                        </div>
                    </div>
                </div>
            </div>
        </section><!-- /.content -->
    </div><!-- /.content-wrapper -->
@endsection