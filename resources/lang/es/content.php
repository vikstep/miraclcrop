<?php

return [
    /*
      |--------------------------------------------------------------------------
      | Pagination Language Lines
      |--------------------------------------------------------------------------
      |
      | The following language lines are used by the paginator library to build
      | the simple pagination links. You are free to change them to anything
      | you want to customize your views to better match your application.
      |
     */
    //common
    'close' => 'Close',
    'cencel' => 'Cancelar',
    'save' => 'Salvar',
    'edit' => 'Editar',
    'create' => 'Crear',
    'next' => 'Siguiente',
    'previous' => 'Anterior',
    'data_not_found' => 'Datos no encontrados',
    //product
    /*     * ************************************** */
    'nothing_to_show' => 'Nothing to show es',
    'product_name' => 'Nombre del producto',
    'quantity' => 'Cantidad',
    'portion' => 'Parte',
    'price' => 'Precio',
    'subtotal' => 'Total parcial',
    'total' => 'Total',
    //blog
    'location' => 'Location es',
    'around_the_web' => 'Around the Web es',
    'about_frelanser' => 'About Freelancer es',
    'frelanser_open' => 'Freelance is a free to use, open source Bootstrap theme created by esss',
    'start_bootstrap' => 'Start Bootstrap ess',
    'web_development' => 'Web Development ess',
    'prolect_title' => 'Project Title es',
    'read_more' => 'Lee mas',
    'use_this_area' => 'es Use this area of the page to describe your project. The icon above is part of a free icon set by <a href="https://sellfy.com/p/8Q9P/jV3VZ/">Flat Icons</a>. On their website, you can download their free set with 16 icons, or you can purchase the entire set with 146 icons for only $12!',
    'there_is_no_content' => 'No hay contenido en este idioma',
    'you_dont_have_patient' => 'Usted no tiene la recomendación del paciente para hacer el orden, para obtener la recomendación del paciente que usted necesita para ponerse en contacto con nuestros médicos',
    'patient_recommandation' => 'Obtenga la recomendación del paciente',
    'your_cart' => 'Su Carrito',
    'update_cart' => 'compra Carrito',
    'add_point' => 'Añadir puntos',
    'add_to_cart' => 'Añadir to Carrito',
    'checkout' => 'es Checkout',
    'continue_shopping' => 'seguir comprando',
    'cart_is_empty' => 'El carrito esta vacío',
    'default_sorting' => 'Clasificación por defecto',
    'sort_by_newess' => 'Ordenar por novedad',
    'low_to_high' => 'Ordenar por precio: de menor a mayor',
    'high_to_low' => 'Ordenar por precio: de mayor a menor',
    'sort_by_popularity' => 'Ordenar por popularidad',
    //profile
    /*     * *************************************** */
    'shipping' => 'Envío',
    'billing' => 'Facturación',
    'city' => 'Ciudad',
    'address' => 'Dirección',
    'address' => 'Dirección',
    'state' => 'Estado',
    'zip_code' => 'Código postal',
    'profile_addresses' => 'Direcciones de perfiles',
    'first_name' => 'Nombre de pila',
    'last_name' => 'Apellido',
    'user_name' => 'Nombre de usuario',
    'email' => 'Email',
    'date_of_birth' => 'Fecha de nacimiento',
    'gender' => 'Género',
    'driver_license' => 'Licencia de conducir',
    'phone' => 'Teleéfono',
    'male' => 'Masculino',
    'female' => 'Hembra',
    'licenses' => 'Licencias',
    'state' => 'Estado',
    'profile_image' => 'Imagen de perfil',
    'profile_edit' => 'Perfil Editar',
    'license_number' => 'Número de licencia',
    'valid_until' => 'Válido hasta',
    'license_image' => 'Imagen de licencia',
    'medical_license' => 'Licencia médica',
    'no_data_available' => 'Datos no disponibles',
    'recommendation' => 'Recomendación del paciente',
    'registration_state' => 'Estado de registro',
    'face_photo' => 'Foto de la cara',
    'sign' => 'Firmar',
    'above' => 'encima',
    'clear' => 'Claro',
    'signature' => 'Firma',
    'complete_signup' => 'Inscripción Completa',
    /*     * ********************************** */
    'strain' => 'Tensión',
    'categories' => 'Categorías',
    'all' => 'Todas',
    'current_balance' => 'Saldo actual',
    'my_wallet' => 'Mi billetera',
    'points' => 'puntos',
    'point' => 'Punto',
    //Register
    /*     * ********************************************* */
    'basic_info' => 'Información básica',
    'more_info' => 'Más información',
    'licensing' => 'Licencias',
    'signing' => 'Firma',
     //Login
    /*******************************************/
    'remember_me' => 'Recuérdame',
    'sign_in' => 'Registrarse',
    'start_session' => 'Inicie sesión para iniciar su sesión',
];
