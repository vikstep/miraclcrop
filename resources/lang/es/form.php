<?php

return [

    /*
    |--------------------------------------------------------------------------
    | Pagination Language Lines
    |--------------------------------------------------------------------------
    |
    | The following language lines are used by the paginator library to build
    | the simple pagination links. You are free to change them to anything
    | you want to customize your views to better match your application.
    |
    */

    'fill_your_data' => 'Rellene sus datos',
    'password' => 'Contraseña',
    'confirm_password' => 'Confirmar Contraseña',
    'first_name' => 'Primer Nombre',
    'last_name' => 'Nombre de Familia',
    'date_of_birth' => 'Fecha de nacimiento',
    'select_type' => 'Seleccione tipo',
    'address' => 'Dirección',

];
