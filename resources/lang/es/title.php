<?php

return [

    /*
    |--------------------------------------------------------------------------
    | Pagination Language Lines
    |--------------------------------------------------------------------------
    |
    | The following language lines are used by the paginator library to build
    | the simple pagination links. You are free to change them to anything
    | you want to customize your views to better match your application.
    |
    */

    'login' => 'Sesión',
    'logout' => 'Cerrar sesión',
    'register' => 'Registro',
    'products' => 'Productos',
    'contact' => 'Enchufe',
    'miracle' => 'Cultivo milagroso',
    'my_profile' => 'Mi perfil',
    'profile_social' => 'Perfiles sociales',
    'purchase_history' => 'Historial de compras',
    'my_wallet' => 'Mi billetera',
    'addresses' => 'Direcciones',
    'edit_profile' => 'Editar perfil',
    'profile_info' => 'Información de perfil',
    'favorites' => 'Favoritos',
    
    'newest' => 'El más nuevo',
    'cart' => 'Carro',
    'favorite' => 'Productos favoritos',
    'home' => 'Casa',
    'addresses' => 'Direcciones',
    'password_reset' => 'Restablecimiento de contraseña',
    'password_forgot' => 'Contraseña olvidada',
];
