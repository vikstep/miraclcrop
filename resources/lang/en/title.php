<?php

return [

    /*
      |--------------------------------------------------------------------------
      | Pagination Language Lines
      |--------------------------------------------------------------------------
      |
      | The following language lines are used by the paginator library to build
      | the simple pagination links. You are free to change them to anything
      | you want to customize your views to better match your application.
      |
     */

    'login' => 'Login',
    'logout' => 'Logout',
    'register' => 'Register',
    'products' => 'Products',
    'contact' => 'Contact',
    'miracle' => 'miracle crop',
    'my_profile' => 'My profile',
    'profile_social' => 'Social profiles',
    'purchase_history' => 'Purchase history',
    'my_wallet' => 'My Wallet',
    'addresses' => 'Addresses',
    'edit_profile' => 'Edit profile',
    'profile_info' => 'Profile info',
    'favorites' => 'Favorites',
    
    'newest' => 'Newest',
    'cart' => 'Cart',
    'favorite' => 'Favorite Products',
    'home' => 'Home',
    'addresses' => 'Addresses',
    'password_reset' => 'Password Reset',
    'password_forgot' => 'Forgot Password',
    
    
];
