<?php

return [
    /*
      |--------------------------------------------------------------------------
      | Pagination Language Lines
      |--------------------------------------------------------------------------
      |
      | The following language lines are used by the paginator library to build
      | the simple pagination links. You are free to change them to anything
      | you want to customize your views to better match your application.
      |
     */
    //common
    'close' => 'Close',
    'cencel' => 'Cancel',
    'save' => 'Save',
    'edit' => 'Edit',
    'create' => 'Create',
    'next' => 'Next',
    'previous' => 'Previous',
    'data_not_found' => 'Data not found',
    //product
    /*     * ************************ */
    'nothing_to_show' => 'Nothing to show',
    'product_name' => 'Product name',
    'quantity' => 'Quantity',
    'portion' => 'Portion',
    'price' => 'Price',
    'subtotal' => 'Subtotal',
    'total' => 'Total',
    //blog
    /*     * ******************************* */
    'location' => 'Location',
    'around_the_web' => 'Around the Web',
    'about_frelanser' => 'About Freelancer',
    'frelanser_open' => 'Freelance is a free to use, open source Bootstrap theme created by',
    'start_bootstrap' => 'Start Bootstrap',
    'web_development' => 'Web Development',
    'prolect_title' => 'Project Title',
    'use_this_area' => 'Use this area of the page to describe your project. The icon above is part of a free icon set by <a href="https://sellfy.com/p/8Q9P/jV3VZ/">Flat Icons</a>. On their website, you can download their free set with 16 icons, or you can purchase the entire set with 146 icons for only $12!',
    'read_more' => 'Read more',
    'there_is_no_content' => 'There is no content in this language',
    'you_dont_have_patient' => 'You don\'t have patient recommendation to make order, to get patient recommendation you need to contact to our doctors',
    'patient_recommandation' => 'Get patient recommendation',
    'your_cart' => 'Your Cart',
    'update_cart' => 'Update Cart',
    'add_to_cart' => 'Add to Cart',
    'add_point' => 'Add Points',
    'checkout' => 'Checkout',
    'continue_shopping' => 'Continue Shopping',
    'cart_is_empty' => 'Cart is empty',
    'default_sorting' => 'Default Sorting',
    'sort_by_newess' => 'Sort by newness',
    'low_to_high' => 'Sort by price: Low to High',
    'high_to_low' => 'Sort by price: High to Low',
    'sort_by_popularity' => 'Sort by popularity',
    //profile
    /*     * ****************************** */
    'shipping' => 'Shipping',
    'billing' => 'Billing',
    'city' => 'City',
    'address' => 'Address',
    'state' => 'State',
    'zip_code' => 'Zip Code',
    'profile_addresses' => 'Profile Addresses',
    'first_name' => 'First Name',
    'last_name' => 'Last Name',
    'user_name' => 'User Name',
    'email' => 'email',
    'date_of_birth' => 'Date of Birth',
    'gender' => 'Gender',
    'driver_license' => 'Driver License',
    'phone' => 'Phone',
    'male' => 'Male',
    'female' => 'Female',
    'licenses' => 'Licenses',
    'state' => 'State',
    'profile_image' => 'Profile Image',
    'profile_edit' => 'Profile Edit',
    'license_number' => 'License Number',
    'valid_until' => 'Valid Until',
    'license_image' => 'License Image',
    'medical_license' => 'Medical License',
    'no_data_available' => 'No data available',
    'recommendation' => 'Patient Recommendation',
    'registration_state' => 'Registration State',
    'face_photo' => 'Face Photo',
    'sign' => 'Sign',
    'above' => 'above',
    'clear' => 'Clear',
    'signature' => 'Signature',
    'complete_signup' => 'Complete Signup',
    /*     * ****************************** */
    'strain' => 'Strains',
    'categories' => 'Categories',
    'all' => 'All',
    'current_balance' => 'Current Balance',
    'my_wallet' => 'My Wallet',
    'points' => 'points',
    'point' => 'Point',
    //Register
    /*     * ********************************************* */
    'basic_info' => 'Basic Info',
    'more_info' => 'More Info',
    'licensing' => 'Licensing',
    'signing' => 'Signing',
    //Login
    /*******************************************/
    'remember_me' => 'Remember Me',
    'sign_in' => 'Sign In',
    'start_session' => 'Sign in to start your session',
];
