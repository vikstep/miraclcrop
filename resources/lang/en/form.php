<?php

return [

    /*
    |--------------------------------------------------------------------------
    | Pagination Language Lines
    |--------------------------------------------------------------------------
    |
    | The following language lines are used by the paginator library to build
    | the simple pagination links. You are free to change them to anything
    | you want to customize your views to better match your application.
    |
    */

    'fill_your_data' => 'Fill your data',
     'password' => 'Password',
    'confirm_password' => 'Confirm Password',
    'first_name' => 'First Name',
    'last_name' => 'Last Name',
    'date_of_birth' => 'Date of birth',
    'select_type' => 'Select type',
    'address' => 'Address',

];
