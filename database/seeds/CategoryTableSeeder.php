<?php

use Illuminate\Database\Seeder;
use Illuminate\Database\Eloquent\Model;
use Waavi\Translation\Repositories\TranslationRepository;

class CategoryTableSeeder extends Seeder {

    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run(TranslationRepository $TranslationRepository) {
        DB::table('categories')->insert([
            'status' => 'enabled',
            'type' => 'blog',
        ]);
        
        $TranslationRepository->create([
            'locale' => 'en',
            'namespace' => '*',
            'group' => 'blog_category',
            'item' => '1.title',
            'text' => 'Uncategorized',
        ]);
    }

}
