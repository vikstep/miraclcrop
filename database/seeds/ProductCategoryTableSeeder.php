<?php

use Illuminate\Database\Seeder;
use Illuminate\Database\Eloquent\Model;
use Waavi\Translation\Repositories\TranslationRepository;

class ProductCategoryTableSeeder extends Seeder {

    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run(TranslationRepository $TranslationRepository) {
        $category_lists = ['Cannabis', 'Flower', 'Concentrates', 'Edibles', 'Topicals', 'Accessories'];

        foreach ($category_lists as $category) {
            $id = DB::table('categories')->insertGetId([
                'status' => 'enabled',
                'type' => 'product',
            ]);

            $TranslationRepository->create([
                'locale' => 'en',
                'namespace' => '*',
                'group' => 'product_category',
                'item' => $id.'.title',
                'text' => $category,
            ]);
        }
    }

}
